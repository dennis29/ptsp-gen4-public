<?php

namespace Packages\Model;

use Illuminate\Database\Eloquent\Model;
use Auth;

abstract class BaseModel extends Model {
  public $incrementing = false;

  protected static function boot() {
    parent::boot();

    static::creating(function($table) {
        $table->id = uniqid().substr(md5(mt_rand()), 0, 5);
        if(empty($table->created_by)){
            $table->created_by = (Auth::user()) ? Auth::user()->id : 'SYSTEM'; //(Auth::check()) ? Auth::user()->username : 'SYSTEM';
        }
    });

    static::updating(function($table) {
        $table->updated_by = (Auth::user()) ? Auth::user()->id : 'SYSTEM'; //(Auth::check()) ? Auth::user()->username : 'SYSTEM';
    });
  }
}
