<?php

namespace Packages\Form;

class Fields{

  public $label;
  public $type;
  public $name;
  public $rule = null;
  public $validation = [];
  public $help;
  public $attributes = [];
  public $openContainer = '<md-input-container class="md-block" flex="100">';
  public $closeContainer = '</md-input-container>';
  public $options = [];

  public function __construct($name, $label, $type){
    $this->name = $name;
    $this->type = $type;
    $this->label = $label;

    return $this;
  }

  public function options($options = ''){
    $this->options = $options;

    return $this;
  }

  public function rule($rule){
    $this->rule = $rule;

    $validation = [];
    $arrRule = explode('|', $this->rule);

    foreach($arrRule as $val){
      $arrVal = explode(':', $val);
      $param = $arrVal[0];
      $value = (isset($arrVal[1])) ? $arrVal[1] : '';

      if($this->type != 'number' && $param == 'min'){
        $param = 'minlength';
      }else if($this->type != 'number' && $param == 'max'){
        $param = 'maxlength';
      }else if($param == 'same'){
        $param = 'equalTo';
        $value = "input[name='".$value."']";
      }else if($param == 'numeric'){
        $param = 'number';
      }

      $validation[$param] = $value;
    }

    $this->validation = $validation;
    $this->attributes($validation);

    return $this;
  }

  public function help($help){
    $this->help = $help;
    return $this;
  }

  public function attributes($attributes){
    if(isset($attributes['class']) && $attributes['class'] != ''){
      $attributes['class'] = $this->attributes['class'].' '.$attributes['class'];
    }

    $this->attributes = array_merge($this->attributes, $attributes);
    return $this;
  }



  public function render(){

    $data =  [
      'name' => $this->name,
      'type' => $this->type
    ];

    if(!empty($this->options)){
      $data['options'] = $this->options;
    }

    if(!empty($this->help)){
      $data['help'] = $this->help;
    }

    if(!empty($this->attributes)){
      $data['attributes'] = $this->attributes;
    }

    if(!empty($this->label)){
      $data['label'] = $this->label;
    }

    return $data;
  }

  public function getHtml(){

    $label = (!empty($this->label)) ? '<label>'.$this->label.'</label>' : '';
    $attributes = [];
    if(!empty($this->attributes)){
      foreach($this->attributes as $key => $val){
        $attributes[] = (empty($val)) ? $key : $key.'='.$val;
      }
    }

    $attributes = (!empty($attributes)) ? ' '.implode(' ',$attributes) : '';
    $html = '';
    switch($this->type){
      case 'text' : $html = $label.'<input type="text" name="'.$this->name.'" ng-model="data.'.$this->name.'"'.$attributes.'>'.$this->getMessages();
                    break;
      case 'password' : $html = $label.'<input type="password" name="'.$this->name.'" ng-model="data.'.$this->name.'"'.$attributes.'>'.$this->getMessages();
                    break;
      case 'email' : $html = $label.'<input type="email" name="'.$this->name.'" ng-model="data.'.$this->name.'"'.$attributes.'>'.$this->getMessages();
                    break;
      case 'textarea' : $html = $label.'<textarea name="'.$this->name.'" ng-model="data.'.$this->name.'"'.$attributes.'></textarea>'.$this->getMessages();
                    break;
      case 'select' : $options = [];
                      if(!empty($this->options)){
                        foreach($this->options as $key => $val){
                          $options[] = '<md-option value="'.$key.'">'.$val.'</md-option>';
                        }
                      }
                      $options = (!empty($options)) ? implode('', $options) : '';
      $html = $label.'<md-select name="'.$this->name.'" ng-model="data.'.$this->name.'"'.$attributes.'>'.$options.'
      </md-select>'.$this->getMessages();
                    break;
      case 'date' : $html = $label.'<md-datepicker name="'.$this->name.'" ng-model="data.'.$this->name.'"'.$attributes.'></md-datepicker>'.$this->getMessages();
                    break;

    }

    return $this->openContainer.$html.$this->closeContainer;
  }

  public function container($open = '', $close = ''){
    if(!empty($open)){
      $this->openContainer = $open;
    }
    if(!empty($close)){
      $this->closeContainer = $close;
    }
  }

  public function getMessages(){

    $messages = [];

    if(!empty($this->validation)){
      foreach($this->validation as $key => $val){
        $messages[] = '<div ng-message="'.$key.'">'.$this->messages($key, $val).'</div>';
      }
    }

    if(count($messages) > 0){
      return '<div ng-messages="form.'.$this->name.'.$error">'.implode('', $messages).'</div>';
    }else{
      return '';
    }
  }

  public function messages($type = null, $value = ''){
    $messages = [
      "required" => $this->label.' harus diisi!',
      "email" => 'Format email salah!',
      "minlength" => $this->label.' minimal '.$value.' karakter!!',
      "maxlength" => $this->label.' maksimal '.$value.' karakter!!',
    ];

    if(isset($messages[$type])){
      return $messages[$type];
    }
  }
}
