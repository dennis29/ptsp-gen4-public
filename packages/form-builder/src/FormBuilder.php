<?php

namespace Packages\Form;

use Illuminate\Support\Facades\Input;
use Request;
use Helper;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class FormBuilder{

  public $fields = [];
  public $action = 'create';
  public $upload = false;
  public $model = null;
  public $saved = false;
  public $hasRequest = false;
  public $title = '';
  public $url = '';
  public $dataPost = [];
  public $validatorMessages = [];
  public $data = null;

  public static function source($model){
    $ins = new static();
    $ins->model = $model;

    if(Request::isMethod('post')){
        $ins->hasRequest = true;
        $ins->dataPost = Input::all();
    }

    if($ins->model->exists){
      $ins->action = 'modify';
      // if(!in_array('modify', $ins->role)){
      //   $ins->action = 'view';
      // }
    }
    // else if(!in_array('create', $ins->role)){
    //   $ins->action = '';
    // }

    return $ins;
  }

  public function pre($fn){

    if (isset($this->dataPost) && is_a($fn, '\Closure')) {
      $this->dataPostModified = call_user_func_array($fn, [$this->dataPost]);
    }

    return $this;
  }

  public function title($title){
    $this->title = $title;
  }

  public function url($url){
    $this->url = $url;
  }

  public function add($name, $label, $type){
    $field = new Fields($name, $label, $type);
    if($type == 'file'){
      $this->upload = true;
    }

    $this->fields[$name] = $field;
    return $field;
  }

  public function isValid(){
    if(!isset($this->dataPost)){
      return false;
    }

    $inputAll = $this->dataPost;
    foreach ($this->fields as $field) {
        if (isset($field->rule)) {
            $rules[$field->name] = $field->rule;
            $attributes[$field->name] = $field->label;
            if($field->type == 'number'){
                $inputAll[$field->name] = floatval(str_replace(',', '', $inputAll[$field->name]));
            }
        }
    }
    if (isset($this->validator)) {
        return !$this->validator->fails();
    }
    if (isset($rules)) {
        $this->validator = Validator::make($inputAll, $rules, $this->validatorMessages, $attributes);
        $errors = $this->validator->errors();
        $this->validatorMessages = implode('<br/>', $errors->all());
        return !$this->validator->fails();
    } else {
        return true;
    }
  }

  public function saved(){
    return $this->saved;
  }

  public function hasRequest(){
    return $this->hasRequest;
  }

  public function save(){

    if(isset($this->dataPost)){
      
      if($this->isValid()){
        if(isset($this->dataPostModified)){
          $this->dataPost = $this->dataPostModified;
        }
        $this->model->fill($this->dataPost);

        $this->model->save();
        $this->saved = true;
      }
    }

    return [
      'action' => $this->action,
      'messages' => $this->validatorMessages,
      'saved' => $this->saved
    ];
  }

  public function response(){
    return [
      'data' => $this->data,
      'url' => $this->url,
      'action' => $this->action
    ];
  }

  public function build(){
    foreach($this->fields as $key => $val){
      $this->fields[$val->name] = $val->render();
    }

    $this->data = $this->model->getAttributes();

    return [
      'fields' => $this->fields,
      'action' => $this->action,
      'title' => $this->title,
      'url' => $this->url,
      'data' => $this->model->getAttributes(),
      // 'role' => $this->role,
    ];

    return $this;
  }

  public function input($name){
    $fields = collect($this->fields)->where('name', $name)->first();

    if(!empty($fields)){
      return $fields['html'];
    }else{
      return '';
    }
  }

  public function field($name){
    $fields = collect($this->fields)->where('name', $name)->first();

    if(isset($fields)){
      return $fields[$name];
    }else{
      return null;
    }
  }

}
