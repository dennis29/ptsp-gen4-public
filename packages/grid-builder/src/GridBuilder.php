<?php

namespace Packages\Grid;

use Illuminate\Support\Facades\Input;
use Request;
use Illuminate\Support\Facades\Auth;

class GridBuilder{

  public $model = '';
  public $perPage = 10;
  public $optionPerpage = [20, 50, 100, 200];
  public $title = '';
  public $create_label = '';
  public $url = '';
  public $columns = [];
  public $massAction = [];

  public static function source($model){
    $ins = new static();
    $ins->model = $model;

    return $ins;
  }

  public function title($title){
    $this->title = $title;

    return $this;
  }

  public function url($url){
    $this->url = $url;
    return $this;
  }

  public function create_label($create_label){
      $this->create_label = $create_label;
      return $this;
  }

  public function perPage($perPage){
    $this->perPage = $perPage;
  }

  // Handle Filter Grid
  public function filter($name, $query){

    $value = Input::get($name);

    if (is_a($query, '\Closure') && !empty($value) && $value != null) {
      $this->model = call_user_func_array($query, [$this->model, $value]);
    }

    return $this;
  }

  public function add($source, $label, $sortable = true){

    $column = new Columns($source, $label, $sortable);
    $this->columns[] = $column;
    return $column;
  }

  public function addMassAction($action = ''){
    $this->massAction[] = $action;
  }

  public function build(){
    $perpage = Input::get('perpage');
    if($perpage != ''){
      $this->perPage = $perpage;
    }

    // OrderBy Query Column
    $sort = Input::get('order');

    foreach($this->columns as $column){
      if($sort == $column->source || $sort == '-'.$column->source){
        $sorting = 'ASC';
        if($sort == '-'.$column->source){
          $sorting = 'DESC';
        }
        // die($column->source.' '.$sorting);
        $this->model->orderBy($column->source, $sorting);
      }
    }
    $export = Input::get('export');
    if(empty($export)){

      $data = $this->model->paginate($this->perPage);
      $data = $data->toArray();
      $dataTable = $data['data'];
    }else{
      $dataTable = $this->model->get()->toArray();
      $data = [];
    }


    // $dataTable = $data['data'];
    $columns = [];
    foreach($this->columns as $column){
      list($col,$dataTable) = $column->build($dataTable);
      $columns[] = $col;
    }

    // $role = Auth::user()->getAccess();
    // if(in_array('delete', $role)){
    //   $this->addMassAction('delete');
    // }

    return [
      'data' => $dataTable,
      'total' => (!empty($data)) ? $data['total'] : 0,
      'url' => $this->url,
      // 'perPage' => (!empty($data)) ? $data['per_page'] : 0,
      'page' => (!empty($data)) ? $data['current_page'] : 0,
      // 'lastPage' => (!empty($data)) ? $data['last_page'] : 0,
      // 'from' => (!empty($data)) ? $data['from'] : 0,
      // 'to' => (!empty($data)) ? $data['to'] : 0,
      'title' => $this->title,
      'create_label' => $this->create_label,
      'columns' => $columns,
      // 'link' => Request::path(),
      // 'create' => Request::path().'/create',
      // 'modify' => Request::path().'/modify',
      // 'optionPerpage' => $this->optionPerpage,
      // 'role' => Auth::user()->getAccess(),
      // 'massAction' => $this->massAction
    ];
  }

}
