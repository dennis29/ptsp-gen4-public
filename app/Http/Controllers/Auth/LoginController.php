<?php

namespace App\Http\Controllers\Auth;

use Modules\SSO\Entities\User;
use Modules\SSO\Entities\Role;
use Modules\Pemohon\Entities\UserProfile;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('frontend::login.index');
    }

    public function login(Request $request)
    {
        // Get user record
        $user = User::where('email', $request->get('email'))->first();


        // Check Condition already confirmed  or Not
        if ($user == null) {
            return back()->withErrors(array('message' => 'Akun belum didaftarkan!'));
        }

        if(! $user->confirmed) {
            return back()->withErrors(array('message' => 'Akun Anda belum aktif, silahkan cek email Anda untuk aktifasi!'));
        }        
         
        // Set Auth Details
        Auth::login($user, true);

        // Set redirect page base on level of user
        $roles = Auth::user()->getRoles();

        if (empty($roles)){
            return redirect()->route('pemohon.dashboard');
        }
        else {
            $level = Role::select(['level'])->whereIn('slug', $roles)->first();

            if ($level['level'] == "admin")
                return redirect()->route('user');
            elseif ($level['level'] == "backoffice"){
                return redirect()->route('backoffice.dashboard');
            }
        }
        return redirect()->route('pemohon.dashboard');
    }
}
