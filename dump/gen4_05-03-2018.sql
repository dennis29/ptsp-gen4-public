
--
-- Data for Name: actions; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY actions (id, code, name, created_by, updated_by, created_at, updated_at) FROM stdin;
5a781dc78c21ade0e6	create	Create	SYSTEM	\N	2018-02-05 09:03:03	2018-02-05 09:03:03
5a781dd3b4e21351e6	update	Update	SYSTEM	\N	2018-02-05 09:03:15	2018-02-05 09:03:15
5a781dde76bf72e041	delete	Delete	SYSTEM	\N	2018-02-05 09:03:26	2018-02-05 09:03:26
5a781de9400491b2fc	view	View	SYSTEM	\N	2018-02-05 09:03:37	2018-02-05 09:03:37
\.


--
-- Data for Name: bidang; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY bidang (id, code, name, active, created_by, updated_by, created_at, updated_at) FROM stdin;
5a9b6819c1d8416826	A	Pendidikan	t	5a811f8be5ce60cf10	\N	2018-03-04 03:29:29	2018-03-04 03:29:29
5a9b68247b4df14564	B	Kesehatan	t	5a811f8be5ce60cf10	\N	2018-03-04 03:29:40	2018-03-04 03:29:40
5a9b685003a7176f74	D	Perumahan Rakyat Dan Kawasan Pemukiman	t	5a811f8be5ce60cf10	\N	2018-03-04 03:30:24	2018-03-04 03:30:24
5a9b68797a1878c2d3	E	Ketentraman, Ketertiban Umum Dan Pelindungan Masyarakat	t	5a811f8be5ce60cf10	\N	2018-03-04 03:31:05	2018-03-04 03:31:05
5a9b6883dfd7efbf4b	F	Sosial	t	5a811f8be5ce60cf10	\N	2018-03-04 03:31:15	2018-03-04 03:31:15
5a9b695362e2b50616	G	Tenaga Kerja	t	5a811f8be5ce60cf10	\N	2018-03-04 03:34:43	2018-03-04 03:34:43
5a9b69e1df339ad909	H	Pemberdayaan Perempuan Dan Pelindungan Anak	t	5a811f8be5ce60cf10	\N	2018-03-04 03:37:05	2018-03-04 03:37:05
5a9b69eb2cc7ad5fca	I	Pangan	t	5a811f8be5ce60cf10	\N	2018-03-04 03:37:15	2018-03-04 03:37:15
5a9b69fc830c2b4bcb	J	Pertanahan Yang Menjadi Kewenangan Daerah	t	5a811f8be5ce60cf10	\N	2018-03-04 03:37:32	2018-03-04 03:37:32
5a9b6a0a98f6ba01e1	K	Lingkungan Hidup	t	5a811f8be5ce60cf10	\N	2018-03-04 03:37:46	2018-03-04 03:37:46
5a9b6a1b34180366a3	L	Pemberdayaan Masyarakat	t	5a811f8be5ce60cf10	\N	2018-03-04 03:38:03	2018-03-04 03:38:03
5a9b6a2e8fe454e1d1	M	Pengendalian Penduduk Dan Keluarga Berencana	t	5a811f8be5ce60cf10	\N	2018-03-04 03:38:22	2018-03-04 03:38:22
5a9b6a3b594fcd2264	N	Perhubungan	t	5a811f8be5ce60cf10	\N	2018-03-04 03:38:35	2018-03-04 03:38:35
5a9b6a49d44475edbf	O	Komunikasi Dan Informatika	t	5a811f8be5ce60cf10	\N	2018-03-04 03:38:49	2018-03-04 03:38:49
5a9b6a783fe8c35e63	P	Koperasi, Serta Usaha Mikro, Kecil Dan Menengah	t	5a811f8be5ce60cf10	\N	2018-03-04 03:39:36	2018-03-04 03:39:36
5a9b6a860ef815918e	Q	Penanaman Modal	t	5a811f8be5ce60cf10	\N	2018-03-04 03:39:50	2018-03-04 03:39:50
5a9b6aab55ee109921	R	Kepemudaan Dan Keolahragaan	t	5a811f8be5ce60cf10	\N	2018-03-04 03:40:27	2018-03-04 03:40:27
5a9b6ab43820e3be60	S	Statistik	t	5a811f8be5ce60cf10	\N	2018-03-04 03:40:36	2018-03-04 03:40:36
5a9b6abe5456534b61	T	Persandian	t	5a811f8be5ce60cf10	\N	2018-03-04 03:40:46	2018-03-04 03:40:46
5a9b6ac943322bce40	U	Kebudayaan	t	5a811f8be5ce60cf10	\N	2018-03-04 03:40:57	2018-03-04 03:40:57
5a9b6ad6489d4bdd7a	V	Perpustakaan	t	5a811f8be5ce60cf10	\N	2018-03-04 03:41:10	2018-03-04 03:41:10
5a9b6ae0ca724492d1	W	Kearsipan	t	5a811f8be5ce60cf10	\N	2018-03-04 03:41:20	2018-03-04 03:41:20
5a9b6aee7512059249	X	Kelautan Dan Perikanan	t	5a811f8be5ce60cf10	\N	2018-03-04 03:41:34	2018-03-04 03:41:34
5a9b6afcaaec475190	Y	Pariwisata	t	5a811f8be5ce60cf10	\N	2018-03-04 03:41:48	2018-03-04 03:41:48
5a9b6b081b680b8fcd	Z	Pertanian	t	5a811f8be5ce60cf10	\N	2018-03-04 03:42:00	2018-03-04 03:42:00
5a9b6b191aa54d39d3	AA	Kehutanan	t	5a811f8be5ce60cf10	\N	2018-03-04 03:42:17	2018-03-04 03:42:17
5a9b6c210a22e35252	BB	Energi Dan Sumber Daya Mineral	t	5a811f8be5ce60cf10	\N	2018-03-04 03:46:41	2018-03-04 03:46:41
5a9b6c2d030b8edff5	CC	Perdagangan	t	5a811f8be5ce60cf10	\N	2018-03-04 03:46:53	2018-03-04 03:46:53
5a9b6c38a538df40f5	DD	Perindustrian	t	5a811f8be5ce60cf10	\N	2018-03-04 03:47:04	2018-03-04 03:47:04
5a9b6c43ebbdeffb07	EE	Transmigrasi	t	5a811f8be5ce60cf10	\N	2018-03-04 03:47:15	2018-03-04 03:47:15
5a9b6c5599fe1ca259	FF	Kesatuan Bangsa Dan Politik Dalam Negri	t	5a811f8be5ce60cf10	\N	2018-03-04 03:47:33	2018-03-04 03:47:33
5a9b6c625d200bd728	GG	Pelayanan Administrasi	t	5a811f8be5ce60cf10	\N	2018-03-04 03:47:46	2018-03-04 03:47:46
5a9b6832a010677c68	C	Pekerjaan Umum Dan Penataan Ruang	t	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-04 03:29:54	2018-03-04 04:10:18
\.


--
-- Data for Name: proses; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY proses (id, name, route, conf, created_by, updated_by, created_at, updated_at) FROM stdin;
5a8315e94a8037a044	Pengajuan	pengajuan	\N	\N	5a811f8be5ce60cf10	2018-02-13 16:44:25	2018-02-21 17:52:22
5a9bc3c63b9cb72916	Pemeriksaan Berkas	uploadberkas	\N	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-04 10:00:38	2018-03-04 10:01:30
5a9bc7516f39daa876	Persetujuan	persetujuan	\N	5a811f8be5ce60cf10	\N	2018-03-04 10:15:45	2018-03-04 10:15:45
5a9bc9039a6c79e430	Cetak Berkas	cetakberkas	\N	5a811f8be5ce60cf10	\N	2018-03-04 10:22:59	2018-03-04 10:22:59
5a9bc9120b219302da	Pengambilan Berkas	pengambilan	\N	5a811f8be5ce60cf10	\N	2018-03-04 10:23:14	2018-03-04 10:23:14
\.

--
-- Data for Name: cities; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY cities (id, province_id, name) FROM stdin;
\.


--
-- Data for Name: data_perijinan; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY data_perijinan (id, tipe_perijinan, status_perijinan, bidang_perijinan, tanggal_mengajukan, due_date, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: districts; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY districts (id, city_id, name) FROM stdin;
\.
--
-- Data for Name: izin_category; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY izin_category (id, bidang, name, created_by, updated_by, created_at, updated_at, code) FROM stdin;
5a9b6e86ba0118ee40	5a9b6832a010677c68	Ketetapan Rencana Kota	5a811f8be5ce60cf10	\N	2018-03-04 03:56:54	2018-03-04 03:56:54	C.23
5a9b6eb1d5ae490d73	5a9b6832a010677c68	Ketetapan Rencana Kota Untuk Konsultasi BKPRD	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-04 03:57:37	2018-03-04 03:58:00	C.24
\.

--
-- Data for Name: izin; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY izin (id, izin_category, name, created_by, updated_by, created_at, updated_at, code) FROM stdin;
5a9b717f3cbc5cccd5	5a9b6e86ba0118ee40	KETETAPAN RENCANA KOTA: BESAR, LUAS TANAH 5000 M2 KEATAS, UNTUK SEMUA JENIS BANGUNAN RUMAH TINGGAL DAN NON-RUMAH TINGGAL	5a811f8be5ce60cf10	\N	2018-03-04 04:09:35	2018-03-04 04:09:35	C.23.1
\.


--
-- Data for Name: izin_berkas; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY izin_berkas (id, izin_proses, jenis_berkas, file, created_by, updated_by, created_at, updated_at) FROM stdin;
\.





--
-- Data for Name: izin_proses; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY izin_proses (id, izin, proses, name, estimated_time, roles, "order", created_by, updated_by, created_at, updated_at) FROM stdin;
5a9bc294450286be5a	5a9b717f3cbc5cccd5	5a8315e94a8037a044	Pengajuan	\N	["pemohon"]	1	5a811f8be5ce60cf10	\N	2018-03-04 09:55:32	2018-03-04 09:55:32
5a9bc4be4de9b170a1	5a9b717f3cbc5cccd5	5a9bc3c63b9cb72916	Pemeriksaan Berkas	\N	["petugas"]	2	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-04 10:04:46	2018-03-04 10:15:09
5a9bc85807833a9450	5a9b717f3cbc5cccd5	5a9bc7516f39daa876	Persetujuan Kepala Seksi	2	["kasek"]	3	5a811f8be5ce60cf10	\N	2018-03-04 10:20:08	2018-03-04 10:20:08
5a9bc8580867e09eb1	5a9b717f3cbc5cccd5	5a9bc7516f39daa876	Persetujuan Kepala Bidang	3	["kabid"]	4	5a811f8be5ce60cf10	\N	2018-03-04 10:20:08	2018-03-04 10:20:08
5a9bc85808eb627b55	5a9b717f3cbc5cccd5	5a9bc7516f39daa876	Persetujuan Kepala Dinas	2	["kadis"]	5	5a811f8be5ce60cf10	\N	2018-03-04 10:20:08	2018-03-04 10:20:08
5a9bc858097b849e4b	5a9b717f3cbc5cccd5	5a9bc9039a6c79e430	Cetak Berkas	3	["petugas"]	6	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-04 10:20:08	2018-03-04 10:24:34
5a9bc96218bf56cd8d	5a9b717f3cbc5cccd5	5a9bc9120b219302da	Pengambilan Berkas	3	["petugas"]	7	5a811f8be5ce60cf10	\N	2018-03-04 10:24:34	2018-03-04 10:24:34
\.


--
-- Data for Name: status; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY status (id, slug, name, label, created_by, updated_by, created_at, updated_at) FROM stdin;
5a831abf25cf1f572b	draft	Draft	Draft	\N	\N	2018-02-13 14:05:03	2018-02-13 14:05:03
5a831abf25cf1f572c	diajukan	Ajukan	Diajukan	\N	\N	2018-02-13 17:05:03	2018-02-13 17:05:03
5a831ae8ec23eca129	disetujui	Setujui	Disetujui Oleh Petugas	\N	5a811f8be5ce60cf10	2018-02-13 17:05:44	2018-03-04 10:06:35
5a9bc54dd03a53f66a	disetujui.kasek	Setujui	Disetujui Kepala Seksi	5a811f8be5ce60cf10	\N	2018-03-04 10:07:09	2018-03-04 10:07:09
5a9bc5677df3317495	disetujui.kabid	Setujui	Disetujui Kepala Bidang	5a811f8be5ce60cf10	\N	2018-03-04 10:07:35	2018-03-04 10:07:35
5a9bc5f7c0d583ff08	ditolak	Ditolak	Ditolak	5a811f8be5ce60cf10	\N	2018-03-04 10:09:59	2018-03-04 10:09:59
5a9bc5a44bd97db77a	disetujui.kadis	Setujui	Disetujui Kepala DPMPTSP	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-04 10:08:36	2018-03-04 10:11:15
5a9bc6a11f7eeb495c	dicetak	Cetak	Dicetak	5a811f8be5ce60cf10	\N	2018-03-04 10:12:49	2018-03-04 10:12:49
5a9bc6c6bbf27d677f	diambil	Pengambilan	Diambil	5a811f8be5ce60cf10	\N	2018-03-04 10:13:26	2018-03-04 10:13:26
5a9bc6dc13f689a296	selesai	Selesai	Selesai	5a811f8be5ce60cf10	\N	2018-03-04 10:13:48	2018-03-04 10:13:48
\.

--
-- Data for Name: izin_proses_status; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY izin_proses_status (id, izin_proses, status, next_proses, "order", created_by, updated_by, created_at, updated_at) FROM stdin;
5a9bc37d2d747a6d51	5a9bc294450286be5a	diajukan	5a9bc4be4de9b170a1	1	5a811f8be5ce60cf10	\N	2018-03-04 09:59:25	2018-03-04 09:59:25
5a9bc72d5b1ba1e520	5a9bc4be4de9b170a1	ditolak	\N	2	5a811f8be5ce60cf10	\N	2018-03-04 10:15:09	2018-03-04 10:15:09
5a9bc72d5a63d5ca54	5a9bc4be4de9b170a1	disetujui	5a9bc85807833a9450	1	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-04 10:15:09	2018-03-04 10:20:08
5a9bc8580e3b7184e1	5a9bc85807833a9450	disetujui.kasek	5a9bc8580867e09eb1	1	5a811f8be5ce60cf10	\N	2018-03-04 10:20:08	2018-03-04 10:20:08
5a9bc8580ea780f05c	5a9bc85807833a9450	ditolak	\N	2	5a811f8be5ce60cf10	\N	2018-03-04 10:20:08	2018-03-04 10:20:08
5a9bc8580efa6fa4d4	5a9bc8580867e09eb1	disetujui.kabid	5a9bc85808eb627b55	1	5a811f8be5ce60cf10	\N	2018-03-04 10:20:08	2018-03-04 10:20:08
5a9bc8580f694545d4	5a9bc8580867e09eb1	ditolak	\N	2	5a811f8be5ce60cf10	\N	2018-03-04 10:20:08	2018-03-04 10:20:08
5a9bc8580fbec20677	5a9bc85808eb627b55	disetujui.kadis	\N	1	5a811f8be5ce60cf10	\N	2018-03-04 10:20:08	2018-03-04 10:20:08
5a9bc858102265652c	5a9bc858097b849e4b	dicetak	5a9bc96218bf56cd8d	1	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-04 10:20:08	2018-03-04 10:24:34
5a9bc9621e13b630b6	5a9bc96218bf56cd8d	selesai	\N	1	5a811f8be5ce60cf10	\N	2018-03-04 10:24:34	2018-03-04 10:24:34
\.


--
-- Data for Name: izin_role; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY izin_role (id, izin, izin_proses, route, role, status, next_status, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: jenis_berkas; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY jenis_berkas (id, name, description, updated_by, created_by, created_at, updated_at) FROM stdin;
5a8e06455f391618f7	Kartu Keluarga	\N	\N	5a811f8be5ce60cf10	2018-02-21 23:52:37	2018-02-21 23:52:37
5a8e057b5b9770caeb	Kartu Tanda Penduduk	\N	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-02-21 23:49:15	2018-02-21 23:52:47
\.


--
-- Data for Name: jenis_izin; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY jenis_izin (id, name, active, updated_by, created_by, created_at, updated_at) FROM stdin;
5a7c66fa36ed14e60c	Ketataruangan	t	barkah.hadi	barkah.hadi	2018-02-08 15:04:26	2018-02-08 15:05:22
\.


--
-- Data for Name: jenis_koperasi; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY jenis_koperasi (id, name, active, updated_by, created_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: jenis_perusahaan; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY jenis_perusahaan (id, code, name, active, updated_by, created_by, created_at, updated_at) FROM stdin;
5a7c729da5519972cb	CV	Commanditaire Vennootschap	t	\N	barkah.hadi	2018-02-08 15:54:05	2018-02-08 15:54:05
5a7c72ac349740e283	FA	Firma	t	\N	barkah.hadi	2018-02-08 15:54:20	2018-02-08 15:54:20
5a7c72bba7e199cb93	PT	Perseroan Terbatas	t	\N	barkah.hadi	2018-02-08 15:54:35	2018-02-08 15:54:35
\.


--
-- Data for Name: ltm_translations; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY ltm_translations (id, status, locale, "group", key, value, created_at, updated_at) FROM stdin;
\.


--
-- Name: ltm_translations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: barkah
--

SELECT pg_catalog.setval('ltm_translations_id_seq', 1, false);


--
-- Data for Name: mapping_output; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY mapping_output (id, name, "order", izin, output, roles, email_penerima, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: media; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY media (id, model_id, model_type, collection_name, name, file_name, mime_type, disk, jenis_berkas, size, manipulations, custom_properties, order_column, created_by, updated_by, created_at, updated_at) FROM stdin;
5a9621bcd3a4c62456	5a811f8be5ce60cf10	Modules\\SSO\\Entities\\User	images	44	44.jpg	image/jpeg	files	\N	182223	[]	[]	3	5a811f8be5ce60cf10	\N	2018-02-28 03:27:56	2018-02-28 03:27:56
5a9622b4ad62dc5697	5a811f8be5ce60cf10	Modules\\SSO\\Entities\\User	images	25	25.jpg	image/jpeg	files	5a8e06455f391618f7	381513	[]	[]	4	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-02-28 03:32:04	2018-02-28 03:53:09
5a9622edd912c6f84c	5a811f8be5ce60cf10	Modules\\SSO\\Entities\\User	images	19	19.jpg	image/jpeg	files	5a8e057b5b9770caeb	109358	[]	[]	5	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-02-28 03:33:01	2018-02-28 03:53:38
5a9655bc06c9afb33e	5a811f8be5ce60cf10	Modules\\SSO\\Entities\\User	images	8	8.jpg	image/jpeg	files	\N	195401	[]	[]	6	5a811f8be5ce60cf10	\N	2018-02-28 07:09:48	2018-02-28 07:09:48
5a979cf2ea53eb3d64	5a811f8be5ce60cf10	Modules\\SSO\\Entities\\User	images	KTP	KTP.jpeg	image/jpeg	files	5a8e057b5b9770caeb	217028	[]	[]	7	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-01 06:25:54	2018-03-01 06:26:30
5a9655bc06398a49fd	5a811f8be5ce60cf10	Modules\\SSO\\Entities\\User	images	16	16.jpg	image/jpeg	files	5a8e06455f391618f7	116716	[]	[]	6	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-02-28 07:09:48	2018-03-03 03:48:44
5a9bcff64c058b9e9b	5a9a28c6d4e8c12ad0	Modules\\SSO\\Entities\\User	images	19	19.jpg	image/jpeg	files	\N	109358	[]	[]	9	5a9a28c6d4e8c12ad0	\N	2018-03-04 10:52:38	2018-03-04 10:52:38
5a9bcff64c32adec6d	5a9a28c6d4e8c12ad0	Modules\\SSO\\Entities\\User	images	20	20.jpg	image/jpeg	files	\N	321534	[]	[]	9	5a9a28c6d4e8c12ad0	\N	2018-03-04 10:52:38	2018-03-04 10:52:38
5a9bcff659014129d7	5a9a28c6d4e8c12ad0	Modules\\SSO\\Entities\\User	images	11	11.jpg	image/jpeg	files	\N	415596	[]	[]	10	5a9a28c6d4e8c12ad0	\N	2018-03-04 10:52:38	2018-03-04 10:52:38
5a9bcff6599f1c29c2	5a9a28c6d4e8c12ad0	Modules\\SSO\\Entities\\User	images	13	13.jpg	image/jpeg	files	\N	304499	[]	[]	10	5a9a28c6d4e8c12ad0	\N	2018-03-04 10:52:38	2018-03-04 10:52:38
5a9bcff6597bdf61d6	5a9a28c6d4e8c12ad0	Modules\\SSO\\Entities\\User	images	12	12.jpg	image/jpeg	files	\N	201805	[]	[]	10	5a9a28c6d4e8c12ad0	\N	2018-03-04 10:52:38	2018-03-04 10:52:38
5a9bcff65b4071c877	5a9a28c6d4e8c12ad0	Modules\\SSO\\Entities\\User	images	21	21.jpg	image/jpeg	files	\N	353370	[]	[]	11	5a9a28c6d4e8c12ad0	\N	2018-03-04 10:52:38	2018-03-04 10:52:38
5a9bcffa680b1b5a0a	5a9a28c6d4e8c12ad0	Modules\\SSO\\Entities\\User	images	28	28.jpg	image/jpeg	files	\N	452220	[]	[]	12	5a9a28c6d4e8c12ad0	\N	2018-03-04 10:52:42	2018-03-04 10:52:42
5a9bcffaadfc79f7d7	5a9a28c6d4e8c12ad0	Modules\\SSO\\Entities\\User	images	29	29.jpg	image/jpeg	files	\N	268163	[]	[]	13	5a9a28c6d4e8c12ad0	\N	2018-03-04 10:52:42	2018-03-04 10:52:42
5a9bd1680d879bd8df	5a9a28c6d4e8c12ad0	Modules\\SSO\\Entities\\User	images	9	9.jpg	image/jpeg	files	\N	268001	[]	[]	14	5a9a28c6d4e8c12ad0	\N	2018-03-04 10:58:48	2018-03-04 10:58:48
5a9bd1680dd4b9b9c7	5a9a28c6d4e8c12ad0	Modules\\SSO\\Entities\\User	images	25	25.jpg	image/jpeg	files	\N	381513	[]	[]	14	5a9a28c6d4e8c12ad0	\N	2018-03-04 10:58:48	2018-03-04 10:58:48
5a9bcffa68170facef	5a9a28c6d4e8c12ad0	Modules\\SSO\\Entities\\User	images	27	27.jpg	image/jpeg	files	5a8e06455f391618f7	168240	[]	[]	12	5a9a28c6d4e8c12ad0	5a9a28c6d4e8c12ad0	2018-03-04 10:52:42	2018-03-05 05:00:43
\.



--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: barkah
--

SELECT pg_catalog.setval('migrations_id_seq', 69, true);


--
-- Data for Name: modules; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY modules (id, code, name, created_by, updated_by, created_at, updated_at) FROM stdin;
5a7923cf79df4e796b	user	Pengguna	SYSTEM	SYSTEM	2018-02-06 03:41:03	2018-02-06 03:41:20
5a7923e7c4a7d77a52	role	Role	SYSTEM	\N	2018-02-06 03:41:27	2018-02-06 03:41:27
5a7923eda09325a65b	module	Module	SYSTEM	\N	2018-02-06 03:41:33	2018-02-06 03:41:33
5a7923f22b7da6cd45	action	Action	SYSTEM	\N	2018-02-06 03:41:38	2018-02-06 03:41:38
5a7923f7235f4c4114	wilayah	Wilayah	SYSTEM	\N	2018-02-06 03:41:43	2018-02-06 03:41:43
5a7c5cc5758164715b	bidang	Bidang	barkah.hadi	\N	2018-02-08 14:20:53	2018-02-08 14:20:53
5a7c644eedd801f160	jenisizin	Jenis Izin	barkah.hadi	barkah.hadi	2018-02-08 14:53:02	2018-02-08 15:02:24
5a7c703daae3895ea6	jenisperusahaan	Jenis Perusahaan	barkah.hadi	\N	2018-02-08 15:43:57	2018-02-08 15:43:57
5a7c704b0a2ae4c597	jeniskoperasi	Jenis Koperasi	barkah.hadi	\N	2018-02-08 15:44:11	2018-02-08 15:44:11
5a82eba089ccfcd85c	izincategory	Kategori Izin	\N	\N	2018-02-13 13:44:00	2018-02-13 13:44:00
5a83132a5fd3ba4b04	proses	Proses	\N	\N	2018-02-13 16:32:42	2018-02-13 16:32:42
5a831a7d2f180424d1	status	Status	\N	\N	2018-02-13 17:03:57	2018-02-13 17:03:57
5a831f46ce451faaae	izin	Izin	\N	\N	2018-02-13 17:24:22	2018-02-13 17:27:23
5a8db0d90ec66cf8e8	output	Output	5a811f8be5ce60cf10	\N	2018-02-21 17:48:09	2018-02-21 17:48:09
5a8e03b400082b69d6	jenisberkas	Jenis Berkas	5a811f8be5ce60cf10	\N	2018-02-21 23:41:40	2018-02-21 23:41:40
\.


--
-- Data for Name: output; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY output (id, name, route_page, route_conf, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY users (id, email, fullname, password, remember_token, picture, wilayah, last_activity, confirmed, confirmation_code, active, created_by, updated_by, created_at, updated_at) FROM stdin;
5a9a28c6d4e8c12ad0	sujono@gmail.com	Sujono	$2y$10$yiS4uJzm62P337W4JGY9EeBQFzH5b4SCSA0v7Htk/YD8NDMkLtmHq	sGn09JALc2830Dgbeni8rBGJNS9Dol2SsHPxbHPrAaHfe39JSkJ6yD56U5Yi	\N	\N	\N	t	\N	t	SYSTEM	\N	2018-03-03 04:47:02	2018-03-03 04:47:06
5a8f61e3b5600a622e	barkah.hadi@yahoo.com	Barkah Hadi Nugroho	$2y$10$1pwxHfo.SZPmGP/qkXKRqubGkxvrah/is2x3EkguqaDhclXtP4PKa	JKOpOYIAokihlpmvg6U38xn5mH4eWLzjLVqazFlOeIL7dCc0req23ZfRv5L3	\N	\N	\N	t	\N	t	SYSTEM	\N	2018-02-23 00:35:47	2018-03-04 03:07:57
5a811f8be5ce60cf10	admin@jakarta.go.id	Administrator	$2y$10$3co.2Z0/LCyWWAA/modt1.ihkPQ3nc1E7h5tIbatKjOHEXkaS6.6C	lEzqPkkcJIC0JJY9BAiJB2V2ANyw33aJ7cC7E9EoZy70oZGoUEn7UFYdtkOJ	\N	\N	\N	t	\N	t	barkah.hadi	\N	2018-02-12 05:00:59	2018-03-04 03:04:45
5a9d1f6d700c330a55	pancasilany@gmail.com	Pancasilany	$2y$10$3co.2Z0/LCyWWAA/modt1.ihkPQ3nc1E7h5tIbatKjOHEXkaS6.6C	lEzqPkkcJIC0JJY9BAiJB2V2ANyw33aJ7cC7E9EoZy70oZGoUEn7UFYdtkOJ	\N	\N	\N	t	\N	t	SYSTEM	\N	2018-03-05 05:00:59	2018-03-05 03:04:45
5a9d1f6d700c330a51	petugas@jakarta.go.id	Petugas	$2y$10$3co.2Z0/LCyWWAA/modt1.ihkPQ3nc1E7h5tIbatKjOHEXkaS6.6C	lEzqPkkcJIC0JJY9BAiJB2V2ANyw33aJ7cC7E9EoZy70oZGoUEn7UFYdtkOJ	\N	\N	\N	t	\N	t	SYSTEM	\N	2018-03-05 05:00:59	2018-03-05 03:04:45
\.


--
-- Data for Name: pengajuan_izin; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY pengajuan_izin (id, "user", izin, status, tipe_perizinan, lat, long, formatted_address, created_by, updated_by, created_at, updated_at) FROM stdin;
\.



--
-- Data for Name: permission_user; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY permission_user (id, permission_id, user_id, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: permissions; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY permissions (id, inherit_id, name, slug, description, created_by, updated_by, created_at, updated_at) FROM stdin;
5a794e569c11f87ddc	\N	editor.user	{"update":true}	\N	SYSTEM	\N	2018-02-06 06:42:30	2018-02-06 06:42:30
5a794e569caa8b7b9d	\N	editor.action	{"delete":true,"update":true}	\N	SYSTEM	\N	2018-02-06 06:42:30	2018-02-06 06:42:30
5a7960e6262e4b1c54	\N	moderator.module	{"create":true}	\N	SYSTEM	\N	2018-02-06 08:01:42	2018-02-06 08:01:42
5a7960e62d89147f54	\N	moderator.user	{"create":true,"update":true,"view":true}	\N	SYSTEM	\N	2018-02-06 08:01:42	2018-02-06 08:01:42
5a9b62a3c2bd29b419	\N	admin.user	{"create":true,"update":true,"delete":true,"view":true}	\N	5a811f8be5ce60cf10	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
5a9b62a3ce1167cbd9	\N	admin.role	{"create":true,"update":true,"delete":true,"view":true}	\N	5a811f8be5ce60cf10	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
5a9b62a3ce53ece834	\N	admin.module	{"create":true,"update":true,"delete":true,"view":true}	\N	5a811f8be5ce60cf10	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
5a9b62a3ce987013c2	\N	admin.action	{"create":true,"update":true,"delete":true,"view":true}	\N	5a811f8be5ce60cf10	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
5a9b62a3ced4ba73f2	\N	admin.wilayah	{"create":true,"update":true,"delete":true,"view":true}	\N	5a811f8be5ce60cf10	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
5a9b62a3cf22d81205	\N	admin.bidang	{"create":true,"update":true,"delete":true,"view":true}	\N	5a811f8be5ce60cf10	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
5a9b62a3cf6da8914f	\N	admin.jenisizin	{"create":true,"update":true,"delete":true,"view":true}	\N	5a811f8be5ce60cf10	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
5a9b62a3cfbb4ff34b	\N	admin.jenisperusahaan	{"create":true,"update":true,"delete":true,"view":true}	\N	5a811f8be5ce60cf10	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
5a9b62a3d01c17d612	\N	admin.jeniskoperasi	{"create":true,"update":true,"delete":true,"view":true}	\N	5a811f8be5ce60cf10	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
5a9b62a3d0b2f34b73	\N	admin.izincategory	{"create":true,"update":true,"delete":true,"view":true}	\N	5a811f8be5ce60cf10	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
5a9b62a3d106a6cad1	\N	admin.proses	{"create":true,"update":true,"delete":true,"view":true}	\N	5a811f8be5ce60cf10	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
5a9b62a3d14cf92a3a	\N	admin.status	{"create":true,"update":true,"delete":true,"view":true}	\N	5a811f8be5ce60cf10	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
5a9b62a3d1903fff8c	\N	admin.izin	{"create":true,"update":true,"delete":true,"view":true}	\N	5a811f8be5ce60cf10	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
5a9b62a3d1d0ad7b74	\N	admin.output	{"create":true,"update":true,"delete":true,"view":true}	\N	5a811f8be5ce60cf10	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
5a9b62a3d211daeaa6	\N	admin.jenisberkas	{"create":true,"update":true,"view":true,"delete":true}	\N	5a811f8be5ce60cf10	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
\.



--
-- Data for Name: provinces; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY provinces (id, name) FROM stdin;
\.




--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY roles (id, name, slug, level, description, created_by, updated_by, created_at, updated_at) FROM stdin;
5a83eb6752539de9db	Petugas	petugas	backoffice	\N	\N	\N	2018-02-05 07:36:43	2018-02-05 07:36:43
5a9bc77d8f108c2e75	Kepala Bidang	kabid	backoffice	\N	\N	\N	2018-02-05 07:36:45	2018-02-05 07:36:45
5a9bc76fb4c8c11903	Kepala Seksi	kasek	backoffice	\N	\N	\N	2018-02-05 07:36:44	2018-02-05 07:36:44
5a9bc7886773878075	Kepala Dinas	kadis	backoffice	\N	\N	\N	2018-02-05 07:36:46	2018-02-05 07:36:46
5a78098a2ac73f180a	Admin	admin	admin	\N	SYSTEM	5a811f8be5ce60cf10	2018-02-05 07:36:42	2018-03-07 06:26:21
\.

--
-- Data for Name: role_user; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY role_user (id, role_id, user_id, created_by, updated_by, created_at, updated_at) FROM stdin;
5a9b624d4d01ec93fc	5a78098a2ac73f180a	5a811f8be5ce60cf10	\N	\N	2018-03-04 03:04:45	2018-03-04 03:04:45
5a9b630d58d7d583e9	5a78098a2ac73f180a	5a8f61e3b5600a622e	\N	\N	2018-03-04 03:07:57	2018-03-04 03:07:57
\.


--
-- Data for Name: permission_role; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY permission_role (id, permission_id, role_id, created_by, updated_by, created_at, updated_at) FROM stdin;
5a9b62a3d2c36fe185	5a9b62a3c2bd29b419	5a78098a2ac73f180a	\N	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
5a9b62a3d366a339cb	5a9b62a3ce1167cbd9	5a78098a2ac73f180a	\N	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
5a9b62a3d421bcca58	5a9b62a3ce53ece834	5a78098a2ac73f180a	\N	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
5a9b62a3d4df6e2e05	5a9b62a3ce987013c2	5a78098a2ac73f180a	\N	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
5a9b62a3d56ece2629	5a9b62a3ced4ba73f2	5a78098a2ac73f180a	\N	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
5a9b62a3d5ee6180aa	5a9b62a3cf22d81205	5a78098a2ac73f180a	\N	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
5a9b62a3d66b42c06c	5a9b62a3cf6da8914f	5a78098a2ac73f180a	\N	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
5a9b62a3d6dfe30c93	5a9b62a3cfbb4ff34b	5a78098a2ac73f180a	\N	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
5a9b62a3d771e5cf15	5a9b62a3d01c17d612	5a78098a2ac73f180a	\N	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
5a9b62a3d80347bdb9	5a9b62a3d0b2f34b73	5a78098a2ac73f180a	\N	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
5a9b62a3d8a54b6186	5a9b62a3d106a6cad1	5a78098a2ac73f180a	\N	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
5a9b62a3d924bdc67b	5a9b62a3d14cf92a3a	5a78098a2ac73f180a	\N	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
5a9b62a3d99ed65c26	5a9b62a3d1903fff8c	5a78098a2ac73f180a	\N	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
5a9b62a3da0eea228c	5a9b62a3d1d0ad7b74	5a78098a2ac73f180a	\N	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
5a9b62a3da7b9437ac	5a9b62a3d211daeaa6	5a78098a2ac73f180a	\N	\N	2018-03-04 03:06:11	2018-03-04 03:06:11
\.




--
-- Data for Name: user_berkas; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY user_berkas (id, users, jenis_berkas, file, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: user_profiles; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY user_profiles (id, user_id, nik, no_kk, nama_ktp, alamat, no_rt, no_rw, kelurahan, kecamatan, kota, provinsi, tempat_lahir, tanggal_lahir, jenis_kelamin, created_by, updated_by, created_at, updated_at) FROM stdin;
5a9a293079727f1286	5a9a28c6d4e8c12ad0	3174100912640001	3174100701091542	SUJONO	JL. H RADIN NO. 35	1	3	1003	10	74	31	ACEH SELATAN	09/12/1964	1	5a9a28c6d4e8c12ad0	\N	2018-03-03 04:48:48	2018-03-03 04:48:48
\.


--
-- Data for Name: user_providers; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY user_providers (id, user_id, provider_id, provider, image, created_at, updated_at) FROM stdin;
\.


--
-- Name: user_providers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: barkah
--

SELECT pg_catalog.setval('user_providers_id_seq', 1, false);



--
-- Data for Name: villages; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY villages (id, district_id, name) FROM stdin;
\.


--
-- Data for Name: wilayah; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY wilayah (id, code, name, lat, long, description, active, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Name: actions actions_code_unique; Type: CONSTRAINT; Schema: public; Owner: barkah
--


--
-- PostgreSQL database dump complete
--

