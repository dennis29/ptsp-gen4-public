--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: actions; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE actions (
    id character varying(25) NOT NULL,
    code character varying(25) NOT NULL,
    name character varying(25) NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE actions OWNER TO "Gabriella";

--
-- Name: bidang; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE bidang (
    id character varying(25) NOT NULL,
    code character varying(20) NOT NULL,
    name character varying(60) NOT NULL,
    active boolean DEFAULT true NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE bidang OWNER TO "Gabriella";

--
-- Name: cities; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE cities (
    id character(4) NOT NULL,
    province_id character(2) NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE cities OWNER TO "Gabriella";

--
-- Name: data_perijinan; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE data_perijinan (
    id character varying(25) NOT NULL,
    tipe_perijinan character varying(255) NOT NULL,
    status_perijinan character varying(255) NOT NULL,
    bidang_perijinan character varying(255) NOT NULL,
    tanggal_mengajukan character varying(255) NOT NULL,
    due_date character varying(255) NOT NULL,
    keterangan text,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE data_perijinan OWNER TO "Gabriella";

--
-- Name: districts; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE districts (
    id character(7) NOT NULL,
    city_id character(4) NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE districts OWNER TO "Gabriella";

--
-- Name: izin; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE izin (
    id character varying(25) NOT NULL,
    code character varying(25) NOT NULL,
    izin_category character varying(25) NOT NULL,
    name character varying(255) NOT NULL,
    description text,
    syarat_description text,
    waktu_penyelesaian integer DEFAULT 0,
    waktu_penyelesaian_tipe character varying(255),
    biaya_retribusi character varying(255),
    masa_berlaku integer DEFAULT 0,
    masa_berlaku_tipe character varying(255),
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    CONSTRAINT izin_masa_berlaku_tipe_check CHECK (((masa_berlaku_tipe)::text = ANY ((ARRAY['jam'::character varying, 'hari'::character varying, 'bulan'::character varying, 'tahun'::character varying])::text[]))),
    CONSTRAINT izin_waktu_penyelesaian_tipe_check CHECK (((waktu_penyelesaian_tipe)::text = ANY ((ARRAY['jam'::character varying, 'hari'::character varying, 'bulan'::character varying, 'tahun'::character varying])::text[])))
);


ALTER TABLE izin OWNER TO "Gabriella";

--
-- Name: izin_berkas; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE izin_berkas (
    id character varying(25) NOT NULL,
    izin_proses character varying(25) NOT NULL,
    jenis_berkas character varying(100) NOT NULL,
    file text NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE izin_berkas OWNER TO "Gabriella";

--
-- Name: izin_category; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE izin_category (
    id character varying(25) NOT NULL,
    code character varying(25) NOT NULL,
    bidang character varying(25) NOT NULL,
    name character varying(100) NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE izin_category OWNER TO "Gabriella";

--
-- Name: izin_proses; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE izin_proses (
    id character varying(25) NOT NULL,
    izin character varying(25) NOT NULL,
    proses character varying(25) NOT NULL,
    name character varying(25) NOT NULL,
    estimated_time integer DEFAULT 0,
    roles json,
    "order" integer DEFAULT 0 NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE izin_proses OWNER TO "Gabriella";

--
-- Name: izin_proses_status; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE izin_proses_status (
    id character varying(25) NOT NULL,
    izin_proses character varying(25) NOT NULL,
    status character varying(60) NOT NULL,
    next_proses character varying(25),
    "order" integer DEFAULT 0 NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE izin_proses_status OWNER TO "Gabriella";

--
-- Name: izin_proses_status_tx; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE izin_proses_status_tx (
    id character varying(25) NOT NULL,
    pengajuan_izin character varying(25) NOT NULL,
    izin_proses character varying(25) NOT NULL,
    status character varying(25) NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE izin_proses_status_tx OWNER TO "Gabriella";

--
-- Name: izin_role; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE izin_role (
    id character varying(25) NOT NULL,
    izin character varying(25) NOT NULL,
    izin_proses character varying(25) NOT NULL,
    route character varying(255) NOT NULL,
    role character varying(60) NOT NULL,
    status character varying(60) NOT NULL,
    is_default boolean DEFAULT false,
    next_status json,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    next_route character varying(255)
);


ALTER TABLE izin_role OWNER TO "Gabriella";

--
-- Name: izin_syarat; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE izin_syarat (
    id character varying(25) NOT NULL,
    izin character varying(25) NOT NULL,
    name text NOT NULL,
    jenis_berkas character varying(25),
    description text,
    "order" integer DEFAULT 0,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE izin_syarat OWNER TO "Gabriella";

--
-- Name: jenis_berkas; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE jenis_berkas (
    id character varying(25) NOT NULL,
    name character varying(60) NOT NULL,
    description text,
    updated_by character varying(25),
    created_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE jenis_berkas OWNER TO "Gabriella";

--
-- Name: jenis_izin; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE jenis_izin (
    id character varying(25) NOT NULL,
    name character varying(60),
    active boolean DEFAULT true NOT NULL,
    updated_by character varying(25),
    created_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE jenis_izin OWNER TO "Gabriella";

--
-- Name: jenis_koperasi; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE jenis_koperasi (
    id character varying(25) NOT NULL,
    name character varying(60),
    active boolean DEFAULT true NOT NULL,
    updated_by character varying(25),
    created_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE jenis_koperasi OWNER TO "Gabriella";

--
-- Name: jenis_perusahaan; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE jenis_perusahaan (
    id character varying(25) NOT NULL,
    code character varying(20) NOT NULL,
    name character varying(60) NOT NULL,
    active boolean DEFAULT true NOT NULL,
    updated_by character varying(25),
    created_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE jenis_perusahaan OWNER TO "Gabriella";

--
-- Name: ltm_translations; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE ltm_translations (
    id integer NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    locale character varying(255) NOT NULL,
    "group" character varying(255) NOT NULL,
    key character varying(255) NOT NULL,
    value text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE ltm_translations OWNER TO "Gabriella";

--
-- Name: ltm_translations_id_seq; Type: SEQUENCE; Schema: public; Owner: Gabriella
--

CREATE SEQUENCE ltm_translations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ltm_translations_id_seq OWNER TO "Gabriella";

--
-- Name: ltm_translations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: Gabriella
--

ALTER SEQUENCE ltm_translations_id_seq OWNED BY ltm_translations.id;


--
-- Name: mapping_output; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE mapping_output (
    id character varying(25) NOT NULL,
    name character varying(255) NOT NULL,
    "order" integer NOT NULL,
    izin character varying(25) NOT NULL,
    output character varying(25) NOT NULL,
    roles character varying(255) NOT NULL,
    email_penerima text,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE mapping_output OWNER TO "Gabriella";

--
-- Name: media; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE media (
    id character varying(25) NOT NULL,
    model_id character varying(25) NOT NULL,
    model_type character varying(255) NOT NULL,
    collection_name character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    file_name character varying(255) NOT NULL,
    mime_type character varying(255),
    disk character varying(255) NOT NULL,
    jenis_berkas character varying(25),
    size integer NOT NULL,
    manipulations json NOT NULL,
    custom_properties json NOT NULL,
    order_column integer,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE media OWNER TO "Gabriella";

--
-- Name: migrations; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE migrations OWNER TO "Gabriella";

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: Gabriella
--

CREATE SEQUENCE migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE migrations_id_seq OWNER TO "Gabriella";

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: Gabriella
--

ALTER SEQUENCE migrations_id_seq OWNED BY migrations.id;


--
-- Name: modules; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE modules (
    id character varying(25) NOT NULL,
    code character varying(25) NOT NULL,
    name character varying(25) NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE modules OWNER TO "Gabriella";

--
-- Name: output; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE output (
    id character varying(25) NOT NULL,
    name character varying(255) NOT NULL,
    route_page character varying(255) NOT NULL,
    route_conf character varying(255),
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE output OWNER TO "Gabriella";

--
-- Name: pengajuan_izin; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE pengajuan_izin (
    id character varying(25) NOT NULL,
    "user" character varying(25) NOT NULL,
    izin character varying(25) NOT NULL,
    status character varying(25) NOT NULL,
    tipe_perizinan character varying(255) NOT NULL,
    lat character varying(25) NOT NULL,
    long character varying(25) NOT NULL,
    formatted_address text,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    CONSTRAINT pengajuan_izin_tipe_perizinan_check CHECK (((tipe_perizinan)::text = ANY ((ARRAY['perorangan'::character varying, 'perusahaan'::character varying])::text[])))
);


ALTER TABLE pengajuan_izin OWNER TO "Gabriella";

--
-- Name: pengajuan_izin_syarat; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE pengajuan_izin_syarat (
    id character varying(25) NOT NULL,
    pengajuan_izin character varying(25) NOT NULL,
    izin_syarat character varying(25) NOT NULL,
    status character varying(60),
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE pengajuan_izin_syarat OWNER TO "Gabriella";

--
-- Name: permission_role; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE permission_role (
    id character varying(25) NOT NULL,
    permission_id character varying(25) NOT NULL,
    role_id character varying(25) NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE permission_role OWNER TO "Gabriella";

--
-- Name: permission_user; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE permission_user (
    id character varying(25) NOT NULL,
    permission_id character varying(25) NOT NULL,
    user_id character varying(25) NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE permission_user OWNER TO "Gabriella";

--
-- Name: permissions; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE permissions (
    id character varying(25) NOT NULL,
    inherit_id character varying(25),
    name character varying(255) NOT NULL,
    slug character varying(255) NOT NULL,
    description text,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE permissions OWNER TO "Gabriella";

--
-- Name: proses; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE proses (
    id character varying(25) NOT NULL,
    name character varying(60) NOT NULL,
    route character varying(255) NOT NULL,
    conf json,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE proses OWNER TO "Gabriella";

--
-- Name: provinces; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE provinces (
    id character(2) NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE provinces OWNER TO "Gabriella";

--
-- Name: role_user; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE role_user (
    id character varying(25) NOT NULL,
    role_id character varying(25) NOT NULL,
    user_id character varying(25) NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE role_user OWNER TO "Gabriella";

--
-- Name: roles; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE roles (
    id character varying(25) NOT NULL,
    name character varying(100) NOT NULL,
    slug character varying(60) NOT NULL,
    level character varying(255) NOT NULL,
    description text,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    CONSTRAINT roles_level_check CHECK (((level)::text = ANY ((ARRAY['admin'::character varying, 'backoffice'::character varying, 'pemohon'::character varying])::text[])))
);


ALTER TABLE roles OWNER TO "Gabriella";

--
-- Name: status; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE status (
    id character varying(25) NOT NULL,
    slug character varying(60) NOT NULL,
    name character varying(60) NOT NULL,
    label character varying(60),
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE status OWNER TO "Gabriella";

--
-- Name: surat_keputusan; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE surat_keputusan (
    id character varying(25) NOT NULL,
    mapping_output character varying(25) NOT NULL,
    template text NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE surat_keputusan OWNER TO "Gabriella";

--
-- Name: user_berkas; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE user_berkas (
    id character varying(25) NOT NULL,
    users character varying(25) NOT NULL,
    jenis_berkas character varying(25) NOT NULL,
    file character varying(255) NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE user_berkas OWNER TO "Gabriella";

--
-- Name: user_profiles; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE user_profiles (
    id character varying(25) NOT NULL,
    user_id character varying(100) NOT NULL,
    nik character varying(30) NOT NULL,
    no_kk character varying(30) NOT NULL,
    nama_ktp character varying(100) NOT NULL,
    alamat character varying(255) NOT NULL,
    no_rt character varying(5) NOT NULL,
    no_rw character varying(5) NOT NULL,
    kelurahan character varying(5) NOT NULL,
    kecamatan character varying(5) NOT NULL,
    kota character varying(5) NOT NULL,
    provinsi character varying(5) NOT NULL,
    tempat_lahir character varying(100) NOT NULL,
    tanggal_lahir character varying(25) NOT NULL,
    jenis_kelamin character varying(5) NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE user_profiles OWNER TO "Gabriella";

--
-- Name: user_providers; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE user_providers (
    id integer NOT NULL,
    user_id character varying(25) NOT NULL,
    provider_id character varying(100) NOT NULL,
    provider character varying(255) NOT NULL,
    image character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    CONSTRAINT user_providers_provider_check CHECK (((provider)::text = ANY ((ARRAY['facebook'::character varying, 'google'::character varying])::text[])))
);


ALTER TABLE user_providers OWNER TO "Gabriella";

--
-- Name: user_providers_id_seq; Type: SEQUENCE; Schema: public; Owner: Gabriella
--

CREATE SEQUENCE user_providers_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_providers_id_seq OWNER TO "Gabriella";

--
-- Name: user_providers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: Gabriella
--

ALTER SEQUENCE user_providers_id_seq OWNED BY user_providers.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE users (
    id character varying(25) NOT NULL,
    email character varying(100) NOT NULL,
    fullname character varying(60) NOT NULL,
    password character varying(255) NOT NULL,
    remember_token character varying(255),
    picture character varying(255),
    wilayah character varying(25),
    last_activity timestamp(0) without time zone,
    confirmed boolean DEFAULT false NOT NULL,
    confirmation_code character varying(255),
    active boolean DEFAULT true NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE users OWNER TO "Gabriella";

--
-- Name: villages; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE villages (
    id character(10) NOT NULL,
    district_id character(7) NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE villages OWNER TO "Gabriella";

--
-- Name: wilayah; Type: TABLE; Schema: public; Owner: Gabriella
--

CREATE TABLE wilayah (
    id character varying(25) NOT NULL,
    code character varying(25) NOT NULL,
    name character varying(100) NOT NULL,
    lat numeric(18,2),
    long numeric(18,2),
    description text,
    active boolean DEFAULT true NOT NULL,
    created_by character varying(25),
    updated_by character varying(25),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE wilayah OWNER TO "Gabriella";

--
-- Name: ltm_translations id; Type: DEFAULT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY ltm_translations ALTER COLUMN id SET DEFAULT nextval('ltm_translations_id_seq'::regclass);


--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY migrations ALTER COLUMN id SET DEFAULT nextval('migrations_id_seq'::regclass);


--
-- Name: user_providers id; Type: DEFAULT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY user_providers ALTER COLUMN id SET DEFAULT nextval('user_providers_id_seq'::regclass);


--
-- Data for Name: actions; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY actions (id, code, name, created_by, updated_by, created_at, updated_at) FROM stdin;
5a781dc78c21ade0e6	create	Create	SYSTEM	\N	2018-02-05 09:03:03	2018-02-05 09:03:03
5a781dd3b4e21351e6	update	Update	SYSTEM	\N	2018-02-05 09:03:15	2018-02-05 09:03:15
5a781dde76bf72e041	delete	Delete	SYSTEM	\N	2018-02-05 09:03:26	2018-02-05 09:03:26
5a781de9400491b2fc	view	View	SYSTEM	\N	2018-02-05 09:03:37	2018-02-05 09:03:37
\.


--
-- Data for Name: bidang; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY bidang (id, code, name, active, created_by, updated_by, created_at, updated_at) FROM stdin;
5a9b6819c1d8416826	A	Pendidikan	t	5a811f8be5ce60cf10	\N	2018-03-04 03:29:29	2018-03-04 03:29:29
5a9b68247b4df14564	B	Kesehatan	t	5a811f8be5ce60cf10	\N	2018-03-04 03:29:40	2018-03-04 03:29:40
5a9b685003a7176f74	D	Perumahan Rakyat Dan Kawasan Pemukiman	t	5a811f8be5ce60cf10	\N	2018-03-04 03:30:24	2018-03-04 03:30:24
5a9b68797a1878c2d3	E	Ketentraman, Ketertiban Umum Dan Pelindungan Masyarakat	t	5a811f8be5ce60cf10	\N	2018-03-04 03:31:05	2018-03-04 03:31:05
5a9b6883dfd7efbf4b	F	Sosial	t	5a811f8be5ce60cf10	\N	2018-03-04 03:31:15	2018-03-04 03:31:15
5a9b695362e2b50616	G	Tenaga Kerja	t	5a811f8be5ce60cf10	\N	2018-03-04 03:34:43	2018-03-04 03:34:43
5a9b69e1df339ad909	H	Pemberdayaan Perempuan Dan Pelindungan Anak	t	5a811f8be5ce60cf10	\N	2018-03-04 03:37:05	2018-03-04 03:37:05
5a9b69eb2cc7ad5fca	I	Pangan	t	5a811f8be5ce60cf10	\N	2018-03-04 03:37:15	2018-03-04 03:37:15
5a9b69fc830c2b4bcb	J	Pertanahan Yang Menjadi Kewenangan Daerah	t	5a811f8be5ce60cf10	\N	2018-03-04 03:37:32	2018-03-04 03:37:32
5a9b6a0a98f6ba01e1	K	Lingkungan Hidup	t	5a811f8be5ce60cf10	\N	2018-03-04 03:37:46	2018-03-04 03:37:46
5a9b6a1b34180366a3	L	Pemberdayaan Masyarakat	t	5a811f8be5ce60cf10	\N	2018-03-04 03:38:03	2018-03-04 03:38:03
5a9b6a2e8fe454e1d1	M	Pengendalian Penduduk Dan Keluarga Berencana	t	5a811f8be5ce60cf10	\N	2018-03-04 03:38:22	2018-03-04 03:38:22
5a9b6a3b594fcd2264	N	Perhubungan	t	5a811f8be5ce60cf10	\N	2018-03-04 03:38:35	2018-03-04 03:38:35
5a9b6a49d44475edbf	O	Komunikasi Dan Informatika	t	5a811f8be5ce60cf10	\N	2018-03-04 03:38:49	2018-03-04 03:38:49
5a9b6a783fe8c35e63	P	Koperasi, Serta Usaha Mikro, Kecil Dan Menengah	t	5a811f8be5ce60cf10	\N	2018-03-04 03:39:36	2018-03-04 03:39:36
5a9b6a860ef815918e	Q	Penanaman Modal	t	5a811f8be5ce60cf10	\N	2018-03-04 03:39:50	2018-03-04 03:39:50
5a9b6aab55ee109921	R	Kepemudaan Dan Keolahragaan	t	5a811f8be5ce60cf10	\N	2018-03-04 03:40:27	2018-03-04 03:40:27
5a9b6ab43820e3be60	S	Statistik	t	5a811f8be5ce60cf10	\N	2018-03-04 03:40:36	2018-03-04 03:40:36
5a9b6abe5456534b61	T	Persandian	t	5a811f8be5ce60cf10	\N	2018-03-04 03:40:46	2018-03-04 03:40:46
5a9b6ac943322bce40	U	Kebudayaan	t	5a811f8be5ce60cf10	\N	2018-03-04 03:40:57	2018-03-04 03:40:57
5a9b6ad6489d4bdd7a	V	Perpustakaan	t	5a811f8be5ce60cf10	\N	2018-03-04 03:41:10	2018-03-04 03:41:10
5a9b6ae0ca724492d1	W	Kearsipan	t	5a811f8be5ce60cf10	\N	2018-03-04 03:41:20	2018-03-04 03:41:20
5a9b6aee7512059249	X	Kelautan Dan Perikanan	t	5a811f8be5ce60cf10	\N	2018-03-04 03:41:34	2018-03-04 03:41:34
5a9b6afcaaec475190	Y	Pariwisata	t	5a811f8be5ce60cf10	\N	2018-03-04 03:41:48	2018-03-04 03:41:48
5a9b6b081b680b8fcd	Z	Pertanian	t	5a811f8be5ce60cf10	\N	2018-03-04 03:42:00	2018-03-04 03:42:00
5a9b6b191aa54d39d3	AA	Kehutanan	t	5a811f8be5ce60cf10	\N	2018-03-04 03:42:17	2018-03-04 03:42:17
5a9b6c210a22e35252	BB	Energi Dan Sumber Daya Mineral	t	5a811f8be5ce60cf10	\N	2018-03-04 03:46:41	2018-03-04 03:46:41
5a9b6c2d030b8edff5	CC	Perdagangan	t	5a811f8be5ce60cf10	\N	2018-03-04 03:46:53	2018-03-04 03:46:53
5a9b6c38a538df40f5	DD	Perindustrian	t	5a811f8be5ce60cf10	\N	2018-03-04 03:47:04	2018-03-04 03:47:04
5a9b6c43ebbdeffb07	EE	Transmigrasi	t	5a811f8be5ce60cf10	\N	2018-03-04 03:47:15	2018-03-04 03:47:15
5a9b6c5599fe1ca259	FF	Kesatuan Bangsa Dan Politik Dalam Negri	t	5a811f8be5ce60cf10	\N	2018-03-04 03:47:33	2018-03-04 03:47:33
5a9b6c625d200bd728	GG	Pelayanan Administrasi	t	5a811f8be5ce60cf10	\N	2018-03-04 03:47:46	2018-03-04 03:47:46
5a9b6832a010677c68	C	Pekerjaan Umum Dan Penataan Ruang	t	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-04 03:29:54	2018-03-04 04:10:18
\.


--
-- Data for Name: cities; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY cities (id, province_id, name) FROM stdin;
\.


--
-- Data for Name: data_perijinan; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY data_perijinan (id, tipe_perijinan, status_perijinan, bidang_perijinan, tanggal_mengajukan, due_date, keterangan, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: districts; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY districts (id, city_id, name) FROM stdin;
\.


--
-- Data for Name: izin; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY izin (id, code, izin_category, name, description, syarat_description, waktu_penyelesaian, waktu_penyelesaian_tipe, biaya_retribusi, masa_berlaku, masa_berlaku_tipe, created_by, updated_by, created_at, updated_at) FROM stdin;
5a9b717f3cbc5cccd5	C.23.1	5a9b6e86ba0118ee40	KETETAPAN RENCANA KOTA: BESAR, LUAS TANAH 5000 M2 KEATAS, UNTUK SEMUA JENIS BANGUNAN RUMAH TINGGAL DAN NON-RUMAH TINGGAL	<p>*Berdasarkan Peraturan Gubernur Provinsi DKI Jakarta No. 166 Tahun 2016 BAB III Pasal 4 tentang Pengecualian Pengenaan SIPPT dan/atau IPPT (sekarang IPPR), lahan yang termasuk dalam pengecualian yaitu:</p>\n\n<ol>\n\t<li>Tanah yang dimiliki oleh Pemerintah Provinsi Daerah Khusus Ibukota Jakarta yang dikelola oleh SKPD/UKPD;</li>\n\t<li>Tanah yang statusnya menjadi aset Badan Usaha Milik Daerah (BUMD) dan tidak dikerjasamakan dengan pihak swasta; dan</li>\n\t<li>Tanah di Kawasan Ekonomi Khusus (KEK, Marunda dan Jakarta Industrial Estate Pulogadung (JIEP) dan kawasan industri lainnya yang dimiliki oleh Pemerintah Pusat, Pemerintah Daerah, Badan Usaha Milik Daerah (BUMD) dan Badan Usaha Milik Negara (BUMN)</li>\n</ol>	<p><strong>Keterangan Persyaratan: &nbsp;</strong></p>\n\n<ol>\n\t<li>Lahan yang dimohon harus dalam kondisi siap ukur (tidak ada ilalang, tumbuhan liar yang mengganggu, binatang buas/melata, lahan tidak dalam kondisi sengketa, bebas dari keributan/kerusuhan di lapangan yang dapat menghambat pengukuran)</li>\n\t<li>Pengukuran dilakukan sesuai luas lahan yang tertera di sertifikat tanah yang dilampirkan</li>\n\t<li>Pihak yang menjadi penunjuk batas saat pengukuran harus orang yang kompeten dan mengerti tentang batas tanah kepemilikan yang dimohon dan memahami sertifikat tanah yang dilampirkan</li>\n\t<li>Pencetakan SKRD dilaksanakan setelah menerima konfirmasi dari pemohon terkait kesanggupan pembayaran retribusi. Konfirmasi dapat melalui SMS, Call Center, atau langsung mendatangi DPMPTSP</li>\n\t<li>Apabila pihak yang mengajukan permohonan &gt; 1 orang, maka semua surat wajib ditandatangani bersama serta melampirkan Fotokopi KTP dan NPWP tiap individu</li>\n\t<li>Fotokopi Peta KRK, Peta SIPPT, dan Gambar Situasi di Sertifikat Tanah harus dilampirkan dalam kondisi utuh dan tidak terpotong</li>\n\t<li>Estimasi waktu pengukuran disesuaikan dengan cuaca dan kondisi fisik di lapangan</li>\n\t<li>Estimasi proses penyelesaian KRK tergantung pada luas tanah yang dimohon dan jumlah surat tanah yang dilampirkan</li>\n\t<li>Estimasi waktu penerbitan izin diberhentikan sementara jika ada nota penolakan (kekurangan dokumen) dan/atau saat menunggu konfirmasi pembayaran SKRD KRK dari pemohon</li>\n</ol>	3	hari	Perda 1 Tahun 2015	20	hari	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-04 04:09:35	2018-03-10 10:16:25
\.


--
-- Data for Name: izin_berkas; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY izin_berkas (id, izin_proses, jenis_berkas, file, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: izin_category; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY izin_category (id, code, bidang, name, created_by, updated_by, created_at, updated_at) FROM stdin;
5a9b6e86ba0118ee40	C.23	5a9b6832a010677c68	Ketetapan Rencana Kota	5a811f8be5ce60cf10	\N	2018-03-04 03:56:54	2018-03-04 03:56:54
5a9b6eb1d5ae490d73	C.24	5a9b6832a010677c68	Ketetapan Rencana Kota Untuk Konsultasi BKPRD	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-04 03:57:37	2018-03-04 03:58:00
\.


--
-- Data for Name: izin_proses; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY izin_proses (id, izin, proses, name, estimated_time, roles, "order", created_by, updated_by, created_at, updated_at) FROM stdin;
5a9bc858097b849e4b	5a9b717f3cbc5cccd5	5a9bc9039a6c79e430	Cetak Berkas	3	["petugas"]	6	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-04 10:20:08	2018-03-04 10:24:34
5a9bc96218bf56cd8d	5a9b717f3cbc5cccd5	5a9bc9120b219302da	Pengambilan Berkas	3	["petugas","pemohon"]	7	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-04 10:24:34	2018-03-10 17:35:34
5a9bc294450286be5a	5a9b717f3cbc5cccd5	5a8315e94a8037a044	Pengajuan	\N	["petugas","pemohon"]	1	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-04 09:55:32	2018-03-12 03:19:04
5a9bc4be4de9b170a1	5a9b717f3cbc5cccd5	5a9bc3c63b9cb72916	Pemeriksaan Berkas	\N	["petugas","kasek"]	2	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-04 10:04:46	2018-03-12 03:36:35
5a9bc85807833a9450	5a9b717f3cbc5cccd5	5a9bc7516f39daa876	Persetujuan Kepala Seksi	2	["kabid","kasek"]	3	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-04 10:20:08	2018-03-12 03:36:35
5a9bc8580867e09eb1	5a9b717f3cbc5cccd5	5a9bc7516f39daa876	Persetujuan Kepala Bidang	3	["kabid","kadis"]	4	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-04 10:20:08	2018-03-12 03:36:35
5a9bc85808eb627b55	5a9b717f3cbc5cccd5	5a9bc7516f39daa876	Persetujuan Kepala Dinas	2	["petugas","kadis"]	5	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-04 10:20:08	2018-03-12 03:36:35
\.


--
-- Data for Name: izin_proses_status; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY izin_proses_status (id, izin_proses, status, next_proses, "order", created_by, updated_by, created_at, updated_at) FROM stdin;
5a9bc72d5b1ba1e520	5a9bc4be4de9b170a1	ditolak	\N	2	5a811f8be5ce60cf10	\N	2018-03-04 10:15:09	2018-03-04 10:15:09
5a9bc72d5a63d5ca54	5a9bc4be4de9b170a1	disetujui	5a9bc85807833a9450	1	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-04 10:15:09	2018-03-04 10:20:08
5a9bc8580e3b7184e1	5a9bc85807833a9450	disetujui.kasek	5a9bc8580867e09eb1	1	5a811f8be5ce60cf10	\N	2018-03-04 10:20:08	2018-03-04 10:20:08
5a9bc8580ea780f05c	5a9bc85807833a9450	ditolak	\N	2	5a811f8be5ce60cf10	\N	2018-03-04 10:20:08	2018-03-04 10:20:08
5a9bc8580efa6fa4d4	5a9bc8580867e09eb1	disetujui.kabid	5a9bc85808eb627b55	1	5a811f8be5ce60cf10	\N	2018-03-04 10:20:08	2018-03-04 10:20:08
5a9bc8580f694545d4	5a9bc8580867e09eb1	ditolak	\N	2	5a811f8be5ce60cf10	\N	2018-03-04 10:20:08	2018-03-04 10:20:08
5a9bc8580fbec20677	5a9bc85808eb627b55	disetujui.kadis	\N	1	5a811f8be5ce60cf10	\N	2018-03-04 10:20:08	2018-03-04 10:20:08
5a9bc858102265652c	5a9bc858097b849e4b	dicetak	5a9bc96218bf56cd8d	1	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-04 10:20:08	2018-03-04 10:24:34
5a9bc9621e13b630b6	5a9bc96218bf56cd8d	selesai	\N	1	5a811f8be5ce60cf10	\N	2018-03-04 10:24:34	2018-03-04 10:24:34
5a9bc37d2d747a6d51	5a9bc294450286be5a	diajukan	5a9bc4be4de9b170a1	1	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-04 09:59:25	2018-03-10 17:35:34
\.


--
-- Data for Name: izin_proses_status_tx; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY izin_proses_status_tx (id, pengajuan_izin, izin_proses, status, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: izin_role; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY izin_role (id, izin, izin_proses, route, role, status, is_default, next_status, created_by, updated_by, created_at, updated_at, next_route) FROM stdin;
5aa5f5c357504d996c	5a9b717f3cbc5cccd5	5a9bc294450286be5a	permohonan.formulir	petugas	diajukan	t	["ditolak","disetujui"]	5a811f8be5ce60cf10	\N	2018-03-12 03:36:35	2018-03-12 03:36:35	process.verifikasi
5aa5f5c35abcf1526c	5a9b717f3cbc5cccd5	5a9bc294450286be5a	permohonan.formulir	pemohon	diajukan	f	["ditolak","disetujui"]	5a811f8be5ce60cf10	\N	2018-03-12 03:36:35	2018-03-12 03:36:35	process.verifikasi
5aa5f5c35c2b1baaa7	5a9b717f3cbc5cccd5	5a9bc4be4de9b170a1	process.verifikasi	petugas	disetujui	f	["disetujui.kasek","ditolak"]	5a811f8be5ce60cf10	\N	2018-03-12 03:36:35	2018-03-12 03:36:35	process.verifikasi
5aa5f5c35ee698d153	5a9b717f3cbc5cccd5	5a9bc4be4de9b170a1	process.verifikasi	kasek	disetujui	f	["disetujui.kasek","ditolak"]	5a811f8be5ce60cf10	\N	2018-03-12 03:36:35	2018-03-12 03:36:35	process.verifikasi
5aa5f5c35f5e4c693c	5a9b717f3cbc5cccd5	5a9bc4be4de9b170a1	process.verifikasi	petugas	ditolak	f	[]	5a811f8be5ce60cf10	\N	2018-03-12 03:36:35	2018-03-12 03:36:35	\N
5aa5f5c360317f3d6e	5a9b717f3cbc5cccd5	5a9bc4be4de9b170a1	process.verifikasi	kasek	ditolak	f	[]	5a811f8be5ce60cf10	\N	2018-03-12 03:36:35	2018-03-12 03:36:35	\N
5aa5f5c362a0e85272	5a9b717f3cbc5cccd5	5a9bc85807833a9450	process.verifikasi	kabid	disetujui.kasek	f	["disetujui.kabid","ditolak"]	5a811f8be5ce60cf10	\N	2018-03-12 03:36:35	2018-03-12 03:36:35	process.verifikasi
5aa5f5c363ea12c5d8	5a9b717f3cbc5cccd5	5a9bc85807833a9450	process.verifikasi	kasek	disetujui.kasek	f	["disetujui.kabid","ditolak"]	5a811f8be5ce60cf10	\N	2018-03-12 03:36:35	2018-03-12 03:36:35	process.verifikasi
5aa5f5c3646d4682cd	5a9b717f3cbc5cccd5	5a9bc85807833a9450	process.verifikasi	kabid	ditolak	f	[]	5a811f8be5ce60cf10	\N	2018-03-12 03:36:35	2018-03-12 03:36:35	\N
5aa5f5c36503bab2b0	5a9b717f3cbc5cccd5	5a9bc85807833a9450	process.verifikasi	kasek	ditolak	f	[]	5a811f8be5ce60cf10	\N	2018-03-12 03:36:35	2018-03-12 03:36:35	\N
5aa5f5c3672a42b9c1	5a9b717f3cbc5cccd5	5a9bc8580867e09eb1	process.verifikasi	kabid	disetujui.kabid	f	["disetujui.kadis"]	5a811f8be5ce60cf10	\N	2018-03-12 03:36:35	2018-03-12 03:36:35	process.verifikasi
5aa5f5c368ca1d3f25	5a9b717f3cbc5cccd5	5a9bc8580867e09eb1	process.verifikasi	kadis	disetujui.kabid	f	["disetujui.kadis"]	5a811f8be5ce60cf10	\N	2018-03-12 03:36:35	2018-03-12 03:36:35	process.verifikasi
5aa5f5c3696538cfad	5a9b717f3cbc5cccd5	5a9bc8580867e09eb1	process.verifikasi	kabid	ditolak	f	[]	5a811f8be5ce60cf10	\N	2018-03-12 03:36:35	2018-03-12 03:36:35	\N
5aa5f5c36a6384b304	5a9b717f3cbc5cccd5	5a9bc8580867e09eb1	process.verifikasi	kadis	ditolak	f	[]	5a811f8be5ce60cf10	\N	2018-03-12 03:36:35	2018-03-12 03:36:35	\N
5aa5f5c36b043a9a40	5a9b717f3cbc5cccd5	5a9bc85808eb627b55	process.verifikasi	petugas	disetujui.kadis	f	[]	5a811f8be5ce60cf10	\N	2018-03-12 03:36:35	2018-03-12 03:36:35	\N
5aa5f5c36b549d1699	5a9b717f3cbc5cccd5	5a9bc85808eb627b55	process.verifikasi	kadis	disetujui.kadis	f	[]	5a811f8be5ce60cf10	\N	2018-03-12 03:36:35	2018-03-12 03:36:35	\N
5aa5f5c36d1d849131	5a9b717f3cbc5cccd5	5a9bc858097b849e4b	process.verifikasi	petugas	dicetak	f	["selesai"]	5a811f8be5ce60cf10	\N	2018-03-12 03:36:35	2018-03-12 03:36:35	process.verifikasi
5aa5f5c36ea4d7ac87	5a9b717f3cbc5cccd5	5a9bc96218bf56cd8d	process.verifikasi	petugas	selesai	f	[]	5a811f8be5ce60cf10	\N	2018-03-12 03:36:35	2018-03-12 03:36:35	\N
5aa5f5c36f1f609d45	5a9b717f3cbc5cccd5	5a9bc96218bf56cd8d	process.verifikasi	pemohon	selesai	f	[]	5a811f8be5ce60cf10	\N	2018-03-12 03:36:35	2018-03-12 03:36:35	\N
\.


--
-- Data for Name: izin_syarat; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY izin_syarat (id, izin, name, jenis_berkas, description, "order", created_by, updated_by, created_at, updated_at) FROM stdin;
5aa39ca04c1a2e6371	5a9b717f3cbc5cccd5	<p><strong>Jika dikuasakan</strong></p>\n\n<p>Surat kuasa di atas kertas bermaterai RP 6.000 dan KTP orang yang diberi kuas dan/atau</p>\n\n<p>Surat kuasa yang di tandatangani bersama jika nama yang tertera di sertipikat lebih dari 1 (satu)</p>	\N	\N	3	5a811f8be5ce60cf10	\N	2018-03-10 08:51:44	2018-03-10 08:51:44
5aa39ca04d08765b2d	5a9b717f3cbc5cccd5	<p>Bukti pembayaran Pajak Bumi dan Bangunan (PBB) tahun terakhir sebelum jatuh tempo, atau Bukti Nihil PBB dari UPPRD Kecamatan setempat <em>(Fotokopi)</em></p>	\N	\N	6	5a811f8be5ce60cf10	\N	2018-03-10 08:51:44	2018-03-10 08:51:44
5aa39ca04d6115e800	5a9b717f3cbc5cccd5	<p>Foto lokasi lahan yang dimohon (min. 3 sudut pandang yang berbeda) dan Peta Lokasi Lahan yang dimohon (diambil dari google maps)</p>	\N	\N	7	5a811f8be5ce60cf10	\N	2018-03-10 08:51:44	2018-03-10 08:51:44
5aa39ca04e99710864	5a9b717f3cbc5cccd5	<p>Fotokopi Izin Prinsip Pemanfaatan Ruang yang masih berlaku (kecuali untuk lahan yang masuk dalam pengecualian dari pengenaan SIPPT dan/atau IPPR) (termasuk Peta Lampiran)</p>	\N	\N	11	5a811f8be5ce60cf10	\N	2018-03-10 08:51:44	2018-03-10 08:51:44
5aa39ca04c9eb40976	5a9b717f3cbc5cccd5	<p><strong>Bukti Kepemilikan Tanah </strong></p>\n\n<ul>\n\t<li><em>Fotokopi yang dilegalisasi Notaris/menunjukkan Asli</em>, Sertipikat Hak Milik/Sertipikat Hak Guna Bangunan/Sertipikat Hak Pakai /Sertipikat Hak Pengelolaan disertai lampiran gambar situasi lahan yang utuh dan jelas, apabila terdapat perbedaan antara nama pemohon dengan yang tertera pada Sertipikat tanah maka dilampirkan AJB (Maksimal 2 kali pergantian kepemilikan), atau akta perjanjian kerjasama notarial atau sejenisnya.</li>\n\t<li>Bila kepemilikan tanah berupa Girik/Verpounding/Surat tanah lainnya dilengkapi dengan Pernyataan tidak sengketa, Keterangan Riwayat tanah/Rekomendasi hak atas tanah dan surat penguasaan fisik tanah (<em>untuk penguasaan fisik tanah harus di tahun yang sama)</em> yang diketahui lurah<em> (Fotokopi yang di legalisasi)</em></li>\n\t<li>Surat Keterangan Aset dari BPAD Provinsi DKIJakarta atau KIB (Kartu inventaris barang) apabila lahan yang dimiliki tidak memiliki sertipikat tanah dan merupakan lahan <strong>milik Pemerintah Provinsi DKI Jakarta</strong></li>\n\t<li>Jika terdapat perbedaan identitas/alamat antara permohonan dengan bukti kepemilikan tanah, maka di lengkapi dengan surat keterangan lurah (PM.1)</li>\n\t<li>Jika nama yang tertera pada bukti kepemilikan tanah sudah meninggal dunia, maka diperlukan surat Pernyataan Ahli Waris yang diketahui Lurah dan Camat (Fotokopi yang dilegalisasi lurah)</li>\n\t<li>Surat Keterangan dari Bank yang menyatakan persetujuan untuk mengurus KRK dan/atau IMB, jika sertipikat sedang diagunkan.</li>\n</ul>	\N	\N	5	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-10 08:51:44	2018-03-10 09:02:05
5aa39ca04da3ddbecd	5a9b717f3cbc5cccd5	<p>Lembar Plot Lokasi (Persil Lahan) pada Peta Zonasi dengan Keterangan Kegiatan Utama & Kegiatan Penunjang yang Diajukan</p>	\N	\N	8	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-10 08:51:44	2018-03-10 09:02:05
5aa39ca04e3cac8a9a	5a9b717f3cbc5cccd5	<p>Fotokopi KRK (Perbal & Peta) dan Persetujuan Prinsip Penataan Kegiatan (Apabila terdapat penataan kegiatan), dan Persetujuan Prinsip lainnya yang pernah diterbitkan.</p>	\N	\N	10	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-10 08:51:44	2018-03-10 09:02:05
5aa39ca04dee30d673	5a9b717f3cbc5cccd5	<p>Ikhtisar tanah (untuk surat tanah ≥ 2 surat tanah), berupa Sketsa Peta/Denah tanah yang menginformasikan posisi tiap sertifikat tanah pada lahan yang dimohon, serta daftar Surat Tanah berisi Nomor dan Tanggal Sertifikat, Nama Pemegang Hak, Luas Tanah, Tanggal masa berakhir (untuk Sertifikat Hak Guna Bangunan), dan Total Luas Tanah yang diakumulasikan dari seluruh sertifikat terlampir,</p>	\N	\N	9	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-10 08:51:44	2018-03-10 09:02:14
5aa39f56eafc40de50	5a9b717f3cbc5cccd5	<p>Khusus Lahan di Kawasan Industri, wajib melampirkan:</p>\n\n<p>a. Fotokopi Surat Rekomendasi dari Pengelola Kawasan Industri tersebut</p>\n\n<p>b. Surat Perjanjian Kerjasama Industri</p>\n\n<p>c. Fotokopi SIPPT Kawasan Industri tersebut</p>	\N	\N	12	5a811f8be5ce60cf10	\N	2018-03-10 09:03:18	2018-03-10 09:03:18
5aa39ca04bcc3df882	5a9b717f3cbc5cccd5	<p><strong>Identitas Pemohon/Penangung Jawab </strong></p>\n\n<ul>\n\t<li>WNI :  Kartu Tanda Penduduk (KTP) dan NPWP <em>(Fotokopi)</em></li>\n\t<li>WNA : Kartu Kartu Izin Tinggal Terbatas (KITAS) atau VISA / Paspor <em>(Fotokopi)</em></li>\n</ul>	5a8e057b5b9770caeb	\N	2	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-10 08:51:44	2018-03-10 10:06:03
5aa39ca04c557503f2	5a9b717f3cbc5cccd5	<p><strong>Jika Badan Hukum / Badan Usaha</strong></p>\n\n<ul>\n\t<li>Akta pendirian dan perubahan (Kantor Pusat dan Kantor Cabang, jika ada) <em>(Fotokopi)</em></li>\n\t<li>SK pengesahan pendirian dan perubahan <em>(Fotokopi) </em>yang dikeluarkan oleh : \n\t<ul>\n\t\t<li>Kemenkunham, jika PT dan Yayasan</li>\n\t\t<li>Kementrian, jika Koperasi</li>\n\t\t<li>Pengadilan Negeri, jika CV</li>\n\t\t<li>NPWP Badan Hukum <em>(Fotokopi)</em></li>\n\t</ul>\n\t</li>\n</ul>\n\n<p><strong>Jika Lembaga/ Kementrian/ SKPD/ BUMN / BUMD</strong></p>\n\n<ul>\n\t<li>Surat Keputusan (SK) Pendirian Badan Usaha dari Instansi Pemerintah apabila merupakan BUMN/BUMD</li>\n\t<li>SK Pengangkatan penanggung jawab dari SKPD/Kementrian</li>\n</ul>	\N	\N	4	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-10 08:51:44	2018-03-10 17:35:34
5aa39ca04b20962c15	5a9b717f3cbc5cccd5	<p>Surat permohonan yang didalamnya terdapat pernyataan kebenaran dan keabsahan dokumen & lahan tidak dalam sengketa, beserta data di atas kertas bermaterai Rp 6.000</p>	\N	\N	1	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-10 08:51:44	2018-03-12 03:00:01
\.


--
-- Data for Name: jenis_berkas; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY jenis_berkas (id, name, description, updated_by, created_by, created_at, updated_at) FROM stdin;
5a8e06455f391618f7	Kartu Keluarga	\N	\N	5a811f8be5ce60cf10	2018-02-21 23:52:37	2018-02-21 23:52:37
5a8e057b5b9770caeb	Kartu Tanda Penduduk	\N	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-02-21 23:49:15	2018-02-21 23:52:47
\.


--
-- Data for Name: jenis_izin; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY jenis_izin (id, name, active, updated_by, created_by, created_at, updated_at) FROM stdin;
5a7c66fa36ed14e60c	Ketataruangan	t	barkah.hadi	barkah.hadi	2018-02-08 15:04:26	2018-02-08 15:05:22
\.


--
-- Data for Name: jenis_koperasi; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY jenis_koperasi (id, name, active, updated_by, created_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: jenis_perusahaan; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY jenis_perusahaan (id, code, name, active, updated_by, created_by, created_at, updated_at) FROM stdin;
5a7c729da5519972cb	CV	Commanditaire Vennootschap	t	\N	barkah.hadi	2018-02-08 15:54:05	2018-02-08 15:54:05
5a7c72ac349740e283	FA	Firma	t	\N	barkah.hadi	2018-02-08 15:54:20	2018-02-08 15:54:20
5a7c72bba7e199cb93	PT	Perseroan Terbatas	t	\N	barkah.hadi	2018-02-08 15:54:35	2018-02-08 15:54:35
\.


--
-- Data for Name: ltm_translations; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY ltm_translations (id, status, locale, "group", key, value, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: mapping_output; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY mapping_output (id, name, "order", izin, output, roles, email_penerima, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: media; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY media (id, model_id, model_type, collection_name, name, file_name, mime_type, disk, jenis_berkas, size, manipulations, custom_properties, order_column, created_by, updated_by, created_at, updated_at) FROM stdin;
5a9621bcd3a4c62456	5a811f8be5ce60cf10	Modules\\SSO\\Entities\\User	images	44	44.jpg	image/jpeg	files	\N	182223	[]	[]	3	5a811f8be5ce60cf10	\N	2018-02-28 03:27:56	2018-02-28 03:27:56
5a9622b4ad62dc5697	5a811f8be5ce60cf10	Modules\\SSO\\Entities\\User	images	25	25.jpg	image/jpeg	files	5a8e06455f391618f7	381513	[]	[]	4	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-02-28 03:32:04	2018-02-28 03:53:09
5a9622edd912c6f84c	5a811f8be5ce60cf10	Modules\\SSO\\Entities\\User	images	19	19.jpg	image/jpeg	files	5a8e057b5b9770caeb	109358	[]	[]	5	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-02-28 03:33:01	2018-02-28 03:53:38
5a9655bc06c9afb33e	5a811f8be5ce60cf10	Modules\\SSO\\Entities\\User	images	8	8.jpg	image/jpeg	files	\N	195401	[]	[]	6	5a811f8be5ce60cf10	\N	2018-02-28 07:09:48	2018-02-28 07:09:48
5a979cf2ea53eb3d64	5a811f8be5ce60cf10	Modules\\SSO\\Entities\\User	images	KTP	KTP.jpeg	image/jpeg	files	5a8e057b5b9770caeb	217028	[]	[]	7	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-01 06:25:54	2018-03-01 06:26:30
5a9655bc06398a49fd	5a811f8be5ce60cf10	Modules\\SSO\\Entities\\User	images	16	16.jpg	image/jpeg	files	5a8e06455f391618f7	116716	[]	[]	6	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-02-28 07:09:48	2018-03-03 03:48:44
5aa578a8c0cb27e050	5aa5737f60c611fd90	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	WhatsApp Image 2018-03-07 at 15.35.37	WhatsApp Image 2018-03-07 at 15.35.37.jpeg	image/jpeg	files	\N	93657	[]	[]	23	5a9a28c6d4e8c12ad0	\N	2018-03-11 18:42:48	2018-03-11 18:42:48
5aa56eb19ead5ad237	5aa56eb198f5df6858	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	ktp	ktp.jpg	image/jpeg	files	\N	304222	[]	[]	13	5a9a28c6d4e8c12ad0	\N	2018-03-11 18:00:17	2018-03-11 18:00:17
5aa56f1507ec9d0e26	5a9a28c6d4e8c12ad0	Modules\\SSO\\Entities\\User	images	WhatsApp Image 2018-02-26 at 16.50.05	WhatsApp Image 2018-02-26 at 16.50.05.jpeg	image/jpeg	files	\N	173217	[]	[]	14	5a9a28c6d4e8c12ad0	\N	2018-03-11 18:01:57	2018-03-11 18:01:57
5aa56f86639772e849	5aa56f865ffa4132c8	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	WhatsApp Image 2018-03-07 at 15.35.37	WhatsApp Image 2018-03-07 at 15.35.37.jpeg	image/jpeg	files	\N	93657	[]	[]	15	5a9a28c6d4e8c12ad0	\N	2018-03-11 18:03:50	2018-03-11 18:03:50
5aa574558fcf2e5f58	5aa5737f5fc91fcdb3	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	WhatsApp Image 2018-03-06 at 09.12.22	WhatsApp Image 2018-03-06 at 09.12.22.jpeg	image/jpeg	files	\N	146580	[]	[]	16	5a9a28c6d4e8c12ad0	\N	2018-03-11 18:24:21	2018-03-11 18:24:21
5aa574be4c24c14504	5aa5737f60c611fd90	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	WhatsApp Image 2018-03-06 at 09.12.22	WhatsApp Image 2018-03-06 at 09.12.22.jpeg	image/jpeg	files	\N	146580	[]	[]	20	5a9a28c6d4e8c12ad0	\N	2018-03-11 18:26:06	2018-03-11 18:26:06
5aa578a41c66b6499a	5a9a28c6d4e8c12ad0	Modules\\SSO\\Entities\\User	images	WhatsApp Image 2018-03-07 at 15.35.37	WhatsApp Image 2018-03-07 at 15.35.37.jpeg	image/jpeg	files	\N	93657	[]	[]	22	5a9a28c6d4e8c12ad0	\N	2018-03-11 18:42:44	2018-03-11 18:42:44
5aa578a41caea0c906	5a9a28c6d4e8c12ad0	Modules\\SSO\\Entities\\User	images	WhatsApp Image 2018-03-06 at 09.12.22	WhatsApp Image 2018-03-06 at 09.12.22.jpeg	image/jpeg	files	\N	146580	[]	[]	22	5a9a28c6d4e8c12ad0	\N	2018-03-11 18:42:44	2018-03-11 18:42:44
5aa578a41cd92cd5f0	5a9a28c6d4e8c12ad0	Modules\\SSO\\Entities\\User	images	WhatsApp Image 2018-02-26 at 16.50.05	WhatsApp Image 2018-02-26 at 16.50.05.jpeg	image/jpeg	files	\N	173217	[]	[]	22	5a9a28c6d4e8c12ad0	\N	2018-03-11 18:42:44	2018-03-11 18:42:44
5aa578d8c365231c06	5aa5737f60c611fd90	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	WhatsApp Image 2018-03-06 at 09.12.22	WhatsApp Image 2018-03-06 at 09.12.22.jpeg	image/jpeg	files	\N	146580	[]	[]	25	5a9a28c6d4e8c12ad0	\N	2018-03-11 18:43:36	2018-03-11 18:43:36
5aa57928656bf7b98e	5aa5737f60c611fd90	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	WhatsApp Image 2018-02-26 at 16.50.05	WhatsApp Image 2018-02-26 at 16.50.05.jpeg	image/jpeg	files	\N	173217	[]	[]	26	5a9a28c6d4e8c12ad0	\N	2018-03-11 18:44:56	2018-03-11 18:44:56
5aa57937ddc9c2e753	5aa5737f612003287f	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	WhatsApp Image 2018-03-07 at 15.35.37	WhatsApp Image 2018-03-07 at 15.35.37.jpeg	image/jpeg	files	\N	93657	[]	[]	27	5a9a28c6d4e8c12ad0	\N	2018-03-11 18:45:11	2018-03-11 18:45:11
5aa5794045f452db6a	5aa5737f612003287f	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	WhatsApp Image 2018-03-07 at 15.35.37	WhatsApp Image 2018-03-07 at 15.35.37.jpeg	image/jpeg	files	\N	93657	[]	[]	28	5a9a28c6d4e8c12ad0	\N	2018-03-11 18:45:20	2018-03-11 18:45:20
5aa57947c635eea417	5aa5737f612003287f	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	WhatsApp Image 2018-02-26 at 16.50.05	WhatsApp Image 2018-02-26 at 16.50.05.jpeg	image/jpeg	files	\N	173217	[]	[]	29	5a9a28c6d4e8c12ad0	\N	2018-03-11 18:45:27	2018-03-11 18:45:27
5aa579548b7d9292b2	5a9a28c6d4e8c12ad0	Modules\\SSO\\Entities\\User	images	ktp	ktp.jpg	image/jpeg	files	\N	304222	[]	[]	30	5a9a28c6d4e8c12ad0	\N	2018-03-11 18:45:40	2018-03-11 18:45:40
5aa5795b8adc6e5b86	5aa5737f612003287f	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	WhatsApp Image 2018-02-26 at 16.50.05	WhatsApp Image 2018-02-26 at 16.50.05.jpeg	image/jpeg	files	\N	173217	[]	[]	32	5a9a28c6d4e8c12ad0	\N	2018-03-11 18:45:47	2018-03-11 18:45:47
5aa579609050b465d5	5aa5737f617cb80da2	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	ktp	ktp.jpg	image/jpeg	files	\N	304222	[]	[]	33	5a9a28c6d4e8c12ad0	\N	2018-03-11 18:45:52	2018-03-11 18:45:52
5aa579642914c42631	5aa5737f617cb80da2	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	WhatsApp Image 2018-03-07 at 15.35.37	WhatsApp Image 2018-03-07 at 15.35.37.jpeg	image/jpeg	files	\N	93657	[]	[]	34	5a9a28c6d4e8c12ad0	\N	2018-03-11 18:45:56	2018-03-11 18:45:56
5aa57b24ab599a9f22	5aa5737f5fc91fcdb3	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	WhatsApp Image 2018-03-07 at 15.35.37	WhatsApp Image 2018-03-07 at 15.35.37.jpeg	image/jpeg	files	\N	93657	[]	[]	35	5a9a28c6d4e8c12ad0	\N	2018-03-11 18:53:24	2018-03-11 18:53:24
5aa57bcd2f5fb74ee2	5aa5737f5fc91fcdb3	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	WhatsApp Image 2018-02-26 at 16.50.05	WhatsApp Image 2018-02-26 at 16.50.05.jpeg	image/jpeg	files	\N	173217	[]	[]	36	5a9a28c6d4e8c12ad0	\N	2018-03-11 18:56:13	2018-03-11 18:56:13
5aa580ebcd91d4cb5e	5aa5737f612003287f	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	WhatsApp Image 2018-03-07 at 15.35.37	WhatsApp Image 2018-03-07 at 15.35.37.jpeg	image/jpeg	files	\N	93657	[]	[]	37	5a9a28c6d4e8c12ad0	\N	2018-03-11 19:18:03	2018-03-11 19:18:03
5aa581d12b27a8353e	5aa581ca623f0b8347	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	WhatsApp Image 2018-03-07 at 15.35.37	WhatsApp Image 2018-03-07 at 15.35.37.jpeg	image/jpeg	files	\N	93657	[]	[]	38	5a9a28c6d4e8c12ad0	\N	2018-03-11 19:21:53	2018-03-11 19:21:53
5aa581d896a2d4ce44	5aa581ca623f0b8347	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	ktp	ktp.jpg	image/jpeg	files	\N	304222	[]	[]	39	5a9a28c6d4e8c12ad0	\N	2018-03-11 19:22:00	2018-03-11 19:22:00
5aa581fe5deb5fe0fe	5aa581ca63cd453e53	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	ktp	ktp.jpg	image/jpeg	files	\N	304222	[]	[]	40	5a9a28c6d4e8c12ad0	\N	2018-03-11 19:22:38	2018-03-11 19:22:38
5aa5dd97e51528595c	5aa5dc0a82009a8c41	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	01. surat permohonan	01. surat permohonan.png	image/png	files	\N	4352	[]	[]	50	5a9d1f6d700c330a55	\N	2018-03-12 01:53:27	2018-03-12 01:53:27
5aa5dd9e2eac5c243d	5aa5dc0a855f3a6b0d	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	02. ktp	02. ktp.png	image/png	files	\N	4352	[]	[]	51	5a9d1f6d700c330a55	\N	2018-03-12 01:53:34	2018-03-12 01:53:34
5aa5dda5b8222a9761	5aa5dc0a875c7e98c3	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	03. surat kuasa	03. surat kuasa.png	image/png	files	\N	4352	[]	[]	52	5a9d1f6d700c330a55	\N	2018-03-12 01:53:41	2018-03-12 01:53:41
5aa5ddac4b2d9c6d71	5aa5dc0a88ecd9365e	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	04. SK	04. SK.png	image/png	files	\N	4352	[]	[]	53	5a9d1f6d700c330a55	\N	2018-03-12 01:53:48	2018-03-12 01:53:48
5aa5ddb2d692b47a52	5aa5dc0a89719a538f	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	05. tanah	05. tanah.png	image/png	files	\N	4352	[]	[]	54	5a9d1f6d700c330a55	\N	2018-03-12 01:53:54	2018-03-12 01:53:54
5aa5ddba869402fbd2	5aa5dc0a89ea1f0b0c	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	06. PBB	06. PBB.png	image/png	files	\N	4352	[]	[]	55	5a9d1f6d700c330a55	\N	2018-03-12 01:54:02	2018-03-12 01:54:02
5aa5ddc1e68a08478d	5aa5dc0a8a5b23c1fb	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	07. lokasi lahan	07. lokasi lahan.png	image/png	files	\N	4352	[]	[]	56	5a9d1f6d700c330a55	\N	2018-03-12 01:54:09	2018-03-12 01:54:09
5aa5ddc713906e98f5	5aa5dc0a8ab8daad4b	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	08. plot lokasi	08. plot lokasi.png	image/png	files	\N	4352	[]	[]	57	5a9d1f6d700c330a55	\N	2018-03-12 01:54:15	2018-03-12 01:54:15
5aa5ddcc65d5aae32e	5aa5dc0a8b01d6eaba	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	09. ikhtisar tanah	09. ikhtisar tanah.png	image/png	files	\N	4352	[]	[]	58	5a9d1f6d700c330a55	\N	2018-03-12 01:54:20	2018-03-12 01:54:20
5aa5ddd16519b5c0d4	5aa5dc0a8b4a2f3683	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	10. krk	10. krk.png	image/png	files	\N	4352	[]	[]	59	5a9d1f6d700c330a55	\N	2018-03-12 01:54:25	2018-03-12 01:54:25
5aa5ddd5699db79a0c	5aa5dc0a8b968a06a2	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	11. izin prinsip	11. izin prinsip.png	image/png	files	\N	4352	[]	[]	60	5a9d1f6d700c330a55	\N	2018-03-12 01:54:29	2018-03-12 01:54:29
5aa5ddd9143e1dfa8a	5aa5dc0a8be11f9d54	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	12. kawasan industri	12. kawasan industri.png	image/png	files	\N	4352	[]	[]	61	5a9d1f6d700c330a55	\N	2018-03-12 01:54:33	2018-03-12 01:54:33
5aa5f7f807a67a4e11	5aa5f7309a9da5aead	Modules\\SSO\\Entities\\User	images	05. tanah	05. tanah.png	image/png	files	\N	4352	[]	[]	62	5aa5f7309a9da5aead	\N	2018-03-12 03:46:00	2018-03-12 03:46:00
5aa5f7f80923362eed	5aa5f7309a9da5aead	Modules\\SSO\\Entities\\User	images	03. surat kuasa	03. surat kuasa.png	image/png	files	\N	4352	[]	[]	62	5aa5f7309a9da5aead	\N	2018-03-12 03:46:00	2018-03-12 03:46:00
5aa5f7f831afdebfe0	5aa5f7309a9da5aead	Modules\\SSO\\Entities\\User	images	04. SK	04. SK.png	image/png	files	\N	4352	[]	[]	63	5aa5f7309a9da5aead	\N	2018-03-12 03:46:00	2018-03-12 03:46:00
5aa5f7f83405ab5eee	5aa5f7309a9da5aead	Modules\\SSO\\Entities\\User	images	02. ktp	02. ktp.png	image/png	files	\N	4352	[]	[]	63	5aa5f7309a9da5aead	\N	2018-03-12 03:46:00	2018-03-12 03:46:00
5aa5f7f837da73f214	5aa5f7309a9da5aead	Modules\\SSO\\Entities\\User	images	01. surat permohonan	01. surat permohonan.png	image/png	files	\N	4352	[]	[]	64	5aa5f7309a9da5aead	\N	2018-03-12 03:46:00	2018-03-12 03:46:00
5aa5f7f8571ed58504	5aa5f7309a9da5aead	Modules\\SSO\\Entities\\User	images	06. PBB	06. PBB.png	image/png	files	\N	4352	[]	[]	65	5aa5f7309a9da5aead	\N	2018-03-12 03:46:00	2018-03-12 03:46:00
5aa5f7f91ecd1041e5	5aa5f7309a9da5aead	Modules\\SSO\\Entities\\User	images	07. lokasi lahan	07. lokasi lahan.png	image/png	files	\N	4352	[]	[]	66	5aa5f7309a9da5aead	\N	2018-03-12 03:46:01	2018-03-12 03:46:01
5aa5f7f91ba5e9fbf3	5aa5f7309a9da5aead	Modules\\SSO\\Entities\\User	images	08. plot lokasi	08. plot lokasi.png	image/png	files	\N	4352	[]	[]	66	5aa5f7309a9da5aead	\N	2018-03-12 03:46:01	2018-03-12 03:46:01
5aa5f7f9439f6e6aa8	5aa5f7309a9da5aead	Modules\\SSO\\Entities\\User	images	09. ikhtisar tanah	09. ikhtisar tanah.png	image/png	files	\N	4352	[]	[]	67	5aa5f7309a9da5aead	\N	2018-03-12 03:46:01	2018-03-12 03:46:01
5aa5f7f944deb6080a	5aa5f7309a9da5aead	Modules\\SSO\\Entities\\User	images	10. krk	10. krk.png	image/png	files	\N	4352	[]	[]	68	5aa5f7309a9da5aead	\N	2018-03-12 03:46:01	2018-03-12 03:46:01
5aa5f7f949bb48b746	5aa5f7309a9da5aead	Modules\\SSO\\Entities\\User	images	11. izin prinsip	11. izin prinsip.png	image/png	files	\N	4352	[]	[]	69	5aa5f7309a9da5aead	\N	2018-03-12 03:46:01	2018-03-12 03:46:01
5aa5f7f984ac331adc	5aa5f7309a9da5aead	Modules\\SSO\\Entities\\User	images	12. kawasan industri	12. kawasan industri.png	image/png	files	\N	4352	[]	[]	70	5aa5f7309a9da5aead	\N	2018-03-12 03:46:01	2018-03-12 03:46:01
5aa5f80f8b86510460	5aa5f7dce372a7d7f0	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	01. surat permohonan	01. surat permohonan.png	image/png	files	\N	4352	[]	[]	71	5aa5f7309a9da5aead	\N	2018-03-12 03:46:23	2018-03-12 03:46:23
5aa5f81ce5ea7b9863	5aa5f7dcea27552d76	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	02. ktp	02. ktp.png	image/png	files	\N	4352	[]	[]	72	5aa5f7309a9da5aead	\N	2018-03-12 03:46:36	2018-03-12 03:46:36
5aa5f82ea20509031b	5aa5f7dcebd7b726e7	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	03. surat kuasa	03. surat kuasa.png	image/png	files	\N	4352	[]	[]	73	5aa5f7309a9da5aead	\N	2018-03-12 03:46:54	2018-03-12 03:46:54
5aa5f83a58af95bc8b	5aa5f7dcec28dd14fd	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	04. SK	04. SK.png	image/png	files	\N	4352	[]	[]	74	5aa5f7309a9da5aead	\N	2018-03-12 03:47:06	2018-03-12 03:47:06
5aa5f846da81332e03	5aa5f7dcec73bcfcc0	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	05. tanah	05. tanah.png	image/png	files	\N	4352	[]	[]	75	5aa5f7309a9da5aead	\N	2018-03-12 03:47:18	2018-03-12 03:47:18
5aa5f85875189f52d8	5aa5f7dcecc486ba7e	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	11. izin prinsip	11. izin prinsip.png	image/png	files	\N	4352	[]	[]	76	5aa5f7309a9da5aead	\N	2018-03-12 03:47:36	2018-03-12 03:47:36
5aa5f85fafb222e071	5aa5f7dced3e97bb49	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	10. krk	10. krk.png	image/png	files	\N	4352	[]	[]	77	5aa5f7309a9da5aead	\N	2018-03-12 03:47:43	2018-03-12 03:47:43
5aa5f866a644139208	5aa5f7dcede1bc8f39	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	12. kawasan industri	12. kawasan industri.png	image/png	files	\N	4352	[]	[]	78	5aa5f7309a9da5aead	\N	2018-03-12 03:47:50	2018-03-12 03:47:50
5aa5f86cc244b16f00	5aa5f7dcee78fed784	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	11. izin prinsip	11. izin prinsip.png	image/png	files	\N	4352	[]	[]	79	5aa5f7309a9da5aead	\N	2018-03-12 03:47:56	2018-03-12 03:47:56
5aa5f873e1a4657e51	5aa5f7dceefdd740d3	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	07. lokasi lahan	07. lokasi lahan.png	image/png	files	\N	4352	[]	[]	80	5aa5f7309a9da5aead	\N	2018-03-12 03:48:03	2018-03-12 03:48:03
5aa5f87c90c3eab91d	5aa5f7dcef4f983ae7	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	09. ikhtisar tanah	09. ikhtisar tanah.png	image/png	files	\N	4352	[]	[]	81	5aa5f7309a9da5aead	\N	2018-03-12 03:48:12	2018-03-12 03:48:12
5aa5f882a0f37de877	5aa5f7dcef97b54b06	Modules\\Pemohon\\Entities\\PengajuanIzinSyarat	images	07. lokasi lahan	07. lokasi lahan.png	image/png	files	\N	4352	[]	[]	82	5aa5f7309a9da5aead	\N	2018-03-12 03:48:18	2018-03-12 03:48:18
\.


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY migrations (id, migration, batch) FROM stdin;
1	2016_08_03_072729_create_provinces_table	1
2	2016_08_03_072750_create_cities_table	1
3	2016_08_03_072804_create_districts_table	1
4	2016_08_03_072819_create_villages_table	1
5	2018_01_22_051402_create_users_table	1
6	2018_01_25_030317_create_data_perijinan_table	1
7	2018_01_26_081420_create_modules_table	1
8	2018_01_26_081441_create_wilayah_table	1
9	2018_02_01_040727_create_user_providers_table	1
10	2018_02_02_073557_create_jenis_perusahaan_table	1
11	2018_02_03_050411_create_jenis_izin_table	1
12	2018_02_03_062450_create_bidang_table	1
13	2018_02_03_122803_create_jenis_koperasi_table	1
14	2018_02_05_081515_create_actions_table	1
15	2018_02_07_172606_create_roles_table	1
16	2018_02_07_172633_create_role_user_table	1
17	2018_02_07_172649_create_permissions_table	1
18	2018_02_07_172657_create_permission_role_table	1
19	2018_02_13_090047_create_user_profiles_table	1
20	2018_02_13_104634_create_proses_table	1
21	2018_02_13_110157_create_status_table	1
22	2018_02_13_111123_create_izin_proses_table	1
23	2018_02_13_130008_create_izin_category_table	1
24	2018_02_13_130009_create_izin_table	1
25	2018_02_14_032748_create_output_table	1
26	2018_02_14_032759_create_mapping_output_table	1
27	2018_02_14_174738_create_izin_proses_status_table	1
28	2018_02_17_152439_create_permission_user_table	1
29	2018_02_21_090148_create_pengajuan_izin_table	1
30	2018_02_21_233041_create_jenis_berkas_table	1
31	2018_02_21_235508_create_izin_berkas_table	1
32	2018_02_22_154724_create_user_berkas_table	1
33	2018_02_23_072831_surat_keputusan	1
34	2018_02_26_153115_create_media_table	1
35	2018_02_27_101123_create_izin_proses_status_tx_table	1
36	2018_03_01_030850_create_izin_role_table	1
37	2018_03_07_033035_create_izin_syarat_table	1
38	2018_03_11_154832_create_pengajuan_izin_syarat_table	1
39	2022_04_02_193005_create_translations_table	1
\.


--
-- Data for Name: modules; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY modules (id, code, name, created_by, updated_by, created_at, updated_at) FROM stdin;
5a7923cf79df4e796b	user	Pengguna	SYSTEM	SYSTEM	2018-02-06 03:41:03	2018-02-06 03:41:20
5a7923e7c4a7d77a52	role	Role	SYSTEM	\N	2018-02-06 03:41:27	2018-02-06 03:41:27
5a7923eda09325a65b	module	Module	SYSTEM	\N	2018-02-06 03:41:33	2018-02-06 03:41:33
5a7923f22b7da6cd45	action	Action	SYSTEM	\N	2018-02-06 03:41:38	2018-02-06 03:41:38
5a7923f7235f4c4114	wilayah	Wilayah	SYSTEM	\N	2018-02-06 03:41:43	2018-02-06 03:41:43
5a7c5cc5758164715b	bidang	Bidang	barkah.hadi	\N	2018-02-08 14:20:53	2018-02-08 14:20:53
5a7c644eedd801f160	jenisizin	Jenis Izin	barkah.hadi	barkah.hadi	2018-02-08 14:53:02	2018-02-08 15:02:24
5a7c703daae3895ea6	jenisperusahaan	Jenis Perusahaan	barkah.hadi	\N	2018-02-08 15:43:57	2018-02-08 15:43:57
5a7c704b0a2ae4c597	jeniskoperasi	Jenis Koperasi	barkah.hadi	\N	2018-02-08 15:44:11	2018-02-08 15:44:11
5a82eba089ccfcd85c	izincategory	Kategori Izin	\N	\N	2018-02-13 13:44:00	2018-02-13 13:44:00
5a83132a5fd3ba4b04	proses	Proses	\N	\N	2018-02-13 16:32:42	2018-02-13 16:32:42
5a831a7d2f180424d1	status	Status	\N	\N	2018-02-13 17:03:57	2018-02-13 17:03:57
5a831f46ce451faaae	izin	Izin	\N	\N	2018-02-13 17:24:22	2018-02-13 17:27:23
5a8db0d90ec66cf8e8	output	Output	5a811f8be5ce60cf10	\N	2018-02-21 17:48:09	2018-02-21 17:48:09
5a8e03b400082b69d6	jenisberkas	Jenis Berkas	5a811f8be5ce60cf10	\N	2018-02-21 23:41:40	2018-02-21 23:41:40
\.


--
-- Data for Name: output; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY output (id, name, route_page, route_conf, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: pengajuan_izin; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY pengajuan_izin (id, "user", izin, status, tipe_perizinan, lat, long, formatted_address, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: pengajuan_izin_syarat; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY pengajuan_izin_syarat (id, pengajuan_izin, izin_syarat, status, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: permission_role; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY permission_role (id, permission_id, role_id, created_by, updated_by, created_at, updated_at) FROM stdin;
5aa48d887f403d1fd1	5aa48d8879a59d1304	5a78098a2ac73f180a	\N	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
5aa48d8880163b15ef	5aa48d887a4cf683d8	5a78098a2ac73f180a	\N	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
5aa48d8880919216dd	5aa48d887aa09a0fca	5a78098a2ac73f180a	\N	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
5aa48d88810f5fbdea	5aa48d887aeb9c382b	5a78098a2ac73f180a	\N	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
5aa48d8881d9670f4f	5aa48d887b3a4de384	5a78098a2ac73f180a	\N	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
5aa48d888287a61420	5aa48d887b88baf6db	5a78098a2ac73f180a	\N	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
5aa48d88831d2b3323	5aa48d887bda485fa2	5a78098a2ac73f180a	\N	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
5aa48d888395a2447f	5aa48d887c20d962b9	5a78098a2ac73f180a	\N	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
5aa48d8883fdbe1dd6	5aa48d887c6ce3eb8d	5a78098a2ac73f180a	\N	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
5aa48d888463211d23	5aa48d887ca89d81c5	5a78098a2ac73f180a	\N	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
5aa48d8884ca8663de	5aa48d887ce81f4387	5a78098a2ac73f180a	\N	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
5aa48d8885307f2e82	5aa48d887d3dff655f	5a78098a2ac73f180a	\N	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
5aa48d8885f21fac33	5aa48d887d92df0a7c	5a78098a2ac73f180a	\N	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
5aa48d88869baaa27f	5aa48d887df4bfa10f	5a78098a2ac73f180a	\N	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
5aa48d8887233811c5	5aa48d887e599895f1	5a78098a2ac73f180a	\N	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
\.


--
-- Data for Name: permission_user; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY permission_user (id, permission_id, user_id, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: permissions; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY permissions (id, inherit_id, name, slug, description, created_by, updated_by, created_at, updated_at) FROM stdin;
5a794e569c11f87ddc	\N	editor.user	{"update":true}	\N	SYSTEM	\N	2018-02-06 06:42:30	2018-02-06 06:42:30
5a794e569caa8b7b9d	\N	editor.action	{"delete":true,"update":true}	\N	SYSTEM	\N	2018-02-06 06:42:30	2018-02-06 06:42:30
5a7960e6262e4b1c54	\N	moderator.module	{"create":true}	\N	SYSTEM	\N	2018-02-06 08:01:42	2018-02-06 08:01:42
5a7960e62d89147f54	\N	moderator.user	{"create":true,"update":true,"view":true}	\N	SYSTEM	\N	2018-02-06 08:01:42	2018-02-06 08:01:42
5aa48d8879a59d1304	\N	admin.user	{"create":true,"update":true,"delete":true,"view":true}	\N	5a811f8be5ce60cf10	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
5aa48d887a4cf683d8	\N	admin.role	{"create":true,"update":true,"delete":true,"view":true}	\N	5a811f8be5ce60cf10	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
5aa48d887aa09a0fca	\N	admin.module	{"create":true,"update":true,"delete":true,"view":true}	\N	5a811f8be5ce60cf10	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
5aa48d887aeb9c382b	\N	admin.action	{"create":true,"update":true,"delete":true,"view":true}	\N	5a811f8be5ce60cf10	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
5aa48d887b3a4de384	\N	admin.wilayah	{"create":true,"update":true,"delete":true,"view":true}	\N	5a811f8be5ce60cf10	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
5aa48d887b88baf6db	\N	admin.bidang	{"create":true,"update":true,"delete":true,"view":true}	\N	5a811f8be5ce60cf10	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
5aa48d887bda485fa2	\N	admin.jenisizin	{"create":true,"update":true,"delete":true,"view":true}	\N	5a811f8be5ce60cf10	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
5aa48d887c20d962b9	\N	admin.jenisperusahaan	{"create":true,"update":true,"delete":true,"view":true}	\N	5a811f8be5ce60cf10	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
5aa48d887c6ce3eb8d	\N	admin.jeniskoperasi	{"create":true,"update":true,"delete":true,"view":true}	\N	5a811f8be5ce60cf10	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
5aa48d887ca89d81c5	\N	admin.izincategory	{"create":true,"update":true,"delete":true,"view":true}	\N	5a811f8be5ce60cf10	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
5aa48d887ce81f4387	\N	admin.proses	{"create":true,"update":true,"delete":true,"view":true}	\N	5a811f8be5ce60cf10	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
5aa48d887d3dff655f	\N	admin.status	{"create":true,"update":true,"delete":true,"view":true}	\N	5a811f8be5ce60cf10	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
5aa48d887d92df0a7c	\N	admin.izin	{"create":true,"update":true,"delete":true,"view":true}	\N	5a811f8be5ce60cf10	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
5aa48d887df4bfa10f	\N	admin.output	{"create":true,"update":true,"delete":true,"view":true}	\N	5a811f8be5ce60cf10	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
5aa48d887e599895f1	\N	admin.jenisberkas	{"create":true,"update":true,"view":true,"delete":true}	\N	5a811f8be5ce60cf10	\N	2018-03-11 01:59:36	2018-03-11 01:59:36
\.


--
-- Data for Name: proses; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY proses (id, name, route, conf, created_by, updated_by, created_at, updated_at) FROM stdin;
5a9bc3c63b9cb72916	Pemeriksaan Berkas	process.verifikasi	\N	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-04 10:00:38	2018-03-04 10:01:30
5a9bc7516f39daa876	Persetujuan	process.verifikasi	\N	5a811f8be5ce60cf10	\N	2018-03-04 10:15:45	2018-03-04 10:15:45
5a9bc9039a6c79e430	Cetak Berkas	process.verifikasi	\N	5a811f8be5ce60cf10	\N	2018-03-04 10:22:59	2018-03-04 10:22:59
5a9bc9120b219302da	Pengambilan Berkas	process.verifikasi	\N	5a811f8be5ce60cf10	\N	2018-03-04 10:23:14	2018-03-04 10:23:14
5a8315e94a8037a044	Pengajuan	permohonan.formulir	\N	\N	5a811f8be5ce60cf10	2018-02-13 16:44:25	2018-02-21 17:52:22
\.


--
-- Data for Name: provinces; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY provinces (id, name) FROM stdin;
\.


--
-- Data for Name: role_user; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY role_user (id, role_id, user_id, created_by, updated_by, created_at, updated_at) FROM stdin;
5a9b624d4d01ec93fc	5a78098a2ac73f180a	5a811f8be5ce60cf10	\N	\N	2018-03-04 03:04:45	2018-03-04 03:04:45
5a9b630d58d7d583e9	5a78098a2ac73f180a	5a8f61e3b5600a622e	\N	\N	2018-03-04 03:07:57	2018-03-04 03:07:57
5aa5de4deb4d0434e9	5a83eb6752539de9db	5aa5de08b1223bbe93	\N	\N	2018-03-12 01:56:29	2018-03-12 01:56:29
5aa5dfd17dcc111097	5a9bc76fb4c8c11903	5aa5df98bb2d52371e	\N	\N	2018-03-12 02:02:57	2018-03-12 02:02:57
5aa5f62cc5eca55320	5a9bc77d8f108c2e75	5aa5f611da174f4482	\N	\N	2018-03-12 03:38:20	2018-03-12 03:38:20
\.


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY roles (id, name, slug, level, description, created_by, updated_by, created_at, updated_at) FROM stdin;
5a83eb6752539de9db	Petugas	petugas	backoffice	\N	\N	\N	2018-02-05 07:36:43	2018-02-05 07:36:43
5a9bc77d8f108c2e75	Kepala Bidang	kabid	backoffice	\N	\N	\N	2018-02-05 07:36:45	2018-02-05 07:36:45
5a9bc76fb4c8c11903	Kepala Seksi	kasek	backoffice	\N	\N	\N	2018-02-05 07:36:44	2018-02-05 07:36:44
5a9bc7886773878075	Kepala Dinas	kadis	backoffice	\N	\N	\N	2018-02-05 07:36:46	2018-02-05 07:36:46
5a78098a2ac73f180a	Admin	admin	admin	\N	SYSTEM	5a811f8be5ce60cf10	2018-02-05 07:36:42	2018-03-07 06:26:21
5aa416e179d47cf9d9	Pemohon	pemohon	pemohon	\N	5a811f8be5ce60cf10	\N	2018-03-10 17:33:21	2018-03-10 17:33:21
\.


--
-- Data for Name: status; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY status (id, slug, name, label, created_by, updated_by, created_at, updated_at) FROM stdin;
5a831abf25cf1f572b	draft	Draft	Draft	\N	\N	2018-02-13 14:05:03	2018-02-13 14:05:03
5a831abf25cf1f572c	diajukan	Ajukan	Diajukan	\N	\N	2018-02-13 17:05:03	2018-02-13 17:05:03
5a831ae8ec23eca129	disetujui	Setujui	Disetujui Oleh Petugas	\N	5a811f8be5ce60cf10	2018-02-13 17:05:44	2018-03-04 10:06:35
5a9bc54dd03a53f66a	disetujui.kasek	Setujui	Disetujui Kepala Seksi	5a811f8be5ce60cf10	\N	2018-03-04 10:07:09	2018-03-04 10:07:09
5a9bc5677df3317495	disetujui.kabid	Setujui	Disetujui Kepala Bidang	5a811f8be5ce60cf10	\N	2018-03-04 10:07:35	2018-03-04 10:07:35
5a9bc5f7c0d583ff08	ditolak	Ditolak	Ditolak	5a811f8be5ce60cf10	\N	2018-03-04 10:09:59	2018-03-04 10:09:59
5a9bc5a44bd97db77a	disetujui.kadis	Setujui	Disetujui Kepala DPMPTSP	5a811f8be5ce60cf10	5a811f8be5ce60cf10	2018-03-04 10:08:36	2018-03-04 10:11:15
5a9bc6a11f7eeb495c	dicetak	Cetak	Dicetak	5a811f8be5ce60cf10	\N	2018-03-04 10:12:49	2018-03-04 10:12:49
5a9bc6c6bbf27d677f	diambil	Pengambilan	Diambil	5a811f8be5ce60cf10	\N	2018-03-04 10:13:26	2018-03-04 10:13:26
5a9bc6dc13f689a296	selesai	Selesai	Selesai	5a811f8be5ce60cf10	\N	2018-03-04 10:13:48	2018-03-04 10:13:48
\.


--
-- Data for Name: surat_keputusan; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY surat_keputusan (id, mapping_output, template, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: user_berkas; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY user_berkas (id, users, jenis_berkas, file, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: user_profiles; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY user_profiles (id, user_id, nik, no_kk, nama_ktp, alamat, no_rt, no_rw, kelurahan, kecamatan, kota, provinsi, tempat_lahir, tanggal_lahir, jenis_kelamin, created_by, updated_by, created_at, updated_at) FROM stdin;
5a9a293079727f1286	5a9a28c6d4e8c12ad0	3174100912640001	3174100701091542	SUJONO	JL. H RADIN NO. 35	1	3	1003	10	74	31	ACEH SELATAN	09/12/1964	1	5a9a28c6d4e8c12ad0	\N	2018-03-03 04:48:48	2018-03-03 04:48:48
5aa5dbf83f5f2cb77c	5a9d1f6d700c330a55	3173075106920008	3173071401096477	MUTHIA INDAH KAMILA I	JL. KEMANGGISAN ILIR NO. 56	3	8	1005	7	73	31	JAKARTA	11/06/1992	2	5a9d1f6d700c330a55	\N	2018-03-12 01:46:32	2018-03-12 01:46:32
5aa5f7aa3088b90629	5aa5f7309a9da5aead	3174070707940001	3174071501092621	FAHRY RAHMANDA	JL SAWO BAWAH	7	2	1010	7	74	31	JAKARTA	07/07/1994	1	5aa5f7309a9da5aead	\N	2018-03-12 03:44:42	2018-03-12 03:44:42
\.


--
-- Data for Name: user_providers; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY user_providers (id, user_id, provider_id, provider, image, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY users (id, email, fullname, password, remember_token, picture, wilayah, last_activity, confirmed, confirmation_code, active, created_by, updated_by, created_at, updated_at) FROM stdin;
5a8f61e3b5600a622e	barkah.hadi@yahoo.com	Barkah Hadi Nugroho	$2y$10$1pwxHfo.SZPmGP/qkXKRqubGkxvrah/is2x3EkguqaDhclXtP4PKa	JKOpOYIAokihlpmvg6U38xn5mH4eWLzjLVqazFlOeIL7dCc0req23ZfRv5L3	\N	\N	\N	t	\N	t	SYSTEM	\N	2018-02-23 00:35:47	2018-03-04 03:07:57
5a9a28c6d4e8c12ad0	sujono@gmail.com	Sujono	$2y$10$yiS4uJzm62P337W4JGY9EeBQFzH5b4SCSA0v7Htk/YD8NDMkLtmHq	Ghg2FTiNfB4vcyBFvaye2IOrtSNWO9cxzVi0jbS8AE8TduRGYjWkExoEMogc	\N	\N	\N	t	\N	t	SYSTEM	\N	2018-03-03 04:47:02	2018-03-03 04:47:06
5a811f8be5ce60cf10	admin@jakarta.go.id	Administrator	$2y$10$3co.2Z0/LCyWWAA/modt1.ihkPQ3nc1E7h5tIbatKjOHEXkaS6.6C	YW62Fm7S85s7TTfS339w0oXtSIfxDXQz9AlZxbsLZZt2B7oKWZFYORd0puTo	\N	\N	\N	t	\N	t	barkah.hadi	\N	2018-02-12 05:00:59	2018-03-04 03:04:45
5a9d1f6d700c330a55	pancasilany@gmail.com	Pancasilany	$2y$10$3co.2Z0/LCyWWAA/modt1.ihkPQ3nc1E7h5tIbatKjOHEXkaS6.6C	odnLA07NNbI3Vzi1lP4s6K2syqIShWQsAkFC6lUPxqQ0WEaPs4qSzaKByaK5	\N	\N	\N	t	\N	t	SYSTEM	\N	2018-03-05 05:00:59	2018-03-05 03:04:45
5aa5f611da174f4482	kabid@jakarta.go.id	kepala bidang	$2y$10$PK1bbKp1EIOFsP7iUH0wAui3lHQtcIvGvkZgPJU4GqOAwtOSUdb8K	wPpOzIrgl2IZ93sU4oDxobZK5KlsxPleDmclMIFN69eOjXx21VEp56W1Y3e5	\N	\N	\N	t	\N	t	SYSTEM	\N	2018-03-12 03:37:53	2018-03-12 03:38:20
5aa5f7309a9da5aead	fahryrahmanda@gmail.com	Fahry Rahmanda	$2y$10$PgF.QYlhe8xKmHxD3P18Eu4pzvW1qXeIIKHiMDhVCtj9CVb2NpDHa	QeMQwRwd2oEFOA1V8B17zYBYwVWCFHP7ITBAkDeMYRfE8XS3I4EMWCQp6G6R	\N	\N	\N	t	\N	t	SYSTEM	\N	2018-03-12 03:42:40	2018-03-12 03:42:50
5aa5de08b1223bbe93	petugas@jakarta.go.id	petugas	$2y$10$9bIhypC92/GyPoZrVHBMI.CeLcgcWPHsRP2hjihcqRy1gv2GljVeK	SFcTSx1mY5LUiZJ5lvA8qeeaxR920NUMpbT3j2uTSVE8fBcWzEpGtHAfI5b1	\N	\N	\N	t	\N	t	SYSTEM	\N	2018-03-12 01:55:20	2018-03-12 01:56:29
5aa5df98bb2d52371e	kasi@jakarta.go.id	Kepala Seksi	$2y$10$v4rLyo1MvAOBOFKSjMq3OO7YkA0cYjGhn3pKR4hSirGt1m49iJ4wy	u5Ic1bGaLNCK3DTqZS1dwUw306qXvWjA8ilKmDWJOHiazyfZ1sVMFhizJXuK	\N	\N	\N	t	\N	t	SYSTEM	\N	2018-03-12 02:02:00	2018-03-12 02:02:57
\.


--
-- Data for Name: villages; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY villages (id, district_id, name) FROM stdin;
\.


--
-- Data for Name: wilayah; Type: TABLE DATA; Schema: public; Owner: Gabriella
--

COPY wilayah (id, code, name, lat, long, description, active, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Name: ltm_translations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: Gabriella
--

SELECT pg_catalog.setval('ltm_translations_id_seq', 1, false);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: Gabriella
--

SELECT pg_catalog.setval('migrations_id_seq', 77, true);


--
-- Name: user_providers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: Gabriella
--

SELECT pg_catalog.setval('user_providers_id_seq', 1, false);


--
-- Name: actions actions_code_unique; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY actions
    ADD CONSTRAINT actions_code_unique UNIQUE (code);


--
-- Name: actions actions_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY actions
    ADD CONSTRAINT actions_pkey PRIMARY KEY (id);


--
-- Name: bidang bidang_code_unique; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY bidang
    ADD CONSTRAINT bidang_code_unique UNIQUE (code);


--
-- Name: bidang bidang_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY bidang
    ADD CONSTRAINT bidang_pkey PRIMARY KEY (id);


--
-- Name: cities cities_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY cities
    ADD CONSTRAINT cities_pkey PRIMARY KEY (id);


--
-- Name: data_perijinan data_perijinan_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY data_perijinan
    ADD CONSTRAINT data_perijinan_pkey PRIMARY KEY (id);


--
-- Name: districts districts_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY districts
    ADD CONSTRAINT districts_pkey PRIMARY KEY (id);


--
-- Name: izin_berkas izin_berkas_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY izin_berkas
    ADD CONSTRAINT izin_berkas_pkey PRIMARY KEY (id);


--
-- Name: izin_category izin_category_code_unique; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY izin_category
    ADD CONSTRAINT izin_category_code_unique UNIQUE (code);


--
-- Name: izin_category izin_category_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY izin_category
    ADD CONSTRAINT izin_category_pkey PRIMARY KEY (id);


--
-- Name: izin izin_code_unique; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY izin
    ADD CONSTRAINT izin_code_unique UNIQUE (code);


--
-- Name: izin izin_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY izin
    ADD CONSTRAINT izin_pkey PRIMARY KEY (id);


--
-- Name: izin_proses izin_proses_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY izin_proses
    ADD CONSTRAINT izin_proses_pkey PRIMARY KEY (id);


--
-- Name: izin_proses_status izin_proses_status_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY izin_proses_status
    ADD CONSTRAINT izin_proses_status_pkey PRIMARY KEY (id);


--
-- Name: izin_proses_status_tx izin_proses_status_tx_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY izin_proses_status_tx
    ADD CONSTRAINT izin_proses_status_tx_pkey PRIMARY KEY (id);


--
-- Name: izin_role izin_role_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY izin_role
    ADD CONSTRAINT izin_role_pkey PRIMARY KEY (id);


--
-- Name: izin_syarat izin_syarat_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY izin_syarat
    ADD CONSTRAINT izin_syarat_pkey PRIMARY KEY (id);


--
-- Name: jenis_berkas jenis_berkas_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY jenis_berkas
    ADD CONSTRAINT jenis_berkas_pkey PRIMARY KEY (id);


--
-- Name: jenis_izin jenis_izin_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY jenis_izin
    ADD CONSTRAINT jenis_izin_pkey PRIMARY KEY (id);


--
-- Name: jenis_koperasi jenis_koperasi_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY jenis_koperasi
    ADD CONSTRAINT jenis_koperasi_pkey PRIMARY KEY (id);


--
-- Name: jenis_perusahaan jenis_perusahaan_code_unique; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY jenis_perusahaan
    ADD CONSTRAINT jenis_perusahaan_code_unique UNIQUE (code);


--
-- Name: jenis_perusahaan jenis_perusahaan_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY jenis_perusahaan
    ADD CONSTRAINT jenis_perusahaan_pkey PRIMARY KEY (id);


--
-- Name: ltm_translations ltm_translations_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY ltm_translations
    ADD CONSTRAINT ltm_translations_pkey PRIMARY KEY (id);


--
-- Name: mapping_output mapping_output_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY mapping_output
    ADD CONSTRAINT mapping_output_pkey PRIMARY KEY (id);


--
-- Name: media media_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY media
    ADD CONSTRAINT media_pkey PRIMARY KEY (id);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: modules modules_code_unique; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY modules
    ADD CONSTRAINT modules_code_unique UNIQUE (code);


--
-- Name: modules modules_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY modules
    ADD CONSTRAINT modules_pkey PRIMARY KEY (id);


--
-- Name: output output_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY output
    ADD CONSTRAINT output_pkey PRIMARY KEY (id);


--
-- Name: pengajuan_izin pengajuan_izin_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY pengajuan_izin
    ADD CONSTRAINT pengajuan_izin_pkey PRIMARY KEY (id);


--
-- Name: pengajuan_izin_syarat pengajuan_izin_syarat_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY pengajuan_izin_syarat
    ADD CONSTRAINT pengajuan_izin_syarat_pkey PRIMARY KEY (id);


--
-- Name: permission_role permission_role_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY permission_role
    ADD CONSTRAINT permission_role_pkey PRIMARY KEY (id);


--
-- Name: permission_user permission_user_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY permission_user
    ADD CONSTRAINT permission_user_pkey PRIMARY KEY (id);


--
-- Name: permissions permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY permissions
    ADD CONSTRAINT permissions_pkey PRIMARY KEY (id);


--
-- Name: proses proses_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY proses
    ADD CONSTRAINT proses_pkey PRIMARY KEY (id);


--
-- Name: provinces provinces_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY provinces
    ADD CONSTRAINT provinces_pkey PRIMARY KEY (id);


--
-- Name: role_user role_user_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY role_user
    ADD CONSTRAINT role_user_pkey PRIMARY KEY (id);


--
-- Name: roles roles_name_unique; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_name_unique UNIQUE (name);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: roles roles_slug_unique; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_slug_unique UNIQUE (slug);


--
-- Name: status status_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY status
    ADD CONSTRAINT status_pkey PRIMARY KEY (id);


--
-- Name: status status_slug_unique; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY status
    ADD CONSTRAINT status_slug_unique UNIQUE (slug);


--
-- Name: surat_keputusan surat_keputusan_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY surat_keputusan
    ADD CONSTRAINT surat_keputusan_pkey PRIMARY KEY (id);


--
-- Name: user_berkas user_berkas_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY user_berkas
    ADD CONSTRAINT user_berkas_pkey PRIMARY KEY (id);


--
-- Name: user_profiles user_profiles_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY user_profiles
    ADD CONSTRAINT user_profiles_pkey PRIMARY KEY (id);


--
-- Name: user_providers user_providers_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY user_providers
    ADD CONSTRAINT user_providers_pkey PRIMARY KEY (id);


--
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: villages villages_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY villages
    ADD CONSTRAINT villages_pkey PRIMARY KEY (id);


--
-- Name: wilayah wilayah_code_unique; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY wilayah
    ADD CONSTRAINT wilayah_code_unique UNIQUE (code);


--
-- Name: wilayah wilayah_pkey; Type: CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY wilayah
    ADD CONSTRAINT wilayah_pkey PRIMARY KEY (id);


--
-- Name: permissions_name_index; Type: INDEX; Schema: public; Owner: Gabriella
--

CREATE INDEX permissions_name_index ON permissions USING btree (name);


--
-- Name: permissions_slug_index; Type: INDEX; Schema: public; Owner: Gabriella
--

CREATE INDEX permissions_slug_index ON permissions USING btree (slug);


--
-- Name: cities cities_province_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY cities
    ADD CONSTRAINT cities_province_id_foreign FOREIGN KEY (province_id) REFERENCES provinces(id);


--
-- Name: districts districts_city_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY districts
    ADD CONSTRAINT districts_city_id_foreign FOREIGN KEY (city_id) REFERENCES cities(id);


--
-- Name: izin_berkas izin_berkas_izin_proses_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY izin_berkas
    ADD CONSTRAINT izin_berkas_izin_proses_foreign FOREIGN KEY (izin_proses) REFERENCES izin_proses(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: izin_berkas izin_berkas_jenis_berkas_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY izin_berkas
    ADD CONSTRAINT izin_berkas_jenis_berkas_foreign FOREIGN KEY (jenis_berkas) REFERENCES jenis_berkas(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: izin_category izin_category_bidang_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY izin_category
    ADD CONSTRAINT izin_category_bidang_foreign FOREIGN KEY (bidang) REFERENCES bidang(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: izin izin_izin_category_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY izin
    ADD CONSTRAINT izin_izin_category_foreign FOREIGN KEY (izin_category) REFERENCES izin_category(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: izin_proses izin_proses_proses_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY izin_proses
    ADD CONSTRAINT izin_proses_proses_foreign FOREIGN KEY (proses) REFERENCES proses(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: izin_proses_status izin_proses_status_izin_proses_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY izin_proses_status
    ADD CONSTRAINT izin_proses_status_izin_proses_foreign FOREIGN KEY (izin_proses) REFERENCES izin_proses(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: izin_proses_status izin_proses_status_status_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY izin_proses_status
    ADD CONSTRAINT izin_proses_status_status_foreign FOREIGN KEY (status) REFERENCES status(slug) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: izin_proses_status_tx izin_proses_status_tx_izin_proses_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY izin_proses_status_tx
    ADD CONSTRAINT izin_proses_status_tx_izin_proses_foreign FOREIGN KEY (izin_proses) REFERENCES izin_proses(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: izin_proses_status_tx izin_proses_status_tx_pengajuan_izin_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY izin_proses_status_tx
    ADD CONSTRAINT izin_proses_status_tx_pengajuan_izin_foreign FOREIGN KEY (pengajuan_izin) REFERENCES pengajuan_izin(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: izin_proses_status_tx izin_proses_status_tx_status_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY izin_proses_status_tx
    ADD CONSTRAINT izin_proses_status_tx_status_foreign FOREIGN KEY (status) REFERENCES status(slug) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: izin_role izin_role_izin_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY izin_role
    ADD CONSTRAINT izin_role_izin_foreign FOREIGN KEY (izin) REFERENCES izin(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: izin_role izin_role_izin_proses_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY izin_role
    ADD CONSTRAINT izin_role_izin_proses_foreign FOREIGN KEY (izin_proses) REFERENCES izin_proses(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: izin_role izin_role_status_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY izin_role
    ADD CONSTRAINT izin_role_status_foreign FOREIGN KEY (status) REFERENCES status(slug) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: izin_syarat izin_syarat_izin_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY izin_syarat
    ADD CONSTRAINT izin_syarat_izin_foreign FOREIGN KEY (izin) REFERENCES izin(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: mapping_output mapping_output_izin_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY mapping_output
    ADD CONSTRAINT mapping_output_izin_foreign FOREIGN KEY (izin) REFERENCES izin(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: mapping_output mapping_output_output_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY mapping_output
    ADD CONSTRAINT mapping_output_output_foreign FOREIGN KEY (output) REFERENCES output(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengajuan_izin pengajuan_izin_izin_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY pengajuan_izin
    ADD CONSTRAINT pengajuan_izin_izin_foreign FOREIGN KEY (izin) REFERENCES izin(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengajuan_izin pengajuan_izin_status_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY pengajuan_izin
    ADD CONSTRAINT pengajuan_izin_status_foreign FOREIGN KEY (status) REFERENCES status(slug) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengajuan_izin_syarat pengajuan_izin_syarat_pengajuan_izin_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY pengajuan_izin_syarat
    ADD CONSTRAINT pengajuan_izin_syarat_pengajuan_izin_foreign FOREIGN KEY (pengajuan_izin) REFERENCES pengajuan_izin(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengajuan_izin pengajuan_izin_user_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY pengajuan_izin
    ADD CONSTRAINT pengajuan_izin_user_foreign FOREIGN KEY ("user") REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: permission_role permission_role_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY permission_role
    ADD CONSTRAINT permission_role_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES permissions(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: permission_role permission_role_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY permission_role
    ADD CONSTRAINT permission_role_role_id_foreign FOREIGN KEY (role_id) REFERENCES roles(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: permission_user permission_user_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY permission_user
    ADD CONSTRAINT permission_user_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES permissions(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: permission_user permission_user_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY permission_user
    ADD CONSTRAINT permission_user_user_id_foreign FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: permissions permissions_inherit_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY permissions
    ADD CONSTRAINT permissions_inherit_id_foreign FOREIGN KEY (inherit_id) REFERENCES permissions(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: role_user role_user_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY role_user
    ADD CONSTRAINT role_user_role_id_foreign FOREIGN KEY (role_id) REFERENCES roles(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: role_user role_user_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY role_user
    ADD CONSTRAINT role_user_user_id_foreign FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: surat_keputusan surat_keputusan_mapping_output_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY surat_keputusan
    ADD CONSTRAINT surat_keputusan_mapping_output_foreign FOREIGN KEY (mapping_output) REFERENCES mapping_output(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_berkas user_berkas_jenis_berkas_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY user_berkas
    ADD CONSTRAINT user_berkas_jenis_berkas_foreign FOREIGN KEY (jenis_berkas) REFERENCES jenis_berkas(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_berkas user_berkas_users_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY user_berkas
    ADD CONSTRAINT user_berkas_users_foreign FOREIGN KEY (users) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_profiles user_profiles_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY user_profiles
    ADD CONSTRAINT user_profiles_user_id_foreign FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: villages villages_district_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: Gabriella
--

ALTER TABLE ONLY villages
    ADD CONSTRAINT villages_district_id_foreign FOREIGN KEY (district_id) REFERENCES districts(id);


--
-- PostgreSQL database dump complete
--

