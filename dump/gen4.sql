--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.3
-- Dumped by pg_dump version 9.6.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- Data for Name: actions; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY actions (id, code, name, created_by, updated_by, created_at, updated_at) FROM stdin;
5a781dc78c21ade0e6	create	Create	SYSTEM	\N	2018-02-05 09:03:03	2018-02-05 09:03:03
5a781dd3b4e21351e6	update	Update	SYSTEM	\N	2018-02-05 09:03:15	2018-02-05 09:03:15
5a781dde76bf72e041	delete	Delete	SYSTEM	\N	2018-02-05 09:03:26	2018-02-05 09:03:26
5a781de9400491b2fc	view	View	SYSTEM	\N	2018-02-05 09:03:37	2018-02-05 09:03:37
\.


--
-- Data for Name: bidang; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY bidang (id, code, name, active, created_by, updated_by, created_at, updated_at) FROM stdin;
5a7c5d674f6791610e	pendidikan	Pendidikan	t	barkah.hadi	\N	2018-02-08 14:23:35	2018-02-08 14:23:35
5a7c5d8713d14ff611	kesehatan	Kesehatan	t	barkah.hadi	\N	2018-02-08 14:24:07	2018-02-08 14:24:07
\.


--
-- Data for Name: provinces; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY provinces (id, name) FROM stdin;
\.


--
-- Data for Name: cities; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY cities (id, province_id, name) FROM stdin;
\.


--
-- Data for Name: data_perijinan; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY data_perijinan (id, tipe_perijinan, status_perijinan, bidang_perijinan, tanggal_mengajukan, due_date, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: districts; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY districts (id, city_id, name) FROM stdin;
\.


--
-- Data for Name: groups; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY groups (id, name, access, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: izin_category; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY izin_category (id, bidang, name, created_by, updated_by, created_at, updated_at) FROM stdin;
5a832230175f0d905c	5a7c5d8713d14ff611	Izin Praktek	\N	\N	2018-02-13 17:36:48	2018-02-13 17:36:48
\.


--
-- Data for Name: izin; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY izin (id, izin_category, name, created_by, updated_by, created_at, updated_at) FROM stdin;
5a8322404b4d14666d	5a832230175f0d905c	Praktek Dokter Gigi	\N	\N	2018-02-13 17:37:04	2018-02-13 17:37:04
\.


--
-- Data for Name: modules; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY modules (id, code, name, created_by, updated_by, created_at, updated_at) FROM stdin;
5a7923cf79df4e796b	user	Pengguna	SYSTEM	SYSTEM	2018-02-06 03:41:03	2018-02-06 03:41:20
5a7923e7c4a7d77a52	role	Role	SYSTEM	\N	2018-02-06 03:41:27	2018-02-06 03:41:27
5a7923eda09325a65b	module	Module	SYSTEM	\N	2018-02-06 03:41:33	2018-02-06 03:41:33
5a7923f22b7da6cd45	action	Action	SYSTEM	\N	2018-02-06 03:41:38	2018-02-06 03:41:38
5a7923f7235f4c4114	wilayah	Wilayah	SYSTEM	\N	2018-02-06 03:41:43	2018-02-06 03:41:43
5a7c5cc5758164715b	bidang	Bidang	barkah.hadi	\N	2018-02-08 14:20:53	2018-02-08 14:20:53
5a7c644eedd801f160	jenisizin	Jenis Izin	barkah.hadi	barkah.hadi	2018-02-08 14:53:02	2018-02-08 15:02:24
5a7c703daae3895ea6	jenisperusahaan	Jenis Perusahaan	barkah.hadi	\N	2018-02-08 15:43:57	2018-02-08 15:43:57
5a7c704b0a2ae4c597	jeniskoperasi	Jenis Koperasi	barkah.hadi	\N	2018-02-08 15:44:11	2018-02-08 15:44:11
5a82eba089ccfcd85c	izincategory	Kategori Izin	\N	\N	2018-02-13 13:44:00	2018-02-13 13:44:00
5a83132a5fd3ba4b04	proses	Proses	\N	\N	2018-02-13 16:32:42	2018-02-13 16:32:42
5a831a7d2f180424d1	status	Status	\N	\N	2018-02-13 17:03:57	2018-02-13 17:03:57
5a831f46ce451faaae	izin	Izin	\N	\N	2018-02-13 17:24:22	2018-02-13 17:27:23
\.


--
-- Data for Name: proses; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY proses (id, module, name, conf, created_by, updated_by, created_at, updated_at) FROM stdin;
5a8315e94a8037a044	wilayah	Pengajuan	\N	\N	\N	2018-02-13 16:44:25	2018-02-13 16:44:25
\.


--
-- Data for Name: izin_proses; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY izin_proses (id, izin, proses, name, roles, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: jenis_izin; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY jenis_izin (id, name, active, updated_by, created_by, created_at, updated_at) FROM stdin;
5a7c66fa36ed14e60c	Ketataruangan	t	barkah.hadi	barkah.hadi	2018-02-08 15:04:26	2018-02-08 15:05:22
\.


--
-- Data for Name: jenis_koperasi; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY jenis_koperasi (id, name, active, updated_by, created_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: jenis_perusahaan; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY jenis_perusahaan (id, code, name, active, updated_by, created_by, created_at, updated_at) FROM stdin;
5a7c729da5519972cb	CV	Commanditaire Vennootschap	t	\N	barkah.hadi	2018-02-08 15:54:05	2018-02-08 15:54:05
5a7c72ac349740e283	FA	Firma	t	\N	barkah.hadi	2018-02-08 15:54:20	2018-02-08 15:54:20
5a7c72bba7e199cb93	PT	Perseroan Terbatas	t	\N	barkah.hadi	2018-02-08 15:54:35	2018-02-08 15:54:35
\.


--
-- Data for Name: ltm_translations; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY ltm_translations (id, status, locale, "group", key, value, created_at, updated_at) FROM stdin;
\.


--
-- Name: ltm_translations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: barkah
--

SELECT pg_catalog.setval('ltm_translations_id_seq', 1, false);


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY migrations (id, migration, batch) FROM stdin;
14	2018_01_22_051402_create_users_table	1
15	2018_01_25_030317_create_data_perijinan_table	1
16	2018_01_26_081420_create_modules_table	1
17	2018_01_26_081431_create_groups_table	1
18	2018_01_26_081441_create_wilayah_table	1
19	2022_02_07_172606_create_roles_table	1
20	2022_02_07_172633_create_role_user_table	1
21	2022_02_07_172649_create_permissions_table	1
22	2022_02_07_172657_create_permission_role_table	1
23	2022_02_17_152439_create_permission_user_table	1
24	2022_04_02_193005_create_translations_table	1
25	2018_02_05_081515_create_actions_table	2
26	2018_02_03_062450_create_bidang_table	3
27	2018_02_03_050411_create_jenis_izin_table	4
32	2018_02_02_073557_create_jenis_perusahaan_table	5
33	2018_02_03_122803_create_jenis_koperasi_table	5
44	2016_08_03_072729_create_provinces_table	6
45	2016_08_03_072750_create_cities_table	6
46	2016_08_03_072804_create_districts_table	6
47	2016_08_03_072819_create_villages_table	6
48	2018_02_01_040727_create_user_providers_table	6
49	2018_02_13_104634_create_proses_table	6
50	2018_02_13_110157_create_status_table	6
51	2018_02_13_111123_create_izin_proses_table	6
52	2018_02_13_130008_create_izin_category_table	6
53	2018_02_13_130009_create_izin_table	6
\.


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: barkah
--

SELECT pg_catalog.setval('migrations_id_seq', 53, true);


--
-- Data for Name: permissions; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY permissions (id, inherit_id, name, slug, description, created_by, updated_by, created_at, updated_at) FROM stdin;
5a83aea81dbe838e0e	\N	admin.module	{"update":true,"create":true,"delete":true,"view":true}	\N	\N	\N	2018-02-14 03:36:08	2018-02-14 03:36:08
5a83aea81e72402359	\N	admin.role	{"create":true,"update":true,"delete":true,"view":true}	\N	\N	\N	2018-02-14 03:36:08	2018-02-14 03:36:08
5a83aea81eb8b878b2	\N	admin.action	{"view":true,"create":true,"update":true,"delete":true}	\N	\N	\N	2018-02-14 03:36:08	2018-02-14 03:36:08
5a83aea81efa2258e5	\N	admin.user	{"view":true,"create":true,"update":true,"delete":true}	\N	\N	\N	2018-02-14 03:36:08	2018-02-14 03:36:08
5a83aea81f48e98ddd	\N	admin.wilayah	{"view":true,"delete":true,"update":true,"create":true}	\N	\N	\N	2018-02-14 03:36:08	2018-02-14 03:36:08
5a83aea81f8bdd4df9	\N	admin.bidang	{"create":true,"update":true,"delete":true,"view":true}	\N	\N	\N	2018-02-14 03:36:08	2018-02-14 03:36:08
5a83aea81ff53079f2	\N	admin.jenisperusahaan	{"view":true,"create":true}	\N	\N	\N	2018-02-14 03:36:08	2018-02-14 03:36:08
5a83aea8203526e6b0	\N	admin.jeniskoperasi	{"create":true,"update":true,"delete":true,"view":true}	\N	\N	\N	2018-02-14 03:36:08	2018-02-14 03:36:08
5a83aea8208cd1346b	\N	admin.izincategory	{"create":true,"update":true,"delete":true,"view":true}	\N	\N	\N	2018-02-14 03:36:08	2018-02-14 03:36:08
5a83aea8211e204da8	\N	admin.proses	{"create":true,"update":true,"delete":true,"view":true}	\N	\N	\N	2018-02-14 03:36:08	2018-02-14 03:36:08
5a83aea821e3221e40	\N	admin.status	{"create":true,"update":true,"delete":true,"view":true}	\N	\N	\N	2018-02-14 03:36:08	2018-02-14 03:36:08
5a83aea82241d9535a	\N	admin.izin	{"create":true,"update":true,"delete":true,"view":true}	\N	\N	\N	2018-02-14 03:36:08	2018-02-14 03:36:08
5a83aea822c8643145	\N	admin.jenisizin	{"create":true,"update":true,"delete":true,"view":true}	\N	\N	\N	2018-02-14 03:36:08	2018-02-14 03:36:08
5a794e569c11f87ddc	\N	editor.user	{"update":true}	\N	SYSTEM	\N	2018-02-06 06:42:30	2018-02-06 06:42:30
5a794e569caa8b7b9d	\N	editor.action	{"delete":true,"update":true}	\N	SYSTEM	\N	2018-02-06 06:42:30	2018-02-06 06:42:30
5a7960e6262e4b1c54	\N	moderator.module	{"create":true}	\N	SYSTEM	\N	2018-02-06 08:01:42	2018-02-06 08:01:42
5a7960e62d89147f54	\N	moderator.user	{"create":true,"update":true,"view":true}	\N	SYSTEM	\N	2018-02-06 08:01:42	2018-02-06 08:01:42
\.


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY roles (id, name, slug, description, created_by, updated_by, created_at, updated_at) FROM stdin;
5a78098a2ac73f180a	Admin	admin	\N	SYSTEM	\N	2018-02-05 07:36:42	2018-02-05 07:36:42
\.


--
-- Data for Name: permission_role; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY permission_role (id, permission_id, role_id, created_by, updated_by, created_at, updated_at) FROM stdin;
5a83aea823bd0e01c9	5a83aea81dbe838e0e	5a78098a2ac73f180a	\N	\N	2018-02-14 03:36:08	2018-02-14 03:36:08
5a83aea82468d504ea	5a83aea81e72402359	5a78098a2ac73f180a	\N	\N	2018-02-14 03:36:08	2018-02-14 03:36:08
5a83aea8253526bc44	5a83aea81eb8b878b2	5a78098a2ac73f180a	\N	\N	2018-02-14 03:36:08	2018-02-14 03:36:08
5a83aea826446a1c99	5a83aea81efa2258e5	5a78098a2ac73f180a	\N	\N	2018-02-14 03:36:08	2018-02-14 03:36:08
5a83aea826ffeda0ff	5a83aea81f48e98ddd	5a78098a2ac73f180a	\N	\N	2018-02-14 03:36:08	2018-02-14 03:36:08
5a83aea827b5d96a85	5a83aea81f8bdd4df9	5a78098a2ac73f180a	\N	\N	2018-02-14 03:36:08	2018-02-14 03:36:08
5a83aea8283258c763	5a83aea81ff53079f2	5a78098a2ac73f180a	\N	\N	2018-02-14 03:36:08	2018-02-14 03:36:08
5a83aea828b0eed28c	5a83aea8203526e6b0	5a78098a2ac73f180a	\N	\N	2018-02-14 03:36:08	2018-02-14 03:36:08
5a83aea829b6986055	5a83aea8208cd1346b	5a78098a2ac73f180a	\N	\N	2018-02-14 03:36:08	2018-02-14 03:36:08
5a83aea82a5f92ac91	5a83aea8211e204da8	5a78098a2ac73f180a	\N	\N	2018-02-14 03:36:08	2018-02-14 03:36:08
5a83aea82b034da62f	5a83aea821e3221e40	5a78098a2ac73f180a	\N	\N	2018-02-14 03:36:08	2018-02-14 03:36:08
5a83aea82badd0a42b	5a83aea82241d9535a	5a78098a2ac73f180a	\N	\N	2018-02-14 03:36:08	2018-02-14 03:36:08
5a83aea82c1d1d068e	5a83aea822c8643145	5a78098a2ac73f180a	\N	\N	2018-02-14 03:36:08	2018-02-14 03:36:08
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY users (id, email, fullname, password, remember_token, picture, wilayah, type, last_activity, active, created_by, updated_by, created_at, updated_at) FROM stdin;
5a811f8be5ce60cf10	admin@jakarta.go.id	Administrator	$2y$10$3T0LxZroQGFWRGNy3FBfJus3PTAWrzCjXqgxx6Ik9w5w.6ebXvNf6	FRaeU7uCaDucQZeJyjdYO9809LAYd8TLhCymbf2ADUl8LOd7fMbgfqRVa9uf	\N	\N	petugas	\N	t	barkah.hadi	\N	2018-02-12 05:00:59	2018-02-13 01:36:17
\.


--
-- Data for Name: permission_user; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY permission_user (id, permission_id, user_id, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: role_user; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY role_user (id, role_id, user_id, created_by, updated_by, created_at, updated_at) FROM stdin;
5a824111af6ec77dd4	5a78098a2ac73f180a	5a811f8be5ce60cf10	\N	\N	2018-02-13 01:36:17	2018-02-13 01:36:17
\.


--
-- Data for Name: status; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY status (id, slug, name, label, created_by, updated_by, created_at, updated_at) FROM stdin;
5a831abf25cf1f572c	diajukan	Ajukan	Diajukan	\N	\N	2018-02-13 17:05:03	2018-02-13 17:05:03
5a831ae8ec23eca129	verifikasi	Verifikasi	Verifikasi	\N	\N	2018-02-13 17:05:44	2018-02-13 17:05:44
\.


--
-- Data for Name: user_providers; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY user_providers (id, user_id, provider_id, provider, image, created_at, updated_at) FROM stdin;
\.


--
-- Name: user_providers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: barkah
--

SELECT pg_catalog.setval('user_providers_id_seq', 1, false);


--
-- Data for Name: villages; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY villages (id, district_id, name) FROM stdin;
\.


--
-- Data for Name: wilayah; Type: TABLE DATA; Schema: public; Owner: barkah
--

COPY wilayah (id, code, name, lat, long, description, active, created_by, updated_by, created_at, updated_at) FROM stdin;
\.


--
-- PostgreSQL database dump complete
--

