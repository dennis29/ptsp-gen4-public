<?php

namespace Modules\Pemohon\Entities;

use Packages\Model\BaseModel;

class UserBerkas extends BaseModel
{
  protected $table = 'user_berkas';
  protected $fillable = ['id', 'name', 'description'];
}
