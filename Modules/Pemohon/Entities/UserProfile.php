<?php

namespace Modules\Pemohon\Entities;

use Packages\Model\BaseModel;

class UserProfile extends BaseModel
{
    protected $table = 'user_profiles';
    protected $fillable = ['user_id', 'nik', 'no_kk', 'nama_ktp', 'alamat', 'no_rt', 'no_rw', 'kelurahan', 'kecamatan', 'kota', 'provinsi', 'tempat_lahir', 'tanggal_lahir', 'jenis_kelamin'];
}
