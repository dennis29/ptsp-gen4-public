<?php

namespace Modules\Permit\Entities;

use Illuminate\Database\Eloquent\Model;
use Packages\Model\BaseModel;


class Permit extends BaseModel
{
    protected $table = 'ijin';
    protected $fillable = ['id', 'nama_ijin', 'bidang_perijinan_id','created_by','updated_by','created_date','updated_date'];



}
