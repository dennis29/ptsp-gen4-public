<?php

namespace Modules\Pemohon\Entities;

use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Media;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;


use Packages\Model\BaseModel;

class PengajuanIzinSyarat extends BaseModel implements HasMediaConversions
{
    use HasMediaTrait;

    protected $table = 'pengajuan_izin_syarat';
    protected $fillable = ['id', 'pengajuan_izin', 'izin_syarat', 'status'];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
              ->width(368)
              ->height(232)
              ->sharpen(10);
    }
}
