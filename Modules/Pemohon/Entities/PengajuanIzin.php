<?php

namespace Modules\Pemohon\Entities;

use Packages\Model\BaseModel;
use Modules\Perizinan\Entities\Izin;
use Modules\Perizinan\Entities\IzinProses;
use Modules\Perizinan\Entities\IzinProsesStatus;
use Auth;
use Modules\Perizinan\Entities\IzinRole;

class PengajuanIzin extends BaseModel
{
    protected $table = 'pengajuan_izin';
    protected $fillable = ['id', 'user', 'izin', 'status', 'tipe_perizinan', 'lat', 'long', 'formatted_address'];

    public static function getCurrentProcessRoute($id = null){

      $pengajuanIzin = PengajuanIzin::find($id);

      if(empty($pengajuanIzin)){
        return false;
      }

      $izin = IzinProses::select(['izin_proses.'])
      ->join('izin_proses_status', 'izin_proses.id', '=', 'izin_proses_status.izin_proses')
      ->where('izin_proses.izin', $pengajuanIzin->izin)
      ->where('izin_proses_status.status', $pengajuanIzin->status);


    }

    public function izin()
    {
        return $this->belongsTo('Modules\Perizinan\Entities\Izin');
    }

    public function status()
    {
        return $this->belongsTo('Modules\Workflow\Entities\Status');
    }

    public function user()
    {
        return $this->belongsTo('Modules\SSO\Entities\User');
    }

    public static function getDataPengajuanIzin($id = null){
        $roles = Auth::user()->getRoles();
        $roles = collect($roles)->values()->toArray();

        $query = PengajuanIzin::select(['pengajuan_izin.id','pengajuan_izin.tipe_perizinan', 'pengajuan_izin.created_at','izin_role.next_status',
            'status.label as status', 'izin.name as izin_name', 'pengajuan_izin.user as user', 'izin_role.izin_proses as izin_proses','izin_role.next_route as next_route', 'izin_role.role'])
            ->leftJoin('izin_role', function($join) use($roles){
                $join->on('izin_role.izin', '=','pengajuan_izin.izin');
                $join->on('izin_role.status', '=','pengajuan_izin.status');
            })
            ->join('izin', 'izin.id', '=','pengajuan_izin.izin')
            ->join('status', 'status.slug', 'pengajuan_izin.status')
//            ->whereRaw("izin_role.role::jsonb = '$roles'::jsonb")
            ->whereIn('izin_role.role', $roles )
            ->where('pengajuan_izin.status','!=', 'draft' )
            ->where('pengajuan_izin.status','!=', 'selesai' )
            ->orderBy('pengajuan_izin.created_at', 'ASC');

        if ($id != null){
            $query->where('pengajuan_izin.id',$id);
        }

        return $query;
    }

    public static function getDataPengajuanIzinByUser($id = null){
        $user_id = Auth::user()->id;

        $query = PengajuanIzin::select(['pengajuan_izin.id','pengajuan_izin.tipe_perizinan', 'pengajuan_izin.created_at',
            'status.name as status', 'izin.name as izin_name', 'pengajuan_izin.user as user', 'izin_role.izin_proses as izin_proses'])
            ->leftJoin('izin_role', 'izin_role.izin', 'pengajuan_izin.izin')
            ->join('izin', 'izin.id', 'pengajuan_izin.izin')
            ->join('status', 'status.slug', 'pengajuan_izin.status')
            ->where('pengajuan_izin.user', $user_id )
            ->where('pengajuan_izin.id',$id)
            ->orderBy('pengajuan_izin.created_at', 'ASC');

        return $query;
    }


}
