<div class="portlet-body" style="display: block;" md-table multiple md-progress="promise">
  <div class="row">
    <div class="col-md-6">
      <div md-cell class="form-group form-md-line-input has-success">
        <div class="input-icon">
          <div  class="form-control">@{{ response.data.nik }}</div>
          <label for="form_control_1">NIK</label>
        </div>
      </div>
      <div md-cell class="form-group form-md-line-input has-success">
        <div class="input-icon">
          <div  class="form-control">@{{ response.data.nama_ktp }}</div>
          <label for="form_control_1">Nama KTP</label>
        </div>
      </div>
      <div md-cell class="form-group form-md-line-input has-success">
        <div class="input-icon">
          <div  class="form-control">@{{ response.data.alamat }}</div>
          <label for="form_control_1">Alamat</label>
        </div>
      </div>
      <div md-cell class="form-group form-md-line-input has-success">
        <div class="input-icon">
          <div  class="form-control">@{{ response.data.kelurahan }}</div>
          <label for="form_control_1">Kelurahan</label>
        </div>
      </div>
      <div md-cell class="form-group form-md-line-input has-success">
        <div class="input-icon">
          <div  class="form-control">@{{ response.data.kecamatan }}</div>
          <label for="form_control_1">Kecamatan</label>
        </div>
      </div>
      <div md-cell class="form-group form-md-line-input has-success">
        <div class="input-icon">
          <div  class="form-control">@{{ response.data.tempat_lahir }}</div>
          <label for="form_control_1">Tempat Lahir</label>
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div md-cell class="form-group form-md-line-input has-success">
        <div class="input-icon">
          <div  class="form-control">@{{ response.data.no_kk }}</div>
          <label for="form_control_1">No. KK</label>
        </div>
      </div>
      <div md-cell class="form-group form-md-line-input has-success">
        <div class="input-icon">
          <div  class="form-control">@{{ response.data.jenis_kelamin }}</div>
          <label for="form_control_1">Jenis Kelamin</label>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div md-cell class="form-group form-md-line-input has-success">
            <div class="input-icon">
              <div  class="form-control">@{{ response.data.no_rt }}</div>
              <label for="form_control_1">RT</label>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div md-cell class="form-group form-md-line-input has-success">
            <div class="input-icon">
              <div  class="form-control">@{{ response.data.no_rw }}</div>
              <label for="form_control_1">RW</label>
            </div>
          </div>
        </div>
      </div>
      <div md-cell class="form-group form-md-line-input has-success">
        <div class="input-icon">
          <div  class="form-control">@{{ response.data.kota }}</div>
          <label for="form_control_1">Kota</label>
        </div>
      </div>
      <div md-cell class="form-group form-md-line-input has-success">
        <div class="input-icon">
          <div  class="form-control">@{{ response.data.provinsi }}</div>
          <label for="form_control_1">Provinsi</label>
        </div>
      </div>
      <div md-cell class="form-group form-md-line-input has-success">
        <div class="input-icon">
          <div  class="form-control">@{{ response.data.tanggal_lahir }}</div>
          <label for="form_control_1">Tanggal Lahir</label>
        </div>
      </div>
    </div>
  </div>
</div>
