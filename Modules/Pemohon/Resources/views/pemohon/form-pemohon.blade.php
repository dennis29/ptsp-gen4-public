@extends('pemohon::layouts.master')
@section('title',"Dashboard Pemohon")

@section('content')
<json-data model="formData">
{{ collect($form)->toJson() }}
</json-data>
<div class="row" ng-controller="FormPemohonController">
  <div class="col-md-12">
    <form name="form">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-user"></i>@{{ formData.title }} </div>
                <div class="actions pull-right">
                    <button class="btn btn-sm green mt-ladda-btn ladda-button" type="submit" ng-click="saveAndReturn($event, formData.url)"  data-style="zoom-out" ng-hide="formData.action == 'view'"><span class="ladda-label">Simpan&nbsp;&nbsp;<i class="fa fa-mail-reply"></i></span></button>
                </div>
            </div>
            <div class="portlet-body" style="display: block;">
                <div class="row">
                    <div class="col-md-6">
                        <form-input attributes="fields.nik"></form-input>
                        <form-input attributes="fields.nama_ktp"></form-input>
                        <form-input attributes="fields.alamat"></form-input>
                        <form-input attributes="fields.kelurahan"></form-input>
                        <form-input attributes="fields.kecamatan"></form-input>
                        <form-input attributes="fields.tempat_lahir"></form-input>
                    </div>
                    <div class="col-md-6">
                        <form-input attributes="fields.no_kk"></form-input>
                        <form-input attributes="fields.jenis_kelamin"></form-input>
                        <div class="row">
                            <div class="col-md-4">
                                <form-input attributes="fields.no_rt"></form-input>
                            </div>
                            <div class="col-md-4">
                                <form-input attributes="fields.no_rw"></form-input>
                            </div>
                        </div>
                        <form-input attributes="fields.kota"></form-input>
                        <form-input attributes="fields.provinsi"></form-input>
                        <form-input attributes="fields.tanggal_lahir"></form-input>
                    </div>
                </div>
            </div>
        </div>
    </form>
  </div>
</div>
@endsection

@section('script')
<script src="{{ asset('modules/pemohon/form-pemohon-controller.js') }}" type="text/javascript"></script>
@endsection
