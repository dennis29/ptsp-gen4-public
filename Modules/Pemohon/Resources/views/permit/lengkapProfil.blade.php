@extends('pemohon::layouts.layout')
@section('title',"Dashboard Pemohon")

@section('header')
    @include('pemohon::layouts.header')
@endsection

@section('breadcrumbs')
    @include('pemohon::layouts.breadcrumbs')
@endsection

@section('page_sidebar')
    @include('pemohon::layouts.page_sidebar')
@endsection

@section('base_content')
    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="page-content-col">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-user"></i>Identitas Pemohon </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body" style="display: block;">

                        <div class="row">
                            <div class="col-md-6">
                                <form role="form">
                                    <div class="form-body">

                                        <div class="form-group form-md-line-input has-success">
                                            <input type="text" class="form-control" id="form_control_1" placeholder="Nama Lengkap Sesuai Identitas Kependudukan">
                                            <label for="form_control_1">Nama</label>
                                        </div>
                                        <div class="form-group form-md-line-input has-success">
                                            <input type="text" class="form-control" id="form_control_1" placeholder="NIK Sesuai Identitas Kependudukan">
                                            <label for="form_control_1">NIK</label>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group form-md-line-input">
                                                    <input type="date" class="form-control" id="form_control_1" placeholder="">
                                                    <label for="form_control_1">Tanggal Lahir</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-md-line-input">
                                                    <input type="text" class="form-control" id="form_control_1" placeholder="">
                                                    <label for="form_control_1">Tempat Lahir</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-md-line-input">
                                                    <select name="jenis_kelamin" class="form-control">
                                                        <option value="">--Pilih--</option>
                                                    </select>
                                                    <label for="form_control_1">Jenis Kelamin</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control" id="form_control_1" placeholder="">
                                            <label for="form_control_1">Alamat</label>
                                            <span class="help-block"></span>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group form-md-line-input">
                                                    <input type="text" class="form-control" id="form_control_1" placeholder="">
                                                    <label for="form_control_1">Kelurahan</label>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-md-line-input">
                                                    <div class="input-icon">
                                                        <input type="text" class="form-control" placeholder="">
                                                        <label for="form_control_1">RT</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-md-line-input">
                                                    <div class="input-icon">
                                                        <input type="text" class="form-control" placeholder="">
                                                        <label for="form_control_1">RW</label>
                                                        <i class="fa fa-note"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group form-md-line-input">
                                                    <input type="text" class="form-control" id="form_control_1" placeholder="">
                                                    <label for="form_control_1">Kecamatan</label>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group form-md-line-input">
                                                    <div class="input-icon">
                                                        <input type="text" class="form-control" placeholder="">
                                                        <label for="form_control_1">Provinsi</label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </form>
                            </div>
                            <div class="col-md-6">
                                <form role="form">
                                    <div class="form-body">

                                        <div class="form-group form-md-line-input has-success">
                                            <input type="text" class="form-control" id="form_control_1" placeholder="">
                                            <label for="form_control_1">No. Kartu Keluarga</label>
                                        </div>

                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control" id="form_control_1" placeholder="">
                                            <label for="form_control_1">No.HP</label>
                                            <span class="help-block"></span>
                                        </div>
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control" id="form_control_1" placeholder="">
                                            <label for="form_control_1">Kode Pos</label>
                                            <span class="help-block"></span>
                                        </div>
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control" id="form_control_1" placeholder="">
                                            <label for="form_control_1">Kewarganegaraan</label>
                                            <span class="help-block"></span>
                                        </div>
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control" id="form_control_1" placeholder="">
                                            <label for="form_control_1">Keterangan Lain</label>
                                            <span class="help-block"></span>
                                        </div>

                                    </div>
                                    <button name="simpan" type="submit" class="btn btn-sm btn-primary">
                                        Simpan
                                    </button>
                                </form>
                            </div>
                        </div>


                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
@stop
