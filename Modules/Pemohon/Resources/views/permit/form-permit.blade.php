@extends('pemohon::layouts.master')
@section('title',"Dashboard Pemohon")

@section('content')
    <json-data model="formData">
        {{ collect($form)->toJson() }}
    </json-data>

    <!-- BEGIN IDENTITY FORM -->
    @include('pemohon::pemohon.layout-grid-pemohon')
    <!-- END IDENTITY FORM -->

    <!-- BEGIN UPLOAD FILES -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-file"></i>Unggah Berkas</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <i class="fa fa-warning"></i> Panduan Penamaan File
                    <form action="../assets/global/plugins/dropzone/upload.php" class="dropzone dropzone-file-area" id="my-dropzone" style="width: 500px; margin-top: 50px;">
                        <h3 class="sbold">Tarik file ke area sini</h3>
                        <p> Untuk memudahkan anda mengunggah berkas kepad kamu </p>
                    </form>
                    <div>
                        <div class="col-md-10"></div>
                        <div class="col-md 4">
                            <button type="submit" name="submit" class="btn  btn-pull-right btn-sm btn-primary"> Ajukan Permohonan </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END UPLOAD FILES -->

@endsection

@section('script')
    <script src="{{ asset('modules/pemohon/form-pemohon-controller.js') }}" type="text/javascript"></script>
@endsection
