@extends('pemohon::layouts.master')
@section('title',"Dashboard Perizinan")

@section('content')
  <div class="row" ng-controller="GridPermitController" ng-init="getData()">
    <div class="col-md-12">
      <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-green-sharp bold">@{{ response.title }}</div>
        </div>
        <div class="portlet-body">
          <div class="row nav-grid">
            <div>
                @component('core::components.grid-filter')
                @endcomponent
            </div>
          </div>

          <md-table-container>
            <!-- md-row-select  ng-model="selected" -->
            <table md-table multiple md-progress="promise">
              <thead md-head md-order="query.order" md-on-reorder="getData">
              <tr md-row>
                <th md-column ng-repeat="column in response.columns"  md-order-by="@{{ (column.sortable) ? (column.source) : '' }}"><span>@{{ column.label }}</span></th>
                <th md-column style="width: 80px;"><span>Tindakan</span></th>
              </tr>
              </thead>
              <tbody md-body>
              <tr md-row ng-repeat="dt in response.data" ng-dblclick="onDblClick(dt.id)" class="odd gradeX">
                <td md-cell>@{{ dt.tipe_perizinan }}</td>
                <td md-cell>@{{ dt.izin_name }}</td>
                <td md-cell>@{{ dt.created_at | amDateFormat:'DD MMM YYYY' }}</td>
                <td md-cell style="text-align:center;">
                  <a href="@{{ response.url+'/view/'+dt.id }}" class="btn btn-sm btn-success">
                    <i class="fa fa-edit"></i> Lihat
                  </a>
                </td>
              </tr>
              </tbody>
            </table>
          </md-table-container>
          @component('core::components.grid-navigation')
          @endcomponent
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script src="{{ asset('modules/pemohon/grid-permit-controller.js') }}" type="text/javascript"></script>
@endsection
