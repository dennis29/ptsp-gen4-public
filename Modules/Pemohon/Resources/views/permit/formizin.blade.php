@extends('pemohon::layouts.layout')
@section('title',"Dashboard Pemohon")

@section('header')
    @include('pemohon::layouts.header')
@endsection

@section('breadcrumbs')
    @include('pemohon::layouts.breadcrumbs')
@endsection

@section('page_sidebar')
    @include('pemohon::layouts.page_sidebar')
@endsection

@section('base_content')
    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="page-content-col">
<div class="row">
    <div class="col-md-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i>Identitas Pemohon </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body" style="display: block;">

                <div class="row">
                    <div class="col-md-6">
                        <form role="form">
                            <div class="form-body">


                                <div class="form-group form-md-line-input has-success">
                                    <input type="text" class="form-control" id="form_control_1" placeholder="Nama Lengkap Sesuai Identitas Kependudukan">
                                    <label for="form_control_1">Nama</label>
                                </div>

                                <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control" id="form_control_1" placeholder="">
                                    <label for="form_control_1">Alamat</label>
                                    <span class="help-block"></span>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control" id="form_control_1" placeholder="">
                                            <label for="form_control_1">Kelurahan</label>
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-md-line-input has-success">
                                            <div class="input-icon">
                                                <input type="text" class="form-control" placeholder="">
                                                <label for="form_control_1">RT</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-md-line-input has-success">
                                            <div class="input-icon">
                                                <input type="text" class="form-control" placeholder="">
                                                <label for="form_control_1">RW</label>
                                                <i class="fa fa-note"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control" id="form_control_1" placeholder="">
                                            <label for="form_control_1">Kecamatan</label>
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-md-line-input has-success">
                                            <div class="input-icon">
                                                <input type="text" class="form-control" placeholder="">
                                                <label for="form_control_1">Provinsi</label>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </form>
                    </div>
                    <div class="col-md-6">
                        <form role="form">
                            <div class="form-body">


                                <div class="form-group form-md-line-input has-success">
                                    <input type="text" class="form-control" id="form_control_1" placeholder="">
                                    <label for="form_control_1">No. NPWP</label>
                                </div>

                                <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control" id="form_control_1" placeholder="">
                                    <label for="form_control_1">No.HP</label>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control" id="form_control_1" placeholder="">
                                    <label for="form_control_1">Kode Pos</label>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control" id="form_control_1" placeholder="">
                                    <label for="form_control_1">Keterangan Lain</label>
                                    <span class="help-block"></span>
                                </div>

                            </div>

                        </form>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->

        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-file"></i>Formulir 1</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body" style="display: block;">

            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>

    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-file"></i>Formulir 2</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body" style="display: block;">

            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-file"></i>Unggah Berkas</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body" style="display: block;">
                <i class="fa fa-warning"></i> Panduan Penamaan File
                <form action="../assets/global/plugins/dropzone/upload.php" class="dropzone dropzone-file-area" id="my-dropzone" style="width: 500px; margin-top: 50px;">
                    <h3 class="sbold">Tarik file ke area sini</h3>
                    <p> Untuk memudahkan anda mengunggah berkas kepad kamu </p>
                </form>
                <div>
                <div class="col-md-10"></div>
                <div class="col-md 4">
                    <button type="submit" name="submit" class="btn  btn-pull-right btn-sm btn-primary"> Ajukan Permohonan
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>
</div>
    </div>
    <!-- END PAGE BASE CONTENT -->
@endsection