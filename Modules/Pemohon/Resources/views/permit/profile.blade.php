@extends('pemohon::layouts.layout')
@section('title',"Dashboard Pemohon")

@section('header')
	@include('pemohon::layouts.header')
@endsection

@section('breadcrumbs')
	@include('pemohon::layouts.breadcrumbs')
@endsection

@section('page_sidebar')
	@include('pemohon::layouts.page_sidebar')
@endsection

@section('base_content')
<!-- BEGIN PAGE BASE CONTENT -->
 <div class="page-content-col">
	 <div class="row">
		 <div class="col-md-12">
			 <!-- BEGIN SAMPLE TABLE PORTLET-->
			 <div class="portlet light bordered">
				 <div class="portlet-body">
					 <div class="table-toolbar">
						 <div class="row">
							 <div class="col-md-6">
								 <div class="btn-group">
									 <button id="sample_editable_1_new" class="btn sbold green"> Buat Permohonan
										 <i class="fa fa-plus"></i>
									 </button>
								 </div>
							 </div>
							 <div class="col-md-6">
								 <div class="btn-group pull-right">
									 <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Pilih Filter
										 <i class="fa fa-angle-down"></i>
									 </button>
									 <ul class="dropdown-menu pull-right">
										 <li>
											 <a href="javascript:;">
												 <i class="fa fa-print"></i> Print </a>
										 </li>
										 <li>
											 <a href="javascript:;">
												 <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
										 </li>
										 <li>
											 <a href="javascript:;">
												 <i class="fa fa-file-excel-o"></i> Export to Excel </a>
										 </li>
									 </ul>
								 </div>
							 </div>
						 </div>
					 </div>
					 <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
						 <thead>
						 <tr>
							 <th>#</th>
							 <th> Tipe Perizinan </th>
							 <th> Status Perizinan </th>
							 <th> Tanggal Mengajukan </th>
							 <th> Status </th>
							 <th class="text-center"> Actions </th>
						 </tr>
						 </thead>
						 <tbody>
						 <tr class="odd gradeX">
							 <td>1.</td>
							 <td> Izin Praktek Dokter Spesialis </td>
							 <td> Baru </td>
							 <td> 22 Januari 2018 </td>
							 <td>
								 <span class="label label-sm label-warning"> Menunggu Verifikasi Dokumen </span>
							 </td>
							 <td style="text-align:center;">
								 <a href="" class="btn btn-sm btn-primary">
									 <i class="fa fa-eye"></i> Lihat
								 </a>
							 </td>
						 </tr>
						 <tr class="odd gradeX">
							 <td>2.</td>
							 <td> Izin Praktek Dokter Spesialis </td>
							 <td> Baru </td>
							 <td> 23 Januari 2018 </td>
							 <td>
								 <span class="label label-sm label-warning"> Proses Pengambilan </span>
							 </td>
							 <td style="text-align:center;">
								 <a href="" class="btn btn-sm btn-primary">
									 <i class="fa fa-eye"></i> Lihat
								 </a>
							 </td>
						 </tr>
						 <tr class="odd gradeX">
							 <td>3.</td>
							 <td> Izin Praktek Dokter Gigi </td>
							 <td> Baru </td>
							 <td> 25 Januari 2018 </td>
							 <td>
								 <span class="label label-sm label-warning"> Perlu Perbaikan </span>
							 </td>
							 <td style="text-align:center;">
								 <a href="" class="btn btn-sm btn-primary">
									 <i class="fa fa-eye"></i> Lihat
								 </a>
								 <a href="" class="btn btn-sm btn-success">
									 <i class="fa fa-edit"></i> Edit
								 </a>

							 </td>
						 </tr>

						 </tbody>
					 </table>

				 </div>
			 </div>
			 <!-- END SAMPLE TABLE PORTLET-->
		 </div>
	 </div>
 </div>
<!-- END PAGE BASE CONTENT -->
@stop
