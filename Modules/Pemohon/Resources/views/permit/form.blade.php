@extends('pemohon::layouts.master')
@section('title',"Dashboard Pemohon")

@section('content')
    <div class="page-content-inner">

        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit ">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-book font-green"></i>
                            <span class="caption-subject font-green bold uppercase">Permohonan Perizinan</span>
                        </div>
                        <div class="actions">

                        </div>
                    </div>
                    <div class="portlet-body">

                        <!-- Begin Form -->
                        <form action="#" class="form-horizontal" id="form_sample_1">
                            <div class="form-body">
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                <div class="alert alert-success display-hide">
                                    <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-3 control-label" for="form_control_1">Tipe
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" placeholder="" name="name" readonly>
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">Tipe Perizinan</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-3 control-label" for="form_control_1">Jenis Perizinan
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="jenis_perizinan">
                                            <option value="">--Pilih--</option>
                                            <option value="2">Jenis 1</option>
                                            <option value="3">Jenis 2</option>
                                            <option value="4">Jenis 3</option>
                                        </select>
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">Jenis Perizinan</span>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-3 control-label" for="form_control_1">Lokasi
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9">
                                        <div class="input-group">
                                       <span class="input-group-addon">
                                           <i class="fa fa-search"></i>
                                       </span>
                                            <input type="text" class="form-control" placeholder="Cari lokasi">
                                        </div>
                                        <div class="label label-danger visible-ie8"> Not supported in Internet Explorer 8 </div>
                                        <br>
                                        <div id="map" class="gmaps"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-3 control-label" for="form_control_1">Status
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="status_permohonan">
                                            <option value="">--Pilih--</option>
                                            <option value="2">1</option>
                                            <option value="3">2</option>
                                            <option value="4">3</option>
                                        </select>
                                        <div class="form-control-focus"> </div>
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-3 control-label" for="form_control_1">Isian Lain Perizinan
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" placeholder="Isian 1" name="isian_1" >
                                        <input type="text" class="form-control" placeholder="Isian 2" name="isian_2" >
                                        <input type="text" class="form-control" placeholder="Isian 3" name="isian_2" >
                                        <div class="form-control-focus"> </div>
                                        <span class="help-block">Keterangan Lain Perizinan</span>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Buat Permohonan</button>
                                        <button type="reset" class="btn default">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection

@section('scripts')
    <script src="{{ asset('plugins/bootstrap-table/bootstrap-table.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/amcharts/amcharts/amcharts.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/amcharts/amcharts/serial.js') }}" type="text/javascript"></script>
    <!--<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAcaMnGUdC999VKpum_jblgxCIfFTw0fro" type="text/javascript"></script>-->
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyAcaMnGUdC999VKpum_jblgxCIfFTw0fro" type="text/javascript"></script>
    <script>
        $(function () {
            var map = new GMaps({
                el: '#map',
                lat: -12.043333,
                lng: -77.028333
            });
            GMaps.geolocate({
                success: function(position) {
                    map.setCenter(position.coords.latitude, position.coords.longitude);

                    var m = map.addMarker({
                        lat: position.coords.latitude,
                        lng: position.coords.longitude,
                        draggable:true
                    });

                },
                error: function(error) {
                    alert('Geolocation failed: '+error.message);
                },
                not_supported: function() {
                    alert("Your browser does not support geolocation");
                },
                always: function() {
                    alert("Lokasi Anda Sekarang!");
                }
            });
        });
    </script>
    <script src="{{ asset('plugins/gmaps/gmaps.min.js')}}" type="text/javascript"></script>
    <!--<script src="http://maps.google.com/maps/api/js" type="text/javascript"></script>-->
    <script src="{{ asset('pages/scripts/maps-google.min.js')}}" type="text/javascript"></script>

@endsection
