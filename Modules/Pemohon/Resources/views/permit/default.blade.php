@extends('pemohon::layouts.layout')
@section('title',"Dashboard Pemohon")

@section('header')
    @include('pemohon::layouts.header')
@endsection

@section('breadcrumbs')
    @include('pemohon::layouts.breadcrumbs')
@endsection

@section('page_sidebar')
    @include('pemohon::layouts.page_sidebar')
@endsection

@section('base_content')
    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="page-content-col">
    <!-- Isi Content nya -->
    </div>
    <!-- END PAGE BASE CONTENT -->
@stop
