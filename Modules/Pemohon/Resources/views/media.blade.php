@extends('pemohon::layouts.master', ['noMenu' => 'true'])
@section('title',"Berkas Saya")

@section('content')
  @include('core::media')
@endsection

@section('script')
<script src="{{ asset('modules/core/media-controller.js') }}" type="text/javascript"></script>
@endsection
