<div class="col-md-3">
  <div class="form-group form-md-line-input">
    <div class="input-group">
        <input type="text" class="form-control" placeholder="Cari" ng-model="keyword" ng-change="keywordChanged()">
        <span class="input-group-addon">
          <i class="fa fa-search"></i>
        </span>
    </div>
  </div>
</div>
{{ $slot }}
