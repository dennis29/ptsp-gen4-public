<md-table-pagination md-limit="query.perpage" md-limit-options="[10, 20, 50, 100]" md-page="query.page" md-total="@{{response.total}}" md-on-paginate="getData" md-page-select></md-table-pagination>
