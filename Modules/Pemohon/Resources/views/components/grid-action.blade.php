{{ $slot }}
@if(Auth::user()->has('update'))
<md-button href="@{{ response.url+'/update/'+dt.id }}"
   class="md-icon-button" >
  <md-icon><i class="fa fa-edit"></i></md-icon>
  <md-tooltip md-direction="left">Ubah</md-tooltip>
</md-button>
@else
<md-button href="@{{ response.url+'/update/'+dt.id }}"
   class="md-icon-button" >
  <md-icon><i class="fa fa-eye"></i></md-icon>
  <md-tooltip md-direction="left">View</md-tooltip>
</md-button>
@endif

@if(Auth::user()->has('delete'))
<md-button
   class="md-icon-button"  link="@{{ response.url+'/delete' }}" delete-confirm="@{{ dt.id }}" >
  <md-icon><i class="fa fa-trash"></i></md-icon>
  <md-tooltip md-direction="left">Hapus</md-tooltip>
</md-button>
@endif
