<md-table-container>
  <!-- md-row-select  ng-model="selected" -->
  <table md-table multiple md-progress="promise">
    <thead md-head md-order="query.order" md-on-reorder="getData">
    <tr md-row>
      <th md-column ng-repeat="column in response.columns"  md-order-by="@{{ (column.sortable) ? (column.source) : '' }}"><span>@{{ column.label }}</span></th>
      <th md-column style="width: 80px;"><span>Tindakan</span></th>
    </tr>
    </thead>
    <tbody md-body>
    <tr md-row ng-repeat="dt in response.data" ng-dblclick="onDblClick(dt.id)" class="odd gradeX">
      {{ $slot }}
    </tr>
    </tbody>
  </table>
</md-table-container>
