@extends('core::layout')

@section('content')
<div class="row" ng-controller="GridRoleController" ng-init="getData()">
  <div class="col-md-12">
    <div class="portlet light bordered">
      <div class="portlet-title">
        @component('core::components.grid-header')
        @endcomponent
      </div>
      <div class="portlet-body">
        <div class="row margin-bottom-20">
          <div class="col-md-12">
            <div class="row nav-grid">
              @component('core::components.grid-filter')
              @endcomponent
            </div>

            @component('core::components.grid-table')
              <td md-cell>@{{ dt.id }}</td>
              <td md-cell>@{{ dt.nama_ijin }}</td>
              <td md-cell>@{{ dt.bidang_perijinan_id }}</td>
            @endcomponent

            @component('core::components.grid-navigation')
            @endcomponent
          </div>
        </div>
      </div>
  </div>
</div>
@endsection

@section('script')
<script src="{{ asset('modules/Permit/grid-permit-controller.js') }}" type="text/javascript"></script>
@endsection
