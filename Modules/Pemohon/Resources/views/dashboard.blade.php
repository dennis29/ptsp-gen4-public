@extends('pemohon::layouts.master')
@section('title',"Dashboard Pemohon")

@section('content')
<div class="row" ng-controller="DashboardPemohonController">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 yellow" href="#">
            <div class="visual">
                <i class="fa fa-file-o"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span>0</span>
                </div>
                <div class="desc"> Izin Baru</div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
            <div class="visual">
                <i class="fa fa-hourglass-half"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span>0</span></div>
                <div class="desc"> Izin Dalam Proses </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 green" href="#">
            <div class="visual">
                <i class="fa fa-check-circle"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span>0</span>
                </div>
                <div class="desc"> Izin Selesai </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 red" href="#">
            <div class="visual">
                <i class="fa fa-times-circle"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span>0</span>
                </div>
                <div class="desc"> Izin Ditolak </div>
            </div>
        </a>
    </div>
</div>
@endsection

@section('script')
<script src="{{ asset('modules/pemohon/dashboard-pemohon-controller.js') }}" type="text/javascript"></script>
@endsection
