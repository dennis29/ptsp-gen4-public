<header class="page-header">
                <nav class="navbar mega-menu" role="navigation">
                    <div class="container-fluid">
                        <div class="clearfix header-wrapper">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="toggle-icon">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </span>
                            </button>
                            <!-- End Toggle Button -->
                            <!-- BEGIN LOGO -->
                            <a id="index" class="page-logo" href="{{ route('landingpage') }}">
                                <img src="{{ asset('img/dpmptsp.png') }}" alt="Logo"> </a>
                            <!-- END LOGO -->
                            <!-- BEGIN SEARCH -->
                            {{--<form class="search" action="extra_search.html" method="GET">--}}
                                {{--<input type="name" class="form-control" name="query" placeholder="Search...">--}}
                                {{--<a href="javascript:;" class="btn submit md-skip">--}}
                                    {{--<i class="fa fa-search"></i>--}}
                                {{--</a>--}}
                            {{--</form>--}}
                            <!-- END SEARCH -->
                            <!-- BEGIN TOPBAR ACTIONS -->
                            <div class="topbar-actions">
                                <!-- BEGIN GROUP NOTIFICATION -->
                                    {{--@include('pemohon::layouts.notification')--}}
                                <!-- END GROUP NOTIFICATION -->
                                <!-- BEGIN GROUP INFORMATION -->
                                <div class="btn-group-red btn-group">
                                    <button type="button" class="btn btn-sm md-skip dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                    <ul class="dropdown-menu-v2" role="menu">
                                        <li>
                                            <a href="{{ route('permohonan.pencarian') }}">Permohonan Baru</a>
                                        </li>
                                        <li>
                                            <a href="#">Upload Berkas</a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- END GROUP INFORMATION -->
                                <!-- BEGIN USER PROFILE -->
                                <div class="btn-group-img btn-group">
                                    <button type="button" class="btn btn-sm md-skip dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                        <span>Hi, {{Auth::user()->fullname}}</span>
                                        @if (!empty(Auth::user()->picture))
                                        <img src="{{Auth::user()->picture}}" alt="">
                                        @endif
                                    </button>
                                    <ul class="dropdown-menu-v2" role="menu">
                                        <li>
                                            <a href="{{ route('landingpage') }}">
                                                <i class="icon-home"></i> Home
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('pemohon.dashboard') }}">
                                                <i class="icon-user"></i> Profil Saya
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                <i class="icon-key"></i> Log Out </a>
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                                <!-- END USER PROFILE -->
                            </div>
                            <!-- END TOPBAR ACTIONS -->
                        </div>
                        <!-- BEGIN HEADER MENU -->
                        @include('pemohon::layouts.menu')
                        <!-- END HEADER MENU -->
                    </div>
                    <!--/container-->
                </nav>
            </header>
