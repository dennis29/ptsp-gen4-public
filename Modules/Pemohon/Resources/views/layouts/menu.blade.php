<div class="nav-collapse collapse navbar-collapse navbar-responsive-collapse">
    <ul class="nav navbar-nav">
        <li class="dropdown dropdown-fw dropdown-fw-disabled {{ active(['pemohon.dashboard'], 'active open') }}">
            <a href="{{ route('pemohon.dashboard') }}" class="text-uppercase">
                <i class="icon-home"></i> Dashboard </a>
        </li>
        <li class="dropdown dropdown-fw dropdown-fw-disabled {{ active(['permohonan.dashboard', 'permohonan.pencarian'], 'active open') }}">
            <a href="{{ route('permohonan.dashboard') }}" class="text-uppercase">
                <i class="fa fa-file-text"></i> Permohonan </a>
            <ul class="dropdown-menu dropdown-menu-fw">
                <li class="{{ active(['permohonan.dashboard']) }}">
                    <a href="{{ route('permohonan.dashboard') }}">
                        <i class="icon-bulb"></i>
                        Daftar Permohonan
                     </a>
                </li>
                <li class="{{ active(['permohonan.pencarian']) }}">
                    <a href="{{ route('permohonan.pencarian') }}">
                        <i class="fa fa-plus"></i> Buat Permohonan Baru </a>
                </li>
            </ul>
        </li>
        <li class="dropdown dropdown-fw dropdown-fw-disabled {{ active(['pemohon.media'], 'active open') }}">
            <a href="{{ route('pemohon.media') }}" class="text-uppercase">
                <i class="fa fa-file-image-o"></i> Berkas Saya </a>
        </li>
        <li class="dropdown dropdown-fw dropdown-fw-disabled {{ active(['pemohon.profil'], 'active open') }}">
            <a href="{{ route('pemohon.profil') }}" class="text-uppercase {{ active(['pemohon.profil'], 'active open') }}">
                <i class="icon-user"></i> Akun </a>
            <ul class="dropdown-menu dropdown-menu-fw">
                <li class="{{ active(['pemohon.profil'], 'active') }}">
                    <a href="{{ route('pemohon.profil') }}">
                        <i class="icon-user"></i>
                        Profil Saya
                     </a>
                </li>
                <li class="{{ active(['pemohon.changepassword'], 'active') }}">
                    <a href="{{ route('pemohon.changepassword') }}">
                        <i class="fa fa-lock"></i>
                        Ubah Password
                     </a>
                </li>
            </ul>
        </li>
    </ul>
</div>
