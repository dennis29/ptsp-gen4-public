<!DOCTYPE html>
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>PTSP DKI @yield('title')</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN LAYOUT FIRST STYLES -->
        <!--<link href="//fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />-->
        <!-- END LAYOUT FIRST STYLES -->
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
     <!--<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />-->
        <link href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{ asset('css/components-md.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ asset('css/plugins-md.min.css') }}" rel="stylesheet" type="text/css" />
        @yield('style')
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{ asset('assets/layouts/layout5/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/layouts/layout5/css/custom.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico"/>
     </head>
    <!-- END HEAD -->
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-md">
        <!-- BEGIN CONTAINER -->
        <div class="wrapper">
        <!-- BEGIN HEADER -->
        @yield('header')
        <!-- END HEADER -->
	   	 <div class="container-fluid">
	          <div class="page-content">
	           	<!-- BEGIN BREADCRUMBS -->
	           	{{-- @include('user::layouts.breadcrumbs') --}}
	           	@yield('breadcrumbs')
	           	<!-- END BREADCRUMBS-->
			<!-- BEGIN SIDEBAR CONTENT LAYOUT -->
	          <div class="page-content-container">
	              <div class="page-content-row">
	              <!-- BEGIN PAGE SIDEBAR -->
	              {{--@include('user::layouts.page_sidebar')--}}
	              @yield('page_sidebar')
	              <!-- END PAGE SIDEBAR -->
	              {{--@include('user::layouts.pagebase_content')--}}
	              @yield("base_content")
	              </div>
               </div>
                    <!-- END SIDEBAR CONTENT LAYOUT -->
               </div>
               <!-- BEGIN FOOTER -->
               @include('pemohon::layouts.footer')
               <!-- FOOTER -->
           </div>
        </div>
	<!-- END CONTAINER -->
	<!-- BEGIN QUICK SIDEBAR & QUICK NAV -->
	{{--@include('user::layouts.quick_sidebar')--}}
	<!-- END QUICK SIDEBAR & QUICK NAV -->

	<!-- BEGIN CORE PLUGINS -->
        <script src="{{ asset('plugins/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/jquery-migrate.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/js.cookie.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
     <!-- END CORE PLUGINS -->
        <script src="{{ asset('plugins/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/js.cookie.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/moment.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/ladda/spin.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/ladda/ladda.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap-toastr/toastr.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
        <!-- END CORE JQUERY PLUGINS -->
        <!-- BEGIN CORE ANGULARJS PLUGINS -->
        <script src="{{ asset('plugins/angularjs/angular.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/angularjs/angular-sanitize.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/angularjs/angular-touch.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/angularjs/angular-moment.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/angularjs/plugins/angular-ui-router.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/angularjs/plugins/ocLazyLoad.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/angularjs/plugins/ui-bootstrap-tpls.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/angularjs/material/angular-material.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/angularjs/angular-animate.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/angularjs/angular-aria.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/angularjs/angular-messages.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/angularjs/material-datatable/md-data-table.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/json-data/json-data.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/angularjs/plugins/angular-xeditable/js/xeditable.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/angularjs/fileupload/ng-file-upload-shim.min.js') }}"></script>
        <script src="{{ asset('plugins/angularjs/fileupload/ng-file-upload.min.js') }}"></script>
        <!-- END CORE ANGULARJS PLUGINS -->
        <!-- BEGIN APP LEVEL ANGULARJS SCRIPTS -->
        <script src="{{ asset('js/main.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/directives.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/form-input.js') }}" type="text/javascript"></script>
        <!-- END APP LEVEL ANGULARJS SCRIPTS -->
     <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{ asset('js/app.min.js') }}" type="text/javascript"></script>
     <!-- END THEME GLOBAL SCRIPTS -->
        @yield('scripts')
     <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="{{ asset('assets/layouts/layout5/scripts/layout.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/quick-sidebar.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/quick-nav.min.js') }}" type="text/javascript"></script>
     <!-- END THEME LAYOUT SCRIPTS -->
        <script>
            $(document).ready(function()
            {
                $('#clickmewow').click(function()
                {
                    $('#radio1003').attr('checked', 'checked');
                });
            })
        </script>
    </body>
</html>
