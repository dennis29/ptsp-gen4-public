@extends('pemohon::layouts.master')
@section('title',"Formulir Izin")

@section('content')

<json-data model="formData">
  {{ collect($form)->toJson() }}
</json-data>

  @include('pemohon::permohonan.step-pengajuan', ['current' => 2])

  <div class="page-content-inner" ng-controller="FormFormulirController" style="margin-bottom: 60px;">
    <div class="portlet-body" style="display: block;">
        <div class="row">
            <div class="col-md-6">
                <form-input attributes="fields.tipe_izin"></form-input>
                <form-input attributes="fields.izin"></form-input>
                <form-input attributes="fields.nik"></form-input>
                <form-input attributes="fields.nama_ktp"></form-input>
                <form-input attributes="fields.alamat"></form-input>
                <form-input attributes="fields.kelurahan"></form-input>
                <form-input attributes="fields.kecamatan"></form-input>
                <form-input attributes="fields.tempat_lahir"></form-input>
            </div>
            <div class="col-md-6">
                <form-input attributes="fields.no_kk"></form-input>
                <form-input attributes="fields.jenis_kelamin"></form-input>
                <div class="row">
                    <div class="col-md-4">
                        <form-input attributes="fields.no_rt"></form-input>
                    </div>
                    <div class="col-md-4">
                        <form-input attributes="fields.no_rw"></form-input>
                    </div>
                </div>
                <form-input attributes="fields.kota"></form-input>
                <form-input attributes="fields.provinsi"></form-input>
                <form-input attributes="fields.tanggal_lahir"></form-input>
            </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="portlet light bordered" ng-repeat="syarat in formData.syarat" style="padding: 0;">
              <div class="portlet-body" style="display: block; padding: 20px;" ng-bind-html="syarat.name">
              </div>
              <div class="upload-syarat-container">
                <h4 style="margin-bottom: 20px;">Upload Berkas Persyaratan:</h4>
                <div class="row">
                  <div class="col-md-1 text-center" ng-repeat="berkas in syarat.media">
                    <a ng-href="@{{ berkas.url }}" target="_blank" style="display: block;height: 80px;overflow: hidden;">
                      <img src="@{{ berkas.thumbUrl }}" style="width: 100%;"/>
                    </a>
                    <a href="javascript:;" class="text-danger" style="text-decoration: none; font-size: 11px;" ng-click="deleteBerkas(berkas.id, syarat.id, $index)"><i class="fa fa-trash"></i>&nbsp; Hapus</a>
                  </div>
                  <div class="col-md-1">
                    <div class="upload-syarat-button" ng-click="browseMedia(syarat)">
                      <i class="fa fa-plus"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">
            <a href="{{ route('permohonan.konfirmasi', $form['pengajuan_izin']) }}" class="btn btn-lg green-haze" ><i class="fa fa-check"></i>&nbsp;&nbsp;Konfirmasi Permohonan&nbsp;&nbsp;</a>
          </div>
        </div>

    </div>
  </div>
  @include('core::media-popup')
@endsection

@section('script')
	<script src="{{ asset('modules/pemohon/form-formulir-controller.js') }}" type="text/javascript"></script>
  <script src="{{ asset('modules/core/media-popup-controller.js') }}" type="text/javascript"></script>
@endsection

@section('style')
<style type="text/css">
.upload-syarat-container{
  padding: 5px 20px 20px;
  background: #f4f4f4;
}

.upload-syarat-button {
    height: 80px;
    border: 2px dashed #ababab;
    text-align: center;
    line-height: 78px;
    font-size: 26px;
    color: #ababab;
    cursor: pointer;
    outline: none;
}
</style>
@endsection
