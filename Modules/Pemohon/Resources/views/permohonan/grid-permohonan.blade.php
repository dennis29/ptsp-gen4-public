@extends('pemohon::layouts.master')
@section('title',"Dashboard Permohonan")

<json-data model="formData">
  {{ collect($data)->toJson() }}
</json-data>

@section('content')
  <div class="row" ng-controller="GridPermohonanController" ng-init="getData()">
    <div class="col-md-12">
      <div class="portlet light bordered">
        <div class="portlet-title">
          <div class="caption font-green-sharp bold">@{{ formData.title }}</div>
          <div class="col-md-6 pull-right">
            <div class="btn-group pull-right">
              <a href="{{ route('permohonan.pencarian') }}" class="btn btn-sm btn-primary" ><i class="fa fa-plus"></i>&nbsp;&nbsp;@{{ formData.create_label }}</a>
              </a>
            </div>
          </div>
        </div>
        <div class="portlet-body">
          <div class="row nav-grid">
            <div>
                @component('core::components.grid-filter')
                @endcomponent
            </div>
          </div>

          @component('pemohon::components.grid-table')
            <td md-cell>@{{ dt.tipe_perizinan }}</td>
            <td md-cell>@{{ dt.izin_name }}</td>
            <td md-cell>@{{ dt.created_at | amDateFormat:'DD MMM YYYY' }}</td>
            <td md-cell><span class="label label-sm label-warning"> @{{ dt.status }} </span></td>

            <td md-cell style="text-align:center;">
              <md-button href="{{ route('permohonan.formulir', '') }}@{{ '/'+dt.id }}"
                 class="md-icon-button" >
                <md-icon><i class="fa fa-edit"></i></md-icon>
                <md-tooltip md-direction="left">Ubah</md-tooltip>
              </md-button>
            </td>
          @endcomponent

          @component('core::components.grid-navigation')
          @endcomponent
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script src="{{ asset('modules/pemohon/grid-permohonan-controller.js') }}" type="text/javascript"></script>
@endsection
