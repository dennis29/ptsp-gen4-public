<div class="mt-element-step">
	<div class="row step-line">
      <div class="col-md-4 mt-step-col first{{ ($current == 1) ? ' done' : '' }}">
          <div class="mt-step-number bg-grey">
              <i class="fa fa-search"></i>
          </div>
          <div class="mt-step-title uppercase font-grey-cascade">Cari Izin</div>
          <div class="mt-step-content font-grey-cascade">Pilih izin yang ingin diajukan</div>
      </div>
      <div class="col-md-4 mt-step-col{{ ($current == 2) ? ' done' : '' }}">
          <div class="mt-step-number bg-grey">
              <i class="fa fa-clipboard"></i>
          </div>
          <div class="mt-step-title uppercase font-grey-cascade">Isi Formulir</div>
          <div class="mt-step-content font-grey-cascade">Isi formulir dan upload berkas</div>
      </div>
      <div class="col-md-4 mt-step-col last{{ ($current == 3) ? ' done' : '' }}">
          <div class="mt-step-number bg-grey">
              <i class="fa fa-check"></i>
          </div>
          <div class="mt-step-title uppercase font-grey-cascade">Konfirmasi</div>
          <div class="mt-step-content font-grey-cascade">Pastikan data yang diinput sudah benar</div>
      </div>
  </div>
</div>
