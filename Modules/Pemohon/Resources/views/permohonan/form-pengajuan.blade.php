@extends('pemohon::layouts.master')
@section('title',"Dashboard Pemohon")

@section('content')
	<json-data model="formData">
		{{ collect($form)->toJson() }}
	</json-data>
@include('pemohon::permohonan.step-pengajuan', ['current' => 1])
<div class="page-content-inner" ng-controller="FormPengajuanController" style="margin-bottom: 60px;">
	<!-- Begin Form -->
	<form name="form">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="alert alert-danger display-hide">
					<button class="close" data-close="alert"></button> You have some form errors. Please check below.
				</div>
				<div class="alert alert-success display-hide">
					<button class="close" data-close="alert"></button> Your form validation is successful!
				</div>

				<form-input attributes="fields.tipe_perizinan"></form-input>
				<form-input attributes="fields.izin"></form-input>
				<div class="form-group form-md-line-input">
					<div class="input-group">
						<span class="input-group-addon">
							<i class="fa fa-map-marker"></i>
						</span>
						<input id="geocomplete" name="formatted_address" class="form-control" type="text" placeholder="Cari Lokasi" value="" />
					</div>
					<div class="label label-danger visible-ie8"> Not supported in Internet Explorer 8 </div>
					<br>
					<div class="map_canvas"></div>
				</div>


				<form-input attributes="fields.lat"></form-input>
				<form-input attributes="fields.long"></form-input>
				<form-input attributes="fields.formatted_address"></form-input>
			</div>
		</div>

		<div class="form-actions margin-bottom-20">
			<div class="row">
				<div class="col-md-offset-2 col-md-9">
					<button type="button" class="btn btn-lg green-haze mt-ladda-btn ladda-button" ng-click="save($event)" data-style="zoom-out"><span class="ladda-label"><i class="fa fa-check"></i>&nbsp;&nbsp;Buat Permohonan&nbsp;&nbsp;</span></button>
				</div>
			</div>
		</div>
	</form>
</div>

@endsection

@section('script')
	<!-- BEGIN APP LEVEL JQUERY SCRIPTS -->
	<script src="{{ asset('layouts/layout3/scripts/layout.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/quick-sidebar.min.js') }}" type="text/javascript"></script>
	<script src="https://maps.google.com/maps/api/js?key=AIzaSyB7_DMJSU-vQgoC8ALctFKg-59MsL9zWmE&libraries=places&.js" ></script>
	<script src="{{asset('js/jquery.geocomplete.js') }}"></script>
	<script src="{{ asset('plugins/gmaps/gmaps.min.js')}}" type="text/javascript"></script>
	<script src="{{ asset('modules/permohonan/form-pengajuan-controller.js') }}" type="text/javascript"></script>

	<!-- END APP LEVEL JQUERY SCRIPTS -->
	<script>
        $(function () {
            ////// geo location check you location if it is ok it will showlocation()
            // ////// if something wrong it will showerror()
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showLocation, showError,
                    {
                        enableHighAccuracy: true,
                        timeout: 10000 // 10s
                        //maximumAge : 0
                    }
                );
            }

            ////// geo location showlocation by passing latitude and longitude to geocomplete ////////

            function showLocation(position) {
                var latitude = position.coords.latitude;
                var longitude = position.coords.longitude;
                $("input[name=lat]").val(latitude);
                $("input[name=long]").val(longitude);

                getAddress(latitude, longitude);

                $("#geocomplete").geocomplete({
                    map: ".map_canvas",
                    markerOptions: {
                        draggable: true
                    },
                    details: "form ",
                    location: [latitude, longitude]
                    // location: [latLng.lat, latLng.ln]

                });

                $("#find").click(function(){
                    $("#geocomplete").trigger("geocode");
                }).click();

                ////// handle marker : if you click in any location then set lang and lat to inputs ////////

                $("#geocomplete").bind("geocode:click", function (event, latLng) {
                    $("input[name=lat]").val(latLng.lat());
                    $("input[name=long]").val(latLng.lng());
                    $(this).geocomplete('marker')
                        .setOptions({position:latLng,map:$(this).geocomplete("map")});

                    // $("#reset,#sr_lat,#sr_lng").show();
                });

                $("#geocomplete").bind("geocode:dragged", function(event, latLng){
                    $("input[name=lat]").val(latLng.lat());
                    $("input[name=long]").val(latLng.lng());
                    //$("#reset").show();

                    var geocoder	= new google.maps.Geocoder();							// create a geocoder object
                    var location	= new google.maps.LatLng(latLng.lat(), latLng.lng());		// turn coordinates into an object

                    geocoder.geocode({'latLng': location}, function (results, status) {
                        if(status == google.maps.GeocoderStatus.OK) {						// if geocode success
                            var ad =  results[0].formatted_address;
                            $("input[name=formatted_address]").val(ad);									// write address to field
                        } else {
                            alert("Geocode failure: " + status);								// alert any other error(s)
                            return false;
                        }
                    });

                });
                $("#find").click(function(){
                    $("#geocomplete").trigger("geocode");
                }).click();
            }
            ////// geo location display error type if something happens ////////

            function showError(error)
            {
                var x= document.getElementById("alert");

                switch(error.code)
                {
                    case error.PERMISSION_DENIED:
                        x.innerHTML="User denied the request for Geolocation.";
                        break;
                    case error.POSITION_UNAVAILABLE:
                        x.innerHTML="Location information is unavailable.";
                        break;
                    case error.TIMEOUT:
                        x.innerHTML="The request to get user location timed out.";
                        break;
                    case error.UNKNOWN_ERROR:
                        x.innerHTML="An unknown error occurred.";
                        break;
                }

                $(x).show();
            }

            // then set default latitude and longitude to geocomplete
            // because there is no location came from geo location /////

            $("#geocomplete").geocomplete({
                map: ".map_canvas",
                details: "form ",
                location: [-6.1802546,106.8254359] //balai kota
            });

            ////// handle marker : if you click in any location then set lang and lat to inputs ////////

            $("#geocomplete").bind("geocode:click", function (event, latLng) {
                $("input[name=lat]").val(latLng.lat());
                $("input[name=long]").val(latLng.lng());
                $(this).geocomplete('marker')
                    .setOptions({position:latLng,map:$(this).geocomplete("map")});

                $("#reset,#sr_lat,#sr_lng").show();
            });

            $("#find").click(function(){
                $("#geocomplete").trigger("geocode");
            }).click();


            /// use this function to get adress by passing latitude and longitude that came from geo location

            function getAddress(myLatitude,myLongitude) {

                var geocoder	= new google.maps.Geocoder();							// create a geocoder object
                var location	= new google.maps.LatLng(myLatitude, myLongitude);		// turn coordinates into an object

                geocoder.geocode({'latLng': location}, function (results, status) {
                    if(status == google.maps.GeocoderStatus.OK) {						// if geocode success
                        var ad =  results[0].formatted_address;
                        $("input[name=formatted_address]").val(ad);									// write address to field
                    } else {
                        alert("Geocode failure: " + status);								// alert any other error(s)
                        return false;
                    }
                });
            }
        });
	</script>
@endsection

@section('style')
	<style>
		.map_canvas {
			width: 100%;
			height: 400px;
			margin: 10px 20px 10px 0;
		}
	</style>
@endsection
