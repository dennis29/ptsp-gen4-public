@extends('pemohon::layouts.master')
@section('title',"Konfirmasi Izin")

@section('content')

  @include('pemohon::permohonan.step-pengajuan', ['current' => 3])

    <json-data model="formData">
      {{ collect($form)->toJson() }}
    </json-data>
    <div class="page-content-inner" ng-controller="FormKonfirmasiController" style="margin-bottom: 60px;">
      <div class="portlet-body" style="display: block;">
          <div class="row">
              <div class="col-md-6">
                  <form-input attributes="fields.tipe_izin"></form-input>
                  <form-input attributes="fields.izin"></form-input>
                  <form-input attributes="fields.nik"></form-input>
                  <form-input attributes="fields.nama_ktp"></form-input>
                  <form-input attributes="fields.alamat"></form-input>
                  <form-input attributes="fields.kelurahan"></form-input>
                  <form-input attributes="fields.kecamatan"></form-input>
                  <form-input attributes="fields.tempat_lahir"></form-input>
              </div>
              <div class="col-md-6">
                  <form-input attributes="fields.no_kk"></form-input>
                  <form-input attributes="fields.jenis_kelamin"></form-input>
                  <div class="row">
                      <div class="col-md-4">
                          <form-input attributes="fields.no_rt"></form-input>
                      </div>
                      <div class="col-md-4">
                          <form-input attributes="fields.no_rw"></form-input>
                      </div>
                  </div>
                  <form-input attributes="fields.kota"></form-input>
                  <form-input attributes="fields.provinsi"></form-input>
                  <form-input attributes="fields.tanggal_lahir"></form-input>
              </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="portlet light bordered" ng-repeat="syarat in formData.syarat" style="padding: 0;">
                <div class="portlet-body" style="display: block; padding: 20px;" ng-bind-html="syarat.name">
                </div>
                <div class="upload-syarat-container">
                  <h4 style="margin-bottom: 20px;">Upload Berkas Persyaratan:</h4>
                  <div class="row">
                    <div class="col-md-1 text-center" ng-repeat="berkas in syarat.media">
                      <a ng-href="@{{ berkas.url }}" target="_blank" style="display: block;height: 80px;overflow: hidden;">
                        <img src="@{{ berkas.thumbUrl }}" style="width: 100%;"/>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 text-center">
              <a href="javascript:;" class="btn btn-lg green-haze" ng-click="submit()"><i class="fa fa-check"></i>&nbsp;&nbsp;Ajukan Izin&nbsp;&nbsp;</a>
            </div>
          </div>

      </div>
    </div>
@endsection

@section('script')
	<script src="{{ asset('modules/pemohon/form-konfirmasi-controller.js') }}" type="text/javascript"></script>
@endsection

@section('style')
<style type="text/css">
.upload-syarat-container{
  padding: 5px 20px 20px;
  background: #f4f4f4;
}

.upload-syarat-button {
    height: 80px;
    border: 2px dashed #ababab;
    text-align: center;
    line-height: 78px;
    font-size: 26px;
    color: #ababab;
    cursor: pointer;
    outline: none;
}
</style>
@endsection
