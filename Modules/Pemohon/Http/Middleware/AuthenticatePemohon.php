<?php

namespace Modules\Pemohon\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Pemohon\Entities\UserProfile;

class AuthenticatePemohon
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$guards)
    {
        if (!Auth::guard($guards)->check()) {
            return redirect()->route('login');
        }

        // Redirect pemohon home page
        $user_id = Auth::user()->id;
        $user_profile = UserProfile::where('user_id', $user_id)->first();
        if ($user_profile != null) {
            if ($request->route()->uri == "pemohon/profil/create"){
                return redirect()->route('pemohon.dashboard');
            }
            return $next($request);
        }
        else{
            if ($request->route()->uri == "pemohon/profil/create"){
                return $next($request);
            }
            return redirect()->route('pemohon.create');
        }
    }
}
