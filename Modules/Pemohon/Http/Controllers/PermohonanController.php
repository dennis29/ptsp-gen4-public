<?php

namespace Modules\Pemohon\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Packages\Form\FormBuilder;
use Packages\Grid\GridBuilder;
use Illuminate\Support\Facades\Input;
use Modules\Pemohon\Entities\PengajuanIzin;
use Modules\Pemohon\Entities\PengajuanIzinSyarat;
use Modules\Perizinan\Entities\Izin;
use Modules\Perizinan\Entities\IzinSyarat;
use Modules\Perizinan\Entities\IzinRole;
use Modules\Pemohon\Entities\UserProfile;
use Auth;

class PermohonanController extends Controller
{
    public $url = '/pemohon/permohonan';
    public $title = 'Daftar Permohonan';
    public $create_label = 'Permohonan Baru';


    public function index()
    {
        $data = [
            'url'  => 'data-permohonan',
            'title' => $this->title,
            'create_label' => $this->create_label,
        ];
        return view('pemohon::permohonan.grid-permohonan', compact('data'));
    }


    public function data(){
        $user_id = Auth::user()->id;

        $query = PengajuanIzin::select(['pengajuan_izin.id','pengajuan_izin.tipe_perizinan', 'pengajuan_izin.created_at',
            'izin.name as izin_name','status.label as status'])
            ->join('status', 'status.slug', 'pengajuan_izin.status')
            ->join('izin', 'izin.id', 'pengajuan_izin.izin')
            ->where('pengajuan_izin.user', $user_id )
            ->where('pengajuan_izin.status','!=', 'selesai' );

        $grid = GridBuilder::source($query);
        $grid->url($this->url);
        $grid->title($this->title);
        $grid->create_label($this->create_label);

        $grid->filter('keyword', function($query, $param, $user_id){
            return $query->where('status.name', 'ilike', '%'.$param.'%')
                ->orWhere('pengajuan_izin.tipe_perizinan', 'ilike', '%'.$param.'%')
                ->orWhere('izin.name', 'ilike', '%'.$param.'%');
        });

        $grid->add('tipe_perizinan', 'Tipe Perizinan', true)->options([
          'perorangan' => 'Perorangan',
          'perusahaan' => 'Perusahaan',
        ]);
        $grid->add('izin_name', 'Izin', true);
        $grid->add('created_at', 'Tanggal Mengajukan', false);
        $grid->add('status', 'Status Perizinan', true);
        $data = $grid->build();

        return response()->json($data);
    }

    public function pencarian(){

      $form = $this->anyForm();
      $form = $form->build();

      $form['optionTipe'] = [
        ['id' => 'perorangan', 'name' => 'Perorangan'],
        ['id' => 'perusahaan', 'name' => 'Perusahaan'],
      ];
      $form['optionIzin'] = Izin::getOption();
      return view('pemohon::permohonan.form-pencarian', compact('form'));

    }

    public function formulir($id){

      $form = $this->formFormulir();
      $form = $form->build();
      $form['optionGender'] = ["1" => "Laki-laki", "2" => "Perempuan"];


      $pengajuanIzin = PengajuanIzin::find($id);
      if(empty($pengajuanIzin)){
        return;
      }
      $izin = Izin::find($pengajuanIzin->izin);
      // $syarat = IzinSyarat::where('izin', $izin->id)->orderBy('order', 'ASC')->get()->toArray();

      $syarat = PengajuanIzinSyarat::select([
        'pengajuan_izin_syarat.id',
        'izin_syarat.id AS izin_syarat',
        'izin_syarat.name',
        'izin_syarat.jenis_berkas'
      ])->where('pengajuan_izin_syarat.pengajuan_izin', $pengajuanIzin->id)
      ->join('izin_syarat', 'izin_syarat.id', '=', 'pengajuan_izin_syarat.izin_syarat')
      ->orderBy('izin_syarat.order', 'ASC')->get();

      $dataSyarat = [];
      foreach($syarat as $val){
        $media = [];
        $dataMedia = $val->getMedia('images');
        foreach($dataMedia as $vmedia){
          $media[] = [
            'id' => $vmedia->id,
            'name' => $vmedia->name,
            'url' => url($vmedia->getUrl()),
            'thumbUrl' => url($vmedia->getUrl('thumb'))
          ];
        }

        $dataSyarat[] = [
          'id' => $val->id,
          'izin_syarat' => $val->izin_syarat,
          'name' => $val->name,
          'jenis_berkas' => $val->jenis_berkas,
          'media' => $media
        ];
      }

      $form['data']['izin'] = $izin->name;
      $form['data']['tipe_izin'] = ucfirst($pengajuanIzin->tipe_perizinan);
      $form['syarat'] = $dataSyarat;
      $form['pengajuan_izin'] = $pengajuanIzin->id;

      return view('pemohon::permohonan.form-formulir', compact('form'));
    }

    public function konfirmasi($id){
      $form = $this->formFormulir();
      $form = $form->build();
      $form['optionGender'] = ["1" => "Laki-laki", "2" => "Perempuan"];


      $pengajuanIzin = PengajuanIzin::find($id);
      if(empty($pengajuanIzin)){
        return;
      }
      $izin = Izin::find($pengajuanIzin->izin);
      // $syarat = IzinSyarat::where('izin', $izin->id)->orderBy('order', 'ASC')->get()->toArray();

      $syarat = PengajuanIzinSyarat::select([
        'pengajuan_izin_syarat.id',
        'izin_syarat.id AS izin_syarat',
        'izin_syarat.name',
        'izin_syarat.jenis_berkas'
      ])->where('pengajuan_izin_syarat.pengajuan_izin', $pengajuanIzin->id)
      ->join('izin_syarat', 'izin_syarat.id', '=', 'pengajuan_izin_syarat.izin_syarat')
      ->orderBy('izin_syarat.order', 'ASC')->get();

      $dataSyarat = [];
      foreach($syarat as $val){
        $media = [];
        $dataMedia = $val->getMedia('images');
        foreach($dataMedia as $vmedia){
          $media[] = [
            'id' => $vmedia->id,
            'name' => $vmedia->name,
            'url' => url($vmedia->getUrl()),
            'thumbUrl' => url($vmedia->getUrl('thumb'))
          ];
        }

        $dataSyarat[] = [
          'id' => $val->id,
          'izin_syarat' => $val->izin_syarat,
          'name' => $val->name,
          'jenis_berkas' => $val->jenis_berkas,
          'media' => $media
        ];
      }

      $form['data']['izin'] = $izin->name;
      $form['data']['tipe_izin'] = ucfirst($pengajuanIzin->tipe_perizinan);
      $form['syarat'] = $dataSyarat;
      $form['pengajuan_izin'] = $pengajuanIzin->id;

      return view('pemohon::permohonan.form-konfirmasi', compact('form'));
    }

    public function submitIzin($id){
      $pengajuanIzin = PengajuanIzin::find($id);
      if(empty($pengajuanIzin)){
        return;
      }

      $izinRole = IzinRole::where('is_default', true)->first();
      $defaultStatus = $izinRole->status;

      $pengajuanIzin->status = $defaultStatus;
      $pengajuanIzin->save();

      return response()->json([
        'status' => true
      ]);
    }

	  public function savePencarian($id = null){
      $form = $this->anyForm($id);
      $form->pre(function($data){
        $data['user'] = Auth::user()->id;
        $data['status'] = "draft";
        return $data;
      });
      $save = $form->save();

      $syarat = IzinSyarat::where('izin', $form->model->izin)->orderBy('order', 'ASC')->get()->toArray();
      foreach($syarat as $val){
        PengajuanIzinSyarat::create([
          'pengajuan_izin' => $form->model->id,
          'izin_syarat' => $val['id'],
          'status' => 'draft'
        ]);
      }

      // $data = Izin::getIzinProsesByIzin($form->dataPost['izin']);
      $save['url'] =  route('permohonan.formulir', $form->model->id);

      return response()->json($save);
    }

    public function createPermit(){


    }

    public function updatePermit($id){

        $form = $this->anyForm($id);
        $form = $form->build();

    		$form['optionTipe'] = [
    			['id' => 'perorangan', 'name' => 'Perorangan'],
    			['id' => 'perusahaan', 'name' => 'Perusahaan'],
    		];
    		$form['optionIzin'] = Izin::getOption();
        return view('pemohon::permohonan.form-pengajuan', compact('form'));
    }

    public function anyForm($id = null){

        $data = PengajuanIzin::find($id);
        if(empty($data)){
            $data = new PengajuanIzin();
        }

        $form = FormBuilder::source($data);
        $form->title($this->title);
        $form->url('pencarian');
        $form->add('tipe_perizinan', 'Tipe Perizinan', 'select')->rule('required')->options('item.id as item.name for item in formData.optionTipe');
        $form->add('izin', 'Jenis Izin', 'select')->rule('required')->options('item.id as item.name for item in formData.optionIzin')->attributes([
    			'data-live-search' => true
    		]);
    		$form->add('lat', 'Lat', 'hidden')->rule('required')->attributes([
    			'id' => 'lat'
    		]);
    		$form->add('long', 'Long', 'hidden')->rule('required')->attributes([
    			'id' => 'long'
    		]);
    		$form->add('formatted_address', 'Address', 'hidden')->attributes([
          'id' => 'formatted_address'
        ]);

        return $form;
    }

    public function formFormulir(){
        $user_id = Auth::user()->id;

        $data = UserProfile::where('user_id', $user_id)->first();

        if(empty($data)){
            $data = new UserProfile();
        }

        $form = FormBuilder::source($data);
        $form->title($this->title);
        $form->url($this->url);

        $form->add('izin', 'Jenis Izin', 'textarea')->rule('required')->attributes([
            'readonly' => true
        ]);

        $form->add('tipe_izin', 'Tipe Pengajuan', 'text')->rule('required')->attributes([
            'readonly' => true
        ]);

        $form->add('nik', 'NIK', 'text')->rule('required')->attributes([
            'readonly' => true
        ]);
        $form->add('no_kk', 'No KK', 'text')->rule('required')->attributes([
            'readonly' => true
        ]);
        $form->add('nama_ktp', 'Nama Sesuai KTP', 'text')->attributes([
            'readonly' => true
        ]);
        $form->add('alamat', 'Alamat', 'text')->attributes([
            'readonly' => true
        ]);
        $form->add('no_rt', 'RT', 'text')->attributes([
            'readonly' => true
        ]);
        $form->add('no_rw', 'RW', 'text')->attributes([
            'readonly' => true
        ]);
        $form->add('kelurahan', 'Kelurahan', 'text')->attributes([
            'readonly' => true
        ]);
        $form->add('kecamatan', 'Kecamatan', 'text')->attributes([
            'readonly' => true
        ]);
        $form->add('kota', 'Kota/Kabupaten', 'text')->attributes([
            'readonly' => true
        ]);
        $form->add('provinsi', 'Provinsi', 'text')->attributes([
            'readonly' => true
        ]);
        $form->add('tempat_lahir', 'Tempat Lahir', 'text')->attributes([
            'readonly' => true
        ]);
        $form->add('tanggal_lahir', 'Tanggal Lahir', 'text')->attributes([
            'readonly' => true
        ]);
        $form->add('jenis_kelamin', 'Jenis Kelamin', 'radio')->options('formData.optionGender')->attributes([
            'readonly' => true
        ]);
        return $form;

    }

    public function addBerkas($pengajuanIzinId, $izinSyaratId, $mediaId){

      $pengajuanIzin = PengajuanIzin::find($pengajuanIzinId);
      $pengajuanIzinSyarat = PengajuanIzinSyarat::find($izinSyaratId);

      if(empty($pengajuanIzin) || empty($pengajuanIzinSyarat)){
        return;
      }

      $user = Auth::user();
      $media = $user->getMedia('images')->where('id', $mediaId)->first();

      $upload = $pengajuanIzinSyarat->copyMedia($media->getPath())->toMediaCollection('images');


      return response()->json([
        'id' => $upload->id,
        'name' => $upload->name,
        'url' => url($upload->getUrl()),
        'thumbUrl' => url($upload->getUrl('thumb'))
      ]);
    }

    public function deleteBerkas($mediaId, $izinSyaratId){
      $pengajuanIzinSyarat = PengajuanIzinSyarat::find($izinSyaratId);
      if(empty($pengajuanIzinSyarat)){
        return;
      }
      $media = $pengajuanIzinSyarat->getMedia('images')->where('id', $mediaId)->first();
      $media->delete();

      return response()->json([
        'status' => true
      ]);
    }
}
