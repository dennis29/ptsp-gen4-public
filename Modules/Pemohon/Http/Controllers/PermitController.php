<?php

namespace Modules\Pemohon\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Packages\Form\FormBuilder;
use Packages\Grid\GridBuilder;
use Illuminate\Support\Facades\Input;
use Modules\Pemohon\Entities\Permit;
use Illuminate\Support\Facades\DB;
use Auth;
use Modules\Pemohon\Entities\PengajuanIzin;

class PermitController extends Controller
{
    public $url = '/permit';
    public $title = 'Dashboard Perizinan';
    public $create_label = 'Buat Permohonan';

    public function index()
    {
        return view('pemohon::permit.grid-permit');
    }

    public function data(){
        $user_id = Auth::user()->id;

        $query = PengajuanIzin::select(['pengajuan_izin.id','pengajuan_izin.tipe_perizinan', 'pengajuan_izin.created_at','izin.name as izin_name','status.name as status'])
            ->join('status', 'status.slug', '=', 'pengajuan_izin.status')
            ->join('izin', 'izin.id', '=', 'pengajuan_izin.izin')
            ->where('pengajuan_izin.user', $user_id )
            ->where('pengajuan_izin.status', 'selesai' );

        $grid = GridBuilder::source($query);
        $grid->url($this->url);
        $grid->title($this->title);
        $grid->create_label($this->create_label);

        $grid->filter('keyword', function($query, $param){
            return $query->where('izin.name', 'ilike', '%'.$param.'%')
                ->orWhere('pengajuan_izin.tipe_perizinan', 'ilike', '%'.$param.'%');
        });

        $grid->add('tipe_perizinan', 'Tipe Perizinan', true);
        $grid->add('izin_name', 'Izin', true);
        $grid->add('created_at', 'Tanggal Mengajukan', false);

        $data = $grid->build();

        return response()->json($data);

    }

    public function create()
    {
        $form = $this->anyForm();
        $form = $form->build();
        $select_bidang = DB::table('bidang_perijinan')->select('id', 'nama_bidang')->get();

        return view('pemohon::form-permit', compact('form'));
    }
    public function anyForm($id = null){

        $data = Permit::find($id);
        if(empty($data)){
            $data = new Permit();
        }

        $form = FormBuilder::source($data);
        $form->title($this->title);
        $form->url($this->url);
        $form->add('nama_ijin', 'Nama Izin', 'text')->rule('required');
        $form->add('bidang_perijinan', 'Bidang Perizinan',
                'select',['choices' => ['x' => 'okay', 'y' => 'no'],
                    'selected' => '',
                    'empty_value' => '--Pilih Bidan Perizinan'
                ])->rule('required');

        return $form;
    }
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */

    public function update($id = null){
        $form = $this->anyForm($id);
        $form = $form->build();


        return view('pemohon::form-permit', compact('form'));
    }

    public function save($id = null){
        $form = $this->anyForm($id);
        $save = $form->save();

        return response()->json($save);
    }

    public function delete($id = null){
        $id = Input::get('id');
        Permit::whereIn('id', $id)->delete();

        return response()->json([
            'status' => true
        ]);
    }

    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('pemohon::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('pemohon::edit');
    }


    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
