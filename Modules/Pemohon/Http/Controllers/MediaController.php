<?php
namespace Modules\Pemohon\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Auth;

use Modules\Master\Entities\JenisBerkas;

class MediaController extends Controller
{

  public function index(){
    return view('pemohon::media');
  }


  public function upload(){

    $user = Auth::user();
    $upload = $user->addMediaFromRequest('file')->toMediaCollection('images');
    $index = Input::get('index');


    $file = [
      'id' => $upload->id,
      'url' => url($upload->getUrl()),
      'name' => $upload->name,
      'jenis_berkas' => $upload->jenis_berkas,
      'file_name' => $upload->file_name,
      'thumbUrl' => url($upload->getUrl('thumb')),
      'type' => $upload->type,
      'size' => $upload->size,
      'order' => $upload->order_column,
      'created_at' => $upload->created_at->format('Y-m-d H:i:s')
    ];

    return response([
      'status' => true,
      'index' => $index,
      'file' => $file
    ]);

  }

  public function delete($id){

    $user = Auth::user();
    $media = $user->getMedia('images')->where('id', $id)->first();
    $media->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function changeBerkas($id, $jb){

    $user = Auth::user();
    $media = $user->getMedia('images')->where('id', $id)->first();
    $media->jenis_berkas = $jb;
    $media->save();

    return response()->json([
      'status' => true
    ]);
  }

  public function getMedia(){

    $user = Auth::user();
    $mediaItem = $user->getMedia('images');
    $media = [];

    foreach($mediaItem as $val){

      $media[] = [
        'id' => $val->id,
        'url' => url($val->getUrl()),
        'name' => $val->name,
        'jenis_berkas' => $val->jenis_berkas,
        'file_name' => $val->file_name,
        'thumbUrl' => url($val->getUrl('thumb')),
        'type' => $val->mime_type,
        'size' => $val->size,
        'order' => $val->order_column,
        'created_at' => $val->created_at->format('d/m/Y H:i')
      ];
    }

    $optJenisBerkas = JenisBerkas::getOption();

    $data = [];
    $data['optionJenisBerkas'] = $optJenisBerkas;

    $dataResponse = [
      'media' => $media,
      'data' => $data
    ];

    return response()->json($dataResponse);

  }

}
