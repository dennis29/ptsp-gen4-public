<?php

namespace Modules\Pemohon\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Packages\Form\FormBuilder;
use Packages\Grid\GridBuilder;
use Illuminate\Support\Facades\Input;

use Modules\Pemohon\Entities\UserProfile;
use Auth;

class PemohonController extends Controller
{
    public $url = '/pemohon/profil';
    public $title = 'Pemohon';


    public function index(){
        $data = [
            'url'  => $this->url,
            'title' => "",
            'user_id' => Auth::user()->id,
            'views' => ['workflow::master.grid-pemohon'],
            'js' => ['modules/workflow/grid-pemohon-controller.js']
        ];

        return view('workflow::container-pemohon', compact('data'));
    }

    public function formpengajuan()
    {
        return view('pemohon::permit.formpengajuan');
    }

    public function create()
    {
        $form = $this->anyForm();
        $form = $form->build();
        $form['optionGender'] = ["1" => "Laki-laki", "2" => "Perempuan"];

        return view('pemohon::pemohon.form-pemohon', compact('form'));
    }

    public function save($id = null){
        $form = $this->anyForm();

        $nik = Input::post('nik');
        $no_kk = Input::post('no_kk');
        $user_id = Auth::user()->id;

        //cek registered pemohon
        $dataProfile = UserProfile::where(['nik' => $nik])->first();
        if($dataProfile != null) {
            $save = [
                'action' => 'create',
                'messages' => "NIK sudah terdaftar",
                'saved' => false
            ];
            return response()->json($save);
        }

        //validate NIK and No KK from Dukcapil
        $profile = $this->getProfileFromDukcapil();
        if($profile == null || $profile['NO_KK'] != $no_kk) {
            $save = [
                'action' => 'create',
                'messages' => "Cek kembali NIK atau No. KK Anda",
                'saved' => false
            ];
            return response()->json($save);
        }

        $form->pre(function($data) use($user_id, $profile){
            $data["user_id"] = $user_id;
            $data["nama_ktp"] = $profile['NAMA_LGKP'];
            $data["alamat"] = $profile['ALAMAT'];
            $data["no_rt"] = $profile['NO_RT'];
            $data["no_rw"] = $profile['NO_RW'];
            $data["kelurahan"] = $profile['NO_KEL'];
            $data["kecamatan"] = $profile['NO_KEC'];
            $data["kota"] = $profile['NO_KAB'];
            $data["provinsi"] = $profile['NO_PROP'];
            $data["tempat_lahir"] = $profile['TMPT_LHR'];
            $data["tanggal_lahir"] = $profile['TGL_LHR'];
            $data["jenis_kelamin"] = $profile['JENIS_KLMIN'];
            return $data;
        });

        $save = $form->save();

        return response()->json($save);
    }

    public function anyForm(){

        $user_id = Auth::user()->id;

        $data = UserProfile::where('user_id', $user_id)->first();

        if(empty($data)){
            $data = new UserProfile();
        }

        $form = FormBuilder::source($data);
        $form->title($this->title);
        $form->url($this->url);

        $form->add('nik', 'NIK', 'text')->rule('required');
        $form->add('no_kk', 'No KK', 'text')->rule('required');
        $form->add('nama_ktp', 'Nama Sesuai KTP', 'text')->attributes([
            'readonly' => true
        ]);
        $form->add('alamat', 'Alamat', 'text')->attributes([
            'readonly' => true
        ]);
        $form->add('no_rt', 'RT', 'text')->attributes([
            'readonly' => true
        ]);
        $form->add('no_rw', 'RW', 'text')->attributes([
            'readonly' => true
        ]);
        $form->add('kelurahan', 'Kelurahan', 'text')->attributes([
            'readonly' => true
        ]);
        $form->add('kecamatan', 'Kecamatan', 'text')->attributes([
            'readonly' => true
        ]);
        $form->add('kota', 'Kota/Kabupaten', 'text')->attributes([
            'readonly' => true
        ]);
        $form->add('provinsi', 'Provinsi', 'text')->attributes([
            'readonly' => true
        ]);
        $form->add('tempat_lahir', 'Tempat Lahir', 'text')->attributes([
            'readonly' => true
        ]);
        $form->add('tanggal_lahir', 'Tanggal Lahir', 'text')->attributes([
            'readonly' => true
        ]);
        $form->add('jenis_kelamin', 'Jenis Kelamin', 'radio')->options('formData.optionGender')->attributes([
            'readonly' => true
        ]);
        return $form;
    }

    public function data(){
        $data = UserProfile::where(['user_id' => Auth::user()->id]);

        if ($data != null){
            $data = $data->first();
        }

        $data_profile = ["data" => [$data->toArray()]];

        return response()->json($data_profile);
    }

    private function getProfileFromDukcapil(){
        $nik = Input::post('nik');
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://soadev.jakarta.go.id/rest/gov/dki/dukcapil/ws/getNIK?app=PTSP&pget=Pendaftaran&pusr=PTSP&nik=$nik",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_USERPWD => "ptspuser:ptspuser",
            CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            return null;
        } else {
            $result = json_decode($response, true);
            if (isset($result['DATA']['ANGGOTA'])) {
                return $result['DATA']['ANGGOTA'];
            } else{
                return null;
            }
        }
    }
}
