<?php

namespace Modules\Pemohon\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Packages\Form\FormBuilder;
use Packages\Grid\GridBuilder;
use Illuminate\Support\Facades\Input;

use Modules\Pemohon\Entities\UserProfile;
use Auth;

class DashboardController extends Controller
{
    public $url = '/pemohon';
    public $title = 'Dashboard';


    public function index(){

      return view('pemohon::dashboard');
    }
}
