<?php

Route::group(['middleware' => ['web'],  'prefix' => 'pemohon', 'namespace' => 'Modules\Pemohon\Http\Controllers'], function()
{
    Route::group(['middleware' => ['authenticate.pemohon']], function()
    {
        Route::get('/', ['uses' => 'DashboardController@index', 'as' => 'pemohon.dashboard']);

        Route::group(['prefix' => 'profil'], function() {
            Route::get('/', ['uses' => 'PemohonController@index', 'as' => 'pemohon.profil']);
            Route::get('create', ['uses' => 'PemohonController@create', 'as' => 'pemohon.create']);
            Route::get('changepassword', ['uses' => 'PemohonController@formizin', 'as' => 'pemohon.changepassword']);
            Route::get('data/{user_id?}', ['uses' => 'PemohonController@data', 'as' => 'pemohon.data']);
        });


        Route::group(['prefix' => 'media'], function(){
          Route::get('/', ['uses' => 'MediaController@index', 'as' => 'pemohon.media']);
          Route::any('upload', 'MediaController@upload');
          Route::any('data', 'MediaController@getMedia');
          Route::any('delete/{id}', 'MediaController@delete');
          Route::any('change-berkas/{id}/{jb}', 'MediaController@changeBerkas');
        });


        Route::get('permohonan', ['uses' => 'PermohonanController@index', 'as' => 'permohonan.dashboard']);
        Route::get('data-permohonan', ['uses' => 'PermohonanController@data', 'as' => 'permohonan.data']);
        Route::get('pencarian', ['uses' => 'PermohonanController@pencarian', 'as' => 'permohonan.pencarian']);
        Route::get('formulir/{id}', ['uses' => 'PermohonanController@formulir', 'as' => 'permohonan.formulir']);
        Route::get('konfirmasi/{id}', ['uses' => 'PermohonanController@konfirmasi', 'as' => 'permohonan.konfirmasi']);
        Route::post('pencarian/save-pencarian/{id?}', ['uses' => 'PermohonanController@savePencarian', 'as' => 'permohonan.savepencarian']);

        Route::get('add-berkas/{pengajuanIzin}/{izinSyaratId}/{mediaId}', ['uses' => 'PermohonanController@addBerkas', 'as' => 'permohonan.addberkas']);
        Route::get('delete-berkas/{mediaId}/{syaratId}', ['uses' => 'PermohonanController@deleteBerkas', 'as' => 'permohonan.deleteberkas']);
        Route::get('submit-izin/{pengajuanIzin}', ['uses' => 'PermohonanController@submitIzin', 'as' => 'permohonan.submitizin']);
    });

    Route::group(['prefix' => 'profil'], function() {
        Route::post('save/{id?}', ['uses' => 'PemohonController@save', 'as' => 'pemohon.save']);
    });
});
