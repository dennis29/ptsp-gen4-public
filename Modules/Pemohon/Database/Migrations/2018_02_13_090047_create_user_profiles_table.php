<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function (Blueprint $table) {

            $table->string('id', 25);
            $table->string('user_id', 100);
            $table->string('nik', 30);
            $table->string('no_kk', 30);
            $table->string('nama_ktp', 100);
            $table->string('alamat', 255);
            $table->string('no_rt', 5);
            $table->string('no_rw', 5);
            $table->string('kelurahan', 5);
            $table->string('kecamatan', 5);
            $table->string('kota', 5);
            $table->string('provinsi', 5);
            $table->string('tempat_lahir', 100);
            $table->string('tanggal_lahir', 25);
            $table->string('jenis_kelamin', 5);

            $table->string('created_by', 25)->nullable();
            $table->string('updated_by', 25)->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
    }
}
