<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserBerkasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_berkas', function (Blueprint $table) {
            $table->string('id', 25);
            $table->string('users', 25);
            $table->string('jenis_berkas', 25);
            $table->string('file', 255);

            $table->string('created_by', 25)->nullable();
            $table->string('updated_by', 25)->nullable();
            $table->timestamps();

            $table->foreign('users')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('jenis_berkas')
                ->references('id')->on('jenis_berkas')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_berkas');
    }
}
