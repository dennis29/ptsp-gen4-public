<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengajuanIzinTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuan_izin', function (Blueprint $table) {

            $table->string('id', 25);
            $table->string('user', 25);
            $table->string('izin', 25);
            $table->string('status', 25);
            $table->enum('tipe_perizinan',['perorangan','perusahaan']);
            $table->string('lat', 25);
            $table->string('long', 25);
			      $table->text('formatted_address')->nullable();

            $table->string('created_by', 25)->nullable();
            $table->string('updated_by', 25)->nullable();
            $table->timestamps();

            $table->foreign('user')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('izin')
                ->references('id')->on('izin')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('status')
                ->references('slug')->on('status')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuan_izin');
    }
}
