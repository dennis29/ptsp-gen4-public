<?php

namespace Modules\Output\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Modules\SSO\Entities\Module;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Module::create(array (
            array (
                'code' => 'outputdocument',
                'name' => 'Output Document',
            ),
        ));
    }
}
