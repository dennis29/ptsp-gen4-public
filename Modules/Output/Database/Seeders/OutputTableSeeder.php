<?php

namespace Modules\Output\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Modules\Workflow\Entities\OutputTable;

class OutputTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        OutputTable::create(array (
            array (
                'name' => 'Output Document',
                'route_page' => 'backoffice/output-document/show/{id_mapping_output?}/{id_pengajuan_izin?}',
                'route_conf' => 'backoffice/output-document/config/{id_mapping_output?}',
            ),
        ));
    }
}
