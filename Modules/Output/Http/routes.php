<?php

#### OLD ROUTE, WILL BE REMOVED

Route::group([
  'prefix' => 'backend',
  'middleware' => ['web', 'auth.admin', 'acl', 'auth.role'],
  'namespace' => 'Modules\Output\Http\Controllers'
], function () {

  // Output: Surat Keputusan
  Route::group(['prefix' => 'surat-keputusan'], function(){
    Route::get('show/{id_mapping_output?}/{id_pengajuan_izin?}', ['uses' => 'SuratKeputusanController@show', 'as' => 'suratkeputusan.show']);
    Route::get('config/{id_mapping_output?}', ['uses' => 'SuratKeputusanController@config', 'as' => 'suratkeputusan.config']);
//    Route::get('data', ['uses' => 'SuratKeputusanController@data', 'as' => 'suratkeputusan.data']);
    Route::any('save/{id_mapping_output?}', ['uses' => 'SuratKeputusanController@save', 'as' => 'suratkeputusan.save']);
  });

});


#### NEW ROUTE

//Route::group([
//  'prefix' => 'backoffice',
//  'middleware' => ['web', 'auth.admin', 'acl', 'auth.role'],
//  'namespace' => 'Modules\Output\Http\Controllers'
//], function () {
//
//  // Output: Output Document
//  Route::group(['prefix' => 'output-document'], function(){
//    Route::get('show/{id_mapping_output?}/{id_pengajuan_izin?}', ['uses' => 'SuratKeputusanController@show', 'as' => 'outputdocument.show']);
//    Route::get('config/{id_mapping_output?}', ['uses' => 'SuratKeputusanController@config', 'as' => 'outputdocument.config']);
//    Route::any('save/{id_mapping_output?}', ['uses' => 'SuratKeputusanController@save', 'as' => 'outputdocument.save']);
//  });
//
//});