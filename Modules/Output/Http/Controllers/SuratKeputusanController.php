<?php

namespace Modules\Output\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Packages\Form\FormBuilder;
use Packages\Grid\GridBuilder;
use Illuminate\Support\Facades\Input;

use Modules\Output\Entities\SuratKeputusan; 
use Modules\Token\Http\Controllers\PemohonIzinToken;

use Modules\Workflow\Entities\MappingOutputTable;

class SuratKeputusanController extends Controller
{
    public $url = '/backend/surat-keputusan';
    public $title = 'Surat Keputusan';
    
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('output::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('output::create');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id_mapping_output, $id_pengajuan_izin)
    {
      $data = SuratKeputusan::where('mapping_output', $id_mapping_output)->first();
//dd($data);
//dd($data->template);  
      $pemohonIzinToken = new PemohonIzinToken();
      
      $output = $pemohonIzinToken->token_replace($data->template, $id_pengajuan_izin);

      return view('output::show', ['template' => $output]);
    }
    
    /**
     * Surat Keputusan teplate configuration
     * @return type
     */
    public function config($id_mapping_output)
    {
//      dd($id_mapping_output);
      // Validasi jika $id_mapping_output-nya ternyata tidak ada
      $exist = MappingOutputTable::where('id', $id_mapping_output)->first();
//dd($exist);
      if(empty($exist)){
        return view('output::error-page', ['error_message' => 'Id not found!']);
      }

      $form = $this->anyForm($id_mapping_output);
      $form = $form->build();
      
      // Token List
      $pemohonIzinToken = new PemohonIzinToken();
      $tokens = $pemohonIzinToken->token_info();

      return view('output::config', compact('form'))->with('tokens', $tokens);
      
//      return view('output::config', compact('form'));
    }
    
    public function save($id_mapping_output){
      $form = $this->anyForm($id_mapping_output);
      $save = $form->save();

      return response()->json($save);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('output::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
    
    public function anyForm($id_mapping_output){
//dd($id_mapping_output);
      $data = SuratKeputusan::where('mapping_output', $id_mapping_output)->first();
//dd($data);
//dd($data->template);
      if(empty($data)){
        $data = new SuratKeputusan();
        $data['mapping_output'] = $id_mapping_output;
      }
//      dd($data);
      $form = FormBuilder::source($data);
//      dd($form);
      $form->title($this->title);
      $form->url($this->url);
      $form->add('mapping_output', 'Mapping Output', 'hidden');
      $form->add('template', '', 'textarea')->rule('required');
//dd($form);

      return $form;
    }
}
