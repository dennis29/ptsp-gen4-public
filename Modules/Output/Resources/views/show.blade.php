{!! $template !!}.

=======

<!--<p align="center">
    <img
        width="78"
        height="78"
        src="file://localhost/private/var/folders/mw/4mgcjsfj6gx8db65mdjkcbxm0000gn/T/TemporaryItems/msoclip/0/clip_image002.gif"
        alt="Description: jaya-raya"
    />
</p>
<p align="center">
    <strong>UNIT PELAKSANA PELAYANAN TERPADU SATU PINTU</strong>
</p>
<p align="center">
    <strong>KELURAHAN ………………….</strong>
</p>
<p align="center">
    <strong>SURAT IZIN USAHA PERDAGANGAN (SIUP) MIKRO</strong>
    <strong></strong>
</p>
<p align="center">
    <strong></strong>
</p>
<p align="center">
    NOMOR ………………………
</p>
<p align="center">
    <strong></strong>
</p>
<table border="0" cellspacing="0" cellpadding="0" width="504">
    <tbody>
        <tr>
            <td width="27" valign="top">
                <p>
                    1.
                </p>
            </td>
            <td width="198" colspan="6" valign="top">
                <p>
                    Nama Perusahaan
                </p>
            </td>
            <td width="14" valign="top">
                <p align="center">
                    :
                </p>
            </td>
            <td width="266" colspan="5" valign="top">
                <p>
                    <strong><u></u></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="27" valign="top">
                <p>
                    2.
                </p>
            </td>
            <td width="198" colspan="6" valign="top">
                <p>
                    Nama Penanggung Jawab &amp; Jabatan
                </p>
            </td>
            <td width="14" valign="top">
                <p align="center">
                    :
                </p>
            </td>
            <td width="266" colspan="5" valign="top">
                <p>
                    <strong><u></u></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="27" valign="top">
                <p>
                    3.
                </p>
            </td>
            <td width="198" colspan="6" valign="top">
                <p>
                    Alamat Perusahaan
                </p>
            </td>
            <td width="14" valign="top">
                <p align="center">
                    :
                </p>
            </td>
            <td width="266" colspan="5" valign="top">
                <p>
                    <strong><u></u></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="27" valign="top">
            </td>
            <td width="198" colspan="6" valign="top">
            </td>
            <td width="14" valign="top">
            </td>
            <td width="266" colspan="5" valign="top">
                <p>
                    <strong><u></u></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="27" valign="top">
            </td>
            <td width="198" colspan="6" valign="top">
            </td>
            <td width="14" valign="top">
            </td>
            <td width="266" colspan="5" valign="top">
                <p>
                    <strong><u></u></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="27" valign="top">
                <p>
                    4.
                </p>
            </td>
            <td width="198" colspan="6" valign="top">
                <p>
                    Nomor Telepon Perusahaan
                </p>
            </td>
            <td width="14" valign="top">
                <p align="center">
                    :
                </p>
            </td>
            <td width="266" colspan="5" valign="top">
                <p>
                    <strong><u></u></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="27" valign="top">
                <p>
                    5.
                </p>
            </td>
            <td width="198" colspan="6" valign="top">
                <p>
                    Nilai Kekayaan Bersih Perusahaan (Tidak Termasuk Nilai
                    Tanah dan Bangunan )
                </p>
            </td>
            <td width="14" valign="top">
                <p align="center">
                    :
                </p>
            </td>
            <td width="266" colspan="5" valign="top">
                <p>
                    <strong><u></u></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="27" valign="top">
                <p>
                    6.
                </p>
            </td>
            <td width="198" colspan="6" valign="top">
                <p>
                    Kelembagaan
                </p>
            </td>
            <td width="14" valign="top">
                <p align="center">
                    :
                </p>
            </td>
            <td width="266" colspan="5" valign="top">
                <p>
                    <strong><u></u></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="27" valign="top">
                <p>
                    7.
                </p>
            </td>
            <td width="198" colspan="6" valign="top">
                <p>
                    Kegiatan Usaha (KBLI)
                </p>
            </td>
            <td width="14" valign="top">
                <p align="center">
                    :
                </p>
            </td>
            <td width="266" colspan="5" valign="top">
                <p>
                    <strong><u></u></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="27" valign="top">
                <p>
                    8.
                </p>
            </td>
            <td width="198" colspan="6" valign="top">
                <p>
                    Barang / Jasa Dagangan Utama
                </p>
            </td>
            <td width="14" valign="top">
                <p align="center">
                    :
                </p>
            </td>
            <td width="266" colspan="5" valign="top">
                <p>
                    <strong><u></u></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="27" rowspan="3" valign="top">
                <p>
                    <strong><u></u></strong>
                </p>
            </td>
            <td width="12" valign="top">
                <p>
                    -
                </p>
            </td>
            <td width="465" colspan="11" valign="top">
                <p>
                    <strong><u></u></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="12" valign="top">
                <p>
                    -
                </p>
            </td>
            <td width="465" colspan="11" valign="top">
                <p>
                    <strong><u></u></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="12" valign="top">
                <p>
                    -
                </p>
            </td>
            <td width="465" colspan="11" valign="top">
                <p>
                    <strong><u></u></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="504" colspan="13" valign="top">
                <p>
                    <strong><u></u></strong>
                </p>
            </td>
        </tr>
        <tr>
            <td width="504" colspan="13" valign="top">
                <p>
                    SURAT IZIN USAHA PERDAGANGAN INI BERLAKU DI SELURUH WILAYAH
                    REPUBLIK INDONESIA, SELAMA PERUSAHAAN MENJALANKAN USAHANYA
                    SESUAI IZIN INI DAN WAJIB DIDAFTAR ULANG (DIPERPANJANG)
                    SETIAP 5 (LIMA) TAHUN SEKALI.
                </p>
            </td>
        </tr>
        <tr>
            <td width="504" colspan="13" valign="top">
            </td>
        </tr>
        <tr>
            <td width="198" colspan="4" valign="top">
                <p>
                    SIUP ini diberikan dengan ketentuan
                </p>
            </td>
            <td width="18" colspan="2" valign="top">
                <p align="center">
                    :
                </p>
            </td>
            <td width="288" colspan="7" valign="top">
            </td>
        </tr>
        <tr>
            <td width="27" valign="top">
                <p>
                    1.
                </p>
            </td>
            <td width="477" colspan="12" valign="top">
                <p>
                    Pemilik SIUP wajib menyampaikan laporan kegiatan usahanya
                    setiap 6 (enam) bulan kepada Pejabat Penerbit SIUP.
                </p>
            </td>
        </tr>
        <tr>
            <td width="27" valign="top">
                <p>
                    2.
                </p>
            </td>
            <td width="477" colspan="12" valign="top">
                <p>
                    SIUP akan dicabut apabila tidak mengikuti
                    ketentuan/peraturan perizinan yang berlaku di bidang usaha
                    perdagangan.
                </p>
            </td>
        </tr>
        <tr>
            <td width="198" colspan="4" valign="top">
                <p>
                    SIUP ini dilarang digunakan untuk melakukan
                </p>
            </td>
            <td width="18" colspan="2" valign="top">
                <p align="center">
                    :<strong><u></u></strong>
                </p>
            </td>
            <td width="288" colspan="7" valign="top">
            </td>
        </tr>
        <tr>
            <td width="27" valign="top">
                <p>
                    a.
                </p>
            </td>
            <td width="477" colspan="12" valign="top">
                <p>
                    Kegiatan usaha perdagangan yang tidak sesuai dengan
                    kelembagaan dan/atau kegiatan usaha, sebagaimana yang
                    tercantum di dalam SIUP;
                </p>
            </td>
        </tr>
        <tr>
            <td width="27" valign="top">
                <p>
                    b.
                </p>
            </td>
            <td width="477" colspan="12" valign="top">
                <p>
                    Kegiatan usaha yang mengaku kegiatan perdagangan, untuk
                    menghimpun dana dari masyarakat dengan menawarkan janji
                    keuntungan yang tidak wajar (<em>money game</em>); atau
                </p>
            </td>
        </tr>
        <tr>
            <td width="27" valign="top">
                <p>
                    c.
                </p>
            </td>
            <td width="477" colspan="12" valign="top">
                <p>
                    Kegiatan usaha perdagangan lainnya (selain butir a dan b)
                    yang telah diatur melalui ketentuan peraturan
                    perundang-undangan tersendiri.
                </p>
            </td>
        </tr>
        <tr>
            <td width="504" colspan="13" valign="top">
                <table cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td
                                width="65"
                                height="75"
                                align="left"
                                valign="top"
                            >
                                <table
                                    cellpadding="0"
                                    cellspacing="0"
                                    width="100%"
                                >
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div>
                                                    <p align="center">
                                                        Foto 3X4
                                                    </p>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td width="212" colspan="5" valign="top">
            </td>
            <td width="140" colspan="6" valign="top">
                <p>
                    Dikeluarkan di
                </p>
            </td>
            <td width="14" valign="top">
                <p align="center">
                    :
                </p>
            </td>
            <td width="140" valign="top">
                <p>
                    JAKARTA
                </p>
            </td>
        </tr>
        <tr>
            <td width="212" colspan="5" valign="top">
            </td>
            <td width="140" colspan="6" valign="top">
                <p>
                    Pada tanggal
                </p>
            </td>
            <td width="14" valign="top">
                <p align="center">
                    :
                </p>
            </td>
            <td width="140" valign="top">
            </td>
        </tr>
        <tr>
            <td width="212" colspan="5" valign="top">
            </td>
            <td width="140" colspan="6" valign="top">
            </td>
            <td width="14" valign="top">
            </td>
            <td width="140" valign="top">
            </td>
        </tr>
        <tr>
            <td width="504" colspan="13" valign="top">
            </td>
        </tr>
        <tr>
            <td width="257" colspan="10" valign="top">
            </td>
            <td width="248" colspan="3" valign="top">
            </td>
        </tr>
        <tr>
            <td width="257" colspan="10" valign="top">
            </td>
            <td width="248" colspan="3" valign="top">
                <p align="center">
                    KEPALA UNIT PELAKSANA PTSP
                </p>
                <p align="center">
                    KELURAHAN ................
                </p>
            </td>
        </tr>
        <tr>
            <td width="257" colspan="10" valign="top">
            </td>
            <td width="248" colspan="3" valign="top">
            </td>
        </tr>
        <tr>
            <td width="257" colspan="10" valign="top">
            </td>
            <td width="248" colspan="3" valign="top">
            </td>
        </tr>
        <tr>
            <td width="177" colspan="3" valign="top">
            </td>
            <td width="79" colspan="6" valign="top">
            </td>
            <td width="248" colspan="4" valign="top">
            </td>
        </tr>
        <tr>
            <td width="257" colspan="10" valign="top">
            </td>
            <td width="248" colspan="3" valign="top">
                <p align="center">
                    NAMA ...................
                </p>
            </td>
        </tr>
        <tr>
            <td width="257" colspan="10" valign="top">
            </td>
            <td width="248" colspan="3" valign="top">
                <p align="center">
                    NIP……………………………..
                </p>
            </td>
        </tr>
        <tr height="0">
            <td width="27">
            </td>
            <td width="15">
            </td>
            <td width="137">
            </td>
            <td width="21">
            </td>
            <td width="13">
            </td>
            <td width="5">
            </td>
            <td width="9">
            </td>
            <td width="14">
            </td>
            <td width="18">
            </td>
            <td width="0">
            </td>
            <td width="94">
            </td>
            <td width="14">
            </td>
            <td width="139">
            </td>
        </tr>
    </tbody>
</table>-->