@extends('core::layout')

@section('content')
  <div class="col-md-12">
    <form name="form">
    <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green-sharp bold">ERROR</div>
      </div>
      <div class="portlet-body">
        <div class="row margin-bottom-20">
          <div class="col-md-8 col-md-offset-2">
          
            {{ $error_message }}
          
          </div>
        </div>
      </div>
    </div>
    </form>
  </div>
@endsection


