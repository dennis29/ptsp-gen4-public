
@extends('core::layout')

@section('content')
<json-data model="formData">
{{ collect($form)->toJson() }}
</json-data>
<div class="row" ng-controller="FormSuratKeputusanController">
  <div class="col-md-12">
    <form name="form">
    <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green-sharp bold">Template Surat Keputusan</div>

        <div class="actions pull-right">
          <a href="@{{ formData.url }}" class="btn btn-sm btn-default" ><i class="fa fa-angle-left"></i>&nbsp;&nbsp;Kembali</a>
        @if(Auth::user()->has('delete'))
          <a href="javascript:;" class="btn btn-sm btn-danger" link="@{{ formData.url+'/delete' }}" delete-confirm="@{{ formData.data.id }}" ng-hide="formData.action == 'create'">Hapus&nbsp;&nbsp;<i class="fa fa-trash"></i></a>
        @endif

        @if(Auth::user()->has('create') || Auth::user()->has('update'))
          <button class="btn btn-sm green mt-ladda-btn ladda-button" type="submit" ng-click="saveAndReturn($event, formData.url)"  data-style="zoom-out" ng-hide="formData.action == 'view'"><span class="ladda-label">Simpan dan Kembali&nbsp;&nbsp;<i class="fa fa-mail-reply"></i></span></button>
          <button class="btn btn-sm green-haze mt-ladda-btn ladda-button" type="submit" ng-click="save($event)" data-style="zoom-out" ng-hide="formData.action == 'view'"><span class="ladda-label">Simpan&nbsp;&nbsp;<i class="fa fa-floppy-o"></i></span></button>
        @endif
        </div>

      </div>
      <div class="portlet-body">
        <div class="row margin-bottom-20">
          <div class="col-md-12">
            <form-input attributes="fields.mapping_output"></form-input>
            <!--<textarea ng-model="data.template" name="template"></textarea>-->
            <form-input attributes="fields.template"></form-input>

          </div>
        </div>
      </div>
      
      <div class="portlet box blue">
          <div class="portlet-title">
              <div class="caption">
                  <i class="fa fa-gift"></i>Replacement Token </div>
              <div class="tools">
                  <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
              </div>
          </div>
          <div class="portlet-body" style="display: block;">
              <div class="table-scrollable">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                          <th><small> Token </small></th>
                          <th><small> Name </small></th>
                          <th><small> Description </small></th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach ( $tokens as $token => $value )
                        <tr>
                          <th><small> [{{ $token }}] </small></th>
                          <td><small> {{ $value['name'] }} </small></td>
                          <td><small> {{ $value['description'] }} </small></td>
                        </tr>
                      @endforeach
                    </tbody>
                </table>
              </div>
          </div>
      </div>

    </div>
    </form>
  </div>
</div>
@endsection

@section('script')
<script src="{{ asset('modules/output/form-surat-keputusan-controller.js') }}" type="text/javascript"></script>

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
<script type="text/javascript">
  setTimeout(function(){
    $('textarea').ckeditor({
        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
        filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=' + $("input[name=_token]").val(),
        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
        filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token=' + $("input[name=_token]").val()
    });
  },100);
//     $('textarea').ckeditor(); // if class is prefered.
//    $('textarea').ckeditor({
//        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
//        filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=' + $("input[name=_token]").val(),
//        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
//        filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token=' + $("input[name=_token]").val()
//    });
</script>

@endsection

