<?php

namespace Modules\Output\Entities;

use Packages\Model\BaseModel;

class SuratKeputusan extends BaseModel
{
    protected $table = 'surat_keputusan';
    protected $fillable = ['id', 'mapping_output', 'template'];
    
//    public function findMappingOutput($id_mapping_output) {
//      
//    }
}