<?php

namespace Modules\Perizinan\Entities;

use Packages\Model\BaseModel;

class IzinProses extends BaseModel
{
    protected $table = 'izin_proses';
    protected $fillable = ['id', 'izin', 'proses', 'name', 'roles', 'estimated_time', 'order'];

    public function izinProsesStatusTxs()
    {
        return $this->hasMany('Modules\Perizinan\Entities\IzinProsesStatusTx');
    }
}
