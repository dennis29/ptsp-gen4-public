<?php

namespace Modules\Perizinan\Entities;

use Packages\Model\BaseModel;

class IzinSyarat extends BaseModel
{
    protected $table = 'izin_syarat';
    protected $fillable = ['id', 'izin', 'name', 'jenis_berkas', 'description', 'order'];

}
