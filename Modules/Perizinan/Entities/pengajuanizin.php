<?php

namespace Modules\Perizinan\Entities;

use Packages\Model\BaseModel;

class pengajuanizin extends BaseModel
{
    protected $table = 'pengajuanizins';
    protected $fillable = ['id', 'tanggal_mengajukan', 'izin', 'status', 'users','created_by','updated_by','created_at','updated_at'];
}
