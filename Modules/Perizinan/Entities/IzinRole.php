<?php

namespace Modules\Perizinan\Entities;

use Packages\Model\BaseModel;

class IzinRole extends BaseModel
{
  protected $table = 'izin_role';
  protected $fillable = ['id', 'izin', 'izin_proses', 'route', 'role', 'is_default', 'status', 'next_status', 'next_route'];
}
