<?php

namespace Modules\Perizinan\Entities;

use Packages\Model\BaseModel;

class IzinCategory extends BaseModel
{
    protected $table = 'izin_category';
    protected $fillable = ['id', 'code', 'bidang', 'name'];

    public static function getOption(){
      $izinCategory = IzinCategory::select(['id', 'code', 'name'])->orderBy('code')->get()->toArray();

      $izinCategory = collect($izinCategory)->map(function($data){
         $data['name'] = $data['code'].'. '.$data['name'];
         return $data;
      })->toArray();

      return $izinCategory;
    }
}
