<?php

namespace Modules\Perizinan\Entities;

use Packages\Model\BaseModel;

class IzinBerkas extends BaseModel
{
    protected $table = 'izin_berkas';
    protected $fillable = ['id', 'izin_proses', 'jenis_berkas', 'file'];
}
