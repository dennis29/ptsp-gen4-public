<?php

namespace Modules\Perizinan\Entities;

use Packages\Model\BaseModel;

class IzinProsesStatus extends BaseModel
{
    protected $table = 'izin_proses_status';
    protected $fillable = ['id', 'izin_proses', 'status', 'next_proses', 'order'];
}
