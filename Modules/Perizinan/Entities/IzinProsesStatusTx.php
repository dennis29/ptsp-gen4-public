<?php

namespace Modules\Perizinan\Entities;

use Packages\Model\BaseModel;

class IzinProsesStatusTx extends BaseModel
{
    protected $table = 'izin_proses_status_tx';
    protected $fillable = ['id', 'pengajuan_id', 'izin_proses', 'status'];

    public function izin()
    {
        return $this->belongsTo('Modules\Perizinan\Entities\Izin');
    }

    public function izinProses()
    {
        return $this->belongsTo('Modules\Perizinan\Entities\IzinProses');
    }

    public function status()
    {
        return $this->belongsTo('Modules\Workflow\Entities\Status');
    }
}