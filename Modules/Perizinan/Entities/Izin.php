<?php

namespace Modules\Perizinan\Entities;

use Packages\Model\BaseModel;
use Modules\Perizinan\Entities\IzinProses;
use Modules\Perizinan\Entities\IzinProsesStatus;

class Izin extends BaseModel
{
    protected $table = 'izin';
    protected $fillable = ['id', 'code', 'izin_category', 'name', 'description', 'syarat_description',
    'waktu_penyelesaian', 'waktu_penyelesaian_tipe', 'biaya_retribusi', 'masa_berlaku', 'masa_berlaku_tipe'];

	public static function getOption(){
    return Izin::select(['id', 'name'])->get()->toArray();
  }

  public static function getDefaultStatus($izin = null){
    $izinProses = IzinProses::where('izin', $izin)->orderBy('order', 'ASC')->first();

    $status = null;
    if(!empty($izinProses)){
      $izinProsesStatus = IzinProsesStatus::where('izin_proses', $izinProses->id)->orderBy('order', 'ASC')->first();
      if(!empty($izinProsesStatus)){
          $data = [
                "status" => $izinProsesStatus->status,
                "izin_proses" => $izinProsesStatus->izin_proses
                ];
      }

    }
    return $data;
  }
    public static function getIzinProsesByIzin($izin = null){
        $data = IzinProses::select(['proses.route'])
            ->join('proses', 'proses.id', 'izin_proses.proses')
            ->where('izin_proses.izin',$izin)
            ->orderBy('izin_proses.order', 'ASC')
            ->first();

        return $data;
    }

    public static function getIzinProsesStatusByIzinProses($izin_proses_id = null){
        $data = IzinProsesStatus::select(['status.slug','status.label'])
            ->join('status', 'izin_proses_status.status', 'status.slug')
            ->where('izin_proses',$izin_proses_id)
            ->orderBy('order', 'ASC')->get();

        return $data;
    }

    public function izinProsesStatusTxs()
    {
        return $this->hasMany('Modules\Perizinan\Entities\IzinProsesStatusTx');
    }

    public function pengajuanIzins()
    {
        return $this->hasMany('Modules\Pemohon\Entities\PengajuanIzin');
    }
}
