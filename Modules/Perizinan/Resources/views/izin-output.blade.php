<div ng-repeat="data in dataOutput" ng-init="idx = $index">
  <div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
          @{{ data.name || '(Output Baru)' }}</div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="javascript:;" class="remove" ng-click="deleteOutput(idx)"> </a>
        </div>
    </div>
    <div class="portlet-body">
      <div class="row margin-top-15">
        <div class="col-md-8">
          <form-input attributes="fieldsOutput.name" model="data.name" name="@{{ 'name'+idx }}"></form-input>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <form-input attributes="fieldsOutput.output" model="data.output" name="@{{ 'output'+idx }}"></form-input>
        </div>
        <div class="col-md-4">
          <form-input attributes="fieldsOutput.order" model="data.order" name="@{{ 'order'+idx }}"></form-input>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <form-input attributes="fieldsOutput.roles" model="data.roles" name="@{{ 'roles'+idx }}"></form-input>
        </div>
      </div>
      <div class="row">
        <div class="col-md-8">
          <form-input attributes="fieldsOutput.email_penerima" model="data.email_penerima" name="@{{ 'email_penerima'+idx }}"></form-input>
        </div>
      </div>
    </div>
  </div>
</div>
<button class="btn btn-primary btn-sm" ng-click="addOutput()" type="button">Tambah Output <i class="fa fa-plus"></i></button>