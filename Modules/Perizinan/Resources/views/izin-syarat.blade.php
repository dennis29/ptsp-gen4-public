<div class="portlet" style="box-shadow: none;">

  <div class="portlet-body">
    <button class="btn btn-sm btn-success" ng-click="addSyarat()"> Tambah <i class="fa fa-plus"></i></button>
    <br/><br/>
      <table class="table table-hover dataTable margin-bottom-20" border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th style="width: 10px;"></th>
            <th style="width: 60%;">
              <div>
                Persyaratan
              </div>
            </th>
            <th style="width: 120px;">
                Jenis Berkas
            </th>
            <th class="text-center"></th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="syarat in dataSyarat" ng-dblclick="editSyarat($index)">
            <td style="vertical-align: top;">@{{ $index + 1 }}.</td>
            <td ng-bind-html="syarat.name">

            </td>
            <td style="vertical-align: top;">
              @{{ (formData.optionJenisBerkas | filter:{id:syarat.jenis_berkas})[0].name || 'Pilih Janis Berkas' }}
            </td>
            <td class="text-right grid-actions" style="width: 60px; vertical-align: top;">
              <a href="javascript:;" class="tooltips" data-original-title="Edit" data-placement="left" ng-click="editSyarat($index)">
                <i class="fa fa-edit"></i>
              </a>&nbsp;&nbsp;
              <a href="javascript:;" class="tooltips" data-original-title="Delete" data-placement="left" ng-click="deleteSyarat($index)">
                <i class="fa fa-trash"></i>
              </a>
            </td>
          </tr>
        </tbody>
      </table>
  </div>
</div>

<script type="text/ng-template" id="formSyaratModal.html">
  <form name="form">
    <div class="modal-header">
      <h3 class="modal-title">Tambah Persyaratan</h3>
    </div>
    <div class="modal-body">
      <div class="row margin-bottom-20">
        <div class="col-md-12">

          <h5>Persyaratan:</h5>
          <form-input attributes="fields.name"></form-input><br/>
          <form-input attributes="fields.jenis_berkas"></form-input>
          <form-input attributes="fields.description"></form-input>
        </div>
      </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary btn-sm" type="button" ng-click="save($event)"><i class="fa fa-floppy-o"></i> Simpan</button>
        <button class="btn btn-default btn-sm" type="button" ng-click="close()"><i class="fa fa-times"></i> Batal</button>
    </div>
  </form>
</script>
