<div ng-repeat="data in dataProcess" ng-init="idx = $index">
  <div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
          @{{ data.name || '(Proses Baru)' }}</div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="javascript:;" class="remove" ng-click="deleteProcess(idx)"> </a>
        </div>
    </div>
    <div class="portlet-body">
      <div class="row margin-top-15">
        <div class="col-md-8">
          <form-input attributes="fieldsProcess.name" model="data.name" name="@{{ 'name'+idx }}"></form-input>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <form-input attributes="fieldsProcess.proses" model="data.proses" name="@{{ 'proses'+idx }}"></form-input>
        </div>
        <div class="col-md-4">
          <form-input attributes="fieldsProcess.estimated_time" model="data.estimated_time" name="@{{ 'estimated_time'+idx }}"></form-input>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <form-input attributes="fieldsProcess.roles" model="data.roles" name="@{{ 'roles'+idx }}"></form-input>
        </div>
      </div>
      <br/>
      <h4 class="title">Proses Status</h4>

      <div class="row" ng-repeat="st in data.status">
        <div class="col-md-4">
          <form-input attributes="fieldsProcess.status" model="st.status" name="@{{ 'status'+$index+idx }}"></form-input>
        </div>
        <div class="col-md-4">
          <form-input attributes="fieldsProcess.next_proses" model="st.next_proses" name="@{{ 'next_proses'+$index+idx }}"></form-input>
        </div>
        <div class="col-md-2 pull-right text-right">
          <a href="javascript:;" class="font-red" ng-click="deleteStatus(idx, $index)"><i class="fa fa-trash"></i></a>
        </div>
      </div>
      <button class="btn btn-success btn-xs" ng-click="addStatus(idx)" type="button">Tambah Status <i class="fa fa-plus"></i></button>
    </div>
  </div>
</div>
<button class="btn btn-primary btn-sm" ng-click="addProcess()" type="button">Tambah Proses <i class="fa fa-plus"></i></button>
