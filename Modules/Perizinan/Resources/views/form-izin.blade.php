@extends('core::layout')

@section('content')
<json-data model="formData">
{{ collect($form)->toJson() }}
</json-data>
<div class="row" ng-controller="FormIzinController">
  <div class="col-md-12">
    <form name="form">
    <div class="portlet light bordered">

      <div class="portlet-body">

        <div class="row margin-bottom-20">
          <div class="col-md-6 col-md-offset-6">
            @component('core::components.form-action')
            @endcomponent
          </div>
        </div>
        <h4 class="title">@{{ data.code +' '+data.name }}</h4>
        <div clas="row">
          <div class="col-md-12">

        </div>
        </div>
        <div class="row margin-bottom-20">
          <div class="col-md-12">
            <div class="tabbable-line">
              <ul class="nav nav-tabs ">
                  <li class="active">
                      <a href="#izin" data-toggle="tab">Master Izin </a>
                  </li>
                  <li>
                      <a href="#persyaratan" data-toggle="tab"> Persyaratan </a>
                  </li>
                  <li>
                      <a href="#mapping_proses" data-toggle="tab"> Mapping Proses </a>
                  </li>
                  <li>
                      <a href="#mapping_output" data-toggle="tab"> Mapping Output </a>
                  </li>
              </ul>
              <div class="tab-content">
                  <div class="tab-pane active" id="izin">
                    <div class="row">
                      <div class="col-md-7">
                        <form-input attributes="fields.izin_category"></form-input>
                        <form-input attributes="fields.code"></form-input>
                        <form-input attributes="fields.name"></form-input>

                      </div>
                      <div class="col-md-5">

                        <div class="row">
                          <div class="col-md-4">
                            <form-input attributes="fields.waktu_penyelesaian"></form-input>
                          </div>
                          <div class="col-md-8">
                            <form-input attributes="fields.waktu_penyelesaian_tipe"></form-input>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-4">
                            <form-input attributes="fields.masa_berlaku"></form-input>
                          </div>
                          <div class="col-md-8">
                            <form-input attributes="fields.masa_berlaku_tipe"></form-input>
                          </div>
                        </div>
                        <form-input attributes="fields.biaya_retribusi"></form-input>
                      </div>
                      <div class="col-md-12">
                        <br/>
                        <h5>Keterangan:</h5>
                        <form-input attributes="fields.description"></form-input>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane" id="persyaratan">
                    <div ng-controller="IzinPersyaratanController">
                      @include('perizinan::izin-syarat')
                    </div>
                    <br/>
                    <h5>Keterangan Persyaratan:</h5>
                    <form-input attributes="fields.syarat_description"></form-input>
                  </div>
                  <div class="tab-pane" id="mapping_proses" ng-controller="IzinProsesController">
                    @include('perizinan::izin-proses')
                  </div>
                  <div class="tab-pane" id="mapping_output" ng-controller="IzinOutputController">
                    @include('perizinan::izin-output')
                  </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
    </form>
  </div>
</div>
@endsection

@section('script')
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/angularjs/angular-ckeditor/angular-ckeditor.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('modules/perizinan/form-izin-controller.js') }}" type="text/javascript"></script>
<script src="{{ asset('modules/perizinan/izin-persyaratan-controller.js') }}" type="text/javascript"></script>
<script src="{{ asset('modules/perizinan/izin-proses-controller.js') }}" type="text/javascript"></script>
<script src="{{ Module::asset('perizinan:izin-output-controller.js') }}" type="text/javascript"></script>

@endsection
