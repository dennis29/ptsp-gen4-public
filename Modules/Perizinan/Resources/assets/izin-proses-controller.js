/* Setup form controller */
angular.module('MetronicApp').controller('IzinProsesController', ['$rootScope', '$scope', 'settings', '$http', '$filter', function($rootScope, $scope, settings, $http, $filter) {

    $scope.fieldsProcess = {};

    $scope.initProcess = function(){

      $scope.$parent.dataProcess = $scope.formData.dataProses;

      // Define Field Process
      $scope.fieldsProcess = {
        name: {
          type: 'text',
          label: 'Nama Proses',
          name: 'name',
          attributes: {
            required: ""
          },
        },
        proses: {
          type: 'select',
          label: 'Proses',
          name: 'proses',
          options: 'item.id as item.name for item in formData.optionProses',
          attributes: {
            'data-live-search' : true,
            required: ""
          },
        },
        roles: {
          type: 'multiple',
          label: 'Role',
          name: 'roles',
          options: 'item.slug as item.name for item in formData.optionRole',
          attributes: {
            required: ""
          },
        },
        estimated_time: {
          type: 'number',
          name: 'estimated_time',
          label: 'Perkiraan Waktu (Hari)'
        },
        status: {
          type: 'select',
          label: 'Status',
          name: 'status',
          options: 'item.slug as item.label for item in formData.optionStatus',
          attributes: {
            'data-live-search' : true
          },
        },
        next_proses: {
          type: 'select',
          name: 'next_proses',
          label: 'Proses Selanjutnya',
          options: 'item.id as item.name for item in formData.optionProses',
          attributes: {
            'data-live-search' : true
          }
        },
        jenis_berkas: {
          type: 'select',
          name: 'jenis_berkas',
          label: 'Jenis Berkas',
          options: 'item.id as item.name for item in formData.optionJenisBerkas',
          attributes: {
            'data-live-search' : true
          }
        }
      };

    }
    $scope.initProcess();

    $scope.addProcess = function(){

      var dataProcess = {
        id: null,
        name: null,
        proses: null,
        roles: [],
        estimated_time: null,
        status: [],
        berkas: []
      };

      $scope.$parent.dataProcess.push(dataProcess);

      setTimeout(function(){
        $('.selectpicker').selectpicker();
      }, 10);
    }

    $scope.addBerkas = function(idx){

      $scope.$parent.dataProcess[idx].berkas.push({
        id: null,
        jenis_berkas: '',
        file: ''
      });

      setTimeout(function(){
        $('.selectpicker').selectpicker();
      }, 10);
    }

    $scope.deleteBerkas = function(idx, index){
      $scope.$parent.dataProcess[idx].berkas.splice(index, 1);
    }

    $scope.addStatus = function(idx){

      $scope.$parent.dataProcess[idx].status.push({
        id: null,
        status: '',
        next_proses: ''
      });

      setTimeout(function(){
        $('.selectpicker').selectpicker();
      }, 10);
    }

    $scope.deleteStatus = function(idx, index){
      $scope.$parent.dataProcess[idx].status.splice(index, 1);
    }

    $scope.deleteProcess = function(idx){
      $scope.$parent.dataProcess.splice(idx, 1);
      console.log("deleted");
    }
}]);
