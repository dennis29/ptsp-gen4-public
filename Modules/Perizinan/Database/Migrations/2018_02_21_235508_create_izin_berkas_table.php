<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIzinBerkasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('izin_berkas', function (Blueprint $table) {
            $table->string('id', 25);
            $table->string('izin_proses', 25);
            $table->string('jenis_berkas', 100);
            $table->text('file');

            $table->string('created_by', 25)->nullable();
            $table->string('updated_by', 25)->nullable();
            $table->timestamps();

            $table->primary('id');

            $table->foreign('izin_proses')
                    ->references('id')->on('izin_proses')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('jenis_berkas')
                    ->references('id')->on('jenis_berkas')
                    ->onDelete('restrict')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('izin_berkas');
    }
}
