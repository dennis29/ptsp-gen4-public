<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIzinProsesStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('izin_proses_status', function (Blueprint $table) {
            $table->string('id', 25);
            $table->string('izin_proses', 25);
            $table->string('status', 60);
            $table->string('next_proses', 25)->nullable();
            $table->integer('order')->default(0);

            $table->string('created_by', 25)->nullable();
            $table->string('updated_by', 25)->nullable();
            $table->timestamps();

            $table->primary('id');

            $table->foreign('izin_proses')
                    ->references('id')->on('izin_proses')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('status')
                    ->references('slug')->on('status')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('izin_proses_status');
    }
}
