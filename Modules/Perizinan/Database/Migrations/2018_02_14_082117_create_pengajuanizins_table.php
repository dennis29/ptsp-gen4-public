<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengajuanizinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuanizins', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('tanggal_mengajukan');
            $table->string('izin',40);
            $table->string('status',40);
            $table->string('users,',40);
            $table->string('created_by', 25)->nullable();
            $table->string('updated_by', 25)->nullable();
            $table->text('value')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuanizins');
    }
}
