<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIzinProsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('izin_proses', function (Blueprint $table) {
          $table->string('id', 25);
          $table->string('izin', 25);
          $table->string('proses', 25);
          $table->string('name', 25);
          $table->integer('estimated_time')->nullable()->default(0);
          $table->json('roles')->nullable();
          $table->integer('order')->default(0);

          $table->string('created_by', 25)->nullable();
          $table->string('updated_by', 25)->nullable();
          $table->timestamps();

          $table->primary('id');

          $table->foreign('proses')
                  ->references('id')->on('proses')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('izin_proses');
    }
}
