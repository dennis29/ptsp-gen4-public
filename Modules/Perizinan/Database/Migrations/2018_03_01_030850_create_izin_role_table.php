<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIzinRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('izin_role', function (Blueprint $table) {
            $table->string('id', 25);
            $table->string('izin', 25);
            $table->string('izin_proses', 25);
            $table->string('route', 255);
            $table->string('next_route', 255)->nullable();
            $table->string('role', 60);
            $table->string('status', 60);
            $table->boolean('is_default')->nullable()->default(false);
            $table->json('next_status')->nullable();

            $table->string('created_by', 25)->nullable();
            $table->string('updated_by', 25)->nullable();
            $table->timestamps();

            $table->primary('id');

            $table->foreign('izin')
                    ->references('id')->on('izin')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('izin_proses')
                    ->references('id')->on('izin_proses')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('status')
                    ->references('slug')->on('status')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('izin_role');
    }
}
