<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIzinSyaratTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('izin_syarat', function (Blueprint $table) {
            $table->string('id', 25);
            $table->string('izin', 25);
            $table->text('name');
            $table->string('jenis_berkas', 25)->nullable();
            $table->text('description')->nullable();
            $table->integer('order')->nullable()->default(0);

            $table->string('created_by', 25)->nullable();
            $table->string('updated_by', 25)->nullable();
            $table->timestamps();

            $table->primary('id');

            $table->foreign('izin')
                    ->references('id')->on('izin')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('izin_syarat');
    }
}
