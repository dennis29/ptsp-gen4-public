<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIzinTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('izin', function (Blueprint $table) {
            $table->string('id', 25);
            $table->string('code', 25);
            $table->string('izin_category', 25);
            $table->string('name', 255);
            $table->text('description')->nullable();
            $table->text('syarat_description')->nullable();
            $table->integer('waktu_penyelesaian')->nullable()->default(0);
            $table->enum('waktu_penyelesaian_tipe',['jam','hari', 'bulan', 'tahun'])->nullable();
            $table->string('biaya_retribusi', 255)->nullable();
            $table->integer('masa_berlaku')->nullable()->default(0);
            $table->enum('masa_berlaku_tipe', ['jam','hari', 'bulan', 'tahun'])->nullable();

            $table->string('created_by', 25)->nullable();
            $table->string('updated_by', 25)->nullable();
            $table->timestamps();

            $table->primary('id');
            $table->unique('code');

            $table->foreign('izin_category')
                    ->references('id')->on('izin_category')
                    ->onDelete('restrict')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('izin');
    }
}
