<?php

namespace Modules\Perizinan\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Packages\Form\FormBuilder;
use Packages\Grid\GridBuilder;

use Modules\Perizinan\Entities\IzinCategory;
use Modules\Perizinan\Entities\IzinProses;
use Modules\Perizinan\Entities\IzinProsesStatus;
use Modules\Perizinan\Entities\IzinSyarat;
use Modules\Perizinan\Entities\IzinRole;
use Modules\Master\Entities\Bidang;
use Modules\Master\Entities\JenisBerkas;
use Modules\Perizinan\Entities\Izin;
use Modules\Workflow\Entities\Proses;
use Modules\Workflow\Entities\Status;
use Modules\SSO\Entities\Role;

// Output
use Modules\Workflow\Entities\MappingOutputTable;
use Modules\Workflow\Entities\OutputTable;

class IzinController extends Controller
{
  public $url = '/backend/izin';
  public $title = 'Izin';

  public function index()
  {
      return view('perizinan::grid-izin');
  }

  public function data(){

    $query = Izin::select(['izin.id', 'izin.code', 'izin.name', 'izin_category.name AS izin_category',
    'bidang.name AS bidang'])
    ->join('izin_category', 'izin_category.id', '=', 'izin.izin_category')
    ->join('bidang', 'bidang.id', '=', 'izin_category.bidang');

    $grid = GridBuilder::source($query);
    $grid->url($this->url);
    $grid->title($this->title);

    $grid->filter('keyword', function($query, $param){
      return $query->where('name', 'ilike', '%'.$param.'%')->orWhere('izin_category', 'ilike', '%'.$param.'%');
    });

    $grid->add('code', 'Kode', true);
    $grid->add('name', 'Izin', true);
    $grid->add('izin_category', 'Kategori Izin', true);
    $grid->add('bidang', 'Bidang', true);
    $data = $grid->build();

    return response()->json($data);

  }

  public function create()
  {
    $form = $this->anyForm();
    $form = $form->build();
    $form = $this->dataForm($form);

    return view('perizinan::form-izin', compact('form'));
  }

  public function update($id = null){

    // Update Proses
    $dataProses = [];

    $form = $this->anyForm($id);
    $form = $form->build();
    $form = $this->dataForm($form);

    $izinProses = IzinProses::select(['id', 'name', 'proses', 'roles', 'estimated_time'])
    ->where('izin', $id)->orderBy('order', 'ASC')->get()->toArray();
    if(count($izinProses) > 0){
      $listIdProses = collect($izinProses)->pluck('id');

      foreach($izinProses as $val){
        $status = [];
        $izinProsesStatus = IzinProsesStatus::select(['id', 'status',
    'next_proses'])->where('izin_proses', $val['id'])->orderBy('order', 'ASC')->get()->toArray();
        if(count($izinProsesStatus) > 0){
          foreach($izinProsesStatus as $vval){
            $status[] = [
              'id' => $vval['id'],
              'status' => $vval['status'],
              'next_proses' => $listIdProses->search($vval['next_proses'])
            ];
          }
        }
        $val['status'] = $status;
        $val['berkas'] = [];
        $val['roles'] = json_decode($val['roles'], 1);
        $dataProses[] = $val;
      }
    }

    $form['dataProses'] = $dataProses;

    // Update Output
    $dataOutput = [];

    $mappingOutputTable = MappingOutputTable::select(['id', 'name', 'output', 'roles', 'order', 'email_penerima'])
    ->where('izin', $id)->get()->toArray();

    $dataOutput = collect($mappingOutputTable)->map(function($value){

        $value['roles'] = json_decode($value['roles'], 1);
        return $value;

    })->toArray();
    $form['dataOutput'] = $dataOutput;

    $dataSyarat = IzinSyarat::select(['id', 'name', 'jenis_berkas'])->where('izin', $id)->orderBy('order', 'ASC')->get()->toArray();
    $form['dataSyarat'] = $dataSyarat;

    return view('perizinan::form-izin', compact('form'));
  }

  public function save($id = null){


    $form = $this->anyForm($id);

    $validateCode = Izin::where('code', Input::get('code'));
    if($form->model->exists){
      $validateCode->where('code', '!=', $form->model->code);
    }
    $validateCode = $validateCode->first();
    if(!empty($validateCode)){
      return response()->json(['status' => false, 'messages' => 'Kode sudah digunakan!']);
    }

    $save = $form->save();

    $dataSyarat = Input::get('syarat');
    $dataProcess = Input::get('process');
    $dataOutput = Input::get('output');


    $this->saveSyarat($form->model->id, $dataSyarat);
    $this->saveProses($form->model->id, $dataProcess);
    $this->saveOutput($form->model->id, $dataOutput);


    return response()->json($save);
  }

  private function saveOutput($izinId, $dataOutput){
    // Save Output
    $listIdOutput = [];
    if(!empty($dataOutput)){
      foreach($dataOutput as $val){
        // Save Output
        $mappingOutputTable = new MappingOutputTable();
        if(!empty($val['id'])){
          $mappingOutputTable = MappingOutputTable::find($val['id']);
        }
        $dataOutput = [
          'izin' => $izinId,
          'name' => $val['name'],
          'output' => $val['output'],
          'order' => $val['order'],
          'roles' => json_encode($val['roles']),
          'email_penerima' => $val['email_penerima'],
        ];

        $mappingOutputTable->fill($dataOutput);
        $mappingOutputTable->save();
        $listIdOutput[] = $mappingOutputTable->id;

      }
    }
    // Delete Not Listed Proses
    MappingOutputTable::where('izin', $izinId)->whereNotIn('id', $listIdOutput)->delete();
    // End Save Output

    return $dataOutput;
  }

  private function saveProses($izinId, $dataProcess){
    // Save Proses
    $listIdProses = [];

    if(!empty($dataProcess)){
      foreach($dataProcess as $key => $val){
        // Save Proses
        $izinProses = new IzinProses();
        if(!empty($val['id'])){
          $izinProses = IzinProses::find($val['id']);
        }
        $dataProses = [
          'izin' => $izinId,
          'proses' => $val['proses'],
          'name' => $val['name'],
          'roles' => json_encode($val['roles']),
          'estimated_time' => $val['estimated_time'],
          'order' => ($key + 1)
        ];

        $izinProses->fill($dataProses);
        $izinProses->save();
        $dataProses[$key]['id'] = $izinProses->id;
        $listIdProses[] = $izinProses->id;
      }
    }
    // Delete Not Listed Proses
    IzinProses::where('izin', $izinId)->whereNotIn('id', $listIdProses)->delete();

    $listIdStatus = [];
    if(!empty($listIdProses)){
      foreach($dataProcess as $key => $val){
        // Save Status
        $idProses = $listIdProses[$key];
        if(count($val['status']) > 0){
          foreach($val['status'] as $kkey => $vval){
            $izinStatus = new IzinProsesStatus();
            if(!empty($vval['id']) && $vval['id'] != null){
              $izinStatus = IzinProsesStatus::find($vval['id']);
            }

            $nextProses = ($vval['next_proses'] != null && isset($listIdProses[$vval['next_proses']])) ? $listIdProses[$vval['next_proses']] : null;

            $dataStatus = [
              'izin_proses' => $idProses,
              'status' => $vval['status'],
              'next_proses' => $nextProses,
              'order' => ($kkey + 1)
            ];

            $izinStatus->fill($dataStatus);
            $izinStatus->save();

            $dataProcess[$key]['status'][$kkey]['id'] = $izinStatus->id;


            $listIdStatus[] = $izinStatus->id;
          }
        }
      }
    }
    IzinProsesStatus::whereNotIn('id', $listIdStatus)->delete();
    // End Save Proses

    // Save Izin Role (Maping)
    IzinRole::where('izin', $izinId)->delete();
    $default = true;
    foreach($dataProcess as $val){

      $roles = $val['roles'];
      $proses = Proses::find($val['proses']);

      if(count($val['status']) > 0){
        foreach($val['status'] as $kkey => $vval){
          // Save Izin Role (Maping)
          foreach($roles as $vrole){

            $dataNextStatus = [];
            $nextRoute = null;
            $nextProses = ($vval['next_proses'] != null && isset($listIdProses[$vval['next_proses']])) ? $listIdProses[$vval['next_proses']] : null;
            if(!empty($nextProses)){
              $nextProses = IzinProses::find($nextProses);
              if(!empty($nextProses)){

                $nProses = Proses::find($nextProses->proses);
                $nextRoute = $nProses->route;

                $nextProsesStatus = IzinProsesStatus::where('izin_proses', $nextProses->id)->get();
                if(count($nextProsesStatus) > 0){
                  foreach($nextProsesStatus as $vnext){
                    $dataNextStatus[] = $vnext->status;
                  }
                }
              }
            }

            IzinRole::create([
              'izin' => $izinId,
              'izin_proses' => $val['id'],
              'route' => $proses->route,
              'next_route' => $nextRoute,
              'role' => $vrole,
              'status' => $vval['status'],
              'next_status' => json_encode($dataNextStatus),
              'is_default' => $default
            ]);

            $default = false;
          }
        }
      }
    }

    return $dataProcess;
  }

  private function saveSyarat($izinId, $dataSyarat){
    // Save Output
    $listIdSyarat = [];
    if(!empty($dataSyarat)){
      foreach($dataSyarat as $key => $val){
        // Save Output
        $syarat = new IzinSyarat();
        if(!empty($val['id'])){
          $syarat = IzinSyarat::find($val['id']);
        }
        $data = [
          'izin' => $izinId,
          'name' => $val['name'],
          'jenis_berkas' => $val['jenis_berkas'],
          'order' => ($key + 1)
        ];

        $syarat->fill($data);
        $syarat->save();
        $listIdSyarat[] = $syarat->id;

      }
    }
    // Delete Not Listed Proses
    IzinSyarat::where('izin', $izinId)->whereNotIn('id', $listIdSyarat)->delete();
    // End Save Output

    return $dataSyarat;
  }

  public function delete($id = null){
    $id = Input::get('id');
    Izin::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function dataForm($form){

    $form['optionIzinCategory'] = IzinCategory::getOption();
    $form['optionProses'] = Proses::getOption();
    $form['optionOutput'] = OutputTable::getOption();
    $form['optionRole'] = Role::getOption();
    $form['optionStatus'] = Status::getOption();
    $form['optionJenisBerkas'] = JenisBerkas::getOption();
    $form['dataProses'] = [];
    $form['dataOutput'] = [];
    return $form;
  }

  public function anyForm($id = null){

    $data = Izin::find($id);
    if(empty($data)){
      $data = new Izin();
    }

    $form = FormBuilder::source($data);
    $form->title($this->title);
    $form->url($this->url);
    $form->add('code', 'Kode Izin', 'text')->rule('required');
    $form->add('name', 'Nama Izin', 'textarea')->rule('required');
    $form->add('izin_category', 'Kategori Izin', 'select')->rule('required')->options('item.id as item.name for item in formData.optionIzinCategory')->attributes([
      'data-live-search' => true
    ]);
    $form->add('description', '', 'text')->attributes([
      'ckeditor' => ''
    ]);
    $form->add('syarat_description', '', 'text')->attributes([
      'ckeditor' => ''
    ]);
    $form->add('waktu_penyelesaian', 'Waktu Penyelesaian', 'number')->attributes([
      'decimals' => 0
    ]);
    $form->add('waktu_penyelesaian_tipe', 'Jenis Waktu', 'select')->options('item.id as item.name for item in optionWaktu');
    $form->add('biaya_retribusi', 'Biaya Retribusi', 'text');
    $form->add('masa_berlaku', 'Masa Berlaku', 'number')->attributes([
      'decimals' => 0
    ]);
    $form->add('masa_berlaku_tipe', 'Jenis Waktu', 'select')->options('item.id as item.name for item in optionWaktu');

    return $form;
  }
}
