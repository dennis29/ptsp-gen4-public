<?php

namespace Modules\Perizinan\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Packages\Form\FormBuilder;
use Packages\Grid\GridBuilder;

use Modules\Perizinan\Entities\IzinCategory;
use Modules\Master\Entities\Bidang;

class IzinCategoryController extends Controller
{
  public $url = '/backend/izin-category';
  public $title = 'Kategori Izin';

  public function index()
  {
      return view('perizinan::grid-izin-category');
  }

  public function data(){

    $query = IzinCategory::select(['izin_category.id', 'izin_category.code', 'izin_category.name', 'bidang.name AS bidang'])
    ->join('bidang', 'bidang.id', '=', 'izin_category.bidang');

    $grid = GridBuilder::source($query);
    $grid->url($this->url);
    $grid->title($this->title);

    $grid->filter('keyword', function($query, $param){
      return $query->where('name', 'ilike', '%'.$param.'%');
    });
    $grid->add('code', 'Kode', true);
    $grid->add('name', 'Kategori Izin', true);
    $grid->add('bidang', 'Bidang', true);
    $data = $grid->build();

    return response()->json($data);

  }

  public function create()
  {
    $form = $this->anyForm();
    $form = $form->build();
    $form = $this->dataForm($form);

    return view('perizinan::form-izin-category', compact('form'));
  }

  public function update($id = null){
    $form = $this->anyForm($id);
    $form = $form->build();
    $form = $this->dataForm($form);

    return view('perizinan::form-izin-category', compact('form'));
  }

  public function save($id = null){



    $form = $this->anyForm($id);

    $validateCode = IzinCategory::where('code', Input::get('code'));
    if($form->model->exists){
      $validateCode->where('code', '!=', $form->model->code);
    }
    $validateCode = $validateCode->first();
    if(!empty($validateCode)){
      return response()->json(['status' => false, 'messages' => 'Kode sudah digunakan!']);
    }

    $save = $form->save();

    return response()->json($save);
  }

  public function delete($id = null){
    $id = Input::get('id');
    IzinCategory::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function dataForm($form){
    $form['optionBidang'] = Bidang::getOption();
    return $form;
  }

  public function anyForm($id = null){

    $data = IzinCategory::find($id);
    if(empty($data)){
      $data = new IzinCategory();
    }

    $form = FormBuilder::source($data);
    $form->title($this->title);
    $form->url($this->url);
    $form->add('code', 'Kode', 'text')->rule('required');
    $form->add('name', 'Nama Kategori', 'text')->rule('required');
    $form->add('bidang', 'Bidang', 'select')->rule('required')->options('item.id as item.name for item in formData.optionBidang');

    return $form;
  }
}
