<?php

Route::group([
  'prefix' => 'backend',
  'middleware' => ['web', 'auth.admin', 'acl', 'auth.role'],
  'namespace' => 'Modules\Perizinan\Http\Controllers'
], function () {

  Route::group(['prefix' => 'izin-category'], function(){
    Route::get('/', ['uses' => 'IzinCategoryController@index', 'as' => 'izincategory']);
    Route::get('data', ['uses' => 'IzinCategoryController@data', 'as' => 'izincategory.data']);
    Route::get('create', ['uses' => 'IzinCategoryController@create', 'as' => 'izincategory.create']);
    Route::get('update/{id?}', ['uses' => 'IzinCategoryController@update', 'as' => 'izincategory.update']);
    Route::post('save/{id?}', ['uses' => 'IzinCategoryController@save', 'as' => 'izincategory.save']);
    Route::post('delete', ['uses' => 'IzinCategoryController@delete', 'as' => 'izincategory.delete']);
  });

  Route::group(['prefix' => 'izin'], function(){
    Route::get('/', ['uses' => 'IzinController@index', 'as' => 'izin']);
    Route::get('data', ['uses' => 'IzinController@data', 'as' => 'izin.data']);
    Route::get('create', ['uses' => 'IzinController@create', 'as' => 'izin.create']);
    Route::get('update/{id?}', ['uses' => 'IzinController@update', 'as' => 'izin.update']);
    Route::post('save/{id?}', ['uses' => 'IzinController@save', 'as' => 'izin.save']);
    Route::post('delete', ['uses' => 'IzinController@delete', 'as' => 'izin.delete']);
  });

});
