<?php

namespace Modules\Token\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;

class PemohonIzinToken extends Controller
{   
    /**
     * Token Pemohon Izin
     */
//    public function pemohon_izin()
//    {
//      $tokens = PemohonIzinToken::token_info();
////dd($tokens);
//      return view('token::pemohon-izin')->with('tokens', $tokens);;
//    }
//    
    public function token_replace($template, $id_pengajuan_izin) {
      
      $token = PemohonIzinToken::token_info($id_pengajuan_izin);
//dd($token);
      $replaced = $template;
      foreach ($token as $key => $value) {
        $replaced = str_replace('[' . $key . ']', $value['value'], $replaced);
      }
    
      return $replaced;
    }
    
    public function token_info($id_pengajuan_izin = null) {
      
      // Table company: company_profile
      
      $token = array();  
      
      ##### Grab data from table "user_profile" ######
      
      // NIK
      $token['nik'] = array(
        'name' => 'NIK',
        'description' => 'Nomor Induk Kependudukan',
        'value' => empty($id_pengajuan_izin) ? null : PemohonIzinToken::get_nik($id_pengajuan_izin),
      );
      
      // No. KK
      $token['no-kk'] = array(
        'name' => 'No. KK',
        'description' => 'Nomor Kartu Keluarga',
        'value' => empty($id_pengajuan_izin) ? null : PemohonIzinToken::get_no_kk($id_pengajuan_izin),
      );
      
      // Nama KTP
      $token['nama-ktp'] = array(
        'name' => 'Nama KTP',
        'description' => 'Nama sesuai KTP',
        'value' => empty($id_pengajuan_izin) ? null : PemohonIzinToken::get_nama_ktp($id_pengajuan_izin),
      );
      
      // Alamat
      $token['alamat'] = array(
        'name' => 'Alamat',
        'description' => 'Alamat Rumah',
        'value' => empty($id_pengajuan_izin) ? null : PemohonIzinToken::get_alamat($id_pengajuan_izin),
      );
      
      // No. RT
      $token['no-rt'] = array(
        'name' => 'No. RT',
        'description' => 'Nomor RT',
        'value' => empty($id_pengajuan_izin) ? null : PemohonIzinToken::get_no_rt($id_pengajuan_izin),
      );
      
      // No. RW
      $token['no-rw'] = array(
        'name' => 'No. RW',
        'description' => 'Nomor RW',
        'value' => empty($id_pengajuan_izin) ? null : PemohonIzinToken::get_no_rw($id_pengajuan_izin),
      );
      
      // Kelurahan
      $token['kelurahan'] = array(
        'name' => 'Kelurahan',
        'description' => 'Kelurahan',
        'value' => empty($id_pengajuan_izin) ? null : PemohonIzinToken::get_kelurahan($id_pengajuan_izin),
      );
      
      // Kecamatan
      $token['kecamatan'] = array(
        'name' => 'Kecamatan',
        'description' => 'Kecamatan',
        'value' => empty($id_pengajuan_izin) ? null : PemohonIzinToken::get_kecamatan($id_pengajuan_izin),
      );
      
      // Kota
      $token['kota'] = array(
        'name' => 'Kota',
        'description' => 'Kota',
        'value' => empty($id_pengajuan_izin) ? null : PemohonIzinToken::get_kota($id_pengajuan_izin),
      );
      
      // Provinsi
      $token['provinsi'] = array(
        'name' => 'Provinsi',
        'description' => 'Provinsi',
        'value' => empty($id_pengajuan_izin) ? null : PemohonIzinToken::get_provinsi($id_pengajuan_izin),
      );
      
      // Tempat Lahir
      $token['tempat-lahir'] = array(
        'name' => 'Tempat Lahir',
        'description' => 'Tempat Lahir.',
        'value' => empty($id_pengajuan_izin) ? null : PemohonIzinToken::get_tempat_lahir($id_pengajuan_izin),
      );
      
      // Jenis Kelamin
      $token['jenis-kelamin'] = array(
        'name' => 'Jenis Kelamin',
        'description' => 'Jenis Kelamin.',
        'value' => empty($id_pengajuan_izin) ? null : PemohonIzinToken::get_jenis_kelamin($id_pengajuan_izin),
      );
      
      return $token;
    }
    
    // Helpers
    private function get_nik($id_pengajuan_izin) {
//      dd($id_pengajuan_izin);
      $user_profile = DB::table('user_profiles')
            ->join('users', 'user_profiles.user_id', '=', 'users.id')
            ->join('pengajuan_izin', 'pengajuan_izin.user', '=', 'users.id')
            ->where('pengajuan_izin.id', $id_pengajuan_izin)
            ->select('user_profiles.nik')
            ->first();
//      dd($user_profile);
      
      return $user_profile->nik;
    }
    
    private function get_no_kk($id_pengajuan_izin) {
      $user_profile = DB::table('user_profiles')
            ->join('users', 'user_profiles.user_id', '=', 'users.id')
            ->join('pengajuan_izin', 'pengajuan_izin.user', '=', 'users.id')
            ->where('pengajuan_izin.id', $id_pengajuan_izin)
            ->select('user_profiles.no_kk')
            ->first();
      return $user_profile->no_kk;
    }
    
    private function get_nama_ktp($id_pengajuan_izin) {
      $user_profile = DB::table('user_profiles')
            ->join('users', 'user_profiles.user_id', '=', 'users.id')
            ->join('pengajuan_izin', 'pengajuan_izin.user', '=', 'users.id')
            ->where('pengajuan_izin.id', $id_pengajuan_izin)
            ->select('user_profiles.nama_ktp')
            ->first();
      return $user_profile->nama_ktp;
    }
    
    private function get_alamat($id_pengajuan_izin) {
      $user_profile = DB::table('user_profiles')
            ->join('users', 'user_profiles.user_id', '=', 'users.id')
            ->join('pengajuan_izin', 'pengajuan_izin.user', '=', 'users.id')
            ->where('pengajuan_izin.id', $id_pengajuan_izin)
            ->select('user_profiles.alamat')
            ->first();
      return $user_profile->alamat;
    }
    
    private function get_no_rt($id_pengajuan_izin) {
      $user_profile = DB::table('user_profiles')
            ->join('users', 'user_profiles.user_id', '=', 'users.id')
            ->join('pengajuan_izin', 'pengajuan_izin.user', '=', 'users.id')
            ->where('pengajuan_izin.id', $id_pengajuan_izin)
            ->select('user_profiles.no_rt')
            ->first();
      return $user_profile->no_rt;
    }
    
    private function get_no_rw($id_pengajuan_izin) {
      $user_profile = DB::table('user_profiles')
            ->join('users', 'user_profiles.user_id', '=', 'users.id')
            ->join('pengajuan_izin', 'pengajuan_izin.user', '=', 'users.id')
            ->where('pengajuan_izin.id', $id_pengajuan_izin)
            ->select('user_profiles.no_rw')
            ->first();
      return $user_profile->no_rw;
    }
    
    private function get_kelurahan($id_pengajuan_izin) {
      $user_profile = DB::table('user_profiles')
            ->join('users', 'user_profiles.user_id', '=', 'users.id')
            ->join('pengajuan_izin', 'pengajuan_izin.user', '=', 'users.id')
            ->where('pengajuan_izin.id', $id_pengajuan_izin)
            ->select('user_profiles.kelurahan')
            ->first();
      return $user_profile->kelurahan;
    }
    
    private function get_kecamatan($id_pengajuan_izin) {
      $user_profile = DB::table('user_profiles')
            ->join('users', 'user_profiles.user_id', '=', 'users.id')
            ->join('pengajuan_izin', 'pengajuan_izin.user', '=', 'users.id')
            ->where('pengajuan_izin.id', $id_pengajuan_izin)
            ->select('user_profiles.kecamatan')
            ->first();
      return $user_profile->kecamatan;
    }
    
    private function get_kota($id_pengajuan_izin) {
      $user_profile = DB::table('user_profiles')
            ->join('users', 'user_profiles.user_id', '=', 'users.id')
            ->join('pengajuan_izin', 'pengajuan_izin.user', '=', 'users.id')
            ->where('pengajuan_izin.id', $id_pengajuan_izin)
            ->select('user_profiles.kota')
            ->first();
      return $user_profile->kota;
    }
    
    private function get_provinsi($id_pengajuan_izin) {
      $user_profile = DB::table('user_profiles')
            ->join('users', 'user_profiles.user_id', '=', 'users.id')
            ->join('pengajuan_izin', 'pengajuan_izin.user', '=', 'users.id')
            ->where('pengajuan_izin.id', $id_pengajuan_izin)
            ->select('user_profiles.provinsi')
            ->first();
      return $user_profile->provinsi;
    }
    
    private function get_tempat_lahir($id_pengajuan_izin) {
      $user_profile = DB::table('user_profiles')
            ->join('users', 'user_profiles.user_id', '=', 'users.id')
            ->join('pengajuan_izin', 'pengajuan_izin.user', '=', 'users.id')
            ->where('pengajuan_izin.id', $id_pengajuan_izin)
            ->select('user_profiles.tempat_lahir')
            ->first();
      return $user_profile->tempat_lahir;
    }
    
    private function get_jenis_kelamin($id_pengajuan_izin) {
      $user_profile = DB::table('user_profiles')
            ->join('users', 'user_profiles.user_id', '=', 'users.id')
            ->join('pengajuan_izin', 'pengajuan_izin.user', '=', 'users.id')
            ->where('pengajuan_izin.id', $id_pengajuan_izin)
            ->select('user_profiles.jenis_kelamin')
            ->first();
      return $user_profile->jenis_kelamin;
    }

}
