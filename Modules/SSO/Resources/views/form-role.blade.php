@extends('core::layout')

@section('content')
<json-data model="formData">
{{ collect($form)->toJson() }}
</json-data>
<div class="row" ng-controller="FormRoleController">
  <div class="col-md-12">
    <form name="form">
    <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green-sharp bold">@{{ formData.title }}</div>
        @component('core::components.form-action')
        @endcomponent
      </div>
      <div class="portlet-body">
        <div class="row margin-bottom-20">
          <div class="col-md-8 col-md-offset-2">
            <form-input attributes="fields.slug"></form-input>
            <form-input attributes="fields.name"></form-input>
            <form-input attributes="fields.level"></form-input>
            <form-input attributes="fields.description"></form-input>
          </div>
        </div>
        <div class="row margin-bottom-20">
          <div class="col-md-8 col-md-offset-2">
            <div class="module" ng-repeat="module in dataModule">
              <div class="module-header row">
                <div class="module-title col-md-10">@{{ module.name }}</div>
                <div class="module-active col pull-right">
                  <form-input type="switch" model="getModule(module.code).active"></form-input>
                </div>
              </div>
              <div class="module-body">
                <div class="row row-role" ng-repeat="role in dataAction" ng-hide="!getModule(module.code).active">
                  <div class="col-md-4 col-md-offset-2 role-label">@{{ role.name }}</div>
                  <div class="col-md-4">
                    <div class="togglebutton">
                      <label>
                        <input type="checkbox" ng-click="changeAccess(module.code, role.code)" ng-checked="isChecked(module.code, role.code)" />
                        <span class="toggle"></span>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </form>
  </div>
</div>
@endsection

@section('style')
<style type="text/css">
.module .module-header{
  border-bottom: 1px solid #eee;
  margin-bottom: 14px;
  height: 30px;
}
.module .module-header .module-title{
  font-size: 16px;
  font-weight: bold;
}
.row-role{
  height: 35px;
  padding: 5px 0;
}
.row-role:hover{
  background: #f2f2f2;
}
.role-label{
  font-size: 12px;
}
</style>
@endsection

@section('script')
<script src="{{ asset('modules/sso/form-role-controller.js') }}" type="text/javascript"></script>
@endsection
