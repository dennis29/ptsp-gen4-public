@extends('core::layout')

@section('content')
<json-data model="formData">
{{ collect($form)->toJson() }}
</json-data>
<div class="row" ng-controller="FormUserController">
  <div class="col-md-12">
    <form name="form">
    <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green-sharp bold">@{{ formData.title }}</div>
        @component('core::components.form-action')
        @endcomponent
      </div>
      <div class="portlet-body">
        <div class="row margin-bottom-20">
          <div class="col-md-4" style="text-align:center;">
            <form-input type="image" model="image"></form-input>
          </div>
          <div class="col-md-8">
            <form-input attributes="fields.fullname"></form-input>
            <form-input attributes="fields.username"></form-input>
            <form-input attributes="fields.email"></form-input>
            <form-input attributes="fields.password"></form-input>
            <form-input attributes="fields.role"></form-input>
            <form-input attributes="fields.wilayah"></form-input>
          </div>
        </div>
      </div>
    </div>
    </form>
  </div>
</div>
@endsection

@section('script')
<script src="{{ asset('modules/sso/form-user-controller.js') }}" type="text/javascript"></script>
@endsection
