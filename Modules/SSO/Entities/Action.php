<?php

namespace Modules\SSO\Entities;

use Packages\Model\BaseModel;

class Action extends BaseModel
{
    protected $table = 'actions';
    protected $fillable = ['id', 'code', 'name'];
}
