<?php

namespace Modules\SSO\Entities;

use Kodeine\Acl\Traits\HasRole;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Notifications\Notifiable;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Media;
// use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;

use Illuminate\Foundation\Auth\User as Authenticatable;

use Packages\Model\BaseModel;

class User extends Authenticatable implements HasMediaConversions
{
    public $incrementing = false;
    use Notifiable, CanResetPassword, HasRole, HasMediaTrait;

    protected $table = 'users';
    protected $fillable = ['id', 'fullname', 'email', 'password', 'remember_token', 'picture', 'wilayah', 'last_activity', 'active', 'confirmation_code', 'confirmed'];
    protected $hidden = ['password'];

    public $currentModule = null;
    public $currentAccess = [];
    public $modules = [];


    public function setCurrentAccess($module = null){
      $this->currentModule = $module;

      if(empty($this->currentModule)){
        $this->currentModule = null;
        return false;
      }

      $access = $this->getPermissions();
      if(isset($access[$this->currentModule])){
        $this->currentAccess = collect($access[$this->currentModule])->keys()->toArray();
      }

      $this->modules = collect($access)->keys()->toArray();
    }

    public function has($action = null){

      if(in_array($action, $this->currentAccess)){
        return true;
      }

      return false;
    }

    public function hasModule($module = null){
      if(empty($module)){
        return false;
      }

      $status = false;
      if(is_array($module)){

        foreach($module as $val){
          if(in_array($val, $this->modules)){
            $status = true;
            break;
          }
        }

      }else{
        if(in_array($module, $this->modules)){
          $status = true;
        }
      }

      return $status;
    }

    public function hasPermission(){
      if(empty($this->currentModule)){
        return false;
      }

      return true;
    }

    public function getCurrentAccess(){
      return $this->currentAccess;
    }

    protected static function boot() {
      parent::boot();

      static::creating(function($table) {
          $table->id = uniqid().substr(md5(mt_rand()), 0, 5);
          if(empty($table->created_by)){
              $table->created_by = (Auth::user()) ? Auth::user()->username : 'SYSTEM'; //(Auth::check()) ? Auth::user()->username : 'SYSTEM';
          }
      });

      static::updating(function($table) {
          $table->updated_by = (Auth::user()) ? Auth::user()->username : 'SYSTEM'; //(Auth::check()) ? Auth::user()->username : 'SYSTEM';
      });
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
              ->width(368)
              ->height(232)
              ->sharpen(10);
    }

    public function pengajuanIzins()
    {
        return $this->hasMany('Modules\Pemohon\Entities\PengajuanIzin');
    }

}
