<?php

namespace Modules\SSO\Entities;

use Packages\Model\BaseModel;

class Module extends BaseModel
{
    protected $table = 'modules';
    protected $fillable = ['id', 'code', 'name'];

    public static function getOption(){
      return Module::select(['code', 'name'])->get()->toArray();
    }
}
