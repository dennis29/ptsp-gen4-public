<?php

Route::group(['middleware' => 'web', 'prefix' => 'backend', 'namespace' => 'Modules\SSO\Http\Controllers'], function()
{

  Route::group(['middleware' => 'authenticated.admin'], function(){
    Route::get('login', ['as' => 'admin.login', 'uses' => 'HomeController@login']);
    Route::post('dologin', ['uses' => 'HomeController@dologin']);
    Route::get('forgot', ['as' => 'admin.forgotPassword', 'uses' => 'HomeController@forgotPassword']);
    Route::post('forgot', ['uses' => 'HomeController@doForgotPassword']);
    Route::get('forgot-success', ['as' => 'admin.forgotSuccess', 'uses' => 'HomeController@forgotPasswordSuccess']);
    Route::any('reset', ['as' => 'admin.resetPassword', 'uses' => 'HomeController@resetPassword']);
    Route::post('reset', ['uses' => 'HomeController@doResetPassword']);
  });

  Route::group(['middleware' => 'auth.admin'], function () {
    Route::get('/', ['as' => 'admin.dashboard', 'uses' => 'HomeController@index']);
    Route::get('logout', ['as' => 'admin.logout', 'uses' => 'HomeController@logout']);
  });

  Route::group(['middleware' => ['auth.admin', 'acl', 'auth.role']], function () {

    Route::group(['prefix' => 'module'], function(){
      Route::get('/', ['uses' => 'ModuleController@index', 'as' => 'module']);
      Route::get('data', ['uses' => 'ModuleController@data', 'as' => 'module.data']);
      Route::get('create', ['uses' => 'ModuleController@create', 'as' => 'module.create']);
      Route::get('update/{id?}', ['uses' => 'ModuleController@update', 'as' => 'module.update']);
      Route::post('save/{id?}', ['uses' => 'ModuleController@save', 'as' => 'module.save']);
      Route::post('delete', ['uses' => 'ModuleController@delete', 'as' => 'module.delete']);
    });

    Route::group(['prefix' => 'role'], function(){
      Route::get('/', ['uses' => 'RoleController@index', 'as' => 'role']);
      Route::get('data', ['uses' => 'RoleController@data', 'as' => 'role.data']);
      Route::get('create', ['uses' => 'RoleController@create', 'as' => 'role.create']);
      Route::get('update/{id?}', ['uses' => 'RoleController@update', 'as' => 'role.update']);
      Route::post('save/{id?}', ['uses' => 'RoleController@save', 'as' => 'role.save']);
      Route::post('delete', ['uses' => 'RoleController@delete', 'as' => 'role.delete']);
    });

    Route::group(['prefix' => 'action'], function(){
      Route::get('/', ['uses' => 'ActionController@index', 'as' => 'action']);
      Route::get('data', ['uses' => 'ActionController@data', 'as' => 'action.data']);
      Route::get('create', ['uses' => 'ActionController@create', 'as' => 'action.create']);
      Route::get('update/{id?}', ['uses' => 'ActionController@update', 'as' => 'action.update']);
      Route::post('save/{id?}', ['uses' => 'ActionController@save', 'as' => 'action.save']);
      Route::post('delete', ['uses' => 'ActionController@delete', 'as' => 'action.delete']);
    });

    Route::group(['prefix' => 'user', 'can' => 'view.user'], function(){
      Route::get('/', ['uses' => 'UserController@index', 'as' => 'user']);
      Route::get('data', ['uses' => 'UserController@data', 'as' => 'user.data']);
      Route::get('create', ['uses' => 'UserController@create', 'as' => 'user.create']);
      Route::get('update/{id?}', ['uses' => 'UserController@update', 'as' => 'user.update']);
      Route::post('save/{id?}', ['uses' => 'UserController@save', 'as' => 'user.save']);
      Route::post('delete', ['uses' => 'UserController@delete', 'as' => 'user.delete']);
    });
  });
});
