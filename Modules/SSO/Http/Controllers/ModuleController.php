<?php

namespace Modules\SSO\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Packages\Form\FormBuilder;
use Packages\Grid\GridBuilder;

use Modules\SSO\Entities\Module;

class ModuleController extends Controller
{
    public $url = '/backend/module';
    public $title = 'Module';

    public function index()
    {
        return view('sso::grid-module');
    }

    public function data(){

      $grid = GridBuilder::source(Module::query());
      $grid->url($this->url);
      $grid->title($this->title);

      $grid->filter('keyword', function($query, $param){
        return $query->where('code', 'ilike', '%'.$param.'%')
        ->orWhere('name', 'ilike', '%'.$param.'%');
      });

      $grid->add('code', 'Kode', true);
      $grid->add('name', 'Module', true);
      $data = $grid->build();

      return response()->json($data);

    }

    public function create()
    {
      $form = $this->anyForm();
      $form = $form->build();

      return view('sso::form-module', compact('form'));
    }

    public function update($id = null){
      $form = $this->anyForm($id);
      $form = $form->build();

      return view('sso::form-module', compact('form'));
    }

    public function save($id = null){
      $form = $this->anyForm($id);
      $save = $form->save();

      return response()->json($save);
    }

    public function delete($id = null){
      $id = Input::get('id');
      Module::whereIn('id', $id)->delete();

      return response()->json([
        'status' => true
      ]);
    }

    public function anyForm($id = null){

      $data = Module::find($id);
      if(empty($data)){
        $data = new Module();
      }

      $form = FormBuilder::source($data);
      $form->title($this->title);
      $form->url($this->url);
      $form->add('code', 'Kode', 'text')->rule('required');
      $form->add('name', 'Nama', 'text')->rule('required');

      return $form;
    }
}
