<?php

namespace Modules\SSO\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Packages\Form\FormBuilder;
use Packages\Grid\GridBuilder;

use Modules\SSO\Entities\Action;

class ActionController extends Controller
{
    public $url = '/backend/action';
    public $title = 'Action';

    public function index()
    {
        return view('sso::grid-action');
    }

    public function data(){

      $grid = GridBuilder::source(Action::query());
      $grid->url($this->url);
      $grid->title($this->title);

      $grid->filter('keyword', function($query, $param){
        return $query->where('code', 'ilike', '%'.$param.'%')
        ->orWhere('name', 'ilike', '%'.$param.'%');
      });

      $grid->add('code', 'Kode', true);
      $grid->add('name', 'Action', true);
      $data = $grid->build();

      return response()->json($data);

    }

    public function create()
    {
      $form = $this->anyForm();
      $form = $form->build();

      return view('sso::form-action', compact('form'));
    }

    public function update($id = null){
      $form = $this->anyForm($id);
      $form = $form->build();

      return view('sso::form-action', compact('form'));
    }

    public function save($id = null){
      $form = $this->anyForm($id);
      $save = $form->save();

      return response()->json($save);
    }

    public function delete($id = null){
      $id = Input::get('id');
      Action::whereIn('id', $id)->delete();

      return response()->json([
        'status' => true
      ]);
    }

    public function anyForm($id = null){

      $data = Action::find($id);
      if(empty($data)){
        $data = new Action();
      }

      $form = FormBuilder::source($data);
      $form->title($this->title);
      $form->url($this->url);
      $form->add('code', 'Kode', 'text')->rule('required');
      $form->add('name', 'Nama', 'text')->rule('required');

      return $form;
    }
}
