<?php

namespace Modules\SSO\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;

use Packages\Form\FormBuilder;
use Packages\Grid\GridBuilder;
use Illuminate\Support\Facades\Input;

use Modules\SSO\Entities\User;
use Modules\SSO\Entities\Group;
use Modules\Master\Entities\Wilayah;

use Modules\SSO\Entities\Role;
use Modules\SSO\Entities\Permission;

class UserController extends Controller
{
    public $url = '/backend/user';
    public $title = 'Pengguna';

    public function index()
    {
      return view('sso::grid-user');
    }

    public function data(){

      $grid = GridBuilder::source(User::query());
      $grid->url($this->url);
      $grid->title($this->title);

      $grid->filter('keyword', function($query, $param){
        return $query->where('fullname', 'ilike', '%'.$param.'%')
        ->orWhere('email', 'ilike', '%'.$param.'%');
      });

      $grid->add('fullname', 'Nama Lengkap', true);
      $grid->add('email', 'Email', true);
      $data = $grid->build();

      return response()->json($data);
    }


    public function create()
    {
      $form = $this->anyForm();
      $form = $form->build();
      $form = $this->dataForm($form);

      return view('sso::form-user', compact('form'));
    }

    public function update($id = null){
      $form = $this->anyForm($id);

      $roles = $form->model->getRoles();
      $roles = collect($roles)->values()->toArray();
      $form = $form->build();
      $form = $this->dataForm($form);

      $form['data']['role'] = $roles;
      return view('sso::form-user', compact('form'));
    }

    public function save($id = null){
      $form = $this->anyForm($id);
      $form->pre(function($input){
        $input['type'] = 'petugas';
        $input['password'] = Hash::make($input['password']);
        return $input;
      });
      $save = $form->save();

      $role = Input::get('role');
      $form->model->revokeAllRoles();
      $form->model->assignRole(implode(',', $role));

      return response()->json($save);
    }

    public function delete($id = null){
      $id = Input::get('id');
      User::whereIn('id', $id)->delete();

      return response()->json([
        'status' => true
      ]);
    }

    public function dataForm($form){

      $form['optionRole'] = Role::getOption();
      $form['optionWilayah'] = Wilayah::getOption();
      return $form;
    }

    public function anyForm($id = null){

      $user = User::select('id', 'fullname', 'email')->find($id);
      if(empty($user)){
        $user = new User();
      }

      $form = FormBuilder::source($user);
      $form->title($this->title);
      $form->url($this->url);
      $form->add('fullname', 'Nama Lengkap', 'text')->rule('required');
      $form->add('email', 'Email', 'email')->rule('required|email');
      $form->add('password', 'Password', 'password')->rule('required|min:6');
      $form->add('role', 'Role', 'multiple')->rule('required')->options('item.slug as item.name for item in formData.optionRole');
      $form->add('wilayah', 'Wilayah', 'select')->options('item.id as item.name for item in formData.optionWilayah');
      $form->add('active', 'Status', 'text');

      return $form;
    }
}
