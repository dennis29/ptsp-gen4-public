<?php

namespace Modules\SSO\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Packages\Form\FormBuilder;
use Packages\Grid\GridBuilder;

use Modules\SSO\Entities\Role;
use Modules\SSO\Entities\Permission;
use Modules\SSO\Entities\Module;
use Modules\SSO\Entities\Action;


class RoleController extends Controller
{
  public $url = '/backend/role';
  public $title = 'Role';

  public function index()
  {
      return view('sso::grid-role');
  }

  public function data(){

    $grid = GridBuilder::source(Role::query());
    $grid->url($this->url);
    $grid->title($this->title);

    $grid->filter('keyword', function($query, $param){
      return $query->where('name', 'ilike', '%'.$param.'%');
    });
    $grid->add('slug', 'Slug', true);
    $grid->add('name', 'Nama Role', true);
    $grid->add('level', 'Level', true);
    $grid->add('description', 'Keterangan', true);
    $data = $grid->build();

    return response()->json($data);

  }

  public function create()
  {
    $form = $this->anyForm();
    $form = $form->build();

    $form['optionLevel'] = [
      ['id' => 'admin', 'name' => 'Administrator'],
      ['id' => 'backoffice', 'name' => 'Backoffice'],
      ['id' => 'pemohon', 'name' => 'Pemohon']
    ];

    $form = $this->getDataDetail($form);

    return view('sso::form-role', compact('form'));
  }

  public function update($id = null){
    $form = $this->anyForm($id);
    $dataPermission = $form->model->getPermissions();
    $form = $form->build();

    $form['optionLevel'] = [
      ['id' => 'admin', 'name' => 'Administrator'],
      ['id' => 'backoffice', 'name' => 'Backoffice'],
      ['id' => 'pemohon', 'name' => 'Pemohon']
    ];

    $form = $this->getDataDetail($form);

    $dataAccess = [];
    if(count($dataPermission) > 0){
      foreach($dataPermission as $key => $val){

        $arrModule = explode('.', $key);
        $dataAccess[] = [
          'module' => $arrModule[1],
          'access' => collect($val)->keys()->all()
        ];
      }
    }

    $form['data']['access'] = $dataAccess;

    return view('sso::form-role', compact('form'));
  }

  public function save($id = null){


    $form = $this->anyForm($id);
    $save = $form->save();

    $form->model->revokeAllPermissions();

    $dataAccess = Input::get('access');
    $permissionToAdd = [];

    Permission::where('name', 'ilike', $form->model->slug.'.%')->delete();


    if(count($dataAccess) > 0){


      foreach($dataAccess as $val){
        if($val['active']){

          $dataSlug = [];
          foreach($val['access'] as $vval){
            $dataSlug[$vval] = true;
          }

          $permissionName = $form->model->slug.'.'.$val['module'];
          $dataPermission = [
            'name' => $permissionName,
            'slug' => $dataSlug
          ];

          $permission = Permission::create($dataPermission);

          $permissionToAdd[] = $permissionName;
        }

      }
    }

    if(count($permissionToAdd) > 0){
      $form->model->assignPermission(implode(',', $permissionToAdd));
    }



    return response()->json($save);
  }

  public function delete($id = null){
    $id = Input::get('id');
    Role::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function getDataDetail($form){

    $dataModule = Module::all()->toArray();
    $dataAction = Action::all()->toArray();
    //
    $form['dataModule'] = $dataModule;
    $form['dataAction'] = $dataAction;
    return $form;
  }

  public function anyForm($id = null){

    $data = Role::find($id);
    if(empty($data)){
      $data = new Role();
    }

    $form = FormBuilder::source($data);
    $form->title($this->title);
    $form->url($this->url);
    $form->add('slug', 'Slug', 'text')->rule('required');
    $form->add('name', 'Nama Role', 'text')->rule('required');
    $form->add('level', 'Level', 'select')->rule('required')->options('item.id as item.name for item in formData.optionLevel');
    $form->add('description', 'Keterangan', 'textarea');

    return $form;
  }
}
