<?php

namespace Modules\SSO\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use Modules\SSO\Entities\Action;
use Modules\SSO\Entities\User;

class HomeController extends Controller
{

    public function index(){
      return view('core::blank');
    }

    public function login()
    {
        return view('sso::login');
    }

    public function dologin() {

        $input = Input::all();
        $rules = [
            'email' => 'required',
            'password' => 'required|min:6',
        ];

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return redirect()->route('admin.login')->withErrors("Email atau password salah!")->withInput(Input::except('password'));
        } else {
            $user = User::where('email', $input['email'])->first();

            if ($user) {

              if($user->active != 1){
                return redirect()->route('admin.login')->withErrors("User tidak aktif");
              }

                if (Hash::check($input['password'], $user->password)) {

                    $session = [
                        'email' => $user->email,
                        'password' => $input['password']
                    ];

                    $remember = true;
                    if (Auth::attempt($session, true)) {
                        return redirect()->intended('backend');
                    } else {
                        // no action
                        //dd("failed attempt");
                    }
                } else {
                    // no action
                }
            }

            return redirect()->route('admin.login')->withErrors("Email atau password salah!");
        }
    }

    public function logout() {

        Auth::logout();
        return redirect()->route('admin.login');
    }

}
