<?php

namespace Modules\SSO\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class AuthenticateAdmin
{
    protected $auth;

    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next, ...$guards)
    {
return $next($request);
      if (!Auth::guard($guards)->check()) {
          if ($request->ajax() || $request->wantsJson()) {
            // return response()->json(['status' => '_logout']);
            return response('Unauthorized.', 401);
          } else {
            return redirect()->route('admin.login');
          }
      }

        return $next($request);
    }
}
