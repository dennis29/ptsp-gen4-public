<?php

namespace Modules\SSO\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class AuthenticatedAdmin
{
    protected $auth;

    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next, ...$guards)
    {

      if (Auth::guard($guards)->check()) {
          return redirect()->route('admin.dashboard');
      }

        return $next($request);
    }
}
