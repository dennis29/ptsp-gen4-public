<?php

namespace Modules\SSO\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next, ...$guards)
     {
       // return $next($request);
         $route = $request->route()->getName();
         $module = '';
         $action = '';
         if(!empty($route)){
           $arrRoute = explode('.', $route);
           $module = $arrRoute[0];
           if(isset($arrRoute[1])){
             $action = $arrRoute[1];
           }
         }
         // dd("fire");

         Auth::user()->setCurrentAccess($module);
         $status = true;
         if(!Auth::user()->has('view')){
           $status = false;
         }

         $blockedPage = ['create', 'delete'];
         if(in_array($action, $blockedPage)){
           if(!Auth::user()->has($action)){
             $status = false;
           }
         }

         if(!$status){
           if ( $request->isJson() || $request->wantsJson() ) {
               return response()->json([
                   'error' => [
                       'status_code' => 401,
                       'code'        => 'INSUFFICIENT_PERMISSIONS',
                       'description' => 'You are not authorized to access this resource.'
                   ],
               ], 401);
           }

           return abort(401, 'You are not authorized to access this resource.');
         }

         return $next($request);
     }
}
