<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => '5a794e569c11f87ddc',
                'inherit_id' => NULL,
                'name' => 'editor.user',
                'slug' => '{"update":true}',
                'description' => NULL,
                'created_by' => 'SYSTEM',
                'updated_by' => NULL,
                'created_at' => '2018-02-06 06:42:30',
                'updated_at' => '2018-02-06 06:42:30',
            ),
            1 => 
            array (
                'id' => '5a794e569caa8b7b9d',
                'inherit_id' => NULL,
                'name' => 'editor.action',
                'slug' => '{"delete":true,"update":true}',
                'description' => NULL,
                'created_by' => 'SYSTEM',
                'updated_by' => NULL,
                'created_at' => '2018-02-06 06:42:30',
                'updated_at' => '2018-02-06 06:42:30',
            ),
            2 => 
            array (
                'id' => '5a7960e6262e4b1c54',
                'inherit_id' => NULL,
                'name' => 'moderator.module',
                'slug' => '{"create":true}',
                'description' => NULL,
                'created_by' => 'SYSTEM',
                'updated_by' => NULL,
                'created_at' => '2018-02-06 08:01:42',
                'updated_at' => '2018-02-06 08:01:42',
            ),
            3 => 
            array (
                'id' => '5a7960e62d89147f54',
                'inherit_id' => NULL,
                'name' => 'moderator.user',
                'slug' => '{"create":true,"update":true,"view":true}',
                'description' => NULL,
                'created_by' => 'SYSTEM',
                'updated_by' => NULL,
                'created_at' => '2018-02-06 08:01:42',
                'updated_at' => '2018-02-06 08:01:42',
            ),
            4 => 
            array (
                'id' => '5a8183982578877a0b',
                'inherit_id' => NULL,
                'name' => 'admin.module',
                'slug' => '{"update":true,"create":true,"delete":true,"view":true}',
                'description' => NULL,
                'created_by' => 'barkah.hadi',
                'updated_by' => NULL,
                'created_at' => '2018-02-12 12:07:52',
                'updated_at' => '2018-02-12 12:07:52',
            ),
            5 => 
            array (
                'id' => '5a818398277fe10ffd',
                'inherit_id' => NULL,
                'name' => 'admin.role',
                'slug' => '{"create":true,"update":true,"delete":true,"view":true}',
                'description' => NULL,
                'created_by' => 'barkah.hadi',
                'updated_by' => NULL,
                'created_at' => '2018-02-12 12:07:52',
                'updated_at' => '2018-02-12 12:07:52',
            ),
            6 => 
            array (
                'id' => '5a81839827e98134d7',
                'inherit_id' => NULL,
                'name' => 'admin.action',
                'slug' => '{"view":true,"create":true,"update":true,"delete":true}',
                'description' => NULL,
                'created_by' => 'barkah.hadi',
                'updated_by' => NULL,
                'created_at' => '2018-02-12 12:07:52',
                'updated_at' => '2018-02-12 12:07:52',
            ),
            7 => 
            array (
                'id' => '5a8183982857916a11',
                'inherit_id' => NULL,
                'name' => 'admin.user',
                'slug' => '{"view":true,"create":true,"update":true,"delete":true}',
                'description' => NULL,
                'created_by' => 'barkah.hadi',
                'updated_by' => NULL,
                'created_at' => '2018-02-12 12:07:52',
                'updated_at' => '2018-02-12 12:07:52',
            ),
            8 => 
            array (
                'id' => '5a81839828ae177fa1',
                'inherit_id' => NULL,
                'name' => 'admin.wilayah',
                'slug' => '{"view":true,"delete":true,"update":true,"create":true}',
                'description' => NULL,
                'created_by' => 'barkah.hadi',
                'updated_by' => NULL,
                'created_at' => '2018-02-12 12:07:52',
                'updated_at' => '2018-02-12 12:07:52',
            ),
            9 => 
            array (
                'id' => '5a81839828f57e76c5',
                'inherit_id' => NULL,
                'name' => 'admin.bidang',
                'slug' => '{"create":true,"update":true,"delete":true,"view":true}',
                'description' => NULL,
                'created_by' => 'barkah.hadi',
                'updated_by' => NULL,
                'created_at' => '2018-02-12 12:07:52',
                'updated_at' => '2018-02-12 12:07:52',
            ),
            10 => 
            array (
                'id' => '5a8183982949c73873',
                'inherit_id' => NULL,
                'name' => 'admin.jenisizin',
                'slug' => '{"create":true,"update":true,"delete":true,"view":true}',
                'description' => NULL,
                'created_by' => 'barkah.hadi',
                'updated_by' => NULL,
                'created_at' => '2018-02-12 12:07:52',
                'updated_at' => '2018-02-12 12:07:52',
            ),
            11 => 
            array (
                'id' => '5a8183982991c6ffa5',
                'inherit_id' => NULL,
                'name' => 'admin.jenisperusahaan',
                'slug' => '{"view":true,"create":true}',
                'description' => NULL,
                'created_by' => 'barkah.hadi',
                'updated_by' => NULL,
                'created_at' => '2018-02-12 12:07:52',
                'updated_at' => '2018-02-12 12:07:52',
            ),
            12 => 
            array (
                'id' => '5a81839829ef0b54a2',
                'inherit_id' => NULL,
                'name' => 'admin.jeniskoperasi',
                'slug' => '{"create":true,"update":true,"delete":true,"view":true}',
                'description' => NULL,
                'created_by' => 'barkah.hadi',
                'updated_by' => NULL,
                'created_at' => '2018-02-12 12:07:52',
                'updated_at' => '2018-02-12 12:07:52',
            ),
        ));
        
        
    }
}