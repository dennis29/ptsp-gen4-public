<?php

use Illuminate\Database\Seeder;

class PermissionRoleTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permission_role')->delete();
        
        \DB::table('permission_role')->insert(array (
            0 => 
            array (
                'id' => '5a8183982bc1ae1acb',
                'permission_id' => '5a8183982578877a0b',
                'role_id' => '5a78098a2ac73f180a',
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2018-02-12 12:07:52',
                'updated_at' => '2018-02-12 12:07:52',
            ),
            1 => 
            array (
                'id' => '5a8183982d17c7b7af',
                'permission_id' => '5a818398277fe10ffd',
                'role_id' => '5a78098a2ac73f180a',
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2018-02-12 12:07:52',
                'updated_at' => '2018-02-12 12:07:52',
            ),
            2 => 
            array (
                'id' => '5a8183982e2737eb2f',
                'permission_id' => '5a81839827e98134d7',
                'role_id' => '5a78098a2ac73f180a',
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2018-02-12 12:07:52',
                'updated_at' => '2018-02-12 12:07:52',
            ),
            3 => 
            array (
                'id' => '5a8183982ed838aabf',
                'permission_id' => '5a8183982857916a11',
                'role_id' => '5a78098a2ac73f180a',
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2018-02-12 12:07:52',
                'updated_at' => '2018-02-12 12:07:52',
            ),
            4 => 
            array (
                'id' => '5a8183982fb5a91057',
                'permission_id' => '5a81839828ae177fa1',
                'role_id' => '5a78098a2ac73f180a',
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2018-02-12 12:07:52',
                'updated_at' => '2018-02-12 12:07:52',
            ),
            5 => 
            array (
                'id' => '5a818398303ab52833',
                'permission_id' => '5a81839828f57e76c5',
                'role_id' => '5a78098a2ac73f180a',
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2018-02-12 12:07:52',
                'updated_at' => '2018-02-12 12:07:52',
            ),
            6 => 
            array (
                'id' => '5a81839830bb2b40f2',
                'permission_id' => '5a8183982949c73873',
                'role_id' => '5a78098a2ac73f180a',
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2018-02-12 12:07:52',
                'updated_at' => '2018-02-12 12:07:52',
            ),
            7 => 
            array (
                'id' => '5a81839831484cf9b9',
                'permission_id' => '5a8183982991c6ffa5',
                'role_id' => '5a78098a2ac73f180a',
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2018-02-12 12:07:52',
                'updated_at' => '2018-02-12 12:07:52',
            ),
            8 => 
            array (
                'id' => '5a8183983214189871',
                'permission_id' => '5a81839829ef0b54a2',
                'role_id' => '5a78098a2ac73f180a',
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2018-02-12 12:07:52',
                'updated_at' => '2018-02-12 12:07:52',
            ),
        ));
        
        
    }
}