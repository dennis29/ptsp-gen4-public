<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => '5a811f8be5ce60cf10',
                'email' => 'admin@jakarta.go.id',
                'fullname' => 'Administrator',
                'password' => '$2y$10$3T0LxZroQGFWRGNy3FBfJus3PTAWrzCjXqgxx6Ik9w5w.6ebXvNf6',
                'remember_token' => 'FRaeU7uCaDucQZeJyjdYO9809LAYd8TLhCymbf2ADUl8LOd7fMbgfqRVa9uf',
                'picture' => NULL,
                'wilayah' => NULL,
                'type' => 'petugas',
                'last_activity' => NULL,
                'active' => true,
                'created_by' => 'barkah.hadi',
                'updated_by' => NULL,
                'created_at' => '2018-02-12 05:00:59',
                'updated_at' => '2018-02-13 01:36:17',
            ),
        ));
        
        
    }
}