<?php

use Illuminate\Database\Seeder;

class ActionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('actions')->delete();
        
        \DB::table('actions')->insert(array (
            0 => 
            array (
                'id' => '5a781dc78c21ade0e6',
                'code' => 'create',
                'name' => 'Create',
                'created_by' => 'SYSTEM',
                'updated_by' => NULL,
                'created_at' => '2018-02-05 09:03:03',
                'updated_at' => '2018-02-05 09:03:03',
            ),
            1 => 
            array (
                'id' => '5a781dd3b4e21351e6',
                'code' => 'update',
                'name' => 'Update',
                'created_by' => 'SYSTEM',
                'updated_by' => NULL,
                'created_at' => '2018-02-05 09:03:15',
                'updated_at' => '2018-02-05 09:03:15',
            ),
            2 => 
            array (
                'id' => '5a781dde76bf72e041',
                'code' => 'delete',
                'name' => 'Delete',
                'created_by' => 'SYSTEM',
                'updated_by' => NULL,
                'created_at' => '2018-02-05 09:03:26',
                'updated_at' => '2018-02-05 09:03:26',
            ),
            3 => 
            array (
                'id' => '5a781de9400491b2fc',
                'code' => 'view',
                'name' => 'View',
                'created_by' => 'SYSTEM',
                'updated_by' => NULL,
                'created_at' => '2018-02-05 09:03:37',
                'updated_at' => '2018-02-05 09:03:37',
            ),
        ));
        
        
    }
}