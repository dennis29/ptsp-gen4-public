<?php

use Illuminate\Database\Seeder;

class ModulesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('modules')->delete();
        
        \DB::table('modules')->insert(array (
            0 => 
            array (
                'id' => '5a7923cf79df4e796b',
                'code' => 'user',
                'name' => 'Pengguna',
                'created_by' => 'SYSTEM',
                'updated_by' => 'SYSTEM',
                'created_at' => '2018-02-06 03:41:03',
                'updated_at' => '2018-02-06 03:41:20',
            ),
            1 => 
            array (
                'id' => '5a7923e7c4a7d77a52',
                'code' => 'role',
                'name' => 'Role',
                'created_by' => 'SYSTEM',
                'updated_by' => NULL,
                'created_at' => '2018-02-06 03:41:27',
                'updated_at' => '2018-02-06 03:41:27',
            ),
            2 => 
            array (
                'id' => '5a7923eda09325a65b',
                'code' => 'module',
                'name' => 'Module',
                'created_by' => 'SYSTEM',
                'updated_by' => NULL,
                'created_at' => '2018-02-06 03:41:33',
                'updated_at' => '2018-02-06 03:41:33',
            ),
            3 => 
            array (
                'id' => '5a7923f22b7da6cd45',
                'code' => 'action',
                'name' => 'Action',
                'created_by' => 'SYSTEM',
                'updated_by' => NULL,
                'created_at' => '2018-02-06 03:41:38',
                'updated_at' => '2018-02-06 03:41:38',
            ),
            4 => 
            array (
                'id' => '5a7923f7235f4c4114',
                'code' => 'wilayah',
                'name' => 'Wilayah',
                'created_by' => 'SYSTEM',
                'updated_by' => NULL,
                'created_at' => '2018-02-06 03:41:43',
                'updated_at' => '2018-02-06 03:41:43',
            ),
            5 => 
            array (
                'id' => '5a7c5cc5758164715b',
                'code' => 'bidang',
                'name' => 'Bidang',
                'created_by' => 'barkah.hadi',
                'updated_by' => NULL,
                'created_at' => '2018-02-08 14:20:53',
                'updated_at' => '2018-02-08 14:20:53',
            ),
            6 => 
            array (
                'id' => '5a7c644eedd801f160',
                'code' => 'jenisizin',
                'name' => 'Jenis Izin',
                'created_by' => 'barkah.hadi',
                'updated_by' => 'barkah.hadi',
                'created_at' => '2018-02-08 14:53:02',
                'updated_at' => '2018-02-08 15:02:24',
            ),
            7 => 
            array (
                'id' => '5a7c703daae3895ea6',
                'code' => 'jenisperusahaan',
                'name' => 'Jenis Perusahaan',
                'created_by' => 'barkah.hadi',
                'updated_by' => NULL,
                'created_at' => '2018-02-08 15:43:57',
                'updated_at' => '2018-02-08 15:43:57',
            ),
            8 => 
            array (
                'id' => '5a7c704b0a2ae4c597',
                'code' => 'jeniskoperasi',
                'name' => 'Jenis Koperasi',
                'created_by' => 'barkah.hadi',
                'updated_by' => NULL,
                'created_at' => '2018-02-08 15:44:11',
                'updated_at' => '2018-02-08 15:44:11',
            ),
        ));
        
        
    }
}