<?php

use Illuminate\Database\Seeder;

class RoleUserTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('role_user')->delete();
        
        \DB::table('role_user')->insert(array (
            0 => 
            array (
                'id' => '5a824111af6ec77dd4',
                'role_id' => '5a78098a2ac73f180a',
                'user_id' => '5a811f8be5ce60cf10',
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2018-02-13 01:36:17',
                'updated_at' => '2018-02-13 01:36:17',
            ),
        ));
        
        
    }
}