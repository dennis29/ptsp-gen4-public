<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => '5a78098a2ac73f180a',
                'name' => 'Admin',
                'slug' => 'admin',
                'description' => NULL,
                'created_by' => 'SYSTEM',
                'updated_by' => NULL,
                'created_at' => '2018-02-05 07:36:42',
                'updated_at' => '2018-02-05 07:36:42',
            ),
        ));
        
        
    }
}