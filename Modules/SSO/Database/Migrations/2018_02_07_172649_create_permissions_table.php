<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->string('id', 25);
            $table->string('inherit_id', 25)->nullable();

            $table->string('name')->index();
            $table->string('slug')->index();
            $table->text('description')->nullable();

            $table->string('created_by', 25)->nullable();
            $table->string('updated_by', 25)->nullable();
            $table->timestamps();

            $table->primary('id');

            $table->foreign('inherit_id')
              ->references('id')->on('permissions')
              ->onDelete('cascade')
              ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('permissions');
    }

}
