<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table->string('id', 25);
            $table->string('email', 100);
            $table->string('fullname', 60);
            $table->string('password', 255);
            $table->string('remember_token', 255)->nullable();
            $table->string('picture', 255)->nullable();
            $table->string('wilayah', 25)->nullable();
            $table->dateTime('last_activity')->nullable();
            $table->boolean('confirmed')->default(false);
            $table->string('confirmation_code', 255)->nullable();
            $table->boolean('active')->default(true);

            $table->string('created_by', 25)->nullable();
            $table->string('updated_by', 25)->nullable();
            $table->timestamps();

            $table->primary('id');
            $table->unique('email');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
