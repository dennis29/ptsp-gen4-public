<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('permission_user', function (Blueprint $table) {
			$table->string('id', 25);
			$table->string('permission_id', 25);
			$table->string('user_id', 25);

      $table->string('created_by', 25)->nullable();
      $table->string('updated_by', 25)->nullable();
			$table->timestamps();

      $table->primary('id');

      $table->foreign('permission_id')
          ->references('id')->on('permissions')
          ->onDelete('cascade')
          ->onUpdate('cascade');

      $table->foreign('user_id')
          ->references('id')->on('users')
          ->onDelete('cascade')
          ->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('permission_user');
	}

}
