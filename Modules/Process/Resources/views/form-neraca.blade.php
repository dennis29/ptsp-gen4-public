@extends('pemohon::layouts.master')
@section('title',"Dashboard Pemohon")
@section('style')
	<style>
	.head{
		font-size: 14pt;
	}
	.footer{
		position: absolute;
		right: 0;
		bottom: 0;
		left: 10;
		padding: 10rem;
		font-size: 9pt;
	}
		.borderhide{
			table-border: 0;
		}
	</style>
@endsection
@section('content')
	<div class="page-content-inner">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light portlet-fit ">
					<div class="portlet-title">
						<div class="col-md-12" style="text-align: center;">
							<table class="table table-bordered" style="font-weight: bold;">
								<tr>
									<td colspan="6">NERACA KEUANGAN</td>
								</tr>
								<tr>
									<td style="text-align: right">PER</td>
									<td width="5">:</td>
									<td></td>
									<td width="10">BULAN</td>
									<td></td>
									<td>20</td>
								</tr>
								<tr>
									<td colspan="6">(dalam ribu rupiah)</td>
								</tr>
							</table>
						</div>
					</div>
					<div class="portlet">
					<table class="table" border="0" style="border: none;">
						<tr>
							<td style="text-align: left;">AKTIVA</td>
							<td style="text-align:right;">PASIVA</td>
						</tr>
					</table>
					<div class="row">
						<div class="col-md-6">
							<table class="table table-bordered">
								<tr>
									<td>I.</td>
									<td>AKTIVA LANCAR</td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<td>Kas</td>
									<td>Rp. .......</td>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<td>Bank</td>
									<td>Rp. .......</td>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<td>Piutang**)</td>
									<td>Rp. .......</td>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<td>Persediaan Barang</td>
									<td>Rp. .......</td>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<td>Pekerjaan dlm proses</td>
									<td>Rp. .......</td>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<td>Jumlah(a)</td>
									<td></td>
									<td>Rp. .......</td>
								</tr>
								<tr>
									<td>II.</td>
									<td>AKTIVA TETAP</td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<td>Peralatan dlm Mesin</td>
									<td>Rp. .......</td>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<td>Investasi</td>
									<td>Rp. .......</td>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<td>Jumlah(a)</td>
									<td></td>
									<td>Rp. .......</td>
								</tr>
								<tr>
									<td>III.</td>
									<td>AKTIVA LAINNYA(c)</td>
									<td></td>
									<td>Rp. .......</td>
								</tr>
								<tr>
									<td></td>
									<td></td>
									<td>JUMLAH</td>
									<td>Rp. .......</td>
								</tr>
							</table>

						</div>

						<div class="col-md-6">
							<table class="table table-bordered">
								<tr>
									<td>IV.</td>
									<td>HUTANG JANGKA PENDEK</td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<td>Hutang Dagang</td>
									<td>Rp. .......</td>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<td>Hutang Pajak</td>
									<td>Rp. .......</td>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<td>Hutang Lainnya</td>
									<td>Rp. .......</td>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<td>Jumlah(d)</td>
									<td></td>
									<td>Rp. .......</td>
								</tr>
								<tr>
									<td>V.</td>
									<td>HUTANG JANGKA PANJANG</td>
									<td></td>
									<td>Rp. .......</td>
								</tr>
								<tr>
									<td>VI.</td>
									<td>KEKAYAAN BERSIH (a+b+c) - (d-e)</td>
									<td></td>
									<td>Rp. .......</td>
								</tr>

								<tr>
									<td></td>
									<td></td>
									<td>JUMLAH</td>
									<td>Rp. .......</td>
								</tr>
							</table>
						</div>
					</div>
					<div class="row">
						<table style="float:right;">
							<tr>
								<td width="5">Jakarta,
								<br>
									PT/CV ...............................
								</td>
							</tr>
							<tr>
								<td style="text-align:center;">DirekturUtama/PenanggungJawab Perusahaan</td>
							</tr>
							<tr>
								<td style="font-size:8pt; text-align: center">TTD dan  Materai 6000 <br>
									Stempel Perusahan
								</td>
							</tr>
							<tr>
								<td style="text-align: center;"><br><br><br><br>
									( ................................... )
								</td>
							</tr>
						</table>

					</div>
					</div>
				</div>
			</div>
		</div>

	</div>

@endsection

@section('script')
	<!-- BEGIN APP LEVEL JQUERY SCRIPTS -->
	<!--<script src="{{ asset('modules/process/form-neraca-controller.js') }}" type="text/javascript"></script>-->
	<!-- END APP LEVEL JQUERY SCRIPTS -->

@endsection

