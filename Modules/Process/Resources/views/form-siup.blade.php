@extends('pemohon::layouts.master')
@section('title',"Dashboard Pemohon")
@section('style')
	<style>
	.head{
		font-size: 14pt;
	}
	.footer{
		position: absolute;
		right: 1;
		bottom: 0;
		left: 1;
		padding: 10rem;
		font-size: 9pt;
	}
		.borderhide{
			table-border: 0;
		}
	</style>
@endsection
@section('content')
	<div class="page-content-inner">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light portlet-fit ">
					<b style="float:right;">Lampiran I Peraturan Menteri Perdagangan RI <br>
						Nomor : 46/M-DAG/PER/9/2009
					</b>
					<div class="portlet-title">
						<div class="col-md-12" style="text-align:center;">
							<i class=" icon-book font-green"></i>
							<span class="caption-subject center bold uppercase"><h4 class="bold">PERMOHONAN SURAT IZIN USAHA PERDAGANGAN MIKRO <br>(SIUP MIKRO)</h4></span>
						</div>
						<div class="actions">
							<table>
								<tr><td>Kepada :</td></tr>
								<tr><td>YTh. Kepala UP PTSP</td>
								<tr><td>Kelurahan  ………………….. </td>
								<tr><td><br></td></tr>
								<tr><td>Di</td></tr>
								<tr><td>JAKARTA</td></tr>
							</table>
						</div>
					</div>
					<div class="portlet">
						<!-- Begin Form -->
					<table class="table">
						<tr>
							<td>DIISI OLEH PEMILIK / PENGURUS / PENANGGUNG JAWAB
								Diisi/diketik dengan huruf cetak
							</td>
						</tr>
						<tr>
							<td class="head">
								Yang bertanda tangan dibawah ini mengajukan permohonan Surat Izin Usaha Perdagangan ( Mikro /Kecil/Menengah/Besar *) sebagaimana dimaksud dalam Peraturan Menteri Perdagangan Republik Indonesia No. 46/M-DAG/PER/9/2009.
							</td>
						</tr>
					</table>
					<div class="row">
					<div class="col-md-6">
					<table class="table" style="border: none;">
						<tr>
							<td width="5">1.</td>
							<td width="100">Permohonan SIUP Baru * )</td>
							<td width="5">:</td>
							<td width="5"><input type="checkbox" class="form-control" name="siup_baru"></td>
						</tr>
						<tr>
							<td>2.</td>
							<td>Permohonan  Pendaftaran
								Ulang, Perubahan SIUP
							</td>
							<td>:</td>
							<td><input type="checkbox" name="siup_daftar_ulang" class="form-control"></td>
						</tr>
					</table>
					</div>
					</div>
					<table>
						<tr><td class="head">I. Identitas Pemilik / Pengurus / Penanggung Jawab ** )</td></tr>
					</table>

					<table class="table table-bordered">
						<tr>
							<td width="5">1.</td>
							<td width="200">Nama</td>
							<td width="5">:</td>
							<td>{{ $user->fullname }}</td>
						</tr>
						<tr>
							<td>2.</td>
							<td>Alamat tempat tinggal</td>
							<td>:</td>
							<td>data</td>
						</tr>
						<tr>
							<td>3.</td>
							<td>Tempat/tanggal lahir</td>
							<td>:</td>
							<td>data</td>
						</tr>
						<tr>
							<td>4.</td>
							<td>Nomor Telp/Fax</td>
							<td>:</td>
							<td>data</td>
						</tr>
						<tr>
							<td>5.</td>
							<td>Nomor KTP/Paspor</td>
							<td>:</td>
							<td>data</td>
						</tr>
						<tr>
							<td>6.</td>
							<td>Kewarganegaraan</td>
							<td>:</td>
							<td>data</td>
						</tr>
					</table>

					<table>
						<tr><td class="head">II. Identitas Perusahaan</td></tr>
					</table>

					<table class="table table-bordered">
						<tr>
							<td width="5">1.</td>
							<td width="200">Nama Perusahaan</td>
							<td width="5">:</td>
							<td><input type="text" name="nama_perusahaan" class="form-control"> </td>
						</tr>
						<tr>
							<td>2.</td>
							<td>Alamat Perusahaan</td>
							<td>:</td>
							<td><textarea name="alamat_perusahaan" class="form-control"></textarea></td>
						</tr>
						<tr>
							<td>3.</td>
							<td>Nomor Telp/Fax</td>
							<td>:</td>
							<td><input type="text" name="nama_perusahaan" class="form-control"> </td>
						</tr>
						<tr>
							<td>4.</td>
							<td>Provinsi</td>
							<td>:</td>
							<td>data </td>
						</tr>
						<tr>
							<td>5.</td>
							<td>Kabupaten/Kota/Kotamadya</td>
							<td>:</td>
							<td>data</td>
						</tr>
						<tr>
							<td>6.</td>
							<td>Kecamatan</td>
							<td>:</td>
							<td><input type="text" name="nama_perusahaan" class="form-control"> </td>
						</tr>
						<tr>
							<td>7.</td>
							<td>Kelurahan/Desa</td>
							<td>:</td>
							<td><input type="text" name="nama_perusahaan" class="form-control"> </td>
						</tr>
						<tr>
							<td>8.</td>
							<td>Status</td>
							<td>:</td>
							<td><div class="mt-radio-inline">
									<label class="mt-radio">
										<input type="radio" name="status" value="PMA" checked class="form-control"> PMA
										<span></span>
									</label>
									<label class="mt-radio">
										<input type="radio" name="status" value="PMD" class="form-control"> PMD
										<span></span>
									</label>
									<label class="mt-radio">
										<input type="radio" name="status" value="LAIN-LAIN" class="form-control"> Lain-lain
										<span></span>
									</label>
								</div>
							</td>
						</tr>
						<tr>
							<td>9.</td>
							<td>Kode Pos</td>
							<td>:</td>
							<td><input type="text" name="nama_perusahaan" class="form-control"> </td>
						</tr>
					</table>

					<table>
						<tr><td class="head">III. Legalitas Perusahaan</td></tr>
						<tr><td>
								Perusahaan Berbentuk Perseroan Terbatas / Koperasi / CV / Firma ** )
							</td></tr>
					</table>

					<table class="table table-bordered">
						<tr>
							<td width="5">1.</td>
							<td width="200">Akta Pendirian</td>
							<td width="5">:</td>
							<td><input type="text" name="akta_pendirian" class="form-control"></td>
						</tr>
						<tr>
							<td></td>
							<td>a. Nomor & tgl Akta</td>
							<td>:</td>
							<td><input type="text" name="nomor_tgl_akta_pendirian" class="form-control"></td>
						</tr>
						<tr>
							<td></td>
							<td>b. Nomor & tgl Pengesahan</td>
							<td>:</td>
							<td><input type="text" name="nomor_tgl_pengesahan_pendirian" class="form-control"></td>
						</tr>
						<tr>
							<td>2.</td>
							<td>Akta Perubahan</td>
							<td>:</td>
							<td><input type="text" name="akta_perubahan" class="form-control"></td>
						</tr>
						<tr>
							<td></td>
							<td>a. Nomor & tgl Akta</td>
							<td>:</td>
							<td><input type="text" name="nomor_tgl_akta_perubahan" class="form-control"></td>
						</tr>
						<tr>
							<td></td>
							<td>b. Nomor & tgl Pengesahan</td>
							<td>:</td>
							<td><input type="text" name="nomor_tgl_akta_perubahan" class="form-control"></td>
						</tr>
					</table>

					<table>
						<tr><td class="head">IV. Modal dan Saham</td></tr>
					</table>
					<table class="table" border="0" style="border: none;">
						<tr>
							<td width="5">1.</td>
							<td width="200">Modal dan Nilai Kekayaan</td>
							<td width="5">:</td>
							<td><input type="text" name="modal_nilai_kekayaan" class="form-control"></td>
						</tr>
						<tr>
							<td></td>
							<td>Bersih Perusahaan,
								( tidak termasuk tanah dan
								Bangunan tempat usaha )
							</td>
							<td colspan="2"></td>
						</tr>
						<tr>
							<td>2.</td>
							<td>Saham ( Khusus untuk penanam modal asing )</td>
							<td colspan="2"></td>
						</tr>
						<tr>
							<td></td>
							<td>a. Total Nilai Saham</td>
							<td>:</td>
							<td><input type="text" name="total_nilai_saham" class="form-control"></td>
						</tr>
						<tr>
							<td></td>
							<td>b. Komposisi Kepemilikan Saham</td>
							<td colspan="2"></td>
						</tr>
						<tr>
							<td></td>
							<td>- Nasional</td>
							<td>:</td>
							<td><input type="text" name="persen_nasional" class="form-control"> %</td>
						</tr>
						<tr>
							<td></td>
							<td>- Asing</td>
							<td>:</td>
							<td><input type="text" name="persen_asing" class="form-control"> %</td>
						</tr>
					</table>

					<table>
						<tr><td class="head">V. Kegiatan Usaha</td></tr>
					</table>
					<table class="table table-bordered">
						<tr>
							<td width="5">1.</td>
							<td width="200">Kelembagaan</td>
							<td width="5">:</td>
							<td><input type="text" name="kelembagaan" class="form-control"></td>
						</tr>
						<tr>
							<td>2.</td>
							<td>Kegiatan usaha (KBLI 4 Digit) </td>
							<td>:</td>
							<td><input type="text" name="kegiatan_usaha" class="form-control"></td>
						</tr>
						<tr>
							<td>3.</td>
							<td>Barang / jasa dagang utama</td>
							<td>:</td>
							<td><input type="text" name="barang_jasa_utama" class="form-control"></td>
						</tr>
					</table>
					<hr style="border-width:2px; border-style: solid; display: block;"/>

						<table>
							<tr>
								<td>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Demikian Surat Permohonan SIUP ini, kami buat dengan sebenarnya dan apabila dikemudian hari ternyata data atau informasi dan keterangan tersebut tidak benar, maka kami menyatakan bersedia untuk dibatalkan SIUP yang telah kami miliki dan dituntut sesuai dengan peraturan  perundang – undangan.
								</td>
							</tr>
						</table>

						<table style="float:right;">
							<tr>
								<td width="5">Jakarta,</td>
								<td width="10">..........</td>
							</tr>
							<tr>
								<td>Tanda Tangan ***)</td>
								<td></td>
							</tr>
							<tr>
								<td><br><br><br><br>
								...................................
								</td>
							</tr>
						</table>

						<table class="footer">
							<tr>
								<td>Catatan :</td>
							</tr>
							<tr>
								<td>
									* ) beri tanda salah satu <br>
									** ) Coret yang tidak perlu <br>
									*** ) Penanggung Jawab Perusahaan, Cap Perusahaan
								</td>
							</tr>
						</table>

					</div>
				</div>
			</div>
		</div>

	</div>

@endsection

@section('script')
	<!-- BEGIN APP LEVEL JQUERY SCRIPTS -->
	<!--<script src="{{ asset('modules/process/form-siup-controller.js') }}" type="text/javascript"></script>-->
	<!-- END APP LEVEL JQUERY SCRIPTS -->

@endsection

