<?php

namespace Modules\Process\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Packages\Form\FormBuilder;
use Packages\Grid\GridBuilder;
use Illuminate\Support\Facades\Input;
use Modules\Pemohon\Entities\UserProfile;

use Auth;

class PermohonanSiupMikroController extends Controller
{
    public $url = '/process';
    public $title = 'Form SIUP';
    public $create_label = 'Permohonan SIUP MIKRO';
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function formSiup()
    {
        $user = Auth::user();

        return view('process::form-siup')->with(compact("user"));
    }

    public function neraca()
    {
        return view('process::form-neraca');
    }

    public function index()
    {
        return view('process::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('process::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('process::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('process::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
