<?php

namespace Modules\Process\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Pemohon\Entities\PengajuanIzin;

use Modules\Workflow\Entities\Status;
use Modules\Perizinan\Entities\Izin;
use Modules\Perizinan\Entities\IzinProses;
use Modules\Perizinan\Entities\IzinProsesStatus;
use Modules\Perizinan\Entities\IzinProsesStatusTx;
use Modules\Perizinan\Entities\IzinRole;
use Auth;

class ProcessController extends Controller
{
    public $url = '/backoffice';
    public $title = 'Dashboard';

    public function upload_berkas($id = null)
    {
        $data = [
            'url'  => $this->url,
            'redirect_url' => route("permohonan.dashboard"),
            'title' => "Detail Permohonan",
            'user_id' => Auth::user()->id,
            'pengajuan_id' => $id,
            'views' => [ 'workflow::master.grid-pengajuan-izin'
                        ,'workflow::master.grid-pemohon'
//                        ,'workflow::master.form-upload-files'
                        ],
            'js' => ['modules/workflow/grid-pengajuan-izin-controller.js',
                     'modules/workflow/grid-pemohon-controller.js']
        ];

        return view('workflow::container-pemohon', compact('data'));
    }

    public function verifikasi($id = null)
    {
        $result = PengajuanIzin::getDataPengajuanIzin($id)->first();
        $next_status = json_decode($result['next_status']);

        $next_status_result = Status::select(['slug', 'label'])->whereIn("slug", $next_status)->get();

        $data = [
            'url'  => $this->url,
            'title' => "Detail Permohonan",
            'user_id'   => $result['user'],
            'pengajuan_id'   => $id,
            'status'    => $result['status'],
            'next_status'    => $next_status_result,
            'views' => ['workflow::master.grid-pemohon',
                'workflow::master.grid-uploaded-files'],
            'js' => ['modules/workflow/grid-pemohon-controller.js',
                'modules/workflow/grid-uploaded-files-controller.js']
        ];

        return view('workflow::container-backoffice', compact('data'));
    }

    public function view($id = null)
    {
        $data = [
            'url'  => $this->url,
            'pengajuan_id'   => $id
        ];

        return view('workflow::master.grid-detail-permohonan', compact('data'));
    }

    public function save($state = null, $id = null){
        $form = $this->anyForm($id);
        $save = $form->save();

        return response()->json($save);
    }

    public function changeStatus($id = null, $status = null)
    {
        //store to pengajuan_izin table
        $data_pengajuan_izin = PengajuanIzin::find($id);
        $data_pengajuan_izin->status = $status;
        $data_pengajuan_izin->save();

//        $izin_proses = $this->getIzinProses($status, $data_pengajuan_izin->izin);
//
//        //store to izin_proses_status_tx table
//        $data_tx = new IzinProsesStatusTx();
//        $data_tx->pengajuan_izin = $id;
//        $data_tx->izin_proses = $izin_proses;
//        $data_tx->status = $status;
//        $data_tx->save();

        //store to izin_role table
        //$this->saveIzinRole($data_pengajuan_izin->izin, $izin_proses, $status);

        $result = [
            "status" => true,
            "url" => $this->url
        ];

        return response()->json($result);
    }

    public function anyForm($id = null){
        $data = PengajuanIzin::find($id);
        if(empty($data)){
            $data = new PengajuanIzin();
        }

        $form = FormBuilder::source($data);
        $form->title($this->title);
        $form->url($this->url);

        return $form;
    }

    private static function getIzinProses($status, $izin_id){
        $izin_role = IzinRole::select("izin_proses")
            ->where("izin", $izin_id)->first();
        $izin_proses_now = $izin_role["izin_proses"];

        if (empty($izin_proses_now)) {
            $izin_proses = IzinProses::select("id")
                ->where("izin", $izin_id)
                ->orderBy("order", "ASC")->limit(2)->offset(0)->get();
            $izin_proses = $izin_proses[1]["id"];
        }
        else{
            $izin_proses_status = IzinProsesStatus::select("next_proses")
                ->where("izin_proses", $izin_proses_now)
                ->where("status", $status)->first();
            $izin_proses = $izin_proses_status["next_proses"];
        }

        return $izin_proses;
    }

    private static function saveIzinRole($izin_id, $izin_proses, $status){
        $data = IzinProses::select(['proses.route','izin_proses.roles'])
            ->join('proses', 'proses.id', 'izin_proses.proses')
            ->where('izin_proses.id', $izin_proses )->first();

        $next_status = IzinProsesStatus::select("status")
                    ->where("izin_proses", $izin_proses)
                    ->pluck("status");

        $izin_role = IzinRole::firstOrNew(array('izin' => $izin_id));
        $izin_role->izin_proses = $izin_proses;
        $izin_role->route = $data["route"];
        $izin_role->role = $data["roles"];
        $izin_role->status = $status;
        $izin_role->next_status = $next_status;
        $izin_role->save();
    }
}
