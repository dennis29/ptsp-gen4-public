<?php

Route::group(['middleware' => 'web', 'namespace' => 'Modules\Process\Http\Controllers'], function()
{
    Route::group(['prefix' => 'process'], function() {
        Route::get('/', 'ProcessController@index');
        //Form SIUP
        Route::get('form-siup', 'PermohonanSiupMikroController@formSiup');
        Route::get('form-neraca', 'PermohonanSiupMikroController@neraca');
    });

    //BackOffice
    Route::group(['prefix' => 'backoffice'], function() {
        Route::get('verifikasi/{id?}', ['uses' => 'ProcessController@verifikasi', 'as' => 'process.verifikasi']); // verifikasi
        Route::get('change-status/{id?}/{status?}', ['uses' => 'ProcessController@changeStatus', 'as' => 'process.changeStatus']); // change status
    });

    Route::group(['prefix' => 'pemohon'], function() {
        Route::get('upload_berkas/{id?}', ['uses' => 'ProcessController@upload_berkas', 'as' => 'process.uploadBerkas']); // verifikasi
    });

});
