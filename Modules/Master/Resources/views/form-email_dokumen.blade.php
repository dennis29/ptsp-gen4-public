@extends('core::layout')

@section('content')
<json-data model="formData">
{{ collect($form)->toJson() }}
</json-data>
<div class="row" ng-controller="FormEmailDokController">
  <div class="col-md-12">
    <form name="form">
    <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green-sharp bold">@{{ formData.title }}</div>
        @component('core::components.form-action')
        @endcomponent
      </div>
      <div class="portlet-body">
        <div class="row margin-bottom-20">
            <div class="col-md-6">
                <form-input attributes="fields.nama"></form-input>
                <form-input attributes="fields.penanggung_jawab"></form-input>
                <form-input attributes="fields.telp_instansi"></form-input>
                <form-input attributes="fields.telp_penanggung_jawab"></form-input>
                <form-input attributes="fields.active"></form-input>
            </div>
            <div class="col-md-6">
                  <div ng-repeat="email in emails">
                    <form-input type="email" attributes="fields.email" model="email.data" name="email@{{ $index }}">
                </div>
                    <button ng-click="addEmail()" class="btn btn-xs btn-primary" type="button">Add Email </button>
                    <button ng-click="deleteEmail()" class="btn btn-xs btn-danger" type="button">Delete Email </button>

            </div>
        </div>
      </div>
    </div>
    </form>
  </div>
</div>
@endsection

@section('script')
<script src="{{ asset('modules/master/form-emaildok-controller.js') }}" type="text/javascript"></script>
@endsection
