<?php

namespace Modules\Master\Entities;

use Packages\Model\BaseModel;

class JenisKoperasi extends BaseModel
{
    protected $table = 'jenis_koperasi';
    protected $fillable = ['name', 'active'];
}
