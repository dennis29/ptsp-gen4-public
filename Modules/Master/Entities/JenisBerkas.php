<?php

namespace Modules\Master\Entities;

use Packages\Model\BaseModel;

class JenisBerkas extends BaseModel
{
  protected $table = 'jenis_berkas';
  protected $fillable = ['id', 'name', 'description'];

  public static function getOption(){
    return JenisBerkas::select(['id', 'name'])->get()->toArray();
  }
}
