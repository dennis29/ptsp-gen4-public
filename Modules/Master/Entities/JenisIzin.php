<?php

namespace Modules\Master\Entities;

use Packages\Model\BaseModel;

class JenisIzin extends BaseModel
{
    protected $table = 'jenis_izin';
    protected $fillable = ['id', 'name', 'active'];
}
