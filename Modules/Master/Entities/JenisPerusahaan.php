<?php

namespace Modules\Master\Entities;

use Packages\Model\BaseModel;

class JenisPerusahaan extends BaseModel
{
    protected $table = 'jenis_perusahaan';
    protected $fillable = ['code', 'name', 'active'];
}
