<?php

namespace Modules\Master\Entities;

use Packages\Model\BaseModel;

class Bidang extends BaseModel
{
    protected $table = 'bidang';
    protected $fillable = ['code', 'name', 'active'];

    public static function getOption(){
      $bidang = Bidang::select(['id', 'code', 'name'])->orderBy('code')->get()->toArray();

      $bidang = collect($bidang)->map(function($data){
         $data['name'] = $data['code'].'. '.$data['name'];
         return $data;
      })->toArray();

      return $bidang;
    }
}
