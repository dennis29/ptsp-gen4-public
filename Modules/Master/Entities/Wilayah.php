<?php

namespace Modules\Master\Entities;

use Packages\Model\BaseModel;

class Wilayah extends BaseModel
{
  protected $table = 'wilayah';
  protected $fillable = ['id', 'code', 'name', 'lat', 'long', 'description', 'active'];

  public static function getOption(){

    return Wilayah::select(['id', 'name'])->get()->toArray();
  }
}
