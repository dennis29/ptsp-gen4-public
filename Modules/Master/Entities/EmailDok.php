<?php

namespace Modules\Master\Entities;

use Packages\Model\BaseModel;

class EmailDok extends BaseModel
{
    protected $table = 'email_doks';
    protected $fillable = ['nama', 'penanggung_jawab','telp_instansi','telp_penanggung_jawab','email', 'active'];

    public static function getOption(){
      return Bidang::select(['nama','penanggung_jawab','telp_instansi','telp_penanggung_jawab','email'])->get()->toArray();
    }
}
