<?php

use Illuminate\Database\Seeder;

class BidangTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('bidang')->delete();
        
        \DB::table('bidang')->insert(array (
            0 => 
            array (
                'id' => '5a7c5d674f6791610e',
                'code' => 'pendidikan',
                'name' => 'Pendidikan',
                'active' => true,
                'created_by' => 'barkah.hadi',
                'updated_by' => NULL,
                'created_at' => '2018-02-08 14:23:35',
                'updated_at' => '2018-02-08 14:23:35',
            ),
            1 => 
            array (
                'id' => '5a7c5d8713d14ff611',
                'code' => 'kesehatan',
                'name' => 'Kesehatan',
                'active' => true,
                'created_by' => 'barkah.hadi',
                'updated_by' => NULL,
                'created_at' => '2018-02-08 14:24:07',
                'updated_at' => '2018-02-08 14:24:07',
            ),
        ));
        
        
    }
}