<?php

use Illuminate\Database\Seeder;

class JenisPerusahaanTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('jenis_perusahaan')->delete();
        
        \DB::table('jenis_perusahaan')->insert(array (
            0 => 
            array (
                'id' => '5a7c729da5519972cb',
                'code' => 'CV',
                'name' => 'Commanditaire Vennootschap',
                'active' => true,
                'updated_by' => NULL,
                'created_by' => 'barkah.hadi',
                'created_at' => '2018-02-08 15:54:05',
                'updated_at' => '2018-02-08 15:54:05',
            ),
            1 => 
            array (
                'id' => '5a7c72ac349740e283',
                'code' => 'FA',
                'name' => 'Firma',
                'active' => true,
                'updated_by' => NULL,
                'created_by' => 'barkah.hadi',
                'created_at' => '2018-02-08 15:54:20',
                'updated_at' => '2018-02-08 15:54:20',
            ),
            2 => 
            array (
                'id' => '5a7c72bba7e199cb93',
                'code' => 'PT',
                'name' => 'Perseroan Terbatas',
                'active' => true,
                'updated_by' => NULL,
                'created_by' => 'barkah.hadi',
                'created_at' => '2018-02-08 15:54:35',
                'updated_at' => '2018-02-08 15:54:35',
            ),
        ));
        
        
    }
}