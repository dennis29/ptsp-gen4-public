<?php

use Illuminate\Database\Seeder;

class JenisIzinTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('jenis_izin')->delete();
        
        \DB::table('jenis_izin')->insert(array (
            0 => 
            array (
                'id' => '5a7c66fa36ed14e60c',
                'name' => 'Ketataruangan',
                'active' => true,
                'updated_by' => 'barkah.hadi',
                'created_by' => 'barkah.hadi',
                'created_at' => '2018-02-08 15:04:26',
                'updated_at' => '2018-02-08 15:05:22',
            ),
        ));
        
        
    }
}