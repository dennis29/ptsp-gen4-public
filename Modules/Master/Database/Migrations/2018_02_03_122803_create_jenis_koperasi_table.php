<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJenisKoperasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jenis_koperasi', function (Blueprint $table) {

            $table->string('id', 25);
            $table->string('name', 60)->nullable();
            $table->boolean('active')->default(true);

            $table->string('updated_by',25)->nullable();
            $table->string('created_by',25)->nullable();
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jenis_koperasi');
    }
}
