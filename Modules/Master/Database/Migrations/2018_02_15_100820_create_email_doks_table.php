<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailDoksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_doks', function (Blueprint $table) {
            $table->string('id', 25)->nullable();
            $table->string('nama', 60)->nullable();
            $table->string('telp_penanggung_jawab', 60)->nullable();
            $table->string('telp_instansi', 60)->nullable();
            $table->string('penanggung_jawab', 60)->nullable();
            $table->json('email')->nullable();
            $table->boolean('active')->default(true);

            $table->string('updated_by', 25)->nullable();
            $table->string('created_by', 25)->nullable();
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_doks');
    }
}
