<?php
Route::group([
  'prefix' => 'backend',
  'middleware' => ['web', 'auth.admin', 'acl', 'auth.role'],
  'namespace' => 'Modules\Master\Http\Controllers'
], function () {

  Route::group(['prefix' => 'wilayah'], function(){
    Route::get('/', ['uses' => 'WilayahController@index', 'as' => 'wilayah']);
    Route::get('data', ['uses' => 'WilayahController@data', 'as' => 'wilayah.data']);
    Route::get('create', ['uses' => 'WilayahController@create', 'as' => 'wilayah.create']);
    Route::get('update/{id?}', ['uses' => 'WilayahController@update', 'as' => 'wilayah.update']);
    Route::post('save/{id?}', ['uses' => 'WilayahController@save', 'as' => 'wilayah.save']);
    Route::post('delete', ['uses' => 'WilayahController@delete', 'as' => 'wilayah.delete']);
  });

  Route::group(['prefix' => 'bidang'], function(){
    Route::get('/', ['uses' => 'BidangController@index', 'as' => 'bidang']);
    Route::get('data', ['uses' => 'BidangController@data', 'as' => 'bidang.data']);
    Route::get('create', ['uses' => 'BidangController@create', 'as' => 'bidang.create']);
    Route::get('update/{id?}', ['uses' => 'BidangController@update', 'as' => 'bidang.update']);
    Route::post('save/{id?}', ['uses' => 'BidangController@save', 'as' => 'bidang.save']);
    Route::post('delete', ['uses' => 'BidangController@delete', 'as' => 'bidang.delete']);
  });

  Route::group(['prefix' => 'jenis-izin'], function(){
    Route::get('/', ['uses' => 'JenisIzinController@index', 'as' => 'jenisizin']);
    Route::get('data', ['uses' => 'JenisIzinController@data', 'as' => 'jenisizin.data']);
    Route::get('create', ['uses' => 'JenisIzinController@create', 'as' => 'jenisizin.create']);
    Route::get('update/{id?}', ['uses' => 'JenisIzinController@update', 'as' => 'jenisizin.update']);
    Route::post('save/{id?}', ['uses' => 'JenisIzinController@save', 'as' => 'jenisizin.save']);
    Route::post('delete', ['uses' => 'JenisIzinController@delete', 'as' => 'jenisizin.delete']);
  });

  Route::group(['prefix' => 'jenis-berkas'], function(){
    Route::get('/', ['uses' => 'JenisBerkasController@index', 'as' => 'jenisberkas']);
    Route::get('data', ['uses' => 'JenisBerkasController@data', 'as' => 'jenisberkas.data']);
    Route::get('create', ['uses' => 'JenisBerkasController@create', 'as' => 'jenisberkas.create']);
    Route::get('update/{id?}', ['uses' => 'JenisBerkasController@update', 'as' => 'jenisberkas.update']);
    Route::post('save/{id?}', ['uses' => 'JenisBerkasController@save', 'as' => 'jenisberkas.save']);
    Route::post('delete', ['uses' => 'JenisBerkasController@delete', 'as' => 'jenisberkas.delete']);
  });

  Route::group(['prefix' => 'jenis-koperasi'], function(){
    Route::get('/', ['uses' => 'JenisKoperasiController@index', 'as' => 'jeniskoperasi']);
    Route::get('data', ['uses' => 'JenisKoperasiController@data', 'as' => 'jeniskoperasi.data']);
    Route::get('create', ['uses' => 'JenisKoperasiController@create', 'as' => 'jeniskoperasi.create']);
    Route::get('update/{id?}', ['uses' => 'JenisKoperasiController@update', 'as' => 'jeniskoperasi.update']);
    Route::post('save/{id?}', ['uses' => 'JenisKoperasiController@save', 'as' => 'jeniskoperasi.save']);
    Route::post('delete', ['uses' => 'JenisKoperasiController@delete', 'as' => 'jeniskoperasi.delete']);
  });

  Route::group(['prefix' => 'jenis-perusahaan'], function(){
    Route::get('/', ['uses' => 'JenisPerusahaanController@index', 'as' => 'jenisperusahaan']);
    Route::get('data', ['uses' => 'JenisPerusahaanController@data', 'as' => 'jenisperusahaan.data']);
    Route::get('create', ['uses' => 'JenisPerusahaanController@create', 'as' => 'jenisperusahaan.create']);
    Route::get('update/{id?}', ['uses' => 'JenisPerusahaanController@update', 'as' => 'jenisperusahaan.update']);
    Route::post('save/{id?}', ['uses' => 'JenisPerusahaanController@save', 'as' => 'jenisperusahaan.save']);
    Route::post('delete', ['uses' => 'JenisPerusahaanController@delete', 'as' => 'jenisperusahaan.delete']);
  });

  Route::group(['prefix' => 'email_dok'], function(){
    Route::get('/', ['uses' => 'EmailDokumenController@index', 'as' => 'email_dok']);
    Route::get('data', ['uses' => 'EmailDokumenController@data', 'as' => 'email_dok.data']);
    Route::get('create', ['uses' => 'EmailDokumenController@create', 'as' => 'email_dok.create']);
    Route::get('update/{id?}', ['uses' => 'EmailDokumenController@update', 'as' => 'email_dok.update']);
    Route::post('save/{id?}', ['uses' => 'EmailDokumenController@save', 'as' => 'email_dok.save']);
    Route::post('delete', ['uses' => 'EmailDokumenController@delete', 'as' => 'email_dok.delete']);
  });

});
