<?php

namespace Modules\Master\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Packages\Form\FormBuilder;
use Packages\Grid\GridBuilder;
use Illuminate\Support\Facades\Input;

use Modules\Master\Entities\JenisBerkas;

class JenisBerkasController extends Controller
{
    public $url = '/backend/jenis-berkas';
    public $title = 'Jenis Berkas';

    public function index()
    {
      return view('master::grid-jenis-berkas');
    }

    public function data(){

      $grid = GridBuilder::source(JenisBerkas::query());
      $grid->url($this->url);
      $grid->title($this->title);

      $grid->filter('keyword', function($query, $param){
        return $query->where('name', 'ilike', '%'.$param.'%')->orWhere('description', 'ilike', '%'.$param.'%');
      });

      $grid->add('name', 'Jenis Berkas', true);
      $grid->add('description', 'Keterangan', false);
      $data = $grid->build();

      return response()->json($data);

    }

    public function create()
    {
      $form = $this->anyForm();
      $form->build();
      return view('master::form-jenis-berkas', compact('form'));
    }

    public function update($id = null){
      $form = $this->anyForm($id);
      $form->build();

      return view('master::form-jenis-berkas', compact('form'));
    }

    public function save($id = null){
      $form = $this->anyForm($id);
      $save = $form->save();

      return response()->json($save);
    }

    public function delete($id = null){
      $id = Input::get('id');
      JenisBerkas::whereIn('id', $id)->delete();

      return response()->json([
        'status' => true
      ]);
    }

    public function anyForm($id = null){

      $data = JenisBerkas::select('id', 'name', 'description')->find($id);
      if(empty($data)){
        $data = new JenisBerkas();
      }

      $form = FormBuilder::source($data);
      $form->title($this->title);
      $form->url($this->url);
      $form->add('name', 'Nama Berkas', 'text')->rule('required');
      $form->add('description', 'Keterangan', 'textarea');

      return $form;

    }

}
