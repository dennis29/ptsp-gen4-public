<?php

namespace Modules\Master\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Packages\Form\FormBuilder;
use Packages\Grid\GridBuilder;
use Illuminate\Support\Facades\Input;

use Modules\Master\Entities\JenisKoperasi;

class JenisKoperasiController extends Controller
{
    public $url = '/backend/jenis-koperasi';
    public $title = 'Jenis Koperasi';

    public function index()
    {
      return view('master::grid-jenis-koperasi');
    }

    public function data(){

      $grid = GridBuilder::source(JenisKoperasi::query());
      $grid->url($this->url);
      $grid->title($this->title);

      $grid->filter('keyword', function($query, $param){
        return  $query->where('name', 'ilike', "%".$param."%");
      });

      $grid->add('name', 'Jenis Koperasi', true);
      $grid->add('active', 'Aktif', true)->options([1 => 'Aktif', 0 => 'Tidak Aktif']);
      $data = $grid->build();

      return response()->json($data);

    }

    public function create()
    {
      $form = $this->anyForm();
      $form->build();
      return view('master::form-jenis-koperasi', compact('form'));
    }

    public function update($id = null){
      $form = $this->anyForm($id);
      $form->build();

      return view('master::form-jenis-koperasi', compact('form'));
    }

    public function save($id = null){
      $form = $this->anyForm($id);
      $save = $form->save();

      return response()->json($save);
    }

    public function delete($id = null){
      $id = Input::get('id');
      JenisKoperasi::whereIn('id', $id)->delete();

      return response()->json([
        'status' => true
      ]);
    }

    public function anyForm($id = null){

      $data = JenisKoperasi::select('id', 'name', 'active')->find($id);
      if(empty($data)){
        $data = new JenisKoperasi();
      }

      $form = FormBuilder::source($data);
      $form->title($this->title);
      $form->url($this->url);
      $form->add('name', 'Jenis Koperasi', 'text')->rule('required|max:60');
      $form->add('active', 'Aktif', 'switch');

      return $form;
    }
}
