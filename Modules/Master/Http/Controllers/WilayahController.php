<?php

namespace Modules\Master\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Packages\Form\FormBuilder;
use Packages\Grid\GridBuilder;

use Modules\Master\Entities\Wilayah;

class WilayahController extends Controller
{
    public $url = '/backend/wilayah';
    public $title = 'Wilayah';

    public function index()
    {
        return view('master::grid-wilayah');
    }

    public function data(){

      $grid = GridBuilder::source(Wilayah::query());
      $grid->url($this->url);
      $grid->title($this->title);

      $grid->filter('keyword', function($query, $param){
        return $query->where('code', 'ilike', '%'.$param.'%')
        ->orWhere('name', 'ilike', '%'.$param.'%');
      });

      $grid->add('code', 'Kode Wilayah', true);
      $grid->add('name', 'Nama Wilayah', true);
      $grid->add('active', 'Status', false);
      $data = $grid->build();

      return response()->json($data);

    }

    public function create()
    {
      $form = $this->anyForm();
      $form = $form->build();

      return view('master::form-wilayah', compact('form'));
    }

    public function update($id = null){
      $form = $this->anyForm($id);
      $form = $form->build();

      return view('master::form-wilayah', compact('form'));
    }

    public function save($id = null){
      $form = $this->anyForm($id);
      $save = $form->save();

      return response()->json($save);
    }

    public function delete($id = null){
      $id = Input::get('id');
      Wilayah::whereIn('id', $id)->delete();

      return response()->json([
        'status' => true
      ]);
    }

    public function anyForm($id = null){

      $data = Wilayah::find($id);
      if(empty($data)){
        $data = new Wilayah();
      }

      $form = FormBuilder::source($data);
      $form->title($this->title);
      $form->url($this->url);
      $form->add('code', 'Kode Wilayah', 'text')->rule('required');
      $form->add('name', 'Nama Wilayah', 'text')->rule('required');
      $form->add('description', 'Keterangan', 'textarea');
      $form->add('active', 'Status', 'switch');

      return $form;
    }
}
