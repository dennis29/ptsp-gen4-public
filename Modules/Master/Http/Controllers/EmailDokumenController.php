<?php

namespace Modules\Master\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Packages\Form\FormBuilder;
use Packages\Grid\GridBuilder;
use Illuminate\Support\Facades\Input;

use Modules\Master\Entities\EmailDok;

class EmailDokumenController extends Controller
{
    public $url = '/backend/email_dok';
    public $title = 'Email Dokumen';

    public function index()
    {
      return view('master::grid-email_dokumen');
    }

    public function data(){

      $grid = GridBuilder::source(emaildok::query());
      $grid->url($this->url);
      $grid->title($this->title);

      $grid->filter('keyword', function($query, $param){
        return $query->where('nama', 'ilike', "%".$param."%")
                     ->orWhere('penanggung_jawab', 'ilike', "%".$param."%")
                     ->orWhere('telp_instansi', 'ilike', "%".$param."%")
                     ->orWhere('telp_penanggung_jawab', 'ilike', "%".$param."%")
                     ->orWhere('email', 'ilike', "%".$param."%");
      });
      $grid->add('nama', 'Nama', true);
      $grid->add('penanggung_jawab','Penanggung Jawab', true);
      $grid->add('telp_instansi', 'Nomor Telepon Instansi', true);
      $grid->add('telp_penanggung_jawab', 'Nomor Telepon Penanggung Jawab', true);
      $grid->add('email', 'Email', true)->render(function($data){
          $email = json_decode($data['email']);
          $email = implode(', ', $email);

          return $email;
      });

      $grid->add('active', 'Aktif', true)->options([1 => 'Aktif', 0 => 'Tidak Aktif']);
      $data = $grid->build();

      return response()->json($data);
    }

    public function create()
    {
      $form = $this->anyForm();
      $form->build();
      $form->data['email'] = [['data' => '']];
      return view('master::form-email_dokumen', compact('form'));
    }

    public function update($id = null){
      $form = $this->anyForm($id);
      $form->build();
      $email = json_decode($form->data['email']);
      $dataEmail = [];
      if(!empty($email)){
          foreach($email as $val){
            $dataEmail[]['data'] = $val;
          }
      }
      $form->data['email']= $dataEmail;

      return view('master::form-email_dokumen', compact('form'));
    }

    public function save($id = null){

      $form = $this->anyForm($id);
      $form->pre(function($data){

        $email = collect($data['email'])->pluck('data')->toArray();
        $email = json_encode($email);
        $data['email'] = $email;

        return $data;
      });
      $save = $form->save();

      return response()->json($save);
    }

    public function delete($id = null){
      $id = Input::get('id');
      emaildok::whereIn('id', $id)->delete();

      return response()->json([
        'status' => true
      ]);
    }

    public function anyForm($id = null){

      $data = emaildok::find($id);
      if(empty($data)){
        $data = new emaildok();
      }

      $form = FormBuilder::source($data);
      $form->title($this->title);
      $form->url($this->url);
      $form->add('nama', 'Nama Instansi', 'text')->rule('required');
      $form->add('penanggung_jawab', 'Penanggung Jawab', 'text')->rule('required');
      $form->add('telp_instansi', 'Nomor Telepon Instansi', 'text')->rule('required')->attributes(['pattern'=>'[0-9,-]+','title'=>"Number only or - or ,"]);
      $form->add('telp_penanggung_jawab', 'Nomor Telepon Penanggung Jawab', 'text')->rule('required')->attributes(['pattern'=>'[0-9,-]+','title'=>"Number only or - or ,"]);
      $form->add('email', 'Email', 'email')->rule('required')->attributes(['style'=>'width:300px']);
      $form->add('active', 'Aktif', 'switch');

      return $form;
    }

}
