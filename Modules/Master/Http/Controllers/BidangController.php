<?php

namespace Modules\Master\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Packages\Form\FormBuilder;
use Packages\Grid\GridBuilder;
use Illuminate\Support\Facades\Input;

use Modules\Master\Entities\Bidang;

class BidangController extends Controller
{
    public $url = '/backend/bidang';
    public $title = 'Bidang';

    public function index()
    {
      return view('master::grid-bidang');
    }

    public function data(){
      $query = Bidang::query();
      if(empty(Input::get('order'))){
        $query->orderBy('code', 'ASC');
      }

      $grid = GridBuilder::source($query);
      $grid->url($this->url);
      $grid->title($this->title);

      $grid->filter('keyword', function($query, $param){
        return $query->where('huruf', 'ilike', "%".$param."%")
                     ->orWhere('name', 'ilike', "%".$param."%");
      });
      $grid->add('huruf', 'Kode', true);
      $grid->add('name', 'Nama', true);
      $grid->add('active', 'Aktif', true)->options([1 => 'Aktif', 0 => 'Tidak Aktif']);
      $data = $grid->build();

      return response()->json($data);
    }

    public function create()
    {
      $form = $this->anyForm();
      $form->build();
      return view('master::form-bidang', compact('form'));
    }

    public function update($id = null){
      $form = $this->anyForm($id);
      $form->build();

      return view('master::form-bidang', compact('form'));
    }

    public function save($id = null){
      $form = $this->anyForm($id);

      $validateCode = Bidang::where('code', Input::get('code'));
      if($form->model->exists){
        $validateCode->where('code', '!=', $form->model->code);
      }
      $validateCode = $validateCode->first();
      if(!empty($validateCode)){
        return response()->json(['status' => false, 'messages' => 'Kode sudah digunakan!']);
      }

      $save = $form->save();

      return response()->json($save);
    }

    public function delete($id = null){
      $id = Input::get('id');
      Bidang::whereIn('id', $id)->delete();

      return response()->json([
        'status' => true
      ]);
    }

    public function anyForm($id = null){

      $data = Bidang::find($id);
      if(empty($data)){
        $data = new Bidang();
      }

      $form = FormBuilder::source($data);
      $form->title($this->title);
      $form->url($this->url);
      $form->add('huruf', 'Kode Bidang', 'text')->rule('required');
      $form->add('name', 'Nama Bidang', 'text')->rule('required');
      $form->add('active', 'Aktif', 'switch');

      return $form;
    }

}
