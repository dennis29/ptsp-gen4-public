<?php
namespace Modules\Master\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Packages\Form\FormBuilder;
use Packages\Grid\GridBuilder;
use Illuminate\Support\Facades\Input;

use Modules\Master\Entities\JenisPerusahaan;

class JenisPerusahaanController extends Controller
{
    public $url = '/backend/jenis-perusahaan';
    public $title = 'Jenis Perusahaan';

    public function index()
    {
      return view('master::grid-jenis-perusahaan');
    }

    public function data(){

      $grid = GridBuilder::source(JenisPerusahaan::query());
      $grid->url($this->url);
      $grid->title($this->title);

      $grid->filter('keyword', function($query, $param){
        return  $query->where('code', 'ilike', "%".$param."%")
                       ->orWhere('name', 'ilike', "%".$param."%");
      });

      $grid->add('code', 'Kode', true);
      $grid->add('name', 'Jenis Perusahaan', true);
      $grid->add('active', 'Aktif', true)->options([1 => 'Aktif', 0 => 'Tidak Aktif']);
      $data = $grid->build();

      return response()->json($data);

    }

    public function create()
    {
      $form = $this->anyForm();
      $form->build();
      return view('master::form-jenis-perusahaan', compact('form'));
    }

    public function update($id = null){
      $form = $this->anyForm($id);
      $form->build();

      return view('master::form-jenis-perusahaan', compact('form'));
    }

    public function save($id = null){
      $form = $this->anyForm($id);
      $save = $form->save();

      return response()->json($save);
    }

    public function delete($id = null){
      $id = Input::get('id');
      JenisPerusahaan::whereIn('id', $id)->delete();

      return response()->json([
        'status' => true
      ]);
    }

    public function anyForm($id = null){

      $data = JenisPerusahaan::select('id', 'code', 'name', 'active')->find($id);
      if(empty($data)){
        $data = new JenisPerusahaan();
      }

      $form = FormBuilder::source($data);
      $form->title($this->title);
      $form->url($this->url);
      $form->add('code', 'Kode', 'text')->rule('required');
      $form->add('name', 'Jenis Pendidikan', 'text')->rule('required');
      $form->add('active', 'Aktif', 'switch');

      return $form;
    }
}
