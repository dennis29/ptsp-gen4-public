@extends('core::layout')
@section('title',"Dashboard Pemohon")

<json-data model="formData">
    {{ collect($data)->toJson() }}
</json-data>

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green-sharp bold">@{{ formData.title }}</div>
                    <div class="col-md-6 pull-right">
                        <div class="btn-group pull-right">
                            @{{ formData.status.id }}
                            <a href="@{{ formData.url }}" class="btn btn-sm btn-default" ><i class="fa fa-angle-left"></i>&nbsp;&nbsp;Kembali</a>
                        </div>
                    </div>
                </div>

                <div class="portlet-body">
                    @foreach($data['views'] as $view)
                        @include($view)
                    @endforeach
                    @if(isset($data['next_status']))
                    <div class="form-group" ng-controller="FormDetailPermohonanController">
                        <select class="selectpicker" type="text" class="form-control" placeholder="status" id="status" ng-options="status.slug as status.label for status in formData.next_status track by status.slug" ng-model="selectedStatus">
                            <option value="">--Pilih Status--</option>
                        </select>
                        <button class="btn btn-sm btn-primary" ng-click="changeStatus(selectedStatus)">Simpan <i class="fa fa-save"></i></button>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @foreach($data['js'] as $js)
        <script src="{{ asset($js) }}" type="text/javascript"></script>
    @endforeach
    <script src="{{ asset('modules/workflow/form-detail-permohonan-controller.js') }}" type="text/javascript"></script>
@endsection