@extends('core::layout')
@section('title',"Dashboard Pemohon")


<json-data model="formData">
    {{ collect($data)->toJson() }}
</json-data>

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green-sharp bold">Detail Permohonan</div>
                    <div class="col-md-6 pull-right">
                        <div class="btn-group pull-right" ng-controller="FormDetailPermohonanController">
                            <a href="@{{ formData.url }}" class="btn btn-sm btn-default" ><i class="fa fa-angle-left"></i>&nbsp;&nbsp;Kembali</a>
                            <button ng-repeat="btn in buttonStatus" class="btn btn-sm @{{ btn.color }} mt-ladda-btn ladda-button" type="submit" ng-click="changeStatus(btn.id)"  data-style="zoom-out"><span class="ladda-label">@{{btn.label}}&nbsp;&nbsp;<i class="fa fa-mail-reply"></i></span></button>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <!-- BEGIN IDENTITY FORM -->
                    <div class="row" ng-controller="GridProfilPemohonController" ng-init="getData('{{$data['pengajuan_id']}}')">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user"></i> Pemohon</div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                                    </div>
                                </div>
                                @include('pemohon::pemohon.layout-grid-pemohon')
                            </div>
                        </div>
                    </div>
                    <!-- END IDENTITY FORM -->
                    <!-- BEGIN UPLOADED FILES -->
                    <div class="row" ng-controller="GridUploadedFilesController" ng-init="getData('{{$data['pengajuan_id']}}')">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-file"></i> Berkas</div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                                    </div>
                                </div>
                                @include('backoffice::layout-grid-uploaded-files')
                            </div>
                        </div>
                    </div>
                    <!-- END UPLOADED FILES -->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('modules/backoffice/form-detail-permohonan-controller.js') }}" type="text/javascript"></script>
    <script src="{{ asset('modules/backoffice/grid-profil-pemohon-controller.js') }}" type="text/javascript"></script>
    <script src="{{ asset('modules/backoffice/grid-uploaded-files-controller.js') }}" type="text/javascript"></script>
@endsection
