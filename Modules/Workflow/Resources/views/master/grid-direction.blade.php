<div class="row" ng-controller="GridDirectionController" ng-init="getData('{{$data['pengajuan_id']}}')">
  <div class="col-md-10"></div>
  <div class="col-md 4">
    <button type="submit" name="submit" class="btn  btn-pull-right btn-sm btn-primary"> Ajukan Permohonan </button>
    <button ng-repeat="btn in buttonStatus" class="btn btn-sm @{{ btn.color }} mt-ladda-btn ladda-button" type="submit" ng-click="changeStatus(btn.id)"  data-style="zoom-out"><span class="ladda-label">@{{btn.label}}&nbsp;&nbsp;<i class="fa fa-mail-reply"></i></span></button>
  </div>
</div>

@section('script')
  <script src="{{ asset('modules/workflow/grid-pemohon-controller.js') }}" type="text/javascript"></script>
@endsection
