<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-sharp bold">Unggah Berkas</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body" style="display: block;" md-table multiple md-progress="promise">
                <div class="row">
                    <i class="fa fa-warning"></i> Panduan Penamaan File
                    <form action="../assets/global/plugins/dropzone/upload.php" class="dropzone dropzone-file-area" id="my-dropzone" style="width: 500px; margin-top: 50px;">
                        <h3 class="sbold">Tarik file ke area sini</h3>
                        <p> Untuk memudahkan anda mengunggah berkas kepad kamu </p>
                    </form>
                    <div>
                        <div class="col-md-10"></div>
                        <div class="col-md 4">
                            <button type="submit" name="submit" class="btn  btn-pull-right btn-sm btn-primary"> Ajukan Permohonan </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>