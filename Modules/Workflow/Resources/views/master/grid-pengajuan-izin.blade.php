 <div class="row" ng-controller="GridPengajuanIzinController" ng-init="getData('{{$data['pengajuan_id']}}')">
    <div class="col-md-12">
      <div class="portlet light bordered">
        <div class="portlet-title">
          <div class="caption">
            <i class="fa fa-th-list"></i>Permohonan</div>
          <div class="tools">
            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
          </div>
        </div>
        <div class="portlet-body" style="display: block;" md-table multiple md-progress="promise">
          <div class="row">
            <div class="col-md-6">
              <div md-cell class="form-group form-md-line-input has-success">
                <div class="input-icon">
                  <div  class="form-control">@{{ response.data.created_at }}</div>
                  <label for="form_control_1">Tanggal Pengajuan</label>
                </div>
              </div>
              <div md-cell class="form-group form-md-line-input has-success">
                <div class="input-icon">
                  <div  class="form-control">@{{ response.data.tipe_perizinan }}</div>
                  <label for="form_control_1">Tipe Perizinan</label>
                </div>
              </div>
              <div md-cell class="form-group form-md-line-input has-success">
                <div class="input-icon">
                  <div  class="form-control">@{{ response.data.izin_name }}</div>
                  <label for="form_control_1" style="margin-bottom: 10px">Izin</label>
                </div>
              </div>
              {{--<div md-cell class="form-group form-md-line-input has-success">--}}
                {{--<div class="input-icon">--}}
                  {{--<div  class="form-control"></div>--}}
                  {{--<label for="form_control_1">Wilayah</label>--}}
                {{--</div>--}}
              {{--</div>--}}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>