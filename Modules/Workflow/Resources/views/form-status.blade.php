@extends('core::layout')

@section('content')
<json-data model="formData">
{{ collect($form)->toJson() }}
</json-data>
<div class="row" ng-controller="FormStatusController">
  <div class="col-md-12">
    <form name="form">
    <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green-sharp bold">@{{ formData.title }}</div>
        @component('core::components.form-action')
        @endcomponent
      </div>
      <div class="portlet-body">
        <div class="row margin-bottom-20">
          <div class="col-md-8 col-md-offset-2">
            <form-input attributes="fields.slug"></form-input>
            <form-input attributes="fields.name"></form-input>
            <form-input attributes="fields.label"></form-input>
          </div>
        </div>
      </div>
    </div>
    </form>
  </div>
</div>
@endsection

@section('script')
<script src="{{ asset('modules/workflow/form-status-controller.js') }}" type="text/javascript"></script>
@endsection
