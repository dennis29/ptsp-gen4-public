@extends('pemohon::layouts.master')
@section('title',"Dashboard Pemohon")

<json-data model="formData">
    {{ collect($data)->toJson() }}
</json-data>
@section('content')
    <div class="row">
        <div class="col-md-12">
            @if (!empty($data['title']))
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green-sharp bold">@{{ formData.title }}</div>
                    <div class="col-md-6 pull-right">
                        <div class="btn-group pull-right">
                            <a href="@{{ formData.url }}" class="btn btn-sm btn-default" ><i class="fa fa-angle-left"></i>&nbsp;&nbsp;Kembali</a>{{--<button ng-repeat="btn in buttonStatus" class="btn btn-sm @{{ btn.color }} mt-ladda-btn ladda-button" type="submit" ng-click="changeStatus(btn.id)"  data-style="zoom-out"><span class="ladda-label">@{{btn.label}}&nbsp;&nbsp;<i class="fa fa-mail-reply"></i></span></button>--}}
                            </a>
                        </div>
                    </div>
                </div>
            @endif

                <div class="portlet-body">
                    @foreach($data['views'] as $view)
                        @include($view)
                    @endforeach
                    @if(isset($data['redirect_url']))
                    <div class="form-group" ng-controller="FormContainerPemohonController">
                        <button class="btn btn-sm btn-primary" ng-click="changeStatus('diajukan', formData.redirect_url)">Simpan <i class="fa fa-save"></i></button>
                    </div>
                    @endif
                </div>
            @if (empty($data['title']))
            </div>
            @endif
        </div>
    </div>
@endsection

@section('script')
    @foreach($data['js'] as $js)
        <script src="{{ asset($js) }}" type="text/javascript"></script>
    @endforeach
    <script src="{{ asset('modules/workflow/form-container-pemohon-controller.js') }}" type="text/javascript"></script>
@endsection