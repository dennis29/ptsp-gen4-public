@extends('core::layout')

@section('content')
<div class="row" ng-controller="GridOutputController" ng-init="getData()">
  <div class="col-md-12">
    <div class="portlet light bordered">
      <div class="portlet-title">
        <div class="caption font-green-sharp bold ng-binding">Output</div>
      </div>
      <div class="portlet-body">
        <div class="row margin-bottom-20">
          <div class="col-md-12">
            <div class="row nav-grid">
              @component('core::components.grid-filter')
              @endcomponent
            </div>

            @component('core::components.grid-table')
              <td md-cell>@{{ dt.name }}</td>
              <td md-cell>@{{ dt.route_page }}</td>
            @endcomponent

            @component('core::components.grid-navigation')
            @endcomponent
          </div>
        </div>
      </div>
  </div>
</div>
</div>

@endsection

@section('script')
<script src="{{ Module::asset('workflow:grid-output-controller.js') }}" type="text/javascript"></script>
@endsection
