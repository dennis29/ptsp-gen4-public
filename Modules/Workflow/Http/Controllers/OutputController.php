<?php

namespace Modules\Workflow\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Packages\Form\FormBuilder;
use Packages\Grid\GridBuilder;

use Modules\Workflow\Entities\OutputTable;
use Modules\SSO\Entities\Module;
use Modules\SSO\Entities\Role;

class OutputController extends Controller
{
    public $url = '/backend/output';
    public $title = 'Output';
    
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('workflow::grid-output');
    }

    public function data(){

      $outputTable = OutputTable::query();

      $grid = GridBuilder::source($outputTable);
      $grid->url($this->url);
      $grid->title($this->title);

      $grid->filter('keyword', function($query, $param){
        return $query->where('name', 'ilike', '%'.$param.'%');
      });

      $grid->add('name', 'Nama', true);
      $grid->add('route_page', 'Landing Page', true);
      $data = $grid->build();
      
      return response()->json($data);

    }
}
