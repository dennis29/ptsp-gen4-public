<?php

namespace Modules\Workflow\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Packages\Form\FormBuilder;
use Packages\Grid\GridBuilder;

use Modules\Workflow\Entities\MappingOutputTable;
use Modules\SSO\Entities\Module;
use Modules\SSO\Entities\Role;

class MappingOutputController extends Controller
{
    public $url = '/backend/mapping-output';
    public $title = 'Mapping Output';
    
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('workflow::grid-mapping-output');
    }

    public function data(){

      $mappingOutputTable = MappingOutputTable::query();

      $grid = GridBuilder::source($mappingOutputTable);
      $grid->url($this->url);
      $grid->title($this->title);

      $grid->filter('keyword', function($query, $param){
        return $query->where('name', 'ilike', '%'.$param.'%')
        ->orWhere('izin', 'ilike', '%'.$param.'%');
      });

      $grid->add('name', 'Nama', true);
      $grid->add('izin', 'Izin', true);
      $grid->add('output', 'Output', true);
      $grid->add('order', 'Order', true);
      $grid->add('roles', 'Roles', true);
      $data = $grid->build();
      
      return response()->json($data);

    }
}
