<?php

namespace Modules\Workflow\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Pemohon\Entities\UserProfile;
use Modules\Pemohon\Entities\PengajuanIzin;
use Packages\Grid\GridBuilder;
use Packages\Form\FormBuilder;
use Auth;

class MasterController extends Controller
{

    public function data_pemohon($id = null){
        $data= UserProfile::select('*')->where('user_id', $id)->first();

        $data_profile = ["data" => $data->toArray()];

        return response()->json($data_profile);
    }

    public function data_pengajuan_izin($id = null){
        $data = PengajuanIzin::getDataPengajuanIzinByUser($id)->first();

        $data_pengajuan_izin = ["data" => $data->toArray()];

        return response()->json($data_pengajuan_izin);
    }

    public function data_uploaded_files($id = null){

        $query = PengajuanIzin::select(['pengajuan_izin.id','pengajuan_izin.tipe_perizinan'])
            ->join('user_profiles', 'user_profiles.user_id', '=', 'pengajuan_izin.user')
            ->where('pengajuan_izin.id', $id);

//        $query = PengajuanIzin::select(['pengajuan_izin.id','pengajuan_izin.tipe_perizinan'])
//            ->join('folder_berkas_trx', 'folder_berkas_trx.id', '=', 'pengajuan_izin.folder_berkas_trx')
//            ->where('pengajuan_izin.id', $id);
//
        $grid = GridBuilder::source($query);

        $grid->add('id', 'ID', true);
        $grid->add('tipe_perizinan', 'Tipe Perizinan', true);
        $data = $grid->build();

        return response()->json($data);
    }

    public function changeStatus($status = null, $id = null){
        $query = PengajuanIzin::find($id);

        $query->status = $status;
        $query->save();

        return response()->json(["status" => true ]);
    }
}