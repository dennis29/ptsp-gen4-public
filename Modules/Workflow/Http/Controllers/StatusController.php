<?php
namespace Modules\Workflow\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Packages\Form\FormBuilder;
use Packages\Grid\GridBuilder;

use Modules\Workflow\Entities\Status;

class StatusController extends Controller
{
  public $url = '/backend/status';
  public $title = 'Status';

  public function index()
  {
      return view('workflow::grid-status');
  }

  public function data(){

    $query = Status::query();

    $grid = GridBuilder::source($query);
    $grid->url($this->url);
    $grid->title($this->title);

    $grid->filter('keyword', function($query, $param){
      return $query->where('name', 'ilike', '%'.$param.'%')
      ->orWhere('slug', 'ilike', '%'.$param.'%')->orWhere('label', 'ilike', '%'.$param.'%');
    });
    $grid->add('slug', 'Slug', true);
    $grid->add('name', 'Nama Status', true);
    $grid->add('label', 'Label', true);
    $data = $grid->build();

    return response()->json($data);

  }

  public function create()
  {
    $form = $this->anyForm();
    $form = $form->build();

    return view('workflow::form-status', compact('form'));
  }

  public function update($id = null){
    $form = $this->anyForm($id);
    $form = $form->build();

    return view('workflow::form-status', compact('form'));
  }

  public function save($id = null){
    $form = $this->anyForm($id);
    $save = $form->save();

    return response()->json($save);
  }

  public function delete($id = null){
    $id = Input::get('id');
    Status::whereIn('id', $id)->delete();

    return response()->json([
      'status' => true
    ]);
  }

  public function anyForm($id = null){

    $data = Status::find($id);
    if(empty($data)){
      $data = new Status();
    }

    $form = FormBuilder::source($data);
    $form->title($this->title);
    $form->url($this->url);
    $form->add('slug', 'Slug', 'text')->rule('required');
    $form->add('name', 'Nama Status', 'text')->rule('required');
    $form->add('label', 'Label Status', 'text');

    return $form;
  }
}
