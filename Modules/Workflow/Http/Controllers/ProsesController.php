<?php

namespace Modules\Workflow\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Packages\Form\FormBuilder;
use Packages\Grid\GridBuilder;

use Modules\Workflow\Entities\Proses;
use Modules\SSO\Entities\Module;
use Modules\SSO\Entities\Role;

class ProsesController extends Controller
{
    public $url = '/backend/proses';
    public $title = 'Proses';

    public function index()
    {
        return view('workflow::grid-proses');
    }

    public function data(){

      $query = Proses::select(['proses.id', 'proses.name', 'proses.route']);

      $grid = GridBuilder::source($query);
      $grid->url($this->url);
      $grid->title($this->title);

      $grid->filter('keyword', function($query, $param){
        return $query->where('name', 'ilike', '%'.$param.'%')->orWhere('route', 'ilike', '%'.$param.'%');
      });

      $grid->add('name', 'Nama Proses', true);
      $grid->add('route', 'Route', true);
      $data = $grid->build();
      
      return response()->json($data);

    }

    public function create()
    {
      $form = $this->anyForm();
      $form = $form->build();
      $form = $this->dataForm($form);

      return view('workflow::form-proses', compact('form'));
    }

    public function update($id = null){
      $form = $this->anyForm($id);
      $form = $form->build();
      $form = $this->dataForm($form);

      return view('workflow::form-proses', compact('form'));
    }

    public function save($id = null){
      $form = $this->anyForm($id);
      $save = $form->save();

      return response()->json($save);
    }

    public function delete($id = null){
      $id = Input::get('id');
      Proses::whereIn('id', $id)->delete();

      return response()->json([
        'status' => true
      ]);
    }

    public function dataForm($form){
      return $form;
    }

    public function anyForm($id = null){

      $data = Proses::find($id);
      if(empty($data)){
        $data = new Proses();
      }

      $form = FormBuilder::source($data);
      $form->title($this->title);
      $form->url($this->url);
      $form->add('name', 'Nama Proses', 'text')->rule('required');
      $form->add('route', 'Route', 'text')->rule('required');
      $form->add('conf', 'Konfigurasi', 'textarea');

      return $form;
    }
}
