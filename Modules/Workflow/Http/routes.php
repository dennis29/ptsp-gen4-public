<?php

Route::group([
  'prefix' => 'backend',
  'middleware' => ['web'],
  'namespace' => 'Modules\Workflow\Http\Controllers'
], function () {
    Route::group(['middleware' => 'auth.admin', 'acl', 'auth.role'], function(){
      Route::group(['prefix' => 'proses'], function(){
        Route::get('/', ['uses' => 'ProsesController@index', 'as' => 'proses']);
        Route::get('data', ['uses' => 'ProsesController@data', 'as' => 'proses.data']);
        Route::get('create', ['uses' => 'ProsesController@create', 'as' => 'proses.create']);
        Route::get('update/{id?}', ['uses' => 'ProsesController@update', 'as' => 'proses.update']);
        Route::post('save/{id?}', ['uses' => 'ProsesController@save', 'as' => 'proses.save']);
        Route::post('delete', ['uses' => 'ProsesController@delete', 'as' => 'proses.delete']);
      });

      Route::group(['prefix' => 'output'], function(){
        Route::get('/', ['uses' => 'OutputController@index', 'as' => 'output']);
        Route::get('data', ['uses' => 'OutputController@data', 'as' => 'output.data']);
      });

      Route::group(['prefix' => 'mapping-output'], function(){
        Route::get('/', ['uses' => 'MappingOutputController@index', 'as' => 'mapping-output']);
        Route::get('data', ['uses' => 'MappingOutputController@data', 'as' => 'mapping-output.data']);
        Route::get('create', ['uses' => 'MappingOutputController@create', 'as' => 'mapping-output.create']);
        Route::get('update/{id?}', ['uses' => 'MappingOutputController@update', 'as' => 'mapping-output.update']);
        Route::post('save/{id?}', ['uses' => 'MappingOutputController@save', 'as' => 'mapping-output.save']);
        Route::post('delete', ['uses' => 'MappingOutputController@delete', 'as' => 'mapping-output.delete']);
      });

      Route::group(['prefix' => 'status'], function(){
        Route::get('/', ['uses' => 'StatusController@index', 'as' => 'status']);
        Route::get('data', ['uses' => 'StatusController@data', 'as' => 'status.data']);
        Route::get('create', ['uses' => 'StatusController@create', 'as' => 'status.create']);
        Route::get('update/{id?}', ['uses' => 'StatusController@update', 'as' => 'status.update']);
        Route::post('save/{id?}', ['uses' => 'StatusController@save', 'as' => 'status.save']);
        Route::post('delete', ['uses' => 'StatusController@delete', 'as' => 'status.delete']);
      });
    });

    Route::group(['prefix' => 'master'], function(){
        Route::get('data-pemohon/{id?}', ['uses' => 'MasterController@data_pemohon', 'as' => 'master.data_pemohon']);
        Route::get('data-uploaded-files/{id?}', ['uses' => 'MasterController@data_uploaded_files', 'as' => 'master.data_uploaded_files']);
        Route::get('data-pengajuan-izin/{id?}', ['uses' => 'MasterController@data_pengajuan_izin', 'as' => 'master.data_pengajuan_izin']);
        Route::get('change-status/{status}/{id}', 'MasterController@changeStatus'); // change status
        Route::get('data', ['uses' => 'MasterController@data', 'as' => 'status.data']);
        Route::get('create', ['uses' => 'MasterController@create', 'as' => 'status.create']);
        Route::get('update/{id?}', ['uses' => 'MasterController@update', 'as' => 'status.update']);
        Route::post('save/{id?}', ['uses' => 'MasterController@save', 'as' => 'status.save']);
        Route::post('delete', ['uses' => 'MasterController@delete', 'as' => 'status.delete']);
    });
});

