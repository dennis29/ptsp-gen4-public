<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMappingOutputTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mapping_output', function (Blueprint $table) {
            $table->string('id', 25);
            $table->string('name', 255);
            $table->integer('order');
            $table->string('izin', 25);
            $table->string('output', 25);
            $table->string('roles', 255);
            $table->text('email_penerima')->nullable();
            
            $table->string('created_by', 25)->nullable();
            $table->string('updated_by', 25)->nullable();
            $table->timestamps();
            
            $table->primary('id');
            
            $table->foreign('izin')
                ->references('id')->on('izin')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('output')
                ->references('id')->on('output')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mapping_output');
    }
}
