<?php

namespace Modules\Workflow\Entities;

use Packages\Model\BaseModel;

class OutputTable extends BaseModel
{
    protected $table = 'output';
    protected $fillable = ['id', 'name', 'route_page', 'route_conf'];
    
    public static function getOption(){
      return OutputTable::select(['id', 'name'])->get()->toArray();
    }
}
