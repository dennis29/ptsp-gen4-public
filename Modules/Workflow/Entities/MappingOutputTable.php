<?php

namespace Modules\Workflow\Entities;

use Packages\Model\BaseModel;

class MappingOutputTable extends BaseModel
{
    protected $table = 'mapping_output';
    protected $fillable = ['id', 'izin', 'output', 'name', 'order', 'roles', 'email_penerima'];
}
