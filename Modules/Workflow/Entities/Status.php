<?php

namespace Modules\Workflow\Entities;

use Packages\Model\BaseModel;

class Status extends BaseModel
{
    protected $table = 'status';
    protected $fillable = ['id', 'slug', 'name', 'label'];

    public static function getOption(){
      return Status::select(['slug', 'label'])->get()->toArray();
    }

    public function getStatus($slug)
    {
        dd($slug);
        return Status::select(['slug', 'label'])->whereIn("slug", $slug)->get();
    }

    public function izinProsesStatusTxs()
    {
        return $this->hasMany('Modules\Perizinan\Entities\IzinProsesStatusTx');
    }

    public function pengajuanIzins()
    {
        return $this->hasMany('Modules\Pemohon\Entities\PengajuanIzin');
    }
}
