<?php

namespace Modules\Workflow\Entities;

use Packages\Model\BaseModel;

class Proses extends BaseModel
{
    protected $table = 'proses';
    protected $fillable = ['id', 'name', 'route', 'conf'];

    public static function getOption(){
      return Proses::select(['id', 'name'])->get()->toArray();
    }
}
