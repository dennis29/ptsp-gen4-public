<?php

Route::group(['middleware' => 'web', 'namespace' => 'Modules\Core\Http\Controllers'], function()
{
    Route::get('/', 'CoreController@index');
    Route::get('/media', 'MediaController@index');
    Route::any('/media/upload', 'MediaController@upload');
    Route::any('/media/data', 'MediaController@getMedia');
    Route::any('/media/delete/{id}', 'MediaController@delete');
    Route::any('/media/change-berkas/{id}/{jb}', 'MediaController@changeBerkas');
});
