<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js" data-ng-app="MetronicApp"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js" data-ng-app="MetronicApp"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" data-ng-app="MetronicApp">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <title>DPMPTSP DKI JAKARTA</title>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> -->
        <link href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/angularjs/plugins/angular-xeditable/css/xeditable.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/angularjs/material/angular-material.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/angularjs/material-datatable/md-data-table.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/ladda/ladda-themeless.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/bootstrap-toastr/toastr.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN DYMANICLY LOADED CSS FILES(all plugin and page related styles must be loaded between GLOBAL and THEME css files ) -->
        <link id="ng_load_plugins_before" />
        <!-- END DYMANICLY LOADED CSS FILES -->
        <!-- BEGIN THEME STYLES -->
        <!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
        <link href="{{ asset('css/components-md.min.css') }}" id="style_components" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/plugins-md.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('layouts/layout/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('layouts/layout/css/themes/grey.min.css') }}" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{{ asset('layouts/layout/css/custom.css') }}" rel="stylesheet" type="text/css" />
        @yield('style')
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="{{ asset('favicon.png') }}" /> </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
    <!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
    <!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
    <!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
    <!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
    <!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
    <!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->

    <body ng-controller="AppController" class="page-header-fixed page-sidebar-closed-hide-logo" ng-class="{'page-content-white': settings.layout.pageContentWhite,'page-container-bg-solid': settings.layout.pageBodySolid, 'page-sidebar-closed': settings.layout.pageSidebarClosed}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" ng-model="csrf_token">
        <!-- BEGIN PAGE SPINNER -->
        <div ng-spinner-bar class="page-spinner-bar">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
        <!-- END PAGE SPINNER -->
        <!-- BEGIN HEADER -->
        <div data-ng-controller="HeaderController" class="page-header navbar navbar-fixed-top">
          @include('core::tpl.header')
        </div>

        <!-- END HEADER -->
        <div class="clearfix"> </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div data-ng-controller="SidebarController" class="page-sidebar-wrapper">
              @include('core::tpl.sidebar')
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN ACTUAL CONTENT -->
                    <div ui-view class="fade-in-up ng-hide" ng-hide="!settings.loaded">
                      @yield('content')
                    </div>
                    <!-- END ACTUAL CONTENT -->
                </div>
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
            <!-- <div data-ng-include="'{{ url('core/tpl/quick-sidebar') }}'" data-ng-controller="QuickSidebarController" class="page-quick-sidebar-wrapper"></div> -->
            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div data-ng-controller="FooterController" class="page-footer">
          @include('core::tpl.footer')
        </div>
        <!-- END FOOTER -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE JQUERY PLUGINS -->
        <!--[if lt IE 9]>
	<script src="../assets/global/plugins/respond.min.js"></script>
	<script src="../assets/global/plugins/excanvas.min.js"></script>
	<![endif]-->
        <script src="{{ asset('plugins/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/js.cookie.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/moment.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/ladda/spin.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/ladda/ladda.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap-toastr/toastr.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
        <!-- END CORE JQUERY PLUGINS -->
        <!-- BEGIN CORE ANGULARJS PLUGINS -->
        <script src="{{ asset('plugins/angularjs/angular.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/angularjs/angular-sanitize.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/angularjs/angular-touch.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/angularjs/angular-moment.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/angularjs/plugins/angular-ui-router.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/angularjs/plugins/ocLazyLoad.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/angularjs/plugins/ui-bootstrap-tpls.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/angularjs/material/angular-material.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/angularjs/angular-animate.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/angularjs/angular-aria.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/angularjs/angular-messages.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/angularjs/material-datatable/md-data-table.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/json-data/json-data.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/angularjs/plugins/angular-xeditable/js/xeditable.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/angularjs/fileupload/ng-file-upload-shim.min.js') }}"></script>
        <script src="{{ asset('plugins/angularjs/fileupload/ng-file-upload.min.js') }}"></script>
        <!-- END CORE ANGULARJS PLUGINS -->
        <!-- BEGIN APP LEVEL ANGULARJS SCRIPTS -->
        <script src="{{ asset('js/main.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/directives.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/form-input.js') }}" type="text/javascript"></script>
        <!-- END APP LEVEL ANGULARJS SCRIPTS -->
        <!-- BEGIN APP LEVEL JQUERY SCRIPTS -->
        <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
        <script src="{{ asset('layouts/layout/scripts/layout.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/quick-sidebar.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/quick-nav.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('layouts/layout/scripts/demo.min.js') }}" type="text/javascript"></script>
        @yield('script')
        <!-- END APP LEVEL JQUERY SCRIPTS -->
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->

</html>
