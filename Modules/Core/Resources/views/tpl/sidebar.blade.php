<div class="page-sidebar navbar-collapse collapse">
    <!-- BEGIN SIDEBAR MENU -->
    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <ul class="page-sidebar-menu margin-top-20" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" ng-class="{'page-sidebar-menu-closed': settings.layout.pageSidebarClosed}">
        <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
        <!-- <li class="sidebar-search-wrapper"> -->
            <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
            <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
            <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
            <!-- <form class="sidebar-search sidebar-search-bordered" action="#" method="POST">
                <a href="javascript:;" class="remove">
                    <i class="icon-close"></i>
                </a>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <a href="javascript:;" class="btn submit">
                            <i class="icon-magnifier"></i>
                        </a>
                    </span>
                </div>
            </form> -->
            <!-- END RESPONSIVE QUICK SEARCH FORM -->
        <!-- </li> -->
        <li class="start nav-item">
            <a ui-sref="dashboard">
                <i class="fa fa-dashboard"></i>
                <span class="title">Dashboard</span>
            </a>
        </li>
        @if(Auth::user()->hasModule(['user', 'role', 'module', 'action']))
        <li class="nav-item {{ active(['backend/user', 'backend/user/*', 'backend/role', 'backend/role/*', 'backend/action', 'backend/action/*', 'backend/module', 'backend/module/*']) }}">
            <a href="#" class="nav-link nav-toggle">
                <i class="fa fa-wrench"></i>
                <span class="title">Pengaturan</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                @if(Auth::user()->hasModule('user'))
                <li class="{{ active(['backend/user', 'backend/user/*']) }}">
                    <a href="{{ url('backend/user') }}">
                        <i class="fa fa-user"></i> <span>Pengguna</span>
                    </a>
                </li>
                @endif
                @if(Auth::user()->hasModule('role'))
                <li class="{{ active(['backend/role', 'backend/role/*']) }}">
                    <a href="{{ url('backend/role') }}">
                        <i class="fa fa-unlock-alt"></i> <span>Role</span>
                    </a>
                </li>
                @endif
                @if(Auth::user()->hasModule('module'))
                <li class="{{ active(['backend/module', 'backend/module/*']) }}">
                    <a href="{{ url('backend/module') }}">
                        <i class="fa fa-puzzle-piece"></i> <span>Module</span>
                    </a>
                </li>
                @endif
                @if(Auth::user()->hasModule('action'))
                <li class="{{ active(['backend/action', 'backend/action/*']) }}">
                    <a href="{{ url('backend/action') }}">
                        <i class="fa fa-magic"></i> Action<span></span>
                    </a>
                </li>
                @endif
            </ul>
        </li>
        @endif

            <a href="#" class="nav-link nav-toggle">
                <i class="fa fa-database"></i>
                <span class="title">Master</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
              @if(Auth::user()->hasModule('wilayah'))
              <li class="{{ active(['backend/wilayah', 'backend/wilayah/*']) }}">
                  <a href="{{ url('backend/wilayah') }}">
                      <i class="fa fa-institution"></i> Kantor Wilayah<span></span>
                  </a>
              </li>
              @endif
              @if(Auth::user()->hasModule('bidang'))
              <li class="{{ active(['backend/bidang', 'backend/bidang/*']) }}">
                  <a href="{{ url('backend/bidang') }}">
                      <i class="fa fa-list-alt"></i> Bidang<span></span>
                  </a>
              </li>
              @endif
              @if(Auth::user()->hasModule('jenisizin'))
              <li class="{{ active(['backend/jenis-izin', 'backend/jenis-izin/*']) }}">
                  <a href="{{ url('backend/jenis-izin') }}">
                      <i class="fa fa-file-text-o"></i> Jenis Izin<span></span>
                  </a>
              </li>
              @endif
              @if(Auth::user()->hasModule('jenisberkas'))
              <li class="{{ active(['backend/jenis-berkas', 'backend/jenis-berkas/*']) }}">
                  <a href="{{ url('backend/jenis-berkas') }}">
                      <i class="fa fa-file-text"></i> Jenis Berkas</span>
                  </a>
              </li>
              @endif
              @if(Auth::user()->hasModule('jenisperusahaan'))
              <li class="{{ active(['backend/jenis-perusahaan', 'backend/jenis-perusahaan/*']) }}">
                  <a href="{{ url('backend/jenis-perusahaan') }}">
                      <i class="fa fa-building"></i> Jenis Perusahaan<span></span>
                  </a>
              </li>
              @endif
              @if(Auth::user()->hasModule('jeniskoperasi'))
              <li class="{{ active(['backend/jenis-koperasi', 'backend/jenis-koperasi/*']) }}">
                  <a href="{{ url('backend/jenis-koperasi') }}">
                      <i class="fa fa-cubes"></i> Jenis Koperasi<span></span>
                  </a>
              </li>
              @endif
              @if(Auth::user()->hasModule('email_dok'))
              <li class="{{ active(['backend/email_dok', 'backend/email_dok/*']) }}">
                  <a href="{{ url('backend/email_dok') }}">
                      <i class="fa fa-cubes"></i> Email Dokumen<span></span>
                  </a>
              </li>
              @endif
            </ul>
        </li>
        @endif

        @if(Auth::user()->hasModule(['izincategory']))
        <li class="nav-item {{ active(['backend/izin-category', 'backend/izin-category/*', 'backend/izin', 'backend/izin/*']) }}">
            <a href="#" class="nav-link nav-toggle">
                <i class="fa fa-files-o"></i>
                <span class="title">Perizinan</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
              @if(Auth::user()->hasModule('izincategory'))
              <li class="{{ active(['backend/izin-category', 'backend/izin-category/*']) }}">
                  <a href="{{ url('backend/izin-category') }}">
                      <i class="fa fa-list"></i> Kategori Izin</span>
                  </a>
              </li>
              @endif
              @if(Auth::user()->hasModule('izin'))
              <li class="{{ active(['backend/izin', 'backend/izin/*']) }}">
                  <a href="{{ url('backend/izin') }}">
                      <i class="fa fa-file-text"></i> Izin</span>
                  </a>
              </li>
              @endif
            </ul>
        </li>
        @endif

        @if(Auth::user()->hasModule(['proses']))
        <li class="nav-item {{ active(['backend/proses', 'backend/proses/*', 'backend/status', 'backend/status/*']) }}">
            <a href="#" class="nav-link nav-toggle">
                <i class="fa fa-sitemap"></i>
                <span class="title">Alur Proses</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
              @if(Auth::user()->hasModule('proses'))
              <li class="{{ active(['backend/proses', 'backend/proses/*']) }}">
                  <a href="{{ url('backend/proses') }}">
                      <i class="fa fa-refresh"></i> Proses</span>
                  </a>
              </li>
              @endif
              @if(Auth::user()->hasModule('proses'))
              <li class="{{ active(['backend/status', 'backend/status/*']) }}">
                  <a href="{{ url('backend/status') }}">
                      <i class="fa fa-check"></i> Status</span>
                  </a>
              </li>
              @endif
            </ul>
        </li>
        @endif
    </ul>
    <!-- END SIDEBAR MENU -->
</div>
