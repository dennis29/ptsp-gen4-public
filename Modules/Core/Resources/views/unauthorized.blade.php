@extends('core::layout')

@section('content')
<div class="row">
                            <div class="col-md-12 page-404">
                                <div class="number font-green"> <i class="fa fa-lock"></i> </div>
                                <div class="details">
                                    <h3>Oops! Akses ditolak.</h3>
                                    <p> Anda tidak memiliki otorisasi untuk mengakses halaman ini!
                                      
                                </div>
                            </div>
                        </div>
                    </div>
@endsection

@section('style')
<link href="{{ asset('pages/css/error.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
