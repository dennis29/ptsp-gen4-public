<script type="text/ng-template" id="formMediaPopup.html">
  <div class="modal-body">
    <div class="upload-container" ng-hide="!showUpload" ngf-drop="uploadFiles($files)" ng-model="files" ngf-model-invalid="invalidFiles" ngf-multiple="true" ngf-drag-over-class="dragOverClass">
      <p class="upload-drop">Drop file kesini.</p>
      <p>Atau</p>
      <button class="btn btn-default btn-sm" ngf-select="uploadFiles($files)" ngf-multiple="true"  ng-model="files"><i class="fa fa-upload"></i> Upload Berkas</button>
      <i class="fa fa-times upload-close" ng-click="hideUpload()"></i>
    </div>
    <div class="media-container" ng-hide="showUpload">
      <div class="row margin-bottom-10">
        <div class="col-md-3">
          <div class="form-group form-md-line-input">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Cari Berkas" ng-model="keyword" ng-change="keywordChanged()">
                <span class="input-group-addon">
                  <i class="fa fa-search"></i>
                </span>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <form-input type="select" options="item.id as item.name for item in formData.optionJenisBerkas" model="type"></form-input>
        </div>
        <div class="col-md-3 pull-right" style="text-align: right; margin-top: 18px;">
          <button type="button" class="btn btn-primary btn-sm" ng-click="openUpload()"><i class="fa fa-plus"></i> Tambah Berkas</button>
        </div>
      </div>
      <div class="row">
        <div class="col-md-8">
          <div class="media-list">
            <div class="media-item" ng-repeat="media in medias | filter:keyword | orderBy:'-order'" ng-click="showAttributes(media, $index)" ng-class="(indexActive == $index) ? 'active' : ''">
              <div class="media-thumb">
                <div ng-if="media.id != null">
                  <img src="@{{ media.thumbUrl }}" alt="media.file_name"/>
                </div>
                <div ng-if="media.id == null" class="media-loading">
                  <img src="{{ asset('img/spinner.svg') }}" style="width: 100%;">
                </div>
              </div>
              <div class="media-title">
                @{{ media.file_name | strLimit:true:35 }}
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="media-attributes">
            <div ng-hide="mediaActive == null">
              <h4 style="margin-top: 0;">Detil Berkas</h4>
              <div class="text-center margin-bottom-10">
                <a ng-href="@{{ mediaActive.url }}" style="width: 60%;display: inline-block;max-height: 135px;overflow: hidden;" target="_blank">
                  <img src="@{{ mediaActive.url }}" alt="mediaActive.file_name" style="vertical-align: middle;width: 100%;"/>
                </a>
              </div>
              <div class="row margin-bottom-5">
                <div class="col-md-4 text-right">
                  <label class="control-label">Nama File:</label>
                </div>
                <div class="col-md-8">
                  <input class="form-control" ng-model="mediaActive.file_name"/>
                </div>
              </div>
              <div class="row margin-bottom-10">
                <div class="col-md-4 text-right">
                  <label class="control-label">Jenis Berkas:</label>
                </div>
                <div class="col-md-8" style="position: relative;">
                  <select class="form-control" ng-model="mediaActive.jenis_berkas" ng-options="item.id as item.name for item in formData.optionJenisBerkas" ng-change="changeJenisBerkas(mediaActive.id)">
                    <option value="">- Pilih Jenis Berkas -</option>
                  </select>
                  <img src="{{ asset('img/input-spinner.gif') }}" alt="Loading" style="position: absolute;top: 9px;right: 32px;" ng-hide="!changingJenisBerkas" />
                </div>
              </div>
              <div class="row margin-bottom-5">
                <div class="col-md-4 text-right">
                  <label class="control-label">Url:</label>
                </div>
                <div class="col-md-8">
                  <input class="form-control" ng-model="mediaActive.url" readonly/>
                </div>
              </div>
              <div class="row margin-bottom-5">
                <div class="col-md-4 text-right">Size:</div>
                <div class="col-md-8">
                  @{{ bytesToSize(mediaActive.size) }}
                </div>
              </div>
              <div class="row margin-bottom-5">
                <div class="col-md-4 text-right">Type:</div>
                <div class="col-md-8">
                  @{{ mediaActive.type }}
                </div>
              </div>
              <div class="row margin-bottom-5">
                <div class="col-md-4 text-right">Tanggal:</div>
                <div class="col-md-8">
                  @{{ mediaActive.created_at }}
                </div>
              </div>
              <div class="row margin-bottom-5">
                <div class="col-md-4 text-right"></div>
                <div class="col-md-8">
                  <img src="{{ asset('img/input-spinner.gif') }}" alt="Loading" ng-hide="!deleting" />
                  <a href="javascript:;" ng-click="delete(mediaActive.id)" class="text-danger" ng-hide="deleting"><i class="fa fa-trash"></i> Hapus Berkas</a>
                </div>
              </div><br/><br/>
              <button class="btn btn-primary btn-block btn-sm" ng-click="addFile()"><i class="fa fa-plus"></i>&nbsp;Input Berkas</button>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</script>
<style type="text/css">
.modal-lg{
  width: 1000px;
}
</style>
