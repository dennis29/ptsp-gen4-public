<div class="caption font-blue bold">@{{ response.title }}</div>
<div class="actions pull-right">
  @if(Auth::user()->has('create'))
  <a href="@{{ response.url+'/create' }}" class="btn btn-sm btn-primary" ><i class="fa fa-plus"></i>&nbsp;&nbsp;Tambah</a>
  @endif
</div>
