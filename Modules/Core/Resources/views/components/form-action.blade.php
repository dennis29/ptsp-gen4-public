<div class="actions pull-right">
  <a href="@{{ formData.url }}" class="btn btn-sm btn-default" ><i class="fa fa-angle-left"></i>&nbsp;&nbsp;Kembali</a>
@if(Auth::user()->has('delete'))
  <a href="javascript:;" class="btn btn-sm btn-danger" link="@{{ formData.url+'/delete' }}" delete-confirm="@{{ formData.data.id }}" ng-hide="formData.action == 'create'">Hapus&nbsp;&nbsp;<i class="fa fa-trash"></i></a>
@endif

@if(Auth::user()->has('create') || Auth::user()->has('update'))
  <button class="btn btn-sm green mt-ladda-btn ladda-button" type="submit" ng-click="saveAndReturn($event, formData.url)"  data-style="zoom-out" ng-hide="formData.action == 'view'"><span class="ladda-label">Simpan dan Kembali&nbsp;&nbsp;<i class="fa fa-mail-reply"></i></span></button>
  <button class="btn btn-sm green-haze mt-ladda-btn ladda-button" type="submit" ng-click="save($event)" data-style="zoom-out" ng-hide="formData.action == 'view'"><span class="ladda-label">Simpan&nbsp;&nbsp;<i class="fa fa-floppy-o"></i></span></button>
@endif
</div>
