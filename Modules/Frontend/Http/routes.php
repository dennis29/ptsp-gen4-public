<?php

Route::group(['middleware' => 'web', 'namespace' => 'Modules\Frontend\Http\Controllers'], function()
{
    Route::get('/', ['as' => 'landingpage', 'uses' => 'HomeController@index']);

	Route::get('auth/{provider}', 'AuthController@redirectToProvider');
	Route::get('auth/{provider}/callback', 'AuthController@handleProviderCallback');

	Route::get('register/verify', [
	    'as' => 'verify',
	    'uses' => 'RegisteredController@verify'
	]);

	Route::get('register/thankyou', [
	    'as' => 'thankyou',
	    'uses' => 'RegisteredController@thankyou'
	]);
});

