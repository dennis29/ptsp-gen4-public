<?php

namespace Modules\Frontend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
Use Modules\Frontend\Entities\UserProvider;
Use Modules\SSO\Entities\User;
use Auth;
use Socialite;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Modules\Pemohon\Entities\UserProfile;

class AuthController extends Controller
{
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();

        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::login($authUser, true);

        return redirect()->route('pemohon.dashboard');
    }

    public function findOrCreateUser($user, $provider)
    {
        $saved_user = User::where('email', $user->email)->first();
        if ($saved_user) {
            $user_providers = UserProvider::where(['provider_id' => $user->id, 'user_id' => $saved_user->id])->first();

            if ($user_providers) {
                return $saved_user;
            }
            else{
                $this->createUserProvider($saved_user->id, $user, $provider);
                return $saved_user;
            }
        }   
        else {
            $userCreated = User::create([
                'fullname' => $user->name,
                'email' => $user->email,
                'password' => bcrypt(substr(md5(mt_rand()), 0, 5)),
                'picture' => $user->avatar,
                'confirmed' => 1
            ]);

            $this->createUserProvider($userCreated->id, $user, $provider);
            return $userCreated;    
        }    
    }

    public function createUserProvider($saved_user_id, $user, $provider) {
        return UserProvider::create([
            'user_id'       => $saved_user_id,  
            'provider'      => $provider,
            'provider_id'   => $user->id,
            'image'         => $user->avatar
        ]);
    }
}
