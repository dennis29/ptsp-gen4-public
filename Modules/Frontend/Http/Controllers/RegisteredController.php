<?php

namespace Modules\Frontend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\SSO\Entities\User;

class RegisteredController extends Controller
{
    public function thankyou(Request $request)
    {
        return view('frontend::registered.thankyou')->with('request', $request);
    }

    public function verify(Request $request)
    {
        if( ! $request->code)
        {
            return view('frontend::registered.verify');
        }

        $user = User::where(['confirmation_code' => $request->code, 'email' => $request->email])->first();
        if ( ! $user)
        {
            return view('frontend::registered.verify');
        }

        $user->confirmed = 1;
        $user->confirmation_code = null;
        $user->save();

        return redirect()->route('login')->with('info','You have successfully verified your account. You can login now');
    }
}
