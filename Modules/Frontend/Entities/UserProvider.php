<?php

namespace Modules\Frontend\Entities;

use Illuminate\Database\Eloquent\Model;

class UserProvider extends Model
{
    protected $fillable = ['user_id', 'provider', 'provider_id', 'image'];
}
