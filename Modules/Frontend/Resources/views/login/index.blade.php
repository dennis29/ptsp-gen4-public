@extends('frontend::layouts.master')

@section('header-menu')
  @include('frontend::layouts.header-menu')
@endsection

@section('content')
<div class="py-5">
    <div class="container">
      <div class="row">
        @if (session('info'))
          <div class="alert alert-info" role="alert">
              <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
              {{ session('info') }}
          </div>
        @endif
        @if($errors->any())
          <div class="alert alert-info" role="alert">
              <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
              {{$errors->first()}}
          </div>
        @endif
        <div class="col-md-12">
          <h3 class="text-center">@lang('frontend::user.login.title')</h3>
          <h5 class="text-center">@lang('frontend::user.login.sub_title')</h5>
          <br>
          <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-4">
              <br>
              <div class="text-center">
                <a class="btn btn-primary form-control rounded-4" href="{{ url('/auth/google') }}"><span class="fa fa-google btn-social"></span>@lang('frontend::user.login.login_with_google')</a>
              </div>
              <br>
              <div class="text-center">
                <a class="btn btn-primary form-control rounded-4" href="{{ url('/auth/facebook') }}"><span class="fa fa-facebook btn-social"></span>@lang('frontend::user.login.login_with_facebook')</a>
              </div>
              <br>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-4">
              <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="email" type="email" class="form-control" name="email" placeholder="@lang('frontend::user.login.username')" value="{{ old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="password" class="form-control" placeholder="@lang('frontend::user.login.password')" name="password" required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-check"> <label class="form-check-label"> <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>  @lang('frontend::user.login.remember_me') </label> </div>
                  </div>
                  <div class="col-md-6 text-right"><a href="{{ route('password.request') }}">@lang('frontend::user.login.forgot_password')</a></div>
                </div><br>
                <div class="form-group">
                  <button type="submit" class="btn btn-primary form-control">@lang('frontend::user.login.login')</button></div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection