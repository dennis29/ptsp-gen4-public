@extends('frontend::layouts.master')

@section('header-menu')
  @include('frontend::layouts.header-menu')
@endsection

@section('title')
    Verifikasi Alamat Email
@endsection

@section('content')
<div class="py-5">
    <div class="container">
      <div class="row">
          <h2>Verifikasi Alamat Email</h2>
        <div>
            Terima kasih telah melakukan registrasi. Silahkan klik <a href="{{ URL::to('register/verify?code=' . $request->code . '&email='. $request->email) }}">link</a> ini untuk melakukan verifikasi email. <br/>
        </div>
      </div>
    </div>
  </div>
@endsection