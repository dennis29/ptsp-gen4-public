@extends('frontend::layouts.master')

@section('header-menu')
    @include('frontend::layouts.header-menu')
@endsection

@section('title')
    Verifikasi Alamat Email
@endsection

@section('content')
    <div class="py-5">
        <div class="container">
            <div class="row">
                <h2>Verifikasi Alamat Email</h2>
                <div>
                    Verifikasi alamat email Anda belum berhasil mohon dicek kembali email Anda.
                    Terima kasih
                </div>
            </div>
        </div>
    </div>
@endsection