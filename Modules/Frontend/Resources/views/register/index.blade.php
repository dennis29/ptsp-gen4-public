@extends('frontend::layouts.master')

@section('header-menu')
  @include('frontend::layouts.header-menu')
@endsection

@section('content')
<div class="py-5">
    <div class="container">
      <div class="row">
        @if($errors->any())
          <div class="alert alert-info" role="alert">
              <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
              {{$errors->first()}}
          </div>
        @endif
        <div class="col-md-12">
          <h3 class="text-center">@lang('frontend::user.register.title')</h3>
          <h5 class="text-center">@lang('frontend::user.register.sub_title')</h5>
          <br>
          <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-4">
              <br><br>
              <div class="text-center">
                <a class="btn btn-primary form-control rounded-4" href="{{ url('/auth/google') }}"><span class="fa fa-google btn-social"></span>@lang('frontend::user.register.register_with_google')</a>
              </div>
              <br>
              <div class="text-center">
                <a class="btn btn-primary form-control rounded-4" href="{{ url('/auth/facebook') }}"><span class="fa fa-facebook btn-social"></span>@lang('frontend::user.register.register_with_facebook')</a>
              </div>
              <br>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-4">
              <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                <div class="form-group{{ $errors->has('fullname') ? ' has-error' : '' }}">
                    <input id="fullname" type="text" class="form-control" name="fullname" value="{{ old('fullname') }}" placeholder="@lang('frontend::user.register.fullname')" required autofocus>
                    @if ($errors->has('fullname'))
                        <span class="help-block">
                            <strong>{{ $errors->first('fullname') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="@lang('frontend::user.register.username')" required>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="password" class="form-control" name="password" placeholder="@lang('frontend::user.register.password')" required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                  <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="@lang('frontend::user.register.retype_password')" required>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-primary form-control">@lang('frontend::user.register.register')</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection