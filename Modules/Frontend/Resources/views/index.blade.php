@extends('frontend::layouts.master')
@section('title', 'Page Title')

@section('styles')
<link href="{{ asset('plugins/bootstrap-table/bootstrap-table.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('plugins/layerslider/css/layerslider.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('header-menu')
  @include('frontend::layouts.header-menu')
@endsection

@section('content')

<div class="row" style="margin: 5px 0 20px 0;">
  <!-- BEGIN HEADER SEARCH BOX -->
        <form class="search-form" action="extra_search.html" method="GET">
            <div class="input-group" style="height: 40px;width: 80%;margin: auto;position: relative;">
                <input type="text" class="form-control" id="query" name="query" style="height: 40px;position: initial;">
                <span class="typer" id="typer" data-words="Membuat IMB, Membuat SIUP, Membuat ijin online" data-colors="grey" data-delay="100" data-deleteDelay="1000" style="position: absolute;left: 2px;padding:10px;"></span>
                <span class="input-group-btn" style="background: #C1CDD4;">
                    <a href="javascript:;" class="btn submit">
                        <i class="icon-magnifier"></i>
                    </a>
                </span>
            </div>
        </form>
        <!-- END HEADER SEARCH BOX -->
</div>

@include('frontend::layouts.slider')

<div class="row" style="margin-bottom: 20px;  margin-top: 20px;">
  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <a class="dashboard-stat dashboard-stat-v2 green" href="#">
          <div class="visual">
              <i class="fa fa-line-chart"></i>
          </div>
          <div class="details">
              <div class="number">
                  200,000.00
              </div>
              <div class="desc"> Nilai Investasi </div>
          </div>
      </a>
  </div>
  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
    <a class="dashboard-stat dashboard-stat-v2 yellow" href="#">
        <div class="visual">
            <i class="fa fa-file-text-o"></i>
        </div>
        <div class="details">
            <div class="number">289</div>
            <div class="desc"> Total Perizinan </div>
        </div>
    </a>
  </div>
  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
    <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
        <div class="visual">
            <i class="fa fa-users"></i>
        </div>
        <div class="details">
            <div class="number">30,000</div>
            <div class="desc"> Total Pengguna </div>
        </div>
    </a>
  </div>
</div>

<div class="row" style="margin-bottom: 40px;">
  <div class="card col-lg-5 col-md-6 col-sm-6 col-xs-12">
    <div class="card-header"> Pertumbuhan Investasi </div>
    <div class="card-body">
      <div id="investasiChart" class="chart" style="height: 180px;"> </div>
    </div>
  </div>

  <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12"></div>

  <div class="card col-lg-5 col-md-6 col-sm-6 col-xs-12">
    <div class="card-header"> Pertumbuhan Pemohon </div>
    <div class="card-body">
      <div id="userChart" class="chart" style="height: 180px;"> </div>
    </div>
  </div>

</div>

<div class="row" style="margin-bottom: 40px;">
  <div class="col-md-8 col-md-offset-2">
          <table data-toggle="table" data-height="188" >
              <thead>
                  <tr>
                      <th>Nama Perusahaan</th>
                      <th>Tanggal Pengajuan</th>
                      <th>Status Perizinan</th>
                  </tr>
              </thead>
              <tbody id="permit_progress">
                <tr>
                  <td>PT. Wijaya Karya, Tbk</td>
                  <td>05 Januari 2018</td>
                  <td>Diajukan</td>
                </tr>
                <tr>
                  <td>PT. Tunas Ridean, Tbk</td>
                  <td>08 Desember 2017</td>
                  <td>Diproses</td>
                </tr>
                <tr>
                  <td>PT. Adikarya</td>
                  <td>21 November 2017</td>
                  <td>Selesai</td>
                </tr>
                <tr>
                  <td>PT. Electronic Data Interchange (EDI).</td>
                  <td>28 Desember 2017</td>
                  <td>Disetujui</td>
                </tr>
                <tr>
                  <td>PT. Wijaya Karya, Tbk</td>
                  <td>05 Januari 2018</td>
                  <td>Diajukan</td>
                </tr>
                <tr>
                  <td>PT. Tunas Ridean, Tbk</td>
                  <td>08 Desember 2017</td>
                  <td>Diproses</td>
                </tr>
                <tr>
                  <td>PT. Adikarya</td>
                  <td>21 November 2017</td>
                  <td>Selesai</td>
                </tr>
                <tr>
                  <td>PT. Electronic Data Interchange (EDI).</td>
                  <td>28 Desember 2017</td>
                  <td>Disetujui</td>
                </tr>
                <tr>
                  <td>PT. Wijaya Karya, Tbk</td>
                  <td>05 Januari 2018</td>
                  <td>Diajukan</td>
                </tr>
                <tr>
                  <td>PT. Tunas Ridean, Tbk</td>
                  <td>08 Desember 2017</td>
                  <td>Diproses</td>
                </tr>
                <tr>
                  <td>PT. Adikarya</td>
                  <td>21 November 2017</td>
                  <td>Selesai</td>
                </tr>
                <tr>
                  <td>PT. Electronic Data Interchange (EDI).</td>
                  <td>28 Desember 2017</td>
                  <td>Disetujui</td>
                </tr>
              </tbody>
          </table>
  </div>
</div>
@endsection

@section('scripts')
<script src="{{ Module::asset('frontend:js/typer.js') }}"></script>
<script src="{{ Module::asset('frontend:js/common.js') }}"></script>
<script src="{{ Module::asset('frontend:js/chart.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-table/bootstrap-table.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/amcharts/amcharts/amcharts.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/amcharts/amcharts/serial.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/jssor-slider/js/jssor.slider.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    jssor_1_slider_init();

    // chart
    initChartSample1();

    //autoscroll permit_progress table
    $('#permit_progress').infiniteScrollUp();
</script>
@endsection
