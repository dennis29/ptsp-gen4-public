<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js" data-ng-app="MetronicApp"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js" data-ng-app="MetronicApp"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" data-ng-app="MetronicApp">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <title>PTSP DKI  @yield('title')</title>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN DYMANICLY LOADED CSS FILES(all plugin and page related styles must be loaded between GLOBAL and THEME css files ) -->
        <link id="ng_load_plugins_before" />
        <!-- END DYMANICLY LOADED CSS FILES -->
        <!-- BEGIN THEME STYLES -->
        <!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
        <link href="{{ asset('css/components.min.css') }}" id="style_components" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('layouts/layout3/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('layouts/layout3/css/themes/default.min.css') }}" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{{ asset('layouts/layout3/css/custom.min.css') }}" rel="stylesheet" type="text/css" />
        @yield('styles')
        <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="{{ asset('favicon.png') }}" /> 
        <link href="{{ Module::asset('frontend:css/common.css') }}" rel="stylesheet" type="text/css"
    <!-- END HEAD -->
  </head>
    <!-- BEGIN BODY -->
    <!-- DOC: Apply "page-header-menu-fixed" class to set the mega menu fixed  -->
    <!-- DOC: Apply "page-header-top-fixed" class to set the top menu fixed  -->
    <body ng-controller="AppController">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <!-- BEGIN HEADER -->
                    <!-- <div data-ng-include="'tpl/header.html'" data-ng-controller="HeaderController" class="page-header"> </div> -->
                    <div class="page-header" data-ng-controller="HeaderController">
                      @include('frontend::layouts.header')
                      @yield('header-menu')
                    </div>
                    <!-- END HEADER -->
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <!-- BEGIN CONTAINER -->
                    <div class="page-container">
                        @yield('pagehead')
                        <!-- BEGIN PAGE CONTENT -->
                        <div class="page-content">
                            <div class="container">
                                <!-- BEGIN ACTUAL CONTENT -->
                                <div class="fade-in-up">
                                  @yield('content')
                                </div>
                                <!-- END ACTUAL CONTENT -->
                            </div>
                        </div>
                        <!-- END PAGE CONTENT -->
                    </div>
                    <!-- END CONTAINER -->
                </div>
            </div>
            <div class="page-wrapper-row">
                <div class="page-wrapper-bottom">
                    <!-- BEGIN FOOTER -->
                    <div data-ng-controller="FooterController">
                      @include('frontend::layouts.footer')
                    </div>
                    <!-- END FOOTER -->
                </div>
            </div>
        </div>
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE JQUERY PLUGINS -->
        <!--[if lt IE 9]>
	<script src="../assets/global/plugins/respond.min.js"></script>
	<script src="../assets/global/plugins/excanvas.min.js"></script>
	<![endif]-->
        <script src="{{ asset('plugins/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/jquery-migrate.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/js.cookie.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
        <!-- END CORE JQUERY PLUGINS -->
        <!-- BEGIN CORE ANGULARJS PLUGINS -->
        {{--<script src="{{ asset('plugins/angularjs/angular.min.js') }}" type="text/javascript"></script>--}}
        {{--<script src="{{ asset('plugins/angularjs/angular-sanitize.min.js') }}" type="text/javascript"></script>--}}
        {{--<script src="{{ asset('plugins/angularjs/angular-touch.min.js') }}" type="text/javascript"></script>--}}
        {{--<script src="{{ asset('plugins/angularjs/plugins/angular-ui-router.min.js') }}" type="text/javascript"></script>--}}
        {{--<script src="{{ asset('plugins/angularjs/plugins/ocLazyLoad.min.js') }}" type="text/javascript"></script>--}}
        {{--<script src="{{ asset('plugins/angularjs/plugins/ui-bootstrap-tpls.min.js') }}" type="text/javascript"></script>--}}
        {{--<script src="{{ asset('plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>--}}
        {{--<!-- END CORE ANGULARJS PLUGINS -->--}}
        {{--<!-- BEGIN APP LEVEL ANGULARJS SCRIPTS -->--}}
        {{--<script src="{{ asset('js/main.js') }}" type="text/javascript"></script>--}}
        {{--<script src="{{ asset('js/directives.js') }}" type="text/javascript"></script>--}}
        <!-- END APP LEVEL ANGULARJS SCRIPTS -->
        <!-- BEGIN APP LEVEL JQUERY SCRIPTS -->
        <script src="{{ asset('js/app.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('layouts/layout3/scripts/layout.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/quick-sidebar.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/quick-nav.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('layouts/layout3/scripts/demo.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/web.js') }}" type="text/javascript"></script>
        <!-- END APP LEVEL JQUERY SCRIPTS -->
        @yield('scripts')
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->

</html>