<!-- BEGIN HEADER MENU -->
<div class="page-header-menu">
    <div class="container">
        <!-- BEGIN MEGA MENU -->
        <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
        <!-- DOC: Remove dropdown-menu-hover and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->

        <div class="hor-menu">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="/">Beranda</a>
                </li>
                <li class="menu-dropdown classic-menu-dropdown ">
                    <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;"> Antrian Online
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu pull-left">
                        <li>
                            <a href="http://ptsp.jakarta.go.id/antrian" target="_blank">Regular</a>
                        </li>
                        <li>
                            <a href="http://ptsp.jakarta.go.id/antrian/ODS" target="_blank">One Day Service</a>
                        </li>
                        <li>
                            <a href="http://ptsp.jakarta.go.id/antrian/fasttrack" target="_blank">Fast Track</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-dropdown classic-menu-dropdown ">
                    <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;"> Berita
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu pull-left">
                        <li>
                            <a href="javascript:;">Cara Mendaftar</a>
                        </li>
                        <li>
                            <a href="javascript:;">Informasi & Publikasi</a>
                        </li>
                        <li>
                            <a href="javascript:;">Regulasi</a>
                        </li>
                        <li>
                            <a href="javascript:;">Pelaporan Whistle Blowing Sistem</a>
                        </li>
                        <li>
                            <a href="javascript:;">Penanaman Modal</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-dropdown classic-menu-dropdown ">
                    <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;"> Profil
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu pull-left">
                        <li>
                            <a href="javascript:;">Perizinan</a>
                        </li>
                        <li>
                            <a href="javascript:;">Izin Terlaris</a>
                        </li>
                        <li>
                            <a href="javascript:;">Kemudahan Berbisnis</a>
                        </li>
                        <li>
                            <a href="javascript:;">Tentang PTSP</a>
                        </li>
                        <li>
                            <a href="javascript:;">Berita</a>
                        </li>
                        <li>
                            <a href="javascript:;">Visi - Misi</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="">FAQ</a>
                </li>
                <li>
                    <a href="">Lokasi</a>
                </li>
                <li class="menu-dropdown mega-menu-dropdown mega-menu-full ">
                    <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="dropdown-toggle"> Izin Terlaris
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="mega-menu-content">
                                <div class="row">
                                    <div class="col-md-3">
                                        <ul class="mega-menu-submenu">
                                          <li>
                                              <h3>Sample Column</h3>
                                          </li>
                                          <li>
                                              <a href="#">
                                                  <i class="fa fa-angle-right"></i> Sample Link </a>
                                          </li>
                                          <li>
                                              <a href="#">
                                                  <i class="fa fa-angle-right"></i> Sample Link
                                                  <span class="badge badge-roundless badge-danger">new</span>
                                              </a>
                                          </li>
                                          <li>
                                              <a href="#">
                                                  <i class="fa fa-angle-right"></i> Sample Link </a>
                                          </li>
                                          <li>
                                              <a href="#">
                                                  <i class="fa fa-angle-right"></i> Sample Link </a>
                                          </li>
                                          <li>
                                              <a href="#">
                                                  <i class="fa fa-angle-right"></i> Sample Link </a>
                                          </li>
                                          <li>
                                              <a href="#">
                                                  <i class="fa fa-angle-right"></i> Sample Link </a>
                                          </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3">
                                        <ul class="mega-menu-submenu">
                                            <li>
                                                <h3>Sample Column</h3>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-angle-right"></i> Sample Link </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-angle-right"></i> Sample Link
                                                    <span class="badge badge-roundless badge-danger">new</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-angle-right"></i> Sample Link </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-angle-right"></i> Sample Link </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-angle-right"></i> Sample Link </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-angle-right"></i> Sample Link </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3">
                                        <ul class="mega-menu-submenu">
                                            <li>
                                                <h3>Sample Column</h3>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-angle-right"></i> Sample Link </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-angle-right"></i> Sample Link</a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-angle-right"></i> Sample Link</a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-angle-right"></i> Sample Link
                                                    <span class="badge badge-roundless badge-success">new</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-angle-right"></i> Sample Link</a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-angle-right"></i> Sample Link</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3">
                                        <ul class="mega-menu-submenu">
                                            <li>
                                                <h3>Sample Column</h3>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-angle-right"></i> Sample Link</a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-angle-right"></i> Sample Link</a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-angle-right"></i> Sample Link</a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-angle-right"></i> Sample Link</a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-angle-right"></i> Sample Link</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- END MEGA MENU -->
    </div>
</div>
<!-- END HEADER MENU -->