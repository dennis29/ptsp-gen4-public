<!-- BEGIN PRE-FOOTER -->
<div class="page-prefooter">
    <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 footer-block">
              <h2>Kontak</h2>
              <address>
                Jl. HR. RASUNA SAID Kav. 22, Jakarta Selatan<br/>
                Call Center 1500164 / (021)1500164 (non Telkomsel)
                <br/>
              </address>
          </div>
          <div class="col-md-3 col-sm-6 col-xs12 footer-block">
              <h2>Email</h2>
              <div class="subscribe-form">
                  <form action="javascript:;">
                      <div class="input-group">
                          <input type="text" placeholder="mail@email.com" class="form-control">
                          <span class="input-group-btn">
                              <button class="btn" type="submit">Submit</button>
                          </span>
                      </div>
                  </form>
              </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12 footer-block">
              <h2>Follow Us On</h2>
              <ul class="social-icons">
                  <li>
                      <a href="javascript:;" data-original-title="facebook" class="facebook"></a>
                  </li>
                  <li>
                      <a href="javascript:;" data-original-title="twitter" class="twitter"></a>
                  </li>
              </ul>
          </div>

        </div>
    </div>
</div>
<!-- END PRE-FOOTER -->
<!-- BEGIN POST-FOOTER -->
<div class="page-footer">
    <div class="container"> &copy; 2018 DPMPTSP Provinsi DKI JAKARTA. </div>
</div>
<div class="scroll-to-top">
    <i class="icon-arrow-up"></i>
</div>
<!-- END POST-FOOTER -->
