<!-- BEGIN HEADER TOP -->
<div class="page-header-top">
    <div class="container">
        <!-- BEGIN LOGO -->
        <a href="/">
        <div class="page-logo">
            <img src="{{ asset('img/logo-dki-small.png') }}">
            <h1>DINAS PENANAMAN MODAL DAN PTSP</h1>
            <h4>PROVINSI DKI JAKARTA</h4>
        </div>
        </a>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler"></a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <div class="top-menu">
          <div class="btn-group">
              <a class="btn dropdown-toggle" data-toggle="dropdown" href="javascript:;" aria-expanded="true" style="color: #333;">
                  <img src="{{ asset('img/flags/id.png') }}"> Bahasa <i class="fa fa-angle-down"></i>
              </a>
              <ul class="dropdown-menu dropdown-menu-right">
                  <li>
                      <a href="javascript:;">
                        <img src="{{ asset('img/flags/id.png') }}">
                        Bahasa
                      </a>
                  </li>
                  <li>
                      <a href="javascript:;">
                        <img src="{{ asset('img/flags/gb.png') }}">
                        English
                      </a>
                  </li>
              </ul>
          </div>
          &nbsp;&nbsp;
          @guest
            <a href="{{ route('login') }}" style="font-weight: bold;">Login</a>
            <a href="javascript:;">|</a>
            <a href="{{ route('register') }}" style="font-weight: bold;">Register</a>
          @else
            <div class="btn-group">
              <a class="btn dropdown-toggle" data-toggle="dropdown" href="javascript:;" aria-expanded="true" style="color: #333;">
                    {{ Auth::user()->fullname }} <i class="fa fa-angle-down"></i>
              </a>
              <ul class="dropdown-menu dropdown-menu-right">
                  <li>
                      <a href="{{ route('logout') }}"
                          onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                          Logout
                      </a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          {{ csrf_field() }}
                      </form>
                  </li>
              </ul>
            </div>
          @endguest
        </div>
    </div>
</div>

<div id="WidgetEvaID" style='background-color:#337ab7'></div>
<script language="javascript">
   var set = new Array();
    set["eva_k"] = "mYynpZKMaKOafJ%2Bokoyfo5min6ySaHmq";
    set["eva_i"] = "699864";
    set["eva_d"] = 2;
    set["eva_t"] = "C";
    set["eva_l"] = true;
    set["eva_ty"] = "login";
    (function(i,s,o,g,r,a,m){i["evaid"]=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,"script","https://api.eva.id/widget2/evaid.js", set);
</script>
<style>
#__wws ._flat,#__wws ._clog > button[type="submit"],#__wws .__mmssg ._bbl {background-color: #337ab7;}
</style>

<script type="text/javascript">
window.onload = function() {

    setTimeout(function()
    {
       if(jQuery("#nm"))
       {
            jQuery("#nm").val('jakarta');
            jQuery("#em").val('jakarta@jakarta.go.id');
            jQuery("#___send").click();
       }

       if(jQuery("._bbl").text())
          jQuery("._bbl").html('Halo Warga Jakarta, ada yang bisa kami bantu? <br/> jika ada kesulitan silahkan hubungi ke nomor : 021-2761203 atau email ke pelayanan@jakarta.go.id');

    }, 2400);

}
</script>