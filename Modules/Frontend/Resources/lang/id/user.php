<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'login' => [
        'title'                 => "Masuk Pelayanan Jakarta",
        'sub_title'             => "Belum punya akun? daftar <a href='register'>di sini</a>",
        'username'              => "Masukan email",
        'password'              => "Masukan password",
        'remember_me'           => "Ingat Saya",
        'forgot_password'       => "Lupa Password?",
        'login_with_google'     => "Masuk dengan Google",
        'login_with_facebook'   => "Masuk dengan Facebook",
        'login'                 => "Masuk"
    ],
    'register' => [
        'title'                 => "Daftar Pelayanan Jakarta",
        'sub_title'             => "Sudah punya akun? masuk di <a href='login'>di sini</a>",
        'fullname'              => "Masukan nama lengkap",
        'username'              => "Masukan email",
        'password'              => "Masukan password",
        'retype_password'       => "Masukan ulang password",
        'remember_me'           => "Ingat Saya",
        'forgot_password'       => "Lupa Password?",
        'register_with_google'  => "Daftar dengan Google",
        'register_with_facebook'=> "Daftar dengan Facebook",
        'register'              => "Daftar"
    ]
];