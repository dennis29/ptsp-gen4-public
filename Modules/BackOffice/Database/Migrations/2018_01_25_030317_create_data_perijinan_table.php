<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataPerijinanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_perijinan', function (Blueprint $table) {
            $table->string('id', 25);
            $table->string('tipe_perijinan', 255);
            $table->string('status_perijinan', 255);
            $table->string('bidang_perijinan', 255);
            $table->string('tanggal_mengajukan', 255);
            $table->string('due_date', 255);
            $table->text('keterangan')->nullable();

            $table->string('created_by', 25)->nullable();
            $table->string('updated_by', 25)->nullable();
            $table->timestamps();
            
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_perijinan');
    }
}
