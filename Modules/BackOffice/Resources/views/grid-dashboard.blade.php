@extends('core::layout')
@section('title',"Dashboard")

@section('content')
    <div class="row" ng-controller="GridDashboardController" ng-init="getData()">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green-sharp bold">@{{ response.title }}</div>
                </div>
                <div class="portlet-body">
                    <div class="row nav-grid">
                        <div>
                            @component('core::components.grid-filter')
                            @endcomponent
                        </div>
                    </div>

                    @component('pemohon::components.grid-table')
                        <td md-cell>@{{ dt.tipe_perizinan }}</td>
                        <td md-cell>@{{ dt.izin_name }}</td>
                        <td md-cell>@{{ dt.created_at | amDateFormat:'DD MMM YYYY' }}</td>
                        <td md-cell><span class="label label-sm label-warning"> @{{ dt.status }} </span></td>

                        <td md-cell style="text-align:center;">
                            <a ng-href="@{{ dt.route }}" class="btn btn-sm btn-success">
                                <i class="fa fa-edit"></i> View
                            </a>
                        </td>
                    @endcomponent

                    @component('core::components.grid-navigation')
                    @endcomponent
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('modules/backoffice/grid-dashboard-controller.js') }}" type="text/javascript"></script>
@endsection
