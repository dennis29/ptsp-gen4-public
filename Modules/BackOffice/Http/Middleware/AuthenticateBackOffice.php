<?php

namespace Modules\BackOffice\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class AuthenticateBackOffice
{
    protected $auth;

    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next, ...$guards)
    {

      if (!Auth::guard($guards)->check()) {
          dd(Auth::user());
          if ($request->ajax() || $request->wantsJson()) {
            // return response()->json(['status' => '_logout']);
            return response('Unauthorized.', 401);
          } else {
            return redirect()->route('admin.login');
          }
      }

        return $next($request);
    }
}
