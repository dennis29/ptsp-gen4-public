<?php

namespace Modules\BackOffice\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Modules\Pemohon\Entities\PengajuanIzin;
use Packages\Grid\GridBuilder;
use Packages\Form\FormBuilder;
use Modules\Perizinan\Entities\IzinProsesStatusTx;
use Modules\Perizinan\Entities\Izin;
use Auth;

class BackOfficeController extends Controller
{
    public $url = '/backoffice';
    public $title = 'Dashboard';

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('backoffice::grid-dashboard');
    }

    public function data_dashboard(){
        $query = PengajuanIzin::getDataPengajuanIzin();

        $grid = GridBuilder::source($query);
        $grid->url($this->url);
        $grid->title($this->title);

        $grid->filter('keyword', function($query, $param){
            return $query->where('status.name', 'ilike', '%'.$param.'%')
                ->where('pengajuan_izin.status','!=', 'selesai' )
                ->orWhere('pengajuan_izin.tipe_perizinan', 'ilike', '%'.$param.'%')
                ->orWhere('izin.name', 'ilike', '%'.$param.'%');
        });

        $grid->add('tipe_perizinan', 'Tipe Perizinan', true);
        $grid->add('izin_name', 'Izin', true);
        $grid->add('created_at', 'Tanggal Mengajukan', false);
        $grid->add('status', 'Status Perizinan', true);
        $data = $grid->build();

        $data['data'] = collect($data['data'])->map(function($dt){
            if (!empty($dt['next_route'])) {
                $dt['route'] = route($dt['next_route'], $dt['id']);
            }
           return $dt;
        });

        return response()->json($data);
    }
}
