<?php

Route::group([
  'prefix' => 'backoffice',
  'middleware' => ['web'],//,'authenticate.backoffice', 'acl', 'auth.role'],
  'namespace' => 'Modules\BackOffice\Http\Controllers'
], function () {
    Route::get('/', ['uses' => 'BackOfficeController@index', 'as' => 'backoffice.dashboard']); // view
    Route::get('data-dashboard', ['uses' => 'BackOfficeController@data_dashboard', 'as' => 'backoffice.dataDashboard']); // data dashboard
});
