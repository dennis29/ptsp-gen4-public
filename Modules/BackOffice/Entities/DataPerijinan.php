<?php

namespace Modules\BackOffice\Entities;

//use Illuminate\Database\Eloquent\Model;
use Packages\Model\BaseModel;

class DataPerijinan extends BaseModel
{
    protected $table = 'data_perijinan';
    protected $fillable = ['id', 'tipe_perijinan', 'status_perijinan', 'bidang_perijinan', 'tanggal_mengajukan', 'due_date', 'keterangan'];
}
