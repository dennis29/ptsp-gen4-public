MetronicApp.directive('formInput', function ($http, $compile, $filter, $parse, FileUploader) {

  var buildAttributes = function(attributes){
    if(typeof attributes == 'undefined'){
      return;
    }

    var attrString = [];
    for(var attr in attributes){
      var attrVal = attributes[attr];
      var notAttr = ['class', 'model', 'name', 'type', 'options', 'label', 'help', 'disabled'];
      if(notAttr.indexOf(attr) != -1){
        continue;
      }

      if(typeof attributes[attr] == 'string')
        attributes[attr].trim();
      if(attrVal == ''){
        attrString.push(attr);
      }else{
        attrString.push(attr+'="'+attributes[attr]+'"');
      }
    }
    return (attrString) ? ' '+attrString.join(' ') : '';
  }


  return {
    restrict: 'E',
    link: function (scope, element, attrs) {
      var fieldAttributes = (attrs.attributes) ? attrs.attributes : '';
      var required = '<span class="required">*</span>';
      if(fieldAttributes){
        fieldAttributes = $parse(fieldAttributes)(scope);
        var fieldName = (fieldAttributes.name) ? fieldAttributes.name : '';
        var fieldType = (fieldAttributes.type) ? fieldAttributes.type : '';
        var fieldClass = (fieldAttributes.class) ? fieldAttributes.class : '';
        var fieldLabel = (fieldAttributes.label) ? fieldAttributes.label : '';
        var fieldOptions = (fieldAttributes.options) ? fieldAttributes.options : '';
        var fieldDisabled = (fieldAttributes.disabled) ? fieldAttributes.disabled : '';
        var fieldModel = (fieldAttributes.model) ? fieldAttributes.model : '';
        var fieldHelp = (fieldAttributes.help) ? fieldAttributes.help : '';
        var fieldAttributes = (fieldAttributes.attributes) ? fieldAttributes.attributes : {};
      }

      if(attrs.name){
        fieldName = attrs.name;
      }
      if(attrs.type){
        fieldType = attrs.type;
      }
      if(attrs.class){
        fieldClass = attrs.class;
      }
      if(attrs.label){
        fieldLabel = attrs.label;
      }
      if(attrs.options){
        fieldOptions = attrs.options;
      }
      if(attrs.disabled){
        fieldDisabled = attrs.disabled;
      }
      if(attrs.model){
        fieldModel = attrs.model;
      }
      if(attrs.help){
        fieldHelp = attrs.help;
      }

      if(fieldModel == '' && fieldName != ''){
        fieldModel = 'data.'+fieldName;
      }

      // File Input
      scope.deleteFile = function(path, ngModel){

        var file = path+'/'+scope.$eval(ngModel);
        $http.post(window.location.origin+'/remove-upload', {file: file}).then(function(res){
          var model = $parse(ngModel);
          model.assign(scope, '');
        })
      }

      var uploaderName = '';
      if(fieldType == 'image' || fieldType == 'file'){
        uploaderName = 'uploader'+fieldName;
        var moveTo = (typeof fieldAttributes.moveTo != 'undefined') ? fieldAttributes.moveTo : '';

        (function(){
          var uploader = scope[uploaderName] = new FileUploader(
          {
              url: window.location.origin+'/upload-file',
          });

          uploader.onAfterAddingFile = function(fileItem)
          {
              if(moveTo != ''){
                fileItem.formData.push({moveTo: moveTo});
              }
              fileItem.upload();
          };

          uploader.onSuccessItem = function(fileItem, response, status, headers)
          {
            var model = $parse(fieldModel)(scope);
            model.assign(scope, response.fileName);
            element.find('.fileinput-preview img').attr('src', moveTo+'/'+response.fileName)
          };

        })();
      }
      //---

      var template = '';
      var container = {
        open : '<div class="form-group form-md-line-input form-md-floating-label">',
        close : '</div>'
      };

      var formClass = "";

      if(fieldClass){
        formClass = fieldClass+' ';
      }
      if(fieldType == 'multiple'){
        fieldAttributes.multiple = 'multiple';
      }

      var label = '';
      if(fieldLabel){
        if(typeof fieldAttributes.required != 'undefined'){
          fieldLabel = fieldLabel+required;
        }
        label = '<label>'+fieldLabel+'</label>';
      }

      var help = '';
      if(fieldHelp){
        help = '<span class="help-block">'+fieldHelp+'</span>';
      }

      if(typeof attrs.container != 'undefined' && attrs.container == 'false'){
        container.open = '';
        container.close = '';
      }

      var disabled = (typeof fieldDisabled != 'undefined') ? fieldDisabled : 'ng-disabled="response.action==\'view\'"';

      var ngModel;
      if(typeof fieldModel != 'undefined'){
        ngModel = $parse(fieldModel)(scope);
      }

      var fieldAttr = '';

      switch (fieldType){
        case 'text' :
        case 'password' :
        case 'email' :
        case 'password-strength':
        case 'hidden':

              fieldAttr = buildAttributes(fieldAttributes);

              if(fieldType == 'password-strength'){
                container.open = '<div class="form-group form-md-line-input form-md-floating-label last password-strength">';
              }else if(fieldType == 'hidden'){
                label = '';
                help = '';
              }
              if(fieldType == 'hidden'){
                container.open = '';
                container.close = '';
              }

              template = container.open+'<input type="'+fieldType+'" name="'+fieldName+'" ng-model="'+fieldModel+'"'+fieldAttr+' class="input-sm form-control '+formClass+'" '+disabled+'/>'+label+help+container.close;
              break;
        case 'date' :
              fieldAttr = buildAttributes(fieldAttributes);

              template = container.open+'<input type="text" name="'+fieldName+'"'+fieldAttr+' class="input-sm form-control date-picker '+formClass+'" '+disabled+' ng-value="'+fieldModel+' | date: \'dd/MM/yyyy\'" />'+label+help+container.close;
              break;
        case 'datetime' :
              fieldAttr = buildAttributes(fieldAttributes);

              template = container.open+'<input type="text" name="'+fieldName+'"'+fieldAttr+' class="input-sm form-control datetime-picker '+formClass+'" '+disabled+' ng-value="'+fieldModel+' | date: \'dd/MM/yyyy HH:mm\'" />'+label+help+container.close;
              break;
        case 'time' :
              fieldAttr = buildAttributes(fieldAttributes);

              template = container.open+'<input type="text" name="'+fieldName+'"'+fieldAttr+' class="input-sm form-control time-picker '+formClass+'" '+disabled+' ng-value="'+fieldModel+'" />'+label+help+container.close;
              break;
        case 'number' :
              fieldAttr = buildAttributes(fieldAttributes);

              template = container.open+'<input type="text" name="'+fieldName+'" ng-model="'+fieldModel+'"'+fieldAttr+' class="input-sm form-control '+formClass+'" '+disabled+' number-format decimals="2"/>'+label+help+container.close;
              break;
        case 'switch' :
              fieldAttr = buildAttributes(fieldAttributes);
              var checked = '';
              if(typeof ngModel == 'undefined'){
                checked = ' ng-init="'+fieldModel+'=true"';
              }
              container.open = '<div class="form-group">';
              template = container.open+'<div class="togglebutton">'+label+' &nbsp;&nbsp<label><input type="checkbox" name="'+fieldName+'" ng-model="'+fieldModel+'"'+fieldAttr+' '+disabled+checked+'><span class="toggle"></span></label></div>'+container.close;
              break
        case 'select' :
        case 'multiple' :
        case 'select-ajax' :
              var defaultOption;
              if(typeof fieldAttributes['default-option'] != 'undefined'){
                defaultOption = '<option value="">'+fieldAttributes['default-option']+'</option>';
              }else{
                defaultOption = '<option value="" style="display: none;"></option>';
              }
              fieldAttr = buildAttributes(fieldAttributes);
              var options = '';
              if(fieldOptions){
                options = fieldOptions;
              }

              var classes = (fieldType == 'select-ajax') ? 'selectpicker-ajax' : 'selectpicker';

              container.open = '<div class="form-group form-md-line-input float">';
              template = container.open+'<select name="'+fieldName+'" ng-model="'+fieldModel+'"'+fieldAttr+' class="form-control '+formClass+' '+classes+' bs-select" '+disabled+' ng-options="'+options+'">'+defaultOption+'</select>'+label+help+container.close;

              break;
        case 'textarea' :

              fieldAttr = buildAttributes(fieldAttributes);

              template = container.open+'<textarea name="'+fieldName+'" ng-model="'+fieldModel+'"'+fieldAttr+' class="input-sm form-control '+formClass+'" '+disabled+'></textarea>'+label+help+container.close;
              break;
        case 'radio' :
              container.open = '<div class="form-group form-md-line-input form-md-floating-label" style="0 0 20px !important;">';

              fieldAttr = buildAttributes(fieldAttributes);
              template = container.open+'<div class="mt-radio-list"><label class="mt-radio" ng-repeat="(key, value) in '+fieldOptions+'"><input type="radio" name="'+fieldName+'" ng-model="'+fieldModel+'" ng-value="key"> {{ value }}<span></span></label></div>'+container.close;
              break;
        case 'image' :
              fieldAttr = buildAttributes(fieldAttributes);

              var thumbnail = '<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div><div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px;"><img src="'+moveTo+'/{{ '+fieldModel+' }}" alt=""></div>';

              template = container.open+'<div class="fileinput" data-provides="fileinput" ng-class="('+fieldModel+') ? \'fileinput-exists\' : \'fileinput-new\'">'+thumbnail+'<div ng-hide="response.action==\'view\'" class="text-center"><span class="btn default btn-file btn-sm"><span class="fileinput-new"> Select image </span><span class="fileinput-exists"> Change </span><input type="file" name="'+fieldName+'" ng-model="'+fieldModel+'"'+fieldAttr+' nv-file-select="" uploader="'+uploaderName+'" /> </span><a href="javascript:;" class="btn red fileinput-exists btn-sm" data-dismiss="fileinput" ng-click="deleteFile(\''+moveTo+'\', \''+fieldModel+'\') "> Remove </a></div></div>'+container.close;
              break;
        case 'file' :
              fieldAttr = buildAttributes(fieldAttributes);

              template = '<div class="fileinput fileinput-new margin-top-15" data-provides="fileinput" ng-class="('+fieldModel+') ? \'fileinput-exists\' : \'fileinput-new\'"><div ng-hide="response.action==\'view\'">'+label+': <span class="btn default btn-file btn-sm"><span class="fileinput-new">Select file</span><span class="fileinput-exists"> Change </span><input type="file" name="'+fieldName+'" ng-model="'+fieldModel+'"'+fieldAttr+' nv-file-select="" uploader="'+uploaderName+'" /></span>&nbsp;&nbsp;<span class="fileinput-filename"></span> &nbsp;<a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput" ng-click="deleteFile(\''+moveTo+'\', \''+fieldModel+'\')"></a></div></div>';

              break;
      }

      var el = $compile(template)(scope);
      element.replaceWith(el);
      element = $(el[0]);


      if(fieldType == 'date'){
          var dt = moment(ngModel).format('DD/MM/YYYY');
          element.find('.date-picker').val(dt);
      }

      if(fieldType == 'datetime'){
          var dt = moment(ngModel).format('DD/MM/YYYY hh:mm');
          element.find('.datetime-picker').val(dt);
      }

      if(fieldType == 'select-ajax'){
          ngModel = (typeof ngModel == 'undefined') ? '' : ngModel;
          var source = fieldAttributes['data-source'];
          $.get(source+'?q='+ngModel).then(function(response){
            if(response.length > 0){
              var opt = '';
              if(ngModel == ''){
                opt += '<option value="" selected="selected">-- Pilihan --</option>';
              }
              for(var r in response){
                var selected = (ngModel != '' && r == 0) ? ' selected="selected"' : '';
                opt += '<option value="'+response[r].id+'"'+selected+'>'+response[r].text+'</option>';
              }
              element.find('.selectpicker-ajax').html(opt).selectpicker('refresh');
            }
          });
      }

      element.find('.date-picker').on('dp.change', function(e){
        ngModel = moment(e.date.toDate()).format('YYYY-MM-DD')+' 00:00:00';
      });

      element.find('.datetime-picker').on('dp.change', function(e){
        ngModel = moment(e.date.toDate()).format('YYYY-MM-DD hh:mm')+':00';
      });

      element.find('.time-picker').on('changeTime.timepicker', function(e){
        var newhour =(e.time.hours);
        var newTime = newhour + ':'+ e.time.minutes;
        ngModel = newTime;
      });

    }
  }
});
