/***
GLobal Directives
***/

// Route State Load Spinner(used on page or content load)
MetronicApp.directive('ngSpinnerBar', ['$rootScope', '$state',
    function($rootScope, $state) {
        return {
            link: function(scope, element, attrs) {
                // by defult hide the spinner bar
                element.addClass('hide'); // hide spinner bar by default

                // display the spinner bar whenever the route changes(the content part started loading)
                $rootScope.$on('$stateChangeStart', function() {
                    element.removeClass('hide'); // show spinner bar
                    Layout.closeMainMenu();
                });

                // hide the spinner bar on rounte change success(after the content loaded)
                $rootScope.$on('$stateChangeSuccess', function(event) {
                    element.addClass('hide'); // hide spinner bar
                    $('body').removeClass('page-on-load'); // remove page loading indicator
                    Layout.setAngularJsMainMenuActiveLink('match', null, event.currentScope.$state); // activate selected link in the sidebar menu

                    // auto scorll to page top
                    setTimeout(function () {
                        App.scrollTop(); // scroll to the top on content load
                    }, $rootScope.settings.layout.pageAutoScrollOnLoad);
                });

                // handle errors
                $rootScope.$on('$stateNotFound', function() {
                    element.addClass('hide'); // hide spinner bar
                });

                // handle errors
                $rootScope.$on('$stateChangeError', function() {
                    element.addClass('hide'); // hide spinner bar
                });
            }
        };
    }
])

// Handle global LINK click
MetronicApp.directive('a',
    function() {
        return {
            restrict: 'E',
            link: function(scope, elem, attrs) {
                if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {
                    elem.on('click', function(e) {
                        e.preventDefault(); // prevent link click for above criteria
                    });
                }
            }
        };
    });

// Handle Dropdown Hover Plugin Integration
MetronicApp.directive('dropdownMenuHover', function () {
  return {
    link: function (scope, elem) {
      elem.dropdownHover();
    }
  };
});

// Handle Input format
MetronicApp.directive('numberFormat', ['$filter', '$parse', function ($filter, $parse) {
  return {
    require: 'ngModel',
    link: function (scope, element, attrs, ngModelController) {

      var decimals = $parse(attrs.decimals)(scope);

      ngModelController.$parsers.push(function (data) {
        // Attempt to convert user input into a numeric type to store
        // as the model value (otherwise it will be stored as a string)
        // NOTE: Return undefined to indicate that a parse error has occurred
        //       (i.e. bad user input)
        var parsed = parseFloat(data);
        return !isNaN(parsed) ? parsed : undefined;
      });

      ngModelController.$formatters.push(function (data) {
        //convert data from model format to view format
        return $filter('number')(data, decimals); //converted
      });

      element.bind('focus', function () {
        element.val(ngModelController.$modelValue);
      });

      element.bind('blur', function () {
        // Apply formatting on the stored model value for display
        var formatted = $filter('number')(ngModelController.$modelValue, decimals);
        element.val(formatted);
      });
    }
  }
}]);

// Handle Template compiler
MetronicApp.directive('compileTemplate', function($compile, $parse){
    return {
      restrict: 'A',
      replace: true,
      link: function(scope, element, attr){
          var parsed = $parse(attr.ngBindHtml);
          function getStringValue() { return (parsed(scope) || '').toString(); }

          //Recompile if the template changes
          scope.$watch(getStringValue, function() {
              $compile(element, null, -9999)(scope);  //The -9999 makes it skip directives so that we do not recompile ourselves
          });
      }
    }
});

MetronicApp.filter('isEmpty', function () {
    var bar;
    return function (obj) {
        for (bar in obj) {
            if (obj.hasOwnProperty(bar)) {
                return false;
            }
        }
        return true;
    };
});



MetronicApp.directive('deleteConfirm', function ($http) {
  var optSwal = {
    title: "Apakah anda yakin?",
    text: "Anda tidak dapat memulihkan data ini kembali!",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-danger btn-sm",
    confirmButtonText: "Ya, Hapus!",
    cancelButtonText: "Batal",
    closeOnConfirm: false,
    showLoaderOnConfirm: true,
  }
  var optSwalSuccess = {
    title: "Dihapus!",
    text: "Data berhasil dihapus.",
    timer: 1000,
    showConfirmButton: false,
    type: 'success'
  }

  return {
    restrict: 'EA',
    link: function (scope, elem, attrs) {

      elem.on('click', function(){
        swal(optSwal, function(){
          var id = [];
          if(attrs.deleteConfirm == 'mass'){
            angular.forEach(scope.selected, function(selected){
              if(selected.isChecked){
                id.push(selected.value);
              }
            });
          } else {
            id.push(attrs.deleteConfirm);
          }

          $http.post(attrs.link, {id: id}).then(function(resp){
            swal(optSwalSuccess);
            scope.onDelete();
          });
        });
      });
    }
  };
});

// Handle Str Limit compiler
MetronicApp.filter('strLimit', function () {
    return function (value, wordwise, max, tail) {
        if (!value) return '';

        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
        if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
              //Also remove . and , so its gives a cleaner result.
              if (value.charAt(lastspace-1) == '.' || value.charAt(lastspace-1) == ',') {
                lastspace = lastspace - 1;
              }
              value = value.substr(0, lastspace);
            }
        }

        return value + (tail || ' …');
    };
});
