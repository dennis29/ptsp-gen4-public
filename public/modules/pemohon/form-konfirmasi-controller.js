/* Setup form controller */
angular.module('MetronicApp').controller('FormKonfirmasiController', ['$rootScope', '$scope', 'settings', '$http', '$filter', '$uibModal', function($rootScope, $scope, settings, $http, $filter, $uibModal) {
    $scope.data = {};
    $scope.fields = {};

    $scope.init = function(){

      if(!$filter('isEmpty')($scope.formData.data)){
          $scope.data = $scope.formData.data;
      }
      if($scope.formData.fields){
        $scope.fields = $scope.formData.fields;
      }

    }
    $scope.init();

    $scope.submit = function(){

      $http.get($scope.baseUrl+'/pemohon/submit-izin/'+$scope.formData.pengajuan_izin).then(function(){
        window.location = $scope.baseUrl+'/pemohon/permohonan';
      });

    }


    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
}]);
