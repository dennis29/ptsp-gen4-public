/* Setup form controller */
angular.module('MetronicApp').controller('FormRoleController', ['$rootScope', '$scope', 'settings', '$http', '$filter', function($rootScope, $scope, settings, $http, $filter) {

    $scope.data = {};
    $scope.fields = {};
    $scope.dataModule = {};
    $scope.dataAction = {};
    $scope.dataAccess = [];

    $scope.init = function(){

      if(!$filter('isEmpty')($scope.formData.data)){
          $scope.data = $scope.formData.data;
          $scope.dataAccess = $scope.data.access || [];
      }
      if($scope.formData.fields){
        $scope.fields = $scope.formData.fields;
      }

      $scope.dataModule = $scope.formData.dataModule;
      $scope.dataAction = $scope.formData.dataAction;
    }
    $scope.init();

    $scope.getDataAccess = function(){
      var dataAccess = [];
      angular.forEach($scope.dataAccess, function(value){
        if(value.active){
          dataAccess.push(value);
        }
      });
      return dataAccess;
    }

    $scope.save = function($event, success){
      var form = $($event.currentTarget).parents('form');

      if(form.valid()){
        var btn = Ladda.create($event.currentTarget);
        btn.start();
        $($event.currentTarget).parents('.actions').find('button').attr('disabled', true);
        var data = $scope.data;
        data.access = $scope.getDataAccess();

        $http.post($scope.formData.url+'/save/'+($scope.data.id || 'null'), data).then(function(resp){

          if(resp.data.saved){
            if(resp.data.action == 'create'){
                $scope.data = {};
                $scope.form.$setUntouched();
              }
              toastr['success']("Data berhasil disimpan!", "Saved");
              if(typeof success != 'undefined'){
                success();
              }
            }else{

              if(typeof resp.data.messages != 'undefined' && resp.data.messages != ''){
                toastr['error'](resp.data.messages, "Failed");
              }else{
                toastr['error']("Data gagal disimpan!", "Failed");
              }
            }
          btn.stop();
          $($event.currentTarget).parents('.actions').find('button').removeAttr('disabled');
        });
      }
    }

    $scope.saveAndReturn = function($event, link){
      $scope.save($event, function(){
        setTimeout(function(){
            window.location = link;
        }, 500);
      });
    }

    $scope.onDelete = function(){
      $scope.data = {};
      setTimeout(function(){
          window.location = $scope.formData.url;
      }, 700);
    }

    /*
    dataAccess = [
      {
        module: '(Module ID)',
        access: ['create', 'update', 'delete', '...']
      },
      { ... }
    ]
    */

    $scope.getModule = function(module){

      var filtered = $filter('filter')($scope.dataAccess, {module: module}, true);

      if(typeof filtered == 'undefined' || filtered.length <= 0){
        filtered = {
          module: module,
          active: false,
          access: []
        };

        $scope.dataAccess.push(filtered);
      }else{
        filtered = filtered[0];
      }

      var idx = $scope.dataAccess.indexOf(filtered);

      return $scope.dataAccess[idx];

    }

    $scope.isChecked = function(module, role){

       var module = $scope.getModule(module);
       if(module.access.indexOf(role) != -1){
         return true;
       }

       return false;

    }

    $scope.changeAccess = function(module, role){
      var module = $scope.getModule(module);

      if(module.access.indexOf(role) == -1){
        module.access.push(role);
      }else{
        module.access.splice(module.access.indexOf(role), 1);
      }
    }


    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
}]);
