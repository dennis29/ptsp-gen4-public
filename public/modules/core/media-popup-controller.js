/* Setup form controller */


angular.module('MetronicApp').controller('MediaPopupController', function($scope, $uibModalInstance, item, $http, $filter, Upload) {

  $scope.deleting = false;

  $scope.changingJenisBerkas = false;

  $scope.medias = [];

  $scope.formData = {};

  $scope.showUpload = false;

  $scope.indexActive = null;
  $scope.mediaActive = null;

  $scope.dragOverClass = {accept:'dragover', reject:'dragover-err', pattern:'image/*,text/*,application/pdf'};

  $scope.openUpload = function(){
    $scope.showUpload = true;
  }

  $scope.hideUpload = function(){
    $scope.showUpload = false;
  }

  // for multiple files:
  $scope.uploadFiles = function (files) {
    $scope.showUpload = false;
    if (files && files.length) {
      for (var i = 0; i < files.length; i++) {

        var idx = $scope.medias.length;
        // continue;
        $scope.medias.push({
          id: null,
          file_name: files[i].name,
          order: 99999
        });

        Upload.upload({
          url: $scope.baseUrl+'/pemohon/media/upload',
          data: {file: files[i], index: idx}
        }).then(function (resp) {
            $scope.medias[resp.data.index] = resp.data.file;
        }, function (resp) {
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
        });
      }
    }
  }

  $scope.delete = function(id){

    $scope.deleting = true;
    $http.get($scope.baseUrl+'/pemohon/media/delete/'+id).then(function(resp){
      var filter = $filter('filter')($scope.medias, {id: id})[0];
      var idx = $scope.medias.indexOf(filter);
      $scope.medias.splice(idx, 1);
      $scope.mediaActive = null;
      $scope.indexActive = null;
      $scope.deleting = false;
    });
  }

  $scope.changeJenisBerkas = function(id){
    $scope.changingJenisBerkas = true;
    $http.get($scope.baseUrl+'/pemohon/media/change-berkas/'+id+'/'+$scope.mediaActive.jenis_berkas).then(function(){
      var filter = $filter('filter')($scope.medias, {id: id})[0];
      var idx = $scope.medias.indexOf(filter);
      $scope.medias[idx].jenis_berkas = $scope.mediaActive.jenis_berkas;
      $scope.changingJenisBerkas = false;
    });
  }

  $scope.showAttributes = function(media, index){
    $scope.mediaActive = media;
    $scope.indexActive = index;
  }

  $scope.getMedia = function(){
    $http.get($scope.baseUrl+'/pemohon/media/data').then(function(resp){
      $scope.medias = resp.data.media;
      $scope.formData = resp.data.data;

    });
  }
  $scope.getMedia();

  $scope.bytesToSize = function(bytes) {
     var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
     if (bytes == 0) return '0 Byte';
     var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
     return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
  };

  $scope.addFile = function(){
    $uibModalInstance.close($scope.mediaActive.id);
  }

});
