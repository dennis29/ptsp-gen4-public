/* Setup form controller */
angular.module('MetronicApp').controller('FormContainerPemohonController', ['$rootScope', '$scope', 'settings', '$http', '$filter', function($rootScope, $scope, settings, $http, $filter) {

    $scope.data = {};
    $scope.fields = {};

    $scope.valueNumber = 8;

    $scope.init = function(){

      if(!$filter('isEmpty')($scope.formData.data)){
          $scope.data = $scope.formData.data;
      }
      if($scope.formData.fields){
        $scope.fields = $scope.formData.fields;
      }

    }

    $scope.buttonStatus = [{id: "reject", label: "Reject", color:'green'}, {id: "selesai", label: "Simpan", color:'red'}];

    $scope.init();

    $scope.changeStatus = function(status, link){
        $http.get($scope.formData.url+'/change-status/'+$scope.formData.pengajuan_id+"/"+status).then(function(resp){
                if (resp.data.status){
                $scope.buttonStatus = [];
                toastr['success']("Data berhasil disimpan!", "Saved");
                window.location = link;
            }
            else{
                toastr['error']("Data gagal disimpan!", "Failed");
            }
        });
    }

    $scope.saveAndReturn = function($event, state, link){
      $scope.save($event, state, function(){
        setTimeout(function(){
            window.location = link;
        }, 500);
      });
    }

    $scope.onDelete = function(){
      $scope.data = {};
      setTimeout(function(){
          window.location = $scope.formData.url;
      }, 700);
    }

    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
}]);
