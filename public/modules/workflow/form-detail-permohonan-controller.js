/* Setup form controller */
angular.module('MetronicApp').controller('FormDetailPermohonanController', ['$rootScope', '$scope', 'settings', '$http', '$filter', function($rootScope, $scope, settings, $http, $filter) {

    $scope.data = {};
    $scope.fields = {};

    $scope.valueNumber = 8;

    $scope.init = function(){

      if(!$filter('isEmpty')($scope.formData.data)){
          $scope.data = $scope.formData.data;
      }
      if($scope.formData.fields){
        $scope.fields = $scope.formData.fields;
      }

    }

    $scope.init();

    
    $scope.save = function($event, state, success){
      var form = $($event.currentTarget).parents('form');

      if(form.valid()){
        var btn = Ladda.create($event.currentTarget);
        btn.start();
        $($event.currentTarget).parents('.actions').find('button').attr('disabled', true);
        var data = $scope.data;

        $http.post($scope.formData.url+'/save/'+state+($scope.data.id || 'null'), data).then(function(resp){

          if(resp.data.saved){
            if(resp.data.action == 'create'){
                $scope.data = {};
                $scope.form.$setUntouched();
              }
              toastr['success']("Data berhasil disimpan!", "Saved");
              if(typeof success != 'undefined'){
                success();
              }
            }else{

              if(typeof resp.data.messages != 'undefined' && resp.data.messages != ''){
                toastr['error'](resp.data.messages, "Failed");
              }else{
                toastr['error']("Data gagal disimpan!", "Failed");
              }
            }
          btn.stop();
          $($event.currentTarget).parents('.actions').find('button').removeAttr('disabled');
        });
      }
    }

    $scope.changeStatus = function(status){
        $http.get($scope.formData.url+'/change-status/'+$scope.formData.pengajuan_id+"/"+status).then(function(resp){
            if (resp.data.status){
                $scope.buttonStatus = [];
                toastr['success']("Data berhasil disimpan!", "Saved");
                window.location = $scope.formData.url;
            }
            else{
                toastr['error']("Data gagal disimpan!", "Failed");
            }
        });
    }

    $scope.saveAndReturn = function($event, state, link){
      $scope.save($event, state, function(){
        setTimeout(function(){
            window.location = link;
        }, 500);
      });
    }

    $scope.onDelete = function(){
      $scope.data = {};
      setTimeout(function(){
          window.location = $scope.formData.url;
      }, 700);
    }

    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
}]);
