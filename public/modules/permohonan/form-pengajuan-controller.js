/* Setup form controller */
angular.module('MetronicApp').controller('FormPengajuanController', ['$rootScope', '$scope', 'settings', '$http', '$filter', function($rootScope, $scope, settings, $http, $filter) {
    $scope.data = {};
    $scope.fields = {};

    $scope.valueNumber = 8;

    $scope.init = function(){

      if(!$filter('isEmpty')($scope.formData.data)){
          $scope.data = $scope.formData.data;
      }
      if($scope.formData.fields){
        $scope.fields = $scope.formData.fields;
      }

    }
    $scope.init();

    $scope.save = function($event, success){
      var form = $($event.currentTarget).parents('form');

      if(form.valid()){

        var data = $scope.data;
        var lat = $('#lat').val();
        var long = $('#long').val();
        var formatted_address = $('#formatted_address').val();

        data.lat = lat;
        data.long = long;
        data.formatted_address = formatted_address;

        if(data.lat =='' || data.long == ''){
          toastr['error']("Pilih lokasi pada map!", "Failed");
          return;
        }

        var btn = Ladda.create($event.currentTarget);
        btn.start();
        $($event.currentTarget).parents('.actions').find('button').attr('disabled', true);

        $http.post($scope.formData.url+'/save-permit/'+($scope.data.id || 'null'), data).then(function(resp){

          if(resp.data.saved){
            if(resp.data.action == 'create'){
                $scope.data = {};
                $scope.form.$setUntouched();
              }
              toastr['success']("Data berhasil disimpan!", "Saved");
              if(typeof success != 'undefined'){
                success();
              }
              window.location = resp.data.url;
            }else{

              if(typeof resp.data.messages != 'undefined' && resp.data.messages != ''){
                toastr['error'](resp.data.messages, "Failed");
              }else{
                toastr['error']("Data gagal disimpan!", "Failed");
              }
            }
          btn.stop();
          $($event.currentTarget).parents('.actions').find('button').removeAttr('disabled');
        });
      }
    }

    $scope.saveAndReturn = function($event, link){
      $scope.save($event, function(){
        setTimeout(function(){
            window.location = link;
        }, 10);
      });
    }

    $scope.onDelete = function(){
      $scope.data = {};
      setTimeout(function(){
          window.location = $scope.formData.url;
      }, 700);
    }

    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
}]);
