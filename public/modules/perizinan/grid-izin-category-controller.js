/* Setup form controller */
angular.module('MetronicApp').controller('GridIzinCategoryController', ['$rootScope', '$scope', 'settings', '$http', '$httpParamSerializer', '$filter', '$q', function($rootScope, $scope, settings, $http, $httpParamSerializer, $filter, $q) {
    $scope.promise = true;
    $scope.query = {
      order: 'name',
      perpage: 10,
      page: 1,
      keyword: ''
    };
    $scope.selected = [];

    $scope.response = null;

    $scope.getData = function(){
      var deferred = $q.defer();
      $http.get('/backend/izin-category/data/?'+$httpParamSerializer($scope.query)).then(function(response){
        $scope.response = response.data;
        deferred.resolve();
      });

      $scope.promise = deferred.promise;
    }

    $scope.onDelete = function(){
      $scope.getData();
    }

    $scope.onDblClick = function(id){
      window.location = $scope.response.url+'/update/'+id;
    }

    var keywordTimeout;
    $scope.keywordChanged = function(){
      clearTimeout(keywordTimeout);
      keywordTimeout = setTimeout(function(){
          $scope.query.keyword = $scope.keyword;
          $scope.getData();
      }, 500);
    }

    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
}]);
