angular.module('MetronicApp').controller('IzinPersyaratanController', ['$rootScope', '$scope', 'settings', '$http', '$filter', '$uibModal', function($rootScope, $scope, settings, $http, $filter, $uibModal) {

  $scope.initSyarat = function(){
    $scope.$parent.dataSyarat = $scope.formData.dataSyarat;
  }
  $scope.initSyarat();

  $scope.addSyarat = function(){
    $scope.openForm({
      id: null,
      name: null,
      jenis_berkas: null,
      description: null
    });
  }

  $scope.editSyarat = function(idx){
    $scope.openForm($scope.dataSyarat[idx], idx)
  }

  $scope.deleteSyarat = function(idx){
    $scope.$parent.dataSyarat.splice(idx, 1);
  }

  $scope.openForm = function (data, idx) {
    var modalInstance = $uibModal.open({
      templateUrl: 'formSyaratModal.html',
      controller: 'FormSyaratModalController',
      scope: $scope,
      size: 'lg',
      resolve: {
        item: {
          optionJenisBerkas: $scope.formData.optionJenisBerkas,
          data: data
        }
      }
    });

    modalInstance.result.then(function (data) {
      if(data.id == null){
        $scope.dataSyarat.push(data);
      }else{
        $scope.dataSyarat[idx] = data;
      }
    }, function () {

    });
  };

}]);

// angular.module('MetronicApp', ['ckeditor']);
angular.module('MetronicApp').controller('FormSyaratModalController', function ($scope, $uibModalInstance, item) {

  $scope.item = item;
  $scope.data = item.data;
  $scope.options = {};

  $scope.fields = {
    name: {
      name: 'name',
      type: 'textarea',
      class: 'editor',
      attributes: {
        required: true,
        ckeditor: 'options'
      }
    },
    jenis_berkas: {
      name: 'jenis_berkas',
      label: 'Jenis Berkas',
      type: 'select',
      options: 'item.id as item.name for item in item.optionJenisBerkas',
      attributes: {
        'default-option' : 'Pilih Jenis Berkas'
      }
    },
    description: {
      name: 'description',
      label: 'Deskripsi',
      type: 'textarea'
    }
  };
  $scope.refreshSelect();

  $scope.save = function ($event) {
    var form = $($event.currentTarget).parents('form');

    if(form.valid()){
      $uibModalInstance.close($scope.data);
    }

  };

  $scope.close = function () {
    $uibModalInstance.dismiss('cancel');
  };
});
