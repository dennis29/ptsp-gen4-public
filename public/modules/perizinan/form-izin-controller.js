/* Setup form controller */
MetronicApp.requires.push('ckeditor');
MetronicApp.controller('FormIzinController', ['$rootScope', '$scope', 'settings', '$http', '$filter', function($rootScope, $scope, settings, $http, $filter) {

    $scope.data = {};
    $scope.fields = {};
    $scope.dataProcess = [];
    $scope.dataOutput = [];
    $scope.dataSyarat = [];
    $scope.optionWaktu = [
      {id: 'jam', name: 'Jam'},
      {id: 'hari', name: 'Hari'},
      {id: 'bulan', name: 'Bulan'},
      {id: 'tahun', name: 'Tahun'},
    ];

    $scope.init = function(){
      if(!$filter('isEmpty')($scope.formData.data)){
          $scope.data = $scope.formData.data;
      }
      if($scope.formData.fields){
        $scope.fields = $scope.formData.fields;
      }
    }
    $scope.init();

    $scope.save = function($event, success){
      var form = $($event.currentTarget).parents('form');

      if(form.valid()){
        var btn = Ladda.create($event.currentTarget);
        btn.start();
        $($event.currentTarget).parents('.actions').find('button').attr('disabled', true);
        var data = $scope.data;
        data.process = $scope.dataProcess;
        data.output = $scope.dataOutput;
        data.syarat = $scope.dataSyarat;

        $http.post($scope.formData.url+'/save/'+($scope.data.id || 'null'), data).then(function(resp){

          if(resp.data.saved){
            if(resp.data.action == 'create'){
                $scope.data = {};
                $scope.form.$setUntouched();
              }
              toastr['success']("Data berhasil disimpan!", "Saved");
              if(typeof success != 'undefined'){
                success();
              }
            }else{

              if(typeof resp.data.messages != 'undefined' && resp.data.messages != ''){
                toastr['error'](resp.data.messages, "Failed");
              }else{
                toastr['error']("Data gagal disimpan!", "Failed");
              }
            }
          btn.stop();
          $($event.currentTarget).parents('.actions').find('button').removeAttr('disabled');
        });
      }
    }

    $scope.saveAndReturn = function($event, link){
      $scope.save($event, function(){
        setTimeout(function(){
            window.location = link;
        }, 10);
      });
    }

    $scope.onDelete = function(){
      $scope.data = {};
      setTimeout(function(){
          window.location = $scope.formData.url;
      }, 10);
    }

    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
}]);
