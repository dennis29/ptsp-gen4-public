/* Setup form controller */
angular.module('MetronicApp').controller('IzinProsesController', ['$rootScope', '$scope', 'settings', '$http', '$filter', function($rootScope, $scope, settings, $http, $filter) {

    $scope.fieldsProcess = {};
    $scope.optionMapingProses = [];

    $scope.refreshOptionMapingProses = function(){
      $scope.optionMapingProses = [];
      var i = 0;
      angular.forEach($scope.$parent.dataProcess, function(value){
        $scope.optionMapingProses.push({
          id: i,
          name: value.name
        });
        i++;
      });
      // console.log($scope.optionMapingProses);

      $scope.refreshSelect();
    }

    $scope.initProcess = function(){

      $scope.$parent.dataProcess = $scope.formData.dataProses;

      // Define Field Process
      $scope.fieldsProcess = {
        name: {
          type: 'text',
          label: 'Nama Proses',
          name: 'name',
          attributes: {
            required: "",
            'ng-keyup': 'refreshOptionMapingProses()'
          },
        },
        proses: {
          type: 'select',
          label: 'Proses',
          name: 'proses',
          options: 'item.id as item.name for item in formData.optionProses',
          attributes: {
            'data-live-search' : true,
            required: ""
          },
        },
        roles: {
          type: 'multiple',
          label: 'Role',
          name: 'roles',
          options: 'item.slug as item.name for item in formData.optionRole',
          attributes: {
            required: ""
          },
        },
        estimated_time: {
          type: 'number',
          name: 'estimated_time',
          label: 'Perkiraan Waktu (Hari)',
          attributes: {
            decimals: 0
          }
        },
        status: {
          type: 'select',
          label: 'Status',
          name: 'status',
          options: 'item.slug as item.label for item in formData.optionStatus',
          attributes: {
            'data-live-search' : true
          },
        },
        next_proses: {
          type: 'select',
          name: 'next_proses',
          label: 'Proses Selanjutnya',
          options: 'item.id as item.name for item in optionMapingProses',
          attributes: {
            'data-live-search' : true
          }
        },
        jenis_berkas: {
          type: 'select',
          name: 'jenis_berkas',
          label: 'Jenis Berkas',
          options: 'item.id as item.name for item in formData.optionJenisBerkas',
          attributes: {
            'data-live-search' : true
          }
        },
        file: {
          type: 'file',
          name: 'file',
          label: 'File Berkas',
          attributes: {
            'moveTo' : 'izin/berkas/'
          }
        }
      };

      $scope.refreshOptionMapingProses();
    }
    $scope.initProcess();

    $scope.addProcess = function(){

      var dataProcess = {
        id: null,
        name: null,
        proses: null,
        roles: [],
        estimated_time: null,
        status: [],
        berkas: []
      };

      $scope.$parent.dataProcess.push(dataProcess);
      $scope.refreshOptionMapingProses();
      $scope.refreshSelect();
    }

    $scope.addBerkas = function(idx){

      $scope.$parent.dataProcess[idx].berkas.push({
        id: null,
        jenis_berkas: '',
        file: ''
      });

      $scope.refreshSelect();
    }

    $scope.deleteBerkas = function(idx, index){
      $scope.$parent.dataProcess[idx].berkas.splice(index, 1);
    }

    $scope.addStatus = function(idx){

      $scope.$parent.dataProcess[idx].status.push({
        id: null,
        status: '',
        next_proses: ''
      });

      $scope.refreshSelect();
    }

    $scope.deleteStatus = function(idx, index){
      $scope.$parent.dataProcess[idx].status.splice(index, 1);
    }

    $scope.deleteProcess = function(idx){
      $scope.$parent.dataProcess.splice(idx, 1);
      $scope.refreshOptionMapingProses();
    }
}]);
