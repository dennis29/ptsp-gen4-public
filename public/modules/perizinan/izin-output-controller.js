/* Setup form controller */
angular.module('MetronicApp').controller('IzinOutputController', ['$rootScope', '$scope', 'settings', '$http', '$filter', function($rootScope, $scope, settings, $http, $filter) {

    $scope.fieldsOutput = {};

    $scope.initOutput = function(){

      $scope.$parent.dataOutput = $scope.formData.dataOutput;

      // Define Field Output
      $scope.fieldsOutput = {
        name: {
          type: 'text',
          label: 'Nama Output',
          name: 'name',
          attributes: {
            required: ""
          },
        },
        output: {
          type: 'select',
          label: 'Output',
          name: 'output',
          options: 'item.id as item.name for item in formData.optionOutput',
          attributes: {
            'data-live-search' : true,
            required: ""
          },
        },
        roles: {
          type: 'multiple',
          label: 'Role',
          name: 'roles',
          options: 'item.slug as item.name for item in formData.optionRole',
          attributes: {
            required: ""
          },
        },
        order: {
          type: 'number',
          name: 'order',
          label: 'Urutan'
        },
        email_penerima: {
          type: 'textarea',
          name: 'email_penerima',
          label: 'Email penerima output, dipisahkan dengan titik koma (;)'
        },
      };

    }
    $scope.initOutput();

    $scope.addOutput = function(){
      
      var dataOutput = {
        name: null,
        output: null,
        roles: [],
      };

      $scope.$parent.dataOutput.push(dataOutput);

      setTimeout(function(){
        $('.selectpicker').selectpicker();
      }, 100);
    }

    $scope.deleteOutput = function(idx){
      $scope.$parent.dataOutput.splice(idx, 1);
      console.log("deleted");
    }
}]);
