<?php
use Illuminate\Support\Facades\Input;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

App::setLocale('id');

// Web Pages
// Route::group(['namespace' => 'Web'], function(){
//
//     // Route::get('/', ['as' => 'home', 'uses' => 'Home@index']);
//
// });

// Upload
Route::any('/upload-file', function(){

  if (Input::hasFile('file')) {
    $moveTo = Input::get('moveTo');
    if($moveTo == ''){
      $moveTo = 'uploads';
    }

    $file = Input::file('file');
    $originalName = $file->getClientOriginalName();
    $extension = $file->getClientOriginalExtension();
    $destinationPath = base_path().'/public/'.$moveTo; // upload path
    $fileName = uniqid().substr(md5(mt_rand()), 0, 5).'.'.$extension; // renameing image

    $file->move($destinationPath, $fileName);
    $realPath = $file->getRealPath();

    return response()->json([
      'originalName' => $originalName,
      'fileName' => $fileName,
      'path' => $moveTo.'/'.$fileName
    ]);
  }

});
Route::any('remove-upload', function(){
  $file = Input::get('file');
  if(!empty($file) && file_exists($file)){
    unlink($file);
    return response()->json([
      'status' => true
    ]);
  }else{
    return response()->json([
      'status' => false,
      'message' => 'File Not Exists!'
    ]);
  }
});

Auth::routes();
